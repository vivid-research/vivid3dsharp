﻿using System.IO;

namespace VividProjects
{
    public class VividProject
    {
        public string Name
        {
            get;
            set;
        }

        public string Author
        {
            get;
            set;
        }

        public string Info
        {
            get;
            set;
        }

        public string Icon
        {
            get;
            set;
        }

        public string Path
        {
            get;
            set;
        }

        public VividProject()
        {
        }

        public override string ToString()
        {
            return Name + " By " + Author;
            //return base.ToString();
        }

        public void SaveProject(string path)
        {

          

        }
        public VividProject(string name, string author, string info, string icon, string path)
        {
            Name = name;
            Author = author;
            Icon = icon;
            Info = info;
            Path = path;
        }

        public void Write(BinaryWriter w)
        {
            w.Write(Name);
            w.Write(Author);
            w.Write(Icon);
            w.Write(Info);
            w.Write(Path);
        }

        public void Read(BinaryReader r)
        {
            Name = r.ReadString();
            Author = r.ReadString();
            Icon = r.ReadString();
            Info = r.ReadString();
            Path = r.ReadString();
        }

        public VividProject(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Open);
            BinaryReader r = new BinaryReader(fs);
            Read(r);
            r.Close();
            fs.Close();
        }

        public void Save(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write);
            BinaryWriter w = new BinaryWriter(fs);

            Write(w);

            w.Close();
            fs.Close();
        }
    }
}
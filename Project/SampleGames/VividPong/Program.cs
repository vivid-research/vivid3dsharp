﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivid.App;
using VividPong.States;

namespace VividPong
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("VividPong - Sample Game 1");

            VividApp.InitState = new InGameState();

            var app = new VividPongApp();

            app.Run();

            


        }
    }
}

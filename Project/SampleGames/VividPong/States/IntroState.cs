﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivid.App;
using Vivid.Texture;

namespace VividPong.States
{
    public class IntroState : Vivid.State.VividState
    {

        public Texture2D Ball1;
        public Texture2D[] Logos = new Texture2D[5];
        public int LogoC = 2;
        public int CLogo = 0;
        public float BallX = -128;
        public Texture2D BgImg = null;
        public Vivid.Audio.VSoundSource Whoosh1;

        public override void InitState()
        {
            base.InitState();

            Whoosh1 = Vivid.Audio.Songs.LoadSound("asset/pong/whoosh1.mp3");
            Ball1 = new Texture2D("asset/pong/pongball2.png", LoadMethod.Single, true);
            BgImg = new Texture2D("asset/pong/bg1.png", LoadMethod.Single, true);

            for(int i = 0; i < LogoC; i++)
            {

                Logos[i] = new Texture2D("asset/pong/logo" + (i + 1) + ".png", LoadMethod.Single, true);

            }
            //Vivid.Audio.Songs.PlaySource(Whoosh1, false);
        }

        public override void UpdateState()
        {
            base.UpdateState();
            Texture2D.UpdateLoading();

        }

        bool PlayedS = false;
        public override void DrawState()
        {
            base.DrawState();

            BallX += 25;

            if(BallX>AppInfo.W+550)
            {
                BallX = -128;
                CLogo++;

               // Vivid.Audio.Songs.PlaySource(Whoosh1, false);
                if (CLogo>=LogoC)
                {
                    VividApp.PushState(new MenuState());
                    return;
                }
                PlayedS = false;
            }

            if (BallX >= AppInfo.W / 2 - 400)
            {

                if (PlayedS == false)
                {
                    Vivid.Audio.Songs.PlaySource(Whoosh1, false);
                    PlayedS = true;
                }
            }

            Vivid.Draw.IntelliDraw.BeginDraw();

            Vivid.Draw.IntelliDraw.DrawImg(AppInfo.W / 2 - 300, AppInfo.H / 2 - 120, 600, 240, Logos[CLogo], new OpenTK.Vector4(1, 1, 1, 1));

            Vivid.Draw.IntelliDraw.DrawImg((int)BallX + 128, 10, 1024, 700, BgImg, new OpenTK.Vector4(1, 1, 1, 1));

            Vivid.Draw.IntelliDraw.DrawImg((int)BallX, AppInfo.H/2 -130, 256, 240, Ball1, new OpenTK.Vector4(1, 1, 1, 1), false);

         
            Vivid.Draw.IntelliDraw.EndDraw();

        }

    }
}

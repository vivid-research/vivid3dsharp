﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Vivid.App;
using Vivid.Import;
using Vivid.Resonance;
using Vivid.Resonance.Forms;
using Vivid.Scene;
using Vivid.Scene.Node;
using Vivid.State;
using Vivid.Texture;

namespace VividPong.States
{
    public class InGameState : VividState
    {

        public int[] Score = new int[2];
        public LabelForm[] ScoreLab = new LabelForm[2];
        public Entity3D ArenaBase;
        public Entity3D[] Bats = new Entity3D[2];
        public Cam3D Cam1;
        public Light3D[] Light = new Light3D[6];
        public SceneGraph3D Graph = null;

        public override void InitState()
        {
            base.InitState();

            Score[0] = 0;
            Score[1] = 0;
            Vivid.Audio.Songs.PlaySong("asset/pong/gamesong1.mp3");

            UIForm bas = new UIForm().Set(0, 0, AppInfo.W, AppInfo.H, "");

            SUI = new UI();

            SUI.Root = bas;

            ScoreLab[0] = new LabelForm().Set(AppInfo.W / 2 - 200, 10, 100, 30, "Player 1 : 0") as LabelForm;
            ScoreLab[1] = new LabelForm().Set(AppInfo.W  /2+ 50, 10, 100, 30, "Player 2 : 0") as LabelForm;

            for(int i = 0; i < 2; i++)
            {

                Bats[i] = Import.ImportNode("asset/pong/bats/bat1.dae") as Entity3D;
                Bats[i].SetMultiPass();

                Bats[i].SetMatDiff(new OpenTK.Vector3(2.5f, 2.5f, 2.5f));
                // Bats[i].Rot(new OpenTK.Vector3(-30, 0, 0), Space.Local);
                Bats[i].LocalScale = new OpenTK.Vector3(0.2f, 0.2f, 0.28f);
                Bats[i].Rot(new OpenTK.Vector3(90, 0, 0), Space.Local);
            }


            Bats[0].SetPos(new OpenTK.Vector3(-5, 0, 1));
            Bats[1].SetPos(new OpenTK.Vector3(5, 0, 0));


            bas.Add(ScoreLab[0], ScoreLab[1]);

            Graph = new SceneGraph3D();
            Cam1 = new Cam3D();

            Light[0] = new Light3D();

            Light[0].SetPos(new OpenTK.Vector3(0, 0,-15));
            Light[0].Range = 100;
            Light[0].Diff = new OpenTK.Vector3(0, 0.5f, 0.5f);

            Light[1] = new Light3D();

            Light[1].SetPos(new OpenTK.Vector3(25, 45, 0));
            Light[1].Range = 150;

            Light[1].Diff = new OpenTK.Vector3(0, 0.5f, 0.7f);


            Light[2] = new Light3D();

            Light[2].SetPos(new OpenTK.Vector3(-25, 45, 0));
            Light[2].Range = 150;
            Light[2].Diff = new OpenTK.Vector3(0.7f, 0.5f, 0);




            Graph.Add(Light[0]);
            //Graph.Add(Light[1]);

           // Graph.Add(Light[2]);
            Graph.Add(Cam1);

            ArenaBase = Import.ImportNode("asset/pong/arenas/Arena3.dae") as Entity3D;
            ArenaBase.LocalScale = new OpenTK.Vector3(1, 1.1f, 0.95f);


            ArenaBase.SetMultiPass();

            Graph.Add(ArenaBase);

            Cam1.SetPos(new OpenTK.Vector3(0, 0,-10));
            Cam1.LookAt(new OpenTK.Vector3(0, 0, 0), new OpenTK.Vector3(0, 1, 0));
            ArenaBase.Rot(new OpenTK.Vector3(0, 90,0), Space.Local);

            for(int i = 0; i < 2; i++)
            {
                Graph.Add(Bats[i]);
            }

        }

        public override void UpdateState()
        {
            base.UpdateState();
            Texture2D.UpdateLoading();
            SUI.Update();
            Graph.Update();
        }
        float ang = 0;
        public override void DrawState()
        {
            base.DrawState();

            ang += 1;

            float fa = Vivid.Util.Maths.DegToRad(ang);

            Bats[0].SetPos(new OpenTK.Vector3((float)Math.Cos(fa) * 1,  (float)Math.Sin(fa) * 1,-1.5f));
          
            Graph.RenderShadows();
            Graph.Render();

            SUI.Render();
        }

    }
}

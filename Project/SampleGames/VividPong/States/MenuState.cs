﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivid.App;
using Vivid.Resonance;
using Vivid.Resonance.Forms;
using Vivid.State;
using Vivid.Texture;

namespace VividPong.States
{
    public class MenuState : VividState
    {



        public override void InitState()
        {
            base.InitState();
            Vivid.Audio.Songs.PlaySong("asset/pong/menusong.mp3");
            SUI = new Vivid.Resonance.UI();

            SUI.Root = new UIForm().Set(0, 0, AppInfo.W, AppInfo.H, "");

            var bgTex = new Texture2D("asset/pong/menubg1.jpg", LoadMethod.Single, false);
            var logoTex = new Texture2D("asset/pong/logo2.png", LoadMethod.Single, false);

            var bg = new ImageForm().Set(0, 0, AppInfo.W, AppInfo.H, "").SetImage(bgTex);
            var logo = new ImageForm().Set(AppInfo.W / 2 - 172, 50, 172 * 2, 160, "").SetImage(logoTex);

            SUI.Root.Add(bg);
            bg.Add(logo);

            var newGame = new ButtonForm().Set(AppInfo.W / 2 - 120, 500, 240, 35, "New Game");
            var credits = new ButtonForm().Set(AppInfo.W / 2 - 120, 540, 240, 35, "Credits");
            var exit = new ButtonForm().Set(AppInfo.W / 2 - 120, 580, 240, 35, "Exit Game");

            bg.Add(newGame);
            bg.Add(credits);
            bg.Add(exit);

            newGame.Click = (b) =>
            {

                Vivid.Audio.Songs.StopSong();
                VividApp.PushState(new InGameState());

                return;

            };

        }
        public override void UpdateState()
        {
            base.UpdateState();
            Texture2D.UpdateLoading();
            SUI.Update();

        }

        public override void DrawState()
        {
            base.DrawState();

            SUI.Render();
        }

    }
}

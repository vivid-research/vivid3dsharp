﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Vivid3DManager.Control
{
    public partial class NewProject : Form
    {

        public string ProjectName = "";
        public string ProjectInfo = "";
        public string ProjectPath = "";
        public string IconPath = "";
        public string ProjectAuthor = "";

        public NewProject()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            var path = folderBrowserDialog1.SelectedPath;
            ProjectPath = path;
            pathBox.Text = path;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            var path = openFileDialog1.FileName;
            IconPath = path;
            var img = new Bitmap(path);
            img = new Bitmap(img, new Size(iconPanel.Width, iconPanel.Width));
            iconPanel.BackgroundImage = img;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form1.Main.NewProj = null;
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1.Main.NewProject(pathBox.Text,nameBox.Text,  IconPath, authorBox.Text, infoBox.Text);
            //Form1.Main.NewPro

            Close();
            Form1.Main.NewProj = null;
        }
    }
}

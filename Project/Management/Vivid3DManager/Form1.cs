﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Vivid3DManager.Control;

using VividProjects;

namespace Vivid3DManager
{
    public partial class Form1 : Form
    {

        public NewProject NewProj;
        public static Form1 Main;

        public List<VividProject> Projects = new List<VividProject>();


        public Form1()
        {
            InitializeComponent();
            Main = this;
            LoadProjects();
        }

        public void NewProject(string path,string name,string icon,string author,string info)
        {
            var proj = new VividProject(name, author, info, icon, path);
            proj.SaveProject(path + "/vividproj.proj");
            Projects.Add(proj);
            SaveProjects();
            RebuildUI();

        }

        void RebuildUI()
        {

            projectList.Items.Clear();
            for(int i = 0; i < Projects.Count; i++)
            {

                int ib = projectList.Items.Add(Projects[i]);

                //projectList.Items[ib].



            }

        }

        public void LoadProjects()
        {

            if(File.Exists("projects.dat"))
            {
                FileStream fs = new FileStream("projects.dat", FileMode.Open, FileAccess.Read);
                BinaryReader r = new BinaryReader(fs);

                int pc = r.ReadInt32();

                for(int i = 0; i < pc; i++)
                {


                    var nproj = new VividProject();
                    nproj.Read(r);
                    Projects.Add(nproj);

                }

                r.Close();
                fs.Close();

            }
            RebuildUI();

        }

        public void SaveProjects()
        {
            FileStream fs = new FileStream("projects.dat", FileMode.Create, FileAccess.Write);
            BinaryWriter w = new BinaryWriter(fs);

            w.Write(Projects.Count);
            foreach(var proj in Projects)
            {
                proj.Write(w);
            }

            w.Flush();
            fs.Flush();
            w.Close();
            fs.Close();

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            NewProj = new NewProject();
            NewProj.Show();
        }

        private void projectList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int it = projectList.SelectedIndex;
            if (it == -1) return;
            var proj = projectList.Items[it] as VividProject;
            infoBox.Text = proj.Info;
            var iconbm = new Bitmap(proj.Icon);
            iconbm = new Bitmap(iconbm, new Size(iconPan.Width, iconPan.Height));
            iconPan.BackgroundImage = iconbm;


        }

        private void button3_Click(object sender, EventArgs e)
        {
            int it = projectList.SelectedIndex;
            if (it == -1) return;
            Projects.Remove(projectList.Items[it] as VividProject);
            projectList.Items.Remove(projectList.Items[it]);
            infoBox.Text = "";
            iconPan.BackgroundImage = null;
            SaveProjects();

            RebuildUI();
        }
    }
}

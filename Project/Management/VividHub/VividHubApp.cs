﻿using System;
using System.Collections.Generic;
using System.Text;
using Vivid.App;
using OpenTK.Windowing.Desktop;
namespace VividHub
{
    public class VividHubApp : Application
    {

        public VividHubApp(GameWindowSettings gameSet, NativeWindowSettings natSet) : base("Vivid Hub",gameSet,natSet)
        {

        }

    }
}

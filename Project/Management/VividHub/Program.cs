﻿using OpenTK.Windowing.Desktop;

using System;

using Vivid.App;


namespace VividHub
{
    class Program
    {
        static void Main(string[] args)
        {
            var gs = GameWindowSettings.Default;
            var ns = new NativeWindowSettings();
            ns.API = OpenTK.Windowing.Common.ContextAPI.OpenGL;
            ns.APIVersion = new Version(4, 5);
            ns.AutoLoadBindings = true;
            ns.Flags = OpenTK.Windowing.Common.ContextFlags.Default;
            ns.IsEventDriven = false;
            ns.IsFullscreen = false;
            ns.Profile = OpenTK.Windowing.Common.ContextProfile.Compatability;
            ns.Size = new OpenTK.Mathematics.Vector2i(1720, 880);
            ns.StartFocused = true;
            ns.StartVisible = true;
            ns.Title = "Vivid3D IDE";
            // gs.IsMultiThreaded = true;
            ns.Size = new OpenTK.Mathematics.Vector2i(800, 500);
            gs.UpdateFrequency = 50;
            gs.RenderFrequency = 25;

            ns.WindowBorder = OpenTK.Windowing.Common.WindowBorder.Fixed;
            ns.WindowState = OpenTK.Windowing.Common.WindowState.Normal;

            Console.WriteLine("Creating application.");

            //var initState = new States.TestUI();// VividIDE.States.SceneEditorState();


            Application.InitState = new VividHubState();

            VividHubApp app = new VividHubApp(gs, ns);

            Console.WriteLine("Running Hub application");
            app.Run();

        }
    }
}

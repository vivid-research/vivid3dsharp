﻿using System;
using System.Collections.Generic;
using System.Text;
using Vivid.Resonance;
using Vivid.Resonance.Forms;
namespace VividHub
{
    public class NewProjectForm : WindowForm
    {
        public ButtonForm Create;
        public string ProjectName;
        public NewProjectForm()
        {

            AfterSet = () =>
            {

                var lab = new LabelForm().Set(5, 30, 5, 5, "Name");
                var nameBox = new TextBoxForm().Set(70, 30, 290, 30) as TextBoxForm;
                Body.Add(lab);
                Body.Add(nameBox);
                ProjectName = "";
                Create = new ButtonForm().Set(10, 75, 120, 30, "Create") as ButtonForm;
                var cancel = new ButtonForm().Set(170, 75, 120, 30, "Cancel");
                nameBox.Enter = (proj) =>
                {
                    ProjectName = proj;
                };
                Body.Add(Create, cancel);

                cancel.Click = (b) =>
                {

                    UI.CurUI.Top = null;

                };



            };

        }

    }
}

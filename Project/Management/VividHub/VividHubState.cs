﻿using System;
using System.Collections.Generic;
using System.Text;
using Vivid.State;
using Vivid.Resonance;
using Vivid.Resonance.Forms;
using Vivid.App;
using System.IO;
using OpenTK.Graphics.OpenGL;
using System.Diagnostics;

namespace VividHub
{
    public class Project
    {
        public string Name, Path;
    }
    public class VividHubState : AppState
    {
        public List<Project> Projects = new List<Project>();
        public string EngineDir = "c:\\VividDir\\";
        public string ProjectsDir = "c:\\ProjectsDir\\";
        void LoadProjects()
        {
            if(!File.Exists("projects.dat"))
            {
                return;
            }
            FileStream fs = new FileStream("projects.dat", FileMode.Open, FileAccess.Read);
            BinaryReader r = new BinaryReader(fs);

            int pc = r.ReadInt32();
            for(int i = 0; i < pc; i++)
            {
                var np = new Project();
                np.Name = r.ReadString();
                np.Path = r.ReadString();
                Projects.Add(np);
            }
            r.Close();
            fs.Close();

        }

        void RebuildUI()
        {
            projList.Items.Clear();
            foreach(var proj in Projects)
            {
                projList.AddItem(proj.Name, null);
            }
        }

        void SaveProjects()
        {

            FileStream fs = new FileStream("projects.dat", FileMode.Create, FileAccess.Write);
            BinaryWriter w = new BinaryWriter(fs);

            w.Write(Projects.Count);
            foreach(var proj in Projects)
            {
                w.Write(proj.Name);
                w.Write(proj.Path);
            }

            w.Flush();
            fs.Flush();
            w.Close();
            fs.Close();
            w = null;
            fs = null;



        }

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the destination directory doesn't exist, create it.       
            Directory.CreateDirectory(destDirName);

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string tempPath = Path.Combine(destDirName, file.Name);
                file.CopyTo(tempPath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string tempPath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, tempPath, copySubDirs);
                }
            }
        }
        ListForm projList;
        Project CurProj = null;
        public override void InitState()
        {
            base.InitState();
            SUI = new Vivid.Resonance.UI();

            var bg = new ColorForm().Set(0, 0, AppInfo.W, AppInfo.H);
            SUI.Root = bg;

            var pan = new PanelForm().Set(20, 20, 400, 200, "Action");
            var projPan = new PanelForm().Set(440, 20, 340, 400, "Projects");

            projList = new ListForm().Set(5, 35, 320, 360) as ListForm;

            projList.Selected = (i) =>
            {
                foreach(var proj in Projects)
                {
                    if(proj.Name == i.Text)
                    {
                        CurProj = proj;
                    }
                }
            };

            projPan.Add(projList);

            bg.Add(pan);
            bg.Add(projPan);

            var newProj = new LabelButtonForm().Set(10, 40, 5, 5, "New Project  ...");
            var loadProj = new LabelButtonForm().Set(10, 100, 5, 5, "Load Project    ");
            pan.Add(newProj);
            pan.Add(loadProj);

            loadProj.Click = (b) =>
            {

                if (CurProj != null)
                {

                    var proc = Process.Start(CurProj.Path + "Editor\\netcoreapp3.1\\Vivid3DIde.exe");
                    proc.Start();


                }

            };

            void CreateProject(string name)
            {
                if (!Directory.Exists(ProjectsDir))
                {
                    Directory.CreateDirectory(ProjectsDir);
                }
                var pdir = ProjectsDir + name + "\\";

                Directory.CreateDirectory(ProjectsDir + name+"\\");
                Directory.CreateDirectory(pdir + "Content\\");

                DirectoryCopy(EngineDir + "Editor\\", pdir + "Editor\\",true);

                File.Copy(EngineDir + "StartEditor.exe", pdir + "StartEditor.exe");
                File.Copy(EngineDir + "StartEditor.dll", pdir + "StartEditor.dll");
                File.Copy(EngineDir + "StartEditor.deps.json", pdir + "StartEditor.deps.json");
                File.Copy(EngineDir + "StartEditor.runtimeconfig.dev.json", pdir + "StartEditor.runtimeconfig.dev.json");
                File.Copy(EngineDir + "StartEditor.runtimeconfig.json", pdir + "StartEditor.runtimeconfig.json");

                //Directory.Move(EngineDir + "Editor\\", pdir+"Editor\\");
                var np = new Project();
                np.Name = name;
                np.Path = pdir;
                Projects.Add(np);
                SaveProjects();
                RebuildUI();



            }

            newProj.Click = (b) =>
            {

                var np = new NewProjectForm().Set(AppInfo.W / 2 - 200, AppInfo.H / 2 - 80, 400, 160, "New Project") as NewProjectForm;
                SUI.Top = np;

                np.Create.Click = (b) =>
                {

                    var pn = np.ProjectName;
                    SUI.Top = null;
                    CreateProject(pn);



                };

            };

            LoadProjects();
            RebuildUI();

        }

        public override void UpdateState()
        {
            base.UpdateState();
            SUI.Update();
        }

        public override void DrawState()
        {
            base.DrawState();
            SUI.Render();
        }
    }
}

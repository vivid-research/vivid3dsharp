﻿using System;
using System.Collections.Generic;
using System.Text;

using OpenTK.Mathematics;
using OpenTK.Windowing.Desktop;

using Vivid.App;
using Vivid.Draw;
using Vivid.Texture;

using VividScript;
using VividScript.Compile;
using VividScript.Run;

namespace ScriptTest1
{
    public class ScriptTestApp : Application
    {
        public ScriptTestApp(GameWindowSettings gameSet, NativeWindowSettings natSet) : base("Vivid3D IDE", gameSet, natSet)
        {
        }

        RunStack runner;

        public override void Init()
        {

            Console.WriteLine("Begining test.");
            var compiler = new Compiler("test/test1.vs");

            var result = compiler.Compile();

            //while(true)

            Console.WriteLine("Module:" + result.Module.Name);

            result.Module.Debug();

            foreach (var cls in result.Module.Classes)
            {
                //Console.WriteLine("Class:" + cls.Name);
                cls.Debug();
            }

            Console.WriteLine("R:" + result.Code);

            VividScript.Run.RunStack run = new VividScript.Run.RunStack();

            run.AddModule(result.Module);

            var res = run.RunFunc("main");
            //base.Init();

            runner = run;

        }
        public override void Update()
        {
            //base.Update();
    
            runner.RunFunc("updateGame");
        }

        public override void Render()
        {
            //base.Render();
          
            runner.RunFunc("drawGame");
        }

    }
}

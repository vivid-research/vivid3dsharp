﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample4_ComplexModel
{
    class Program
    {
        static void Main(string[] args)
        {

            System.Console.WriteLine("Running sample 4 - Complex Model - Vivid Engine");

            Vivid.App.VividApp.InitState = new SampleComplexModelState();

            var app = new SampleComplexModelApp();

            app.Run();

        }
    }
}

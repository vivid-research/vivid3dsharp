﻿using SampleBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivid.Scene;
using Vivid.Import;
namespace Sample4_ComplexModel
{

    public class SampleComplexModelState : SampleBase.SampleBaseState
    {


        public Entity3D Char1 = null;
        public Entity3D CubeRoom = null;
        public float LightR = 0.0f;

        public override void InitState()
        {
            base.InitState();
            CubeRoom = Import.ImportNode("Asset/CubeRoom1T.obj") as Entity3D;
            CubeRoom.SetMultiPass();



            Char1 = Import.ImportNode("Asset/ComplexModel/scene.gltf") as Entity3D;
            Char1.SetMultiPass();
            //Environment.Exit(1);
            Graph.Add(Char1);
            Graph.Add(CubeRoom);
            Char1.LocalScale = new OpenTK.Vector3(4.5f, 4.5f, 4.5f);
            Char1.SetPos(new OpenTK.Vector3(0,-1,0));
        }

        public override void UpdateState()
        {
            base.UpdateState();
            Char1.Turn(new OpenTK.Vector3(0, 1, 0), Space.Local);
            LightR = LightR + 0.3f;

            float LA = 0;
            LA = Vivid.Util.Maths.DegToRad(LightR);



            Light1.SetPos(new OpenTK.Vector3((float)Math.Sin(LA) * 13, 3, (float)Math.Cos(LA) * 13));


        }

    }
}

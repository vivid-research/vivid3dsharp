﻿using SampleBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivid.Scene;
using Vivid.Import;
using Vivid.Composition;
using Vivid.Composition.Compositers;
using Vivid.Draw;
using Vivid.App;
using Vivid.Resonance.Forms;
using Vivid.Resonance;

namespace Sample6_Compositor
{
    public class SampleCompositorState : SampleBaseState
    {
        public Entity3D Char1 = null;
        public Entity3D CubeRoom = null;
        public float LightR = 0.0f;
        public Composite Comp;
        public Compositer Blur, Bloom, Dof, SSR, Outline, BloomUI;

        public override void InitState()
        {
            base.InitState();
            CubeRoom = Import.ImportNode("Asset/CubeRoom1T.obj") as Entity3D;
            CubeRoom.SetMultiPass();



            Char1 = Import.ImportNode("Asset/ComplexModel/scene.gltf") as Entity3D;
            Char1.SetMultiPass();
            //Environment.Exit(1);
            Graph.Add(Char1);
            Graph.Add(CubeRoom);
            Char1.LocalScale = new OpenTK.Vector3(4.5f, 4.5f, 4.5f);
            Char1.SetPos(new OpenTK.Vector3(0, -1, 0));

            Comp = new Composite();

            Comp.SetGraph(Graph);

            Blur = new BlurCompositer();
            Outline = new OutlineCompositer();
            Bloom = new BloomCompositer();
            Dof = new DOFCompositer();

         //   Comp.AddCompositer(Bloom);
          //  Comp.AddCompositer(Blur);
           // Comp.AddCompositer(Outline);

            Comp.Render = (b) =>
            {

               
                IntelliDraw.BeginDraw();

                IntelliDraw.DrawImg(0, 0, Vivid.App.AppInfo.W,AppInfo.H, b, new OpenTK.Vector4(1, 1, 1, 1),true);

                IntelliDraw.EndDraw();

            };

            SUI = new Vivid.Resonance.UI();

            var root = new UIForm().Set(0, 0, AppInfo.W, AppInfo.H,"");

            var win = new WindowForm().Set(32, 32, 350, 250, "Compositor");

            SUI.Root = root;

            root.Add(win);

            var dofWin = new WindowForm().Set(32, 550, 350, 150, "Depth Of Field");

            root.Add(dofWin);

            var lab2 = new LabelForm().Set(10, 70, 120, 20, "Focus Range");

            var dofR = new HorizontalSliderForm().Set(10, 100, 180, 5, "") as HorizontalSliderForm;
            dofR.Max = 1;

            var dofRLab = new LabelForm().Set(200, 100, 120, 20, "R:0");

            dofR.ValChanged = (v) =>
            {
                dofRLab.Text = "R:" + v.ToString();
                var dof = Dof as DOFCompositer;
                dof.SetZRange(v);
            };



            var lab = new LabelForm().Set(10, 25, 120, 20, "Z Focus");

            var dofZ = new HorizontalSliderForm().Set(10, 60, 180, 5, "") as HorizontalSliderForm;
            dofZ.Max = 1;

            var dofLab = new LabelForm().Set(200, 55, 120, 25, "Z:0");

            dofZ.ValChanged = (v) =>
            {
                dofLab.Text = "Z:" + v.ToString();
                //Dof.SetZ
                var dof = Dof as DOFCompositer;
                dof.SetZFocus(v);
                //dofRLab

            };
            dofWin.Add(dofRLab);
            dofWin.Add(dofZ);
            dofWin.Add(lab);
            dofWin.Add(dofLab);
            dofWin.Add(dofR);
            dofWin.Add(lab2);
            //dofWin.Add()


            var bloomCB = new CheckBoxForm().Set(20, 30, 28,28, "Bloom?");
            var blurCB = new CheckBoxForm().Set(20, 60, 28, 28, "Blur?");
            var outlineCB = new CheckBoxForm().Set(20, 90, 28, 28, "Outline?");
            var dofCB = new CheckBoxForm().Set(20, 120, 28, 28, "Dof?");

            win.Add(bloomCB);
            win.Add(blurCB);
            win.Add(outlineCB);
            win.Add(dofCB);

            cBloom = cOutline = cBlur = false;

            dofCB.Click = (b) =>
            {

                if(cDof)
                {
                    cDof = false;
                }
                else
                {
                    cDof = true;
                }
                setComp();

            };

            bloomCB.Click = (b) =>
            {
                if (cBloom)
                {
                    cBloom = false;
                }
                else
                {
                    cBloom = true;
                }
                setComp();
                //Environment.Exit(1);

            };

            outlineCB.Click = (b) =>
            {

                if (cOutline)
                {
                    cOutline = false;
                }
                else
                {
                    cOutline = true;
                }
                setComp();

            };

            blurCB.Click = (b) =>
            {

                if (cBlur)
                {
                    cBlur = false;
                }
                else
                {
                    cBlur = true;
                }

                setComp();

            };
            
            //Cam1.SetPos(new OpenTK.Vector3(0, 0, 80));
        }

        void setComp()
        {
            Comp.Composites.Clear();
            if (cBloom)
            {
                Comp.AddCompositer(Bloom);
            };
            if (cBlur)
            {
                Comp.AddCompositer(Blur);
            };
            if(cOutline)
            {
                Comp.AddCompositer(Outline);
            }
            if(cDof)
            {
                Comp.AddCompositer(Dof);
            }
            //Comp.SetGraph

        }

        bool cBloom, cOutline, cBlur, cDof;
        public override void UpdateState()
        {
            base.UpdateState();
            Char1.Turn(new OpenTK.Vector3(0, 1, 0), Space.Local);
            LightR = LightR + 0.3f;

            SUI.Update();
            float LA = 0;
            LA = Vivid.Util.Maths.DegToRad(LightR);



            Light1.SetPos(new OpenTK.Vector3((float)Math.Sin(LA) * 13, 3, (float)Math.Cos(LA) * 13));


        }

        public override void DrawState()
        {
            //base.DrawState();
            Graph.RenderShadows();

            if (Comp.Composites.Count == 0)
            {
                Graph.Render();
            };







            Comp.RenderFinal();

            SUI.Render();

        }

    }
}

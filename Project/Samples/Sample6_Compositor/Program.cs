﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample6_Compositor
{
    class Program
    {
        static void Main(string[] args)
        {

            System.Console.WriteLine("Running sample 6 - Compositor - Vivid Engine");

            Vivid.App.VividApp.InitState = new SampleCompositorState();

            var app = new SampleCompositorApp();

            app.Run();

        }
    }
}

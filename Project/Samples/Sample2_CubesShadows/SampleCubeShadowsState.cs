﻿using SampleBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivid.Scene;
using Vivid.Import;
//using Vivid.

namespace Sample2_CubesShadows
{
    public class SampleCubeShadowsState : SampleBaseState
    {

        public Entity3D Cube1 = null;
        public Entity3D CubeRoom = null;
        public float LightR = 0.0f;

        public override void InitState()
        {
            base.InitState();
            CubeRoom = Import.ImportNode("Asset/CubeRoom1T.obj") as Entity3D;
            CubeRoom.SetMultiPass();
             


            Cube1 = Import.ImportNode("Asset/Cube1NormalT.obj") as Entity3D;
            Cube1.SetMultiPass();
            //Environment.Exit(1);
            Graph.Add(Cube1);
            Graph.Add(CubeRoom);

        }

        public override void UpdateState()
        {
            base.UpdateState();
            Cube1.Turn(new OpenTK.Vector3(1, 1, 0), Space.Local);
            LightR = LightR + 0.3f;

            float LA = 0;
            LA = Vivid.Util.Maths.DegToRad(LightR);



            Light1.SetPos(new OpenTK.Vector3((float)Math.Sin(LA) * 13, 3, (float)Math.Cos(LA) * 13));


        }


    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample2_CubesShadows
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Running sample 2 - Cubes Shadows - Vivid Engine");

            Vivid.App.VividApp.InitState = new SampleCubeShadowsState();

            var app = new SampleCubeShadowsApp();

            app.Run();



        }
    }
}

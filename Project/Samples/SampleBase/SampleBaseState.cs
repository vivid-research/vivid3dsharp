﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivid.Scene;
using Vivid.Scene.Node;
using Vivid.Texture;

namespace SampleBase
{
    public class SampleBaseState : Vivid.State.VividState
    {

        public SceneGraph3D Graph = null;
        public Cam3D Cam1 = null;
        public Light3D Light1 = null, Light2 = null;
        
        public SampleBaseState()
        {

        }

        public override void InitState()
        {
            base.InitState();

            Graph = new SceneGraph3D();
            Cam1 = new Cam3D();

            Graph.Add(Cam1);

            Cam1.SetPos(new OpenTK.Vector3(0, 1, 20));

            Light1 = new Light3D();
            Light2 = new Light3D();

            Light1.Range = 55;
            Light2.Range = 65;

            Light1.SetPos(new OpenTK.Vector3(0, 10, 30));
            Light2.SetPos(new OpenTK.Vector3(-5, 10, 30));

            Light2.Diff = new OpenTK.Vector3(0, 1, 1);

            Graph.Add(Light1);
            Graph.Add(Light2);

        }

        public override void UpdateState()
        {
            //base.UpdateState();
          //  Texture2D.UpdateLoading();
            Graph.Update();
        }

        public override void DrawState()
        {
            base.DrawState();

            Graph.RenderShadows();
            Graph.Render();
        }

    }
}

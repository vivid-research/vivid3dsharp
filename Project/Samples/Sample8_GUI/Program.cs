﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Vivid.App;

namespace Sample8_GUI
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Sample 8 - GUI");

            VividApp.InitState = new Sample8GUIState();

            var app = new Sample8GUIApp();

            app.Run();

        }
    }
}

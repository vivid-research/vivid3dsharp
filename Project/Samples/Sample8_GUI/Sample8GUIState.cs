﻿using KnightEngine.Resonance.Forms;

using SampleBase;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using Vivid.App;
using Vivid.Resonance;
using Vivid.Resonance.Forms;

using VividEngine.Resonance.Forms;

namespace Sample8_GUI
{
    public class Sample8GUIState : SampleBaseState
    {

        public override void InitState()
        {
            base.InitState();

            SUI = new Vivid.Resonance.UI();

            var menu = new MenuForm().Set(0, 0, AppInfo.W, 30) as MenuForm;

            var gui = menu.AddItem("GUI sample");

            var about = gui.Menu.AddItem("About");
            var load = gui.Menu.AddItem("Load File");
            var exit = gui.Menu.AddItem("Exit");

            var mb = new TextAreaForm().Set(200, 200, 300, 250) as TextAreaForm;

            mb.Lines[0] = "This is a text area form";
            mb.Lines[1] = "";
            mb.Lines[2] = "This is a editable text form.";


            var datepick = new DatePickerForm().Set(10, 470, 300, 180);


            var list1 = new ListForm().Set(320, 470, 200, 200) as ListForm;

            list1.AddItem("Item 1",null);
            list1.AddItem("Item2", null);
            list1.AddItem("Third", null);

            load.Click = (b) =>
            {

                var fr = new RequestFileForm("Select file", "C:\\");

                SUI.Top = fr;

                fr.Selected = (f) =>
                {

                    SUI.Top = null;

                };


            };

            exit.Click = (b) =>
            {
                Environment.Exit(1);
            };

            about.Click = (b) =>
            {
                var msgBox = new MessageBoxForm("About!").Set(100, 100, 350, 200, "This is the gui sample") as MessageBoxForm;
                msgBox.OK.Click = (b1) =>
                {
                    SUI.Top = null;

                };

                msgBox.Cancel.Click = (b2) =>
                {
                    SUI.Top = null;
                };

                SUI.Top = msgBox;


            };





            var root = new WindowForm().Set(0, 0, AppInfo.W, AppInfo.H) as WindowForm;

            SUI.Root = root;

            //root.Add()


            var hello_label = new LabelForm().Set(10, 100, 20, 20, "Hello Vivid World!");

            root.Body.Add(hello_label);

            var button1 = new ButtonForm().Set(10, 140, 80, 25, "Button 1");

            var button2 = new ButtonForm().Set(90, 140, 80, 25, "Button 2");

            var playVid = new ButtonForm().Set(10, 190, 90, 25, "Play Video");

            var cb1 = new CheckBoxForm().Set(185, 140, 25, 25, "CheckBox?");

            var dropL = new DropDownListForm().Set(320, 40, 200, 30) as DropDownListForm;

            var img1 = new ImageForm().Set(540, 40, 256, 256, "").SetImage(new Vivid.Texture.Texture2D("asset/bg1.jpg",Vivid.Texture.LoadMethod.Single,false));


            var num1 = new NumericForm().Set(540, 310, 140, 30) as NumericForm;

            var tf1 = new TextBoxForm().Set(540, 350, 140, 30) as TextBoxForm;

            var tree1 = new TreeViewForm().Set(540, 390, 180, 250) as TreeViewForm;

            tree1.Root = new TreeNode("Project");

            tree1.Root.Nodes.Add(new TreeNode("Images"));
            tree1.Root.Nodes.Add(new TreeNode("Sounds"));
            tree1.Root.Nodes.Add(new TreeNode("Videos"));

            tree1.Root.Nodes[0].Nodes.Add(new TreeNode("Background 1"));
            tree1.Root.Nodes[0].Nodes.Add(new TreeNode("Image 2"));

            tree1.Root.Nodes[1].Nodes.Add(new TreeNode("Whistle.mp3"));
            tree1.Root.Nodes[1].Nodes.Add(new TreeNode("Song1.mp3"));

            tree1.Root.Nodes[2].Nodes.Add(new TreeNode("Video 1"));
            tree1.Root.Nodes[2].Nodes.Add(new TreeNode("Film1.mp4"));


            tf1.Text = "Example!";

            num1.SetValue(25);
                
            dropL.AddItem("Item 1");
            dropL.AddItem("Item 2");
            dropL.AddItem("Item 3");
            dropL.AddItem("Item Four");

            root.Body.Add(tree1);
            root.Body.Add(num1);

            root.Body.Add(dropL);

            root.Body.Add(tf1);
            root.Body.Add(img1);

            root.Body.Add(playVid);

            root.Body.Add(cb1);

            playVid.Click = (b) =>
            {

                var vF = new WindowForm().Set(200, 200, 512, 512, "Video Playback") as WindowForm;

                root.Add(vF);

                var vidP = new VideoForm().Set(0, 0, vF.Body.W, vF.Body.H) as VideoForm;

                vF.Body.Add(vidP);

                vidP.SetVideo("asset/intromovie3.mp4");

                var cm = new ContextMenuForm().Set(0, 0, 200, 200) as ContextMenuForm;

                var i1 = cm.AddItem("Stop Video");
                var i2 = cm.AddItem("Close Player");

                vidP.ContextMenu = cm;

                i2.Click = () =>
                {
                    root.Forms.Remove(vF);
                };


                i1.Click = () =>
                {
                    vidP.Stop();
                };


            };

            root.Body.Add(button1, button2);

            button2.Click = (b) =>
            {

                var msgBox = new MessageBoxForm("Hello World!").Set(100, 100, 300, 200, "Hello") as MessageBoxForm;
                msgBox.OK.Click = (b1) =>
                {
                    SUI.Top = null;

                };

                msgBox.Cancel.Click = (b2) =>
                {
                    SUI.Top = null;
                };

                SUI.Top = msgBox;

            };
            root.Body.Add(datepick);
            root.Body.Add(mb);
            root.Body.Add(list1);

            root.Body.Add(menu);

        }

        public override void UpdateState()
        {
            base.UpdateState();

            SUI.Update();

        }

        public override void DrawState()
        {
            base.DrawState();

            SUI.Render();

        }

    }
}

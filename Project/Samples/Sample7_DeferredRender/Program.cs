﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Vivid.App;

namespace Sample7_DeferredRender
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Sample App 7 Deferred Renderer");

            VividApp.InitState = new Sample7DeferredRenderState();

            var app = new SampleDeferredRendererApp();

            app.Run();


        
        }
    }
}

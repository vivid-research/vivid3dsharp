﻿using SampleBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivid.Scene;
using Vivid.Import;
using Vivid.App;

namespace Sample7_DeferredRender
{
    public class Sample7DeferredRenderState : SampleBase.SampleBaseState
    {

        public Entity3D Char1 = null;
        public Entity3D CubeRoom = null;
        public float LightR = 0.0f;

        public Vivid.Deffered.DefferedRendererMRT DRM = null;

        public override void InitState()
        {
            base.InitState();
            CubeRoom = Import.ImportNode("Asset/CubeRoom1T.obj") as Entity3D;
            CubeRoom.SetMultiPass();



            Char1 = Import.ImportNode("Asset/ComplexModel/scene.gltf") as Entity3D;
            Char1.SetMultiPass();
            //Environment.Exit(1);
            Graph.Add(Char1);
            Graph.Add(CubeRoom);
            Char1.LocalScale = new OpenTK.Vector3(4.5f, 4.5f, 4.5f);
            Char1.SetPos(new OpenTK.Vector3(0, -1, 0));
            DRM = new Vivid.Deffered.DefferedRendererMRT(AppInfo.W, AppInfo.H);
            DRM.Graph = Graph;
        }

        public override void UpdateState()
        {
            //base.UpdateState();
            Char1.Turn(new OpenTK.Vector3(0, 1, 0), Space.Local);
            LightR = LightR + 0.3f;

            float LA = 0;
            LA = Vivid.Util.Maths.DegToRad(LightR);



            Light1.SetPos(new OpenTK.Vector3((float)Math.Sin(LA) * 13, 3, (float)Math.Cos(LA) * 13));


        }

        public override void DrawState()
        {
           // base.DrawState();

            Graph.RenderShadows();
            //Graph.Render();
            DRM.Render();

            Vivid.Draw.IntelliDraw.BeginDraw();

            Vivid.Draw.IntelliDraw.DrawImg(0, 0, AppInfo.W, AppInfo.H, DRM.FinalFB.BB, new OpenTK.Vector4(1, 1, 1, 1),true);

            Vivid.Draw.IntelliDraw.EndDraw();


        }

    }
}

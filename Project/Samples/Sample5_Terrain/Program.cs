﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample5_Terrain
{
    class Program
    {
        static void Main(string[] args)
        {

            System.Console.WriteLine("Running sample 5 - Terrain - Vivid Engine");

            Vivid.App.VividApp.InitState = new SampleTerrainState();

            var app = new SampleTerrainApp();

            app.Run();

        }
    }
}

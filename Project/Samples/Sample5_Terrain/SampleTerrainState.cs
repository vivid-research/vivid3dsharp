﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivid.Material;
using Vivid.Texture;

namespace Sample5_Terrain
{
    public class SampleTerrainState : SampleBase.SampleBaseState
    {

        public Vivid.Terrain.Terrain3D Ter1;

        public override void InitState()
        {
            base.InitState();

            var TerTex = new Texture2D("asset/terHM1.png", LoadMethod.Single, false);

            Ter1 = new Vivid.Terrain.Terrain3D(70, 70,0, 0.07f, 70, 70, TerTex);

            //Ter1.turn

            //Graph.Lights.Clear();
            //Graph.Add(Light1);

            // Ter1.SetMultiPass();
            Light1.SetPos(new Vector3(0, 30, -90));
            Light2.SetPos(new Vector3(0, 30, 90));

            Light2.Range = 1;

            Graph.Add(Ter1);

            //Cam1.Turn(new OpenTK.Vector3(-30, 0, 0), Vivid.Scene.Space.Local);
            Cam1.SetPos(new OpenTK.Vector3(0, 30, -90));
            Cam1.LookAt(new Vector3(0, 0, 0), Vector3.UnitY);

            var tMat1 = new Material3D();

            tMat1.ColorMap = new Texture2D("asset/ter1/grass1Color.png",LoadMethod.Single,false);
            tMat1.NormalMap = new Texture2D("asset/ter1/grass1Normal.png", LoadMethod.Single, false);
            tMat1.SpecularMap = new Texture2D("asset/ter1/grass1Spec.png", LoadMethod.Single, false);


            Ter1.SetMat(tMat1);



        }

        public override void UpdateState()
        {
            base.UpdateState();
            Ter1.Turn(new Vector3(0, 1, 0), Vivid.Scene.Space.Local);
            //Cube1.Turn(new OpenTK.Vector3(1, 1, 0), Space.Local);
            //Ter1.Turn(new OpenTK.Vector3(1, 1, 0),Vivid.Scene.Space.Local);
        }

    }
}

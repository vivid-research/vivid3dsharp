﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample3_CubesNormalMaps
{
    class Program
    {
        static void Main(string[] args)
        {

            System.Console.WriteLine("Running sample 3 - Cubes Normal Maps - Vivid Engine");

            Vivid.App.VividApp.InitState = new SampleCubeNormalsState();

            var app = new SampleCubeNormalsApp();

            app.Run();


        }
    }
}

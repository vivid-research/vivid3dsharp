﻿using SampleBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vivid.Import;
using Vivid.Scene;

namespace Sample1_Cubes
{
    public class SampleCubesState : SampleBaseState
    {

        public Entity3D Cube1 = null;

        public override void InitState()
        {
            base.InitState();
            Cube1 = Import.ImportNode("Asset/Cube1T.obj") as Entity3D;
            Cube1.SetMultiPass();
            //Environment.Exit(1);
            Graph.Add(Cube1);
       
        }

        public override void UpdateState()
        {
            base.UpdateState();
            Cube1.Turn(new OpenTK.Vector3(1, 1, 0), Space.Local);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample1_Cubes
{
    class Program
    {
        static void Main(string[] args)
        {

            System.Console.WriteLine("Running sample 1 - Cubes - Vivid Engine");

            Vivid.App.VividApp.InitState = new SampleCubesState();

            var app = new SampleCubesApp();

            app.Run();

        }
    }
}

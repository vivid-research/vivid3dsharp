﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Vivid.Texture;

namespace SampleApp1_Contacts
{
    public class ContactEntry
    {

        public int BirthDay;
        public int BirthMonth;
        public int BirthYear;

        public string Name = "John Doe";

        public Texture2D Face;

        public string[] Notes;

        public string Telephone = "";
        public string Mobile = "";
        public string Email = "";

    }
}

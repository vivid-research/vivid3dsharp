﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Vivid.State;
using Vivid.Resonance;
using Vivid.Resonance.Forms;
using Vivid.App;
using KnightEngine.Resonance.Forms;
using Vivid.Texture;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

namespace SampleApp1_Contacts.States
{
    public class ContactsNewContactUI : VividState
    {

        public override void InitState()
        {
            base.InitState();
            SUI = new Vivid.Resonance.UI();

            var root = new UIForm().Set(0, 0, AppInfo.W, AppInfo.H);

            SUI.Root = root;

            var win = new WindowForm().Set(0, 0, root.W, root.H, "New Contact");

            root.Add(win);

            var nameLab = new LabelForm().Set(5, 35, 5, 30, "Name");

            win.Add(nameLab);

            var nameBox = new TextBoxForm().Set(55, 35, 270, 28, "") as TextBoxForm;

            nameBox.Enter = (name) =>
            {

                Contacts.Editing.Name = name;

            };

            win.Add(nameBox);

            var yearBLab = new LabelForm().Set(5, 235, 5, 30, "Year Born");

            win.Add(yearBLab);

            var yearSel = new NumericForm().Set(90, 235, 120, 30, "25") as NumericForm;

            yearSel.Changed = (v) =>
            {
                Contacts.Editing.BirthYear = (int)v;
            };

            yearSel.SetValue(1980);

                    win.Add(yearSel);


            var dobLab = new LabelForm().Set(5, 70, 5, 30, "DOB");

            var dobSel = new DatePickerForm().Set(55, 70, 270, 28) as DatePickerForm;


            Contacts.Editing.BirthDay = 1;
            Contacts.Editing.BirthMonth = 1;
            Contacts.Editing.BirthYear = 1980;
            dobSel.Picked = (day,month) =>{

                Contacts.Editing.BirthDay = day;
                Contacts.Editing.BirthMonth = month;

            };



            win.Add(dobLab);
            win.Add(dobSel);

            var faceLab = new LabelForm().Set(5, 280, 5, 30, "Face");

            win.Add(faceLab);

            var facePan = new ImageForm().Set(60, 280, 100, 60, "").SetImage(new Vivid.Texture.Texture2D("app/face1.jpg", Vivid.Texture.LoadMethod.Single, false));

            var setFace = new ButtonForm().Set(60, 350, 80, 25, "Set");

            setFace.Click = (b) =>
            {

                var fileR = new RequestFileForm("Select Face Image", Environment.GetFolderPath(Environment.SpecialFolder.MyPictures));
                SUI.Top = fileR;
                fileR.Selected = (path) =>
                {
                    SUI.Top = null;
                    var nf = new Texture2D(path, LoadMethod.Single, false);
                    facePan.SetImage(nf);
                    Contacts.Editing.Face = nf;
                };


            };

            var mobLab = new LabelForm().Set(170, 275, 5, 5, "Mobile"); ;
            var emailLab = new LabelForm().Set(170, 305, 5, 5, "Email"); 

            win.Add(mobLab);
            win.Add(emailLab);

            var mobBox = new TextBoxForm().Set(225, 275, 195, 25, "") as TextBoxForm;
            var mailBox = new TextBoxForm().Set(225, 305, 195, 25, "") as TextBoxForm;

            mobBox.Enter = (t) =>
            {
                Contacts.Editing.Mobile = t;
            };
            mailBox.Enter = (t) =>
            {
                Contacts.Editing.Email = t;
            };

            win.Add(mobBox, mailBox);

            var noteLab = new LabelForm().Set(5, 375, 5, 25, "Notes");

            var notesBox = new TextAreaForm().Set(5, 390, AppInfo.W - 15, 200) as TextAreaForm;


            win.Add(notesBox);
            win.Add(facePan);
            win.Add(setFace);

            var save = new ButtonForm().Set(AppInfo.W - 90, 360, 80, 25, "Save");

            save.Click = (b) =>
            {

                Contacts.Editing.Notes = notesBox.Lines;
                //Contacts.SaveEdit();
                VividApp.PopState();

            };

            win.Add(save);

            win.Add(noteLab);

        }

        public override void UpdateState()
        {
            base.UpdateState();
            SUI.Update();

        }

        public override void DrawState()
        {
            base.DrawState();
            SUI.Render();
        }

    }
}

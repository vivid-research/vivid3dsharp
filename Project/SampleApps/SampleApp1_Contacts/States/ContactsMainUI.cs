﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Vivid.App;
using Vivid.Resonance;
using Vivid.Resonance.Forms;
using Vivid.State;
using Vivid.Texture;

namespace SampleApp1_Contacts.States
{
    public class ContactsMainUI : VividState
    {
        public Contacts Main = new Contacts();
        public int ActiveContact = 0;

        public override void InitState()
        {
            base.InitState();
            SUI = new Vivid.Resonance.UI();
            LoadContacts();
            SetupUI();
        }

        private void SetupUI()
        {
            var root = new UIForm().Set(0, 0, AppInfo.W, AppInfo.H);

            SUI.Root = root;

            var menu = new MenuForm().Set(0, 0, root.W, 30) as MenuForm;

       

            var contacts = menu.AddItem("Contacts");

        
            var clearContacts = contacts.Menu.AddItem("Clear Contacts");

            clearContacts.Click = (b) =>
            {

                Main.ContactList.Clear();
                SaveContacts();
                SetupUI();

            };

                      

            var exitProg = contacts.Menu.AddItem("Exit");

            exitProg.Click = (b) =>
            {
                Environment.Exit(0);
            };


            var bg = new ImageForm().Set(0, 30, root.W, root.H - 29).SetImage(new Texture2D("app/bg2.jpg", LoadMethod.Single, false));

            root.Add(bg);

            var pan = new PanelForm().Set(0, root.H - 82, root.W, 60);

            bg.Add(pan);

            var newContact = new ButtonForm().Set(root.W - 60, 5, 58, 45, "+");
            pan.Add(newContact);
            root.Add(menu);

            var conPan = new PanelForm().Set(0, 0, root.W, 30, "Contacts:0");

            var conLab = new LabelForm().Set(conPan.W/2-60, 2, 25, 25, "Contacts:0");

            if(Main.ContactList.Count>0)
            {
                conLab.Set(conPan.W/2-60, 2, 25, 25, "Contacts:" + Main.ContactList.Count);



            }

            bg.Add(conPan);
            conPan.Add(conLab);

            var prevC = new ButtonForm().Set(5, 4, 60, 22, "Prev");
            var nextC = new ButtonForm().Set(70, 4, 60, 22, "Next");

            prevC.Click = (b) =>
            {
                ActiveContact--;
                if (ActiveContact < 0)
                {
                    ActiveContact = Main.ContactList.Count - 1;
                    if (ActiveContact < 0)
                    {
                        ActiveContact = 0;
                        return;
                    }
                }
                SetupUI();
                return;
            };

            nextC.Click = (b) =>
            {

                ActiveContact++;
                if (ActiveContact >= Main.ContactList.Count)
                {
                    ActiveContact = 0;
                    if(Main.ContactList.Count==0)
                    {
                        return;
                    }
                }
                SetupUI();
            };

            conPan.Add(prevC, nextC);

            newContact.Click = (b) =>
            {

                ContactEntry newCon = new ContactEntry();
                Contacts.Editing = newCon;
                Main.ContactList.Add(newCon);
                VividApp.PushState(new ContactsNewContactUI());

            };

            CA = new UIForm().Set(0, 30, root.W, 20);

            bg.Add(CA);

            SetContact(ActiveContact);
        }

        public UIForm CA;

        public void SetContact(int num)
        {
            if (num >= Main.ContactList.Count) return;
            var c = Main.ContactList[num];

            var faceImg = new ImageForm().Set(CA.W/2-50, 15, 100, 64).SetImage(c.Face);
            var nameLab = new LabelForm().Set(5, 100, 20, 20, "Name:"+c.Name);
            var dobLab = new LabelForm().Set(5, 125, 20, 20, "Date Of Birth:" + c.BirthDay + "/" + c.BirthMonth + "/" + c.BirthYear);
            CA.Add(nameLab);
            CA.Add(dobLab);
            CA.Add(faceImg);

            var mobLab = new LabelForm().Set(5, 150,5,5, "Mobile:" + c.Mobile);

            var emailLab = new LabelForm().Set(5, 175, 5, 5, "Email:" + c.Email);

            CA.Add(mobLab, emailLab);

            var notesLab = new LabelForm().Set(5, 210, 5, 5, "Notes");
            CA.Add(notesLab);

            int nY = 0;
            for(int i = 0; i < 400; i++)
            {
                if (i >= c.Notes.Length) return;
                if(c.Notes[i] == null)
                {
                    continue;
                }
                if (c.Notes[i].Length > 0)
                {
                    var nn = new LabelForm().Set(5, 255 + nY, 5, 5, c.Notes[i]);
                    nY = nY + 20;
                    CA.Add(nn);

                }

            }

        }

        public override void ResumeState()
        {
            // Environment.Exit(1);
            SaveContacts();
            SetupUI();
        }
        
        public void LoadContacts()
        {
            if(!File.Exists("app/contacts.dat"))
            {
                return;
            }

            Main.ContactList.Clear();

            FileStream fs = new FileStream("app/contacts.dat", FileMode.Open, FileAccess.Read);
            BinaryReader r = new BinaryReader(fs);

            int cc = r.ReadInt32();

            for (int i = 0; i < cc; i++)
            {

                var nc = new ContactEntry();
                nc.Name = r.ReadString();
                nc.BirthDay = r.ReadInt32();
                nc.BirthMonth = r.ReadInt32();
                nc.BirthYear = r.ReadInt32();
                nc.Email = r.ReadString();
                nc.Mobile = r.ReadString();
                int nl = r.ReadInt32();
                nc.Notes = new string[nl + 1];
                for (int ic = 0; ic < nl; ic++)
                {
                    nc.Notes[ic] = r.ReadString();
                }

                int fw = r.ReadInt32();
                int fh = r.ReadInt32();

                byte[] dat;

                dat = r.ReadBytes(fw * fh * 3);

                var t2 = new Texture2D(fw, fh, dat, false);

                nc.Face = t2;

                Main.ContactList.Add(nc);

            }
            r.Close();
            fs.Close();
        }

        public void SaveContacts()
        {
            FileStream fs = new FileStream("app/contacts.dat", FileMode.Create, FileAccess.Write);
            BinaryWriter w = new BinaryWriter(fs);

            w.Write(Main.ContactList.Count);

            for(int i = 0; i < Main.ContactList.Count; i++)
            {
                var c = Main.ContactList[i];
                w.Write(c.Name);
                w.Write(c.BirthDay);
                w.Write(c.BirthMonth);
                w.Write(c.BirthYear);
                w.Write(c.Email);
                w.Write(c.Mobile);

                int nl = 0;
                for(int l = 0; l < 400; l++)
                {
                    if(c.Notes[l] == null)
                    {
                        continue;
                    }
                    if (c.Notes[l].Length > 0)
                    {
                        nl++;
                    }
                }
                w.Write(nl);
                for(int l = 0; l < 400; l++)
                {
                    var t = c.Notes[l];
                    if (t == null) continue;
                    if (t.Length > 0)
                    {
                        w.Write(t);
                    }
                }

                w.Write(c.Face.W);
                w.Write(c.Face.H);

                for(int y = 0; y < c.Face.H; y++)
                {

                    for(int x = 0; x < c.Face.W; x++)
                    {

                        int loc = y * c.Face.W * 3;
                        loc = loc + (x * 3);

                        w.Write(c.Face.RawData[loc]);
                        w.Write(c.Face.RawData[loc++]);
                        w.Write(c.Face.RawData[loc++]);

                    }

                }

            }

            w.Flush();
            fs.Flush();
            w.Close();
            fs.Close();


        }

        public override void UpdateState()
        {
            base.UpdateState();
            Texture2D.UpdateLoading();
            SUI.Update();

        }

        public override void DrawState()
        {
            base.DrawState();
            SUI.Render();
        }

    }
}

﻿using SampleApp1_Contacts.States;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Vivid.App;

namespace SampleApp1_Contacts
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Vivid Engine - Sample App Contacts");
            VividApp.InitState = new ContactsMainUI();
            var app = new SampleAppContacts();

          
                app.Run();
           
        }
    }
}

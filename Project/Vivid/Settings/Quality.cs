﻿namespace Vivid.Settings
{
    public static class Quality
    {
        public static int ShadowMapWidth = 1024;
        public static int ShadowMapHeight = 1024;
        public static float ShadowDepth = 1000;
    }
}
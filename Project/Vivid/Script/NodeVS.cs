﻿using OpenTK.Mathematics;

using Vivid.Scene;

using VividScript.Compile;
using VividScript.Run;
using VividScript.Struct;

namespace Vivid.Script
{
    public class NodeVS
    {

        public static VividScript.Run.RunStack Runner = new VividScript.Run.RunStack();

        public StructModule Module;

        public string FullPath = "";

        public bool Updated = false;

        public Node3D Node = null;

        public NodeVS(string path, Node3D node)
        {

            Node = node;
            FullPath = path;
            var com = new Compiler(path);
            var result = com.Compile();
            Module = result.Module;
            Runner.AddModule(result.Module);

            SysFunc rotateFunc = (p) =>
            {

                //p[0].T
                p[0].Turn(new Vector3((float)p[1], (float)p[2], (float)p[3]));
                return null;
            };

            Runner.RegisterSystemFunc("rotateNode", rotateFunc);

        }

        public string GetName()
        {
            return Module.Name;
        }

        public void InitNode()
        {
            var f = Module.FuncsTable["initNode"];

            var res = Runner.RunFunc(f, Node);
        }

        public void InitUI()
        {

        }

        public void UpdateNode()
        {

            var f = Module.FuncsTable["updateNode"];


            Runner.RunFunc(f);



        }
        public void RenderNode()
        { }
        public void EndNode()
        {

        }
        public void SaveNode()
        {

        }
        public void LoadNode()
        {

        }
    }
}

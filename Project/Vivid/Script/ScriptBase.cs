﻿using Vivid.Scene;

namespace Vivid.Script
{
    public class ScriptLink : Node3D
    {
        private dynamic script;

        public string FilePath
        {
            get;
            set;
        }

        public bool Compiled
        {
            get;
            set;
        }

        public ScriptLink()
        {
            Compiled = false;
        }

        public void Compile(Node3D node)
        {
            if (Compiled)
            {
                return;
            }

            Compiled = true;

            script.Node = node;
            System.Console.WriteLine("Script:" + FilePath + " Compiled.");
        }

#pragma warning disable CS0114 // 'ScriptLink.Update()' hides inherited member 'Node3D.Update()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        public void Update()
#pragma warning restore CS0114 // 'ScriptLink.Update()' hides inherited member 'Node3D.Update()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        {
            script.Update();
        }

#pragma warning disable CS0108 // 'ScriptLink.Begin()' hides inherited member 'Node3D.Begin()'. Use the new keyword if hiding was intended.
        public void Begin()
#pragma warning restore CS0108 // 'ScriptLink.Begin()' hides inherited member 'Node3D.Begin()'. Use the new keyword if hiding was intended.
        {
            script.Begin();
        }

#pragma warning disable CS0108 // 'ScriptLink.End()' hides inherited member 'Node3D.End()'. Use the new keyword if hiding was intended.
        public void End()
#pragma warning restore CS0108 // 'ScriptLink.End()' hides inherited member 'Node3D.End()'. Use the new keyword if hiding was intended.
        {
            script.End();
        }
    }
}
﻿using System.Collections.Generic;

namespace Vivid.Scripting
{
    public class ScriptList
    {
        public List<NodeComponent> Scripts = new List<NodeComponent>();
    }
}
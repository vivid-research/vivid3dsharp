﻿
using VividScript.Reporting;
using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseNew : VividParser
    {

        public ParseNew(TokenStream stream, int index) : base(stream, index)
        {

        }

        public override CodeStruct Parse()
        {

            var tok = NextToken();

            if (tok.Type != TokenType.New)
            {
                ErrorReport.CompileError("Expecting 'new' operator.", Stream, TokenIndex);
            }

            var snew = new StructNew();

            snew.ClsName = NextToken().Actual;

            var cp = new ParseCallParams(Stream, TokenIndex);

            var scp = cp.Parse() as StructCallParams;

            snew.Params = scp as StructCallParams;

            TokenIndex = cp.TokenIndex;

            return snew;

            //return base.Parse();

            return null;
        }


    }
}

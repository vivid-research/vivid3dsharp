﻿
using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseAssign : VividParser
    {
        public ParseAssign(TokenStream stream, int index) : base(stream, index)
        {
        }

        public override CodeStruct Parse()
        {
            var ret = new StructAssign();

            var tok = NextToken();

            string[] acc = new string[255];
            bool ac = false;

            if (PeekNextToken().Type == TokenType.Access)
            {
                ac = true;
                int ai = 1;
                acc[0] = tok.Actual;
                while (true)
                {
                    var nt = NextToken();
                    if (nt.Type == TokenType.Access)
                    {
                        continue;
                    }
                    if (nt.Type == TokenType.ID)
                    {
                        acc[ai] = nt.Actual;
                        ai++;
                        continue;
                    }
                    break;
                }

                int vv = 0;

            }
            else
            {

            }
            StructVar ntar;



            if (!ac)
            {
                ntar = new StructVar(tok.Actual);
            }
            else
            {
                ntar = new StructVar(acc);
            }
            var ass = new StructAssign();

            ass.Target = ntar;

            if (PeekNextToken().Type == TokenType.LeftArray)
            {
                ConsumeNextToken();

                var parseIndex = new ParseExpr(Stream, TokenIndex);

                ass.ArrayIndex = parseIndex.Parse() as StructExpr;

                TokenIndex = parseIndex.TokenIndex;


            }

            var parseExpr = new ParseExpr(Stream, TokenIndex);

            ass.Expr = parseExpr.Parse() as StructExpr;

            TokenIndex = parseExpr.TokenIndex;

            return ass;

            //return base.Parse();
        }
    }
}
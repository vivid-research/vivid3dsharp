﻿using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseIf : VividParser
    {
        public ParseIf(TokenStream stream, int index) : base(stream, index)
        {
        }

        public override CodeStruct Parse()
        {
            var sif = new StructIf();

            var tok = NextToken();

            if (tok.Type == TokenType.If)
            {
                ConsumeNextToken();
            }
            else
            {
                BackToken();
            }

            tok = NextToken();

            if (tok.Type == TokenType.LeftPara)
            {
            }
            else
            {
                BackToken();
            }

            var ifExp = new ParseExpr(Stream, TokenIndex);

            var ife = ifExp.Parse();

            TokenIndex = ifExp.TokenIndex;

            sif.Exp = ife as StructExpr;

            var tt = NextToken();

            if (tt.Type == TokenType.EndLine)
            {
            }
            else
            {
                ConsumeNextToken();
            }

            var codep = new ParseCode(Stream, TokenIndex);

            var code = codep.Parse();

            sif.TrueCode = code as StructCode;

            TokenIndex = codep.TokenIndex;

            tt = NextToken();

            if (tt.Type == TokenType.Else)
            {
                if (PeekNextToken().Type == TokenType.If)
                {
                    int vv = 5;
                    var pif = new ParseIf(Stream, TokenIndex);

                    sif.ElseIf = pif.Parse() as StructIf;

                    TokenIndex = pif.TokenIndex;
                }

                codep = new ParseCode(Stream, TokenIndex);
                code = codep.Parse();
                sif.ElseCode = code as StructCode;
                TokenIndex = codep.TokenIndex;
            }

            return sif;
            //return base.Parse();
        }
    }
}
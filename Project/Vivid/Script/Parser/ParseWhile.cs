﻿using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseWhile : VividParser
    {
        public ParseWhile(TokenStream stream, int index) : base(stream, index)
        {
        }

        public override CodeStruct Parse()
        {
            var sw = new StructWhile();

            var t = NextToken();

            if (t.Type == TokenType.While)
            {
                ConsumeNextToken();
            }

            if (t.Type == TokenType.LeftPara)
            {
                ConsumeNextToken();
                t = NextToken();
            }

            var parseExp = new ParseExpr(Stream, TokenIndex);

            sw.Expr = parseExp.Parse() as StructExpr;

            TokenIndex = parseExp.TokenIndex;

            var codeP = new ParseCode(Stream, TokenIndex);

            sw.Code = codeP.Parse() as StructCode;

            TokenIndex = codeP.TokenIndex;

            //t = NextToken();

            return sw;
        }
    }
}
﻿using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseParams : VividParser
    {
        public ParseParams(TokenStream stream, int index) : base(stream, index)
        {
        }

        public override CodeStruct Parse()
        {
            var ret = new StructParams();

            var tok = NextToken();

            if (tok.Type == TokenType.LeftPara)
            {
            }

            while (true)
            {
                var name = NextToken();

                var nv = new StructVar(name.Actual);
                ret.AddParam(nv);

                if (PeekNextToken().Type == TokenType.Comma)
                {
                    ConsumeNextToken();
                    continue;
                }
                else
                {
                    break;
                }
            }

            return ret;

            //return base.Parse();
        }
    }
}
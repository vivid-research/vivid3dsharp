﻿
using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseExpr : VividParser
    {
        public ParseExpr(TokenStream stream, int index) : base(stream, index)
        {
        }

        public override CodeStruct Parse()
        {
            var tok = NextToken();

            var sexp = new StructExpr();

            var ex = sexp.Exp;

            if (tok.Type == TokenType.Assign)
            {
                //tok = NextToken();
            }
            else
            {
                TokenIndex--;
            }

            tok = PeekNextToken();

            if (tok.Type == TokenType.New)
            {

                //BackToken();

                var parseNew = new ParseNew(Stream, TokenIndex);

                var snew = parseNew.Parse() as StructNew;

                TokenIndex = parseNew.TokenIndex;

                ex.AddNew(snew);


            }

            if (tok.Type == TokenType.RightPara)
            {
                BackToken();
            }

            while (true)
            {
                tok = NextToken();

                switch (tok.Type)
                {
                    case TokenType.Equals:

                        ex.Add(OperatorType.Equals);
                        break;

                    case TokenType.RightArray:
                        goto Done1;
                        break;

                    case TokenType.LeftPara:

                        if(PeekNextToken().Type == TokenType.RightPara)
                        {

                            ConsumeNextToken();
                            continue;

                        }

                        var nep = new ParseExpr(Stream, TokenIndex);

                        var ne = nep.Parse() as StructExpr;

                        ex.Add(ne.Exp);

                        TokenIndex = nep.TokenIndex;

                        break;

                    case TokenType.String:

                        ex.Add(tok.Actual);

                        break;

                    case TokenType.Number:

                        var num = double.Parse(tok.Actual);

                        ex.Add(num);

                        break;

                    case TokenType.ID:

                        if (PeekNextToken().Type == TokenType.LeftArray)
                        {
                            //   int vv = 0;
                            var indexp = new ParseExpr(Stream, TokenIndex);
                            var exp = indexp.Parse() as StructExpr;

                            ex.AddArrayVar(tok.Actual, exp);

                            TokenIndex = indexp.TokenIndex;
                        }
                        else
                        {
                            if (PeekNextToken().Type == TokenType.LeftPara)
                            {
                                TokenIndex--;

                                var parseFC = new ParseFlatCall(Stream, TokenIndex);

                                var fc = parseFC.Parse() as StructFlatCall;

                                ex.AddFlatCall(fc);
                                TokenIndex = parseFC.TokenIndex;

                                if (PeekNextToken().Type == TokenType.RightPara)
                                {
                                    ConsumeNextToken();
                                }
                                // Token tt = NextToken();

                                // BackToken();

                                break;
                            }
                            if (PeekNextToken().Type == TokenType.Access)
                            {
                                int vv = 0;
                                string[] acc = new string[255];
                                int ai = 1;
                                acc[0] = tok.Actual;

                                while (true)
                                {

                                    var nt = NextToken();
                                    if (nt.Type == TokenType.Access)
                                    {
                                        continue;
                                    }
                                    if (nt.Type == TokenType.ID)
                                    {
                                        acc[ai] = nt.Actual;
                                        ai++;
                                        continue;
                                    }
                                    break;

                                }
                                BackToken();
                                ex.AddVar(acc);
                            }
                            else
                            {
                                ex.AddVar(tok.Actual);
                            }
                        }
                        break;

                    case TokenType.GreaterThan:
                        ex.Add(OperatorType.GreaterThan);
                        break;

                    case TokenType.LessThan:
                        ex.Add(OperatorType.LessThan);
                        break;

                    case TokenType.Plus:

                        ex.Add(OperatorType.Plus);
                        break;

                    case TokenType.Minus:
                        ex.Add(OperatorType.Mins);
                        break;

                    case TokenType.Times:
                        ex.Add(OperatorType.Times);
                        break;

                    case TokenType.Divide:
                        ex.Add(OperatorType.Divide);
                        break;

                    case TokenType.RightPara:
                        TokenIndex--;
                        goto Done1;
                    case TokenType.Comma:
                    case TokenType.EndLine:
                        goto Done1;
                        break;
                }
            }
        Done1:

            return sexp;
        }
    }
}
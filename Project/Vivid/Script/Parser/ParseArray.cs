﻿using VividScript.Reporting;
using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseArray : VividParser
    {
        public ParseArray(TokenStream stream, int index) : base(stream, index)
        {
        }

        public override CodeStruct Parse()
        {
            var cs = new StructArray();

            cs.Name = NextToken().Actual;

            var t = NextToken();

            if (t.Type == TokenType.LeftArray)
            {
                t = NextToken();
            }

            int aa = int.Parse(t.Actual);

            cs.Vars = new StructVar[aa];

            for (int i = 0; i < cs.Vars.Length; i++)
            {
                cs.Vars[i] = new StructVar(cs.Name);
            }

            t = NextToken();

            if (t.Type != TokenType.RightArray)
            {
                ErrorReport.CompileError("Expecting ']'", Stream, TokenIndex);
            }

            return cs;
            //return base.Parse();
        }
    }
}
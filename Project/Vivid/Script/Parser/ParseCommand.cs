﻿using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseCommand : VividParser
    {

        public ParseCommand(TokenStream stream, int index) : base(stream, index)
        {

        }

        public override CodeStruct Parse()
        {
            var scom = new StructCommand();
            GuessType gt = GuessType.Unknown;


            for (int i = 0; i < Stream.Count; i++)
            {
                var t = NextToken();
                if (t.Type == TokenType.LeftPara)
                {
                    gt = GuessType.FlatCall;
                    break;
                }

            }

            TokenIndex = 0;

            switch (gt)
            {
                case GuessType.FlatCall:

                    var pfc = new ParseFlatCall(Stream, TokenIndex);

                    scom.FlatCall = pfc.Parse() as StructFlatCall;


                    break;
            }
            return scom;
            //return base.Parse();
        }

    }
}

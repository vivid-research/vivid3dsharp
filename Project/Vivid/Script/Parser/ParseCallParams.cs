﻿using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseCallParams : VividParser
    {
        public ParseCallParams(TokenStream stream, int index) : base(stream, index)
        {
        }

        public override CodeStruct Parse()
        {
            var res = new StructCallParams();

            var tok = NextToken();

            //var ntok = NextToken();
            while (true)
            {
            Next1:
                var t = PeekNextToken();

                switch (t.Type)
                {
                    case TokenType.RightPara:
                        goto Done1;
                    case TokenType.Comma:
                        goto Next1;
                        break;

                    case TokenType.End:
                    case TokenType.EndLine:
                        goto Done1;
                        break;
                }

                var eParse = new ParseExpr(Stream, TokenIndex);

                var exp = eParse.Parse() as StructExpr;

                res.Pars.Add(exp);

                TokenIndex = eParse.TokenIndex;
            }
        Done1:

            return res;

            //return base.Parse();
        }
    }
}
﻿using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseVars : VividParser
    {
        public ParseVars(TokenStream stream, int index) : base(stream, index)
        {
        }

        public override CodeStruct Parse()
        {
            var type = NextToken();

            var cv = new StructDefineVar();

            while (true)
            {
                if (TokenIndex >= Stream.Count)
                {
                    break;
                }
                var ntok = NextToken();

                if (ntok == null)
                {
                    break;
                }

                if (ntok.Type == TokenType.Comma)
                {
                    continue;
                }

                if (ntok.Type == TokenType.EndLine)
                {
                    break;
                }

                if (ntok.Type == TokenType.ID)
                {
                    var nvar = new StructVar(ntok.Actual);
                    cv.AddVar(nvar);

                    var pk = PeekNextToken();

                    if (pk.Type == TokenType.Assign)
                    {
                        ConsumeNextToken();

                        var parseInit = new ParseExpr(Stream, TokenIndex);

                        cv.Init.Add(parseInit.Parse() as StructExpr);
                    }
                }
            }

            //return base.Parse();
            return cv;
        }
    }
}
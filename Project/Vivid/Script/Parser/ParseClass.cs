﻿//#define DEBUGPARSECLASS

using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseClass : VividParser
    {
        public ParseClass(TokenStream stream, int index) : base(stream, index)
        {
        }

        public override CodeStruct Parse()
        {
            var name = NextToken().Actual;

            StructClass cls = new StructClass(name);

#if (DEBUGPARSECLASS)

            Console.WriteLine("Class:" + name);

#endif
            NextLine();

            while (true)
            {
                var guess = GuessNext(TokenIndex);

                switch (guess)
                {
                    case GuessType.End:
                        return cls;
                        break;

                    case GuessType.Array:

                        var tk = NextToken();

                        if (tk.Type == TokenType.Array)
                        {
                        }
                        else
                        {
                            BackToken();
                        }

                        var pArr = new ParseArray(Stream, TokenIndex);

                        var sa = pArr.Parse() as StructArray;

                        var sv = new StructVar(sa.Name);
                        sv.VarType = TokenType.Array;

                        //sv.Name = sa.Name;
                        TokenIndex = pArr.TokenIndex;

                        cls.AddInstanceVar(sv);

                        break;

                    case GuessType.DefineFunc:

                        var parseFunc = new ParseFunc(Stream, TokenIndex);

                        var funcstr = parseFunc.Parse();

                        cls.AddInstanceFunc(funcstr as StructFunc);

                        TokenIndex = parseFunc.TokenIndex;

                        break;

                    case GuessType.DefineVar:

                        int vv = 0;

                        //var typeTok = NextToken();

                        var parseVars = new ParseVars(Stream, TokenIndex);
                        var vars = parseVars.Parse() as StructDefineVar;
                        TokenIndex = parseVars.TokenIndex - 1;

                        foreach (var v in vars.Vars)
                        {
                            cls.AddInstanceVar(v);
                        }

                        break;
                }

                var tok = NextToken();

                if (tok == null) break;
            }

            return cls;

            //return base.Parse();
        }

        public override GuessType GuessNext(int index)
        {
            while (true)
            {
                var tok = Stream[index];

                switch (tok.Type)
                {
                    case TokenType.Array:
                        return GuessType.Array;
                        break;

                    case TokenType.Func:
                        return GuessType.DefineFunc;
                        break;

                    case TokenType.End:
                        return GuessType.End;
                        break;

                    case TokenType.Var:

                        index++;
                        while (true)
                        {
                            var ntok = Stream[index];

                            if (ntok.Type == TokenType.EndLine)
                            {
                                return GuessType.DefineVar;
                            }
                            if (ntok.Type == TokenType.LeftPara)
                            {
                                return GuessType.DefineFunc;
                            }

                            index++;
                            if (index >= Stream.Count)
                            {
                                break;
                            }
                        }

                        break;
                }

                index++;
                if (index >= Stream.Count)
                {
                    return GuessType.Unknown;
                }
            }

            //return base.GuessNext();
        }
    }
}
﻿using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseFlatCall : VividParser
    {
        public ParseFlatCall(TokenStream stream, int index) : base(stream, index)
        {
        }

        public override CodeStruct Parse()
        {
            var ret = new StructFlatCall();

            var tok = NextToken();

            if (tok.Type == TokenType.EndLine)
            {
                tok = NextToken();
            }

            ret.FuncName = tok.Actual;

            var parsePars = new ParseCallParams(Stream, TokenIndex);

            var cp = parsePars.Parse() as StructCallParams;

            ret.Params = cp;

            TokenIndex = parsePars.TokenIndex;

            return ret;
            while (true)
            {
                var tt = NextToken();
                if (VividScript.Help.Helper.IsOp(tt)) return ret;
                if (tt.Type == TokenType.EndLine)
                {
                    return ret;
                }
            }

            return ret;
            //return base.Parse();
        }
    }
}
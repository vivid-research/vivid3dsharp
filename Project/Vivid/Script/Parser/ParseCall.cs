﻿
using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseCall : VividParser
    {

        public ParseCall(TokenStream stream, int index) : base(stream, index)
        {

        }

        public override CodeStruct Parse()
        {
            var ret = new StructCall();

            var tok = NextToken();

            if (tok.Type == TokenType.EndLine)
            {
                tok = NextToken();
            }

            string[] acc = new string[255];
            bool ac = false;

            if (PeekNextToken().Type == TokenType.Access)
            {
                ac = true;
                int ai = 1;
                acc[0] = tok.Actual;
                while (true)
                {
                    var nt = NextToken();
                    if (nt.Type == TokenType.Access)
                    {
                        continue;
                    }
                    if (nt.Type == TokenType.ID)
                    {
                        acc[ai] = nt.Actual;
                        ai++;
                        continue;
                    }
                    break;
                }

                int vv = 0;
            }

            BackToken();

            ret.FuncNames = acc;
            var parsePars = new ParseCallParams(Stream, TokenIndex);

            var cp = parsePars.Parse() as StructCallParams;

            ret.Params = cp;

            TokenIndex = parsePars.TokenIndex;

            return ret;

            // return base.Parse();

        }

    }
}

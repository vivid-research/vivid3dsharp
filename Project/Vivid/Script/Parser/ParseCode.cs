﻿using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseCode : VividParser
    {
        public ParseCode(TokenStream stream, int index) : base(stream, index)
        {
        }

        public override CodeStruct Parse()
        {
            var ret = new StructCode();

            while (true)
            {
                //var tok = NextToken();
                GuessType Guess()
                {
                    int pindex = TokenIndex;
                    while (true)
                    {
                        var t = NextToken();
                        if (t.Type == TokenType.Return)
                        {
                            TokenIndex = pindex;
                            return GuessType.Return;
                        }
                        if (t.Type == TokenType.EndLine)
                        {
                            TokenIndex = pindex;
                            return GuessType.EndLine;
                        }
                        if (t.Type == TokenType.For)
                        {
                            TokenIndex = pindex;
                            return GuessType.For;
                        }
                        if (t.Type == TokenType.Array)
                        {
                            TokenIndex = pindex;
                            return GuessType.Array;
                        }
                        if (t.Type == TokenType.While)
                        {
                            TokenIndex = pindex;
                            return GuessType.While;
                        }
                        if (t.Type == TokenType.Else)
                        {
                            TokenIndex = pindex;
                            return GuessType.End;
                        }
                        if (t.Type == TokenType.If)
                        {
                            TokenIndex = pindex;
                            return GuessType.If;
                        }
                        if (t.Type == TokenType.Var)
                        {
                            TokenIndex = pindex;
                            return GuessType.DefineVar;
                        }
                        if (t.Type == TokenType.LeftPara)
                        {
                            TokenIndex = pindex;
                            return GuessType.FlatCall;
                        }
                        if (t.Type == TokenType.End)
                        {
                            TokenIndex = pindex;
                            return GuessType.End;
                        }
                        if (t.Type == TokenType.Assign)
                        {
                            TokenIndex = pindex;
                            return GuessType.Assign;
                        }

                        //switch (t.Type)
                        //{
                        //   case TokenType.Int:
                        //    case
                        //}
                    }

                    TokenIndex = pindex;
                    return GuessType.Unknown;
                }

                var gt = Guess();
                if (gt == GuessType.EndLine)
                {
                    ConsumeNextToken();
                    continue;
                }
                switch (gt)
                {
                    case GuessType.Return:

                        var parseRet = new ParseReturn(Stream, TokenIndex);

                        var rets = parseRet.Parse() as StructReturn;

                        TokenIndex = parseRet.TokenIndex;

                        ret.Code.Add(rets);

                        break;

                    case GuessType.Array:

                        var tk = NextToken();

                        if (tk.Type == TokenType.Array)
                        {
                        }
                        else
                        {
                            BackToken();
                        }

                        var pArr = new ParseArray(Stream, TokenIndex);

                        var sa = pArr.Parse() as StructArray;

                        var sv = new StructVar(sa.Name);
                        sv.VarType = TokenType.Array;

                        sa.Var = sv;
                        TokenIndex = pArr.TokenIndex;

                        //sv.Name = sa.Name;
                        ret.AddCode(sa);

                        break;

                    case GuessType.For:

                        var parseFor = new ParseFor(Stream, TokenIndex);

                        var cFor = parseFor.Parse();

                        TokenIndex = parseFor.TokenIndex;

                        var ct = PeekNextToken();

                        if (ct.Type == TokenType.End)
                        {
                            ConsumeNextToken();
                            ct = PeekNextToken();
                        }

                        ret.AddCode(cFor);

                        break;

                    case GuessType.While:

                        var parseWhile = new ParseWhile(Stream, TokenIndex);

                        var cWhile = parseWhile.Parse() as StructWhile;

                        TokenIndex = parseWhile.TokenIndex;

                        ret.AddCode(cWhile);

                        break;

                    case GuessType.If:

                        var parseIf = new ParseIf(Stream, TokenIndex);

                        var sif = parseIf.Parse();

                        TokenIndex = parseIf.TokenIndex;

                        if (PeekNextToken().Type == TokenType.End)
                        {
                            ConsumeNextToken();
                        }

                        ret.AddCode(sif);

                        break;

                    case GuessType.FlatCall:

                        var t4 = PeekNextToken(1);

                        //t4 = PeekNextToken();


                        if (t4.Type == TokenType.Access)
                        {

                            var parseCall = new ParseCall(Stream, TokenIndex);

                            var sCall = parseCall.Parse();

                            ret.AddCode(sCall);

                            TokenIndex = parseCall.TokenIndex;

                        }
                        else
                        {


                            var parseFlat = new ParseFlatCall(Stream, TokenIndex);

                            var flatCall = parseFlat.Parse();

                            ret.AddCode(flatCall);
                            TokenIndex = parseFlat.TokenIndex;

                        }


                        break;

                    case GuessType.DefineVar:

                        var parseVar = new ParseVars(Stream, TokenIndex);

                        var defVar = parseVar.Parse() as StructDefineVar;

                        ret.AddCode(defVar);

                        TokenIndex = parseVar.TokenIndex;

                        break;

                    case GuessType.End:

                        return ret;

                        break;

                    case GuessType.Assign:

                        var tt = PeekNextToken();

                        var parseAssign = new ParseAssign(Stream, TokenIndex);

                        var assign = parseAssign.Parse() as StructAssign;

                        ret.AddCode(assign);

                        TokenIndex = parseAssign.TokenIndex;

                        break;
                }

                //if(tok.Type == TokenType.End)
                //{
                //    return ret;
            }

            //return base.Parse();

            return ret;
        }
    }
}
﻿using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseReturn : VividParser
    {
        public ParseReturn(TokenStream stream, int index) : base(stream, index)
        {
        }

        public override CodeStruct Parse()
        {
            var ret = new StructReturn();
            var t = NextToken();
            if (t.Type != TokenType.Return)
            {
                BackToken();
            }

            t = NextToken();

            if (t.Type == TokenType.EndLine)
            {
                return ret;
            }

            BackToken();

            var parseExp = new ParseExpr(Stream, TokenIndex);

            ret.Expr = parseExp.Parse() as StructExpr;
            TokenIndex = parseExp.TokenIndex;

            return ret;

            //return base.Parse();
        }
    }
}
﻿#define DEBUGPARSER

using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class VividParser
    {
        public TokenStream Stream
        {
            get;
            set;
        }

        public int TokenIndex
        {
            get;
            set;
        }

        public VividParser(TokenStream stream, int index = 0)
        {
            Stream = stream;
            TokenIndex = index;
        }

        public virtual CodeStruct Parse()
        {
            return null;
        }

        public Token PrevToken()
        {
            return Stream[TokenIndex - 1];
        }

        public Token NextToken()
        {
            if (TokenIndex >= Stream.Count - 1) return null;
            var tok = Stream[TokenIndex];
            TokenIndex++;
            return tok;
        }

        public Token PeekNextToken(int add = 0)
        {
            return Stream[TokenIndex + add];
        }

        public Token GetToken(int id)
        {
            return Stream[id];
        }

        public void ConsumeNextToken()
        {
            TokenIndex++;
        }

        public void BackToken()
        {
            TokenIndex--;
        }

        public virtual GuessType GuessNext(int index)
        {
            return GuessType.Unknown;
        }

        public void NextLine()
        {
            while (true)
            {
                if (Stream[TokenIndex].Type == TokenType.EndLine)
                {
                    TokenIndex += 1;
                    return;
                }
            }
        }
    }

    public enum GuessType
    {
        DefineVar, DefineFunc, DefineMeth, Assign, Call, Unknown, End, FlatCall, If, While, For, Array, EndLine, Return
    }
}
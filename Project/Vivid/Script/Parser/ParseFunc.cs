﻿using VividScript.Reporting;
using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseFunc : VividParser
    {
        public ParseFunc(TokenStream stream, int index) : base(stream, index)
        {
            if (TokenIndex < 0) TokenIndex = 0;

        }

        public override CodeStruct Parse()
        {
            var ret = new StructFunc();

            var rtype = NextToken();

            //if (!VividScript.Help.Helper.IsVarType(rtype))
            {
                //    ErrorReport.CompileError("Return type of function is not recognized.",Stream,TokenIndex-1);
            }

            ConsumeNextToken();

            if (PeekNextToken().Type == TokenType.LeftPara)
            {
                BackToken();
            }

            var name = NextToken();

            ret.Name = name.Actual;

            bool GuessPars(int index)
            {
                bool lp = false;
                while (true)
                {
                    var tok = Stream[index];

                    if (tok.Type == TokenType.LeftPara)
                    {
                        lp = true;
                    }
                    if (tok.Type == TokenType.RightPara)
                    {
                        if (lp == false)
                        {
                            ErrorReport.CompileError("Expecting \"(\"", Stream, index);
                        }
                        return false;
                    }
                    if (tok.Type == TokenType.ID)
                    {
                        return true;
                    }

                    index++;
                }
            }

            var g = GuessPars(TokenIndex);

            if (g)
            {
                var pp = new ParseParams(Stream, TokenIndex);

                var sp = pp.Parse() as StructParams;

                ret.Params = sp;

                TokenIndex = pp.TokenIndex;

                //int aa = 5;
            }

            var ntok = NextToken();

            if (ntok.Type == TokenType.LeftPara)
            {
                ntok = NextToken();
            }

            if (ntok.Type == TokenType.RightPara)
            {
                ntok = NextToken();
            }

            if (ntok.Type == TokenType.EndLine)
            {
                //ntok = NextToken();
            }

            //var guess = GuessNext();

            var parseBody = new ParseCode(Stream, TokenIndex);

            var code = parseBody.Parse();

            ret.Code = code as StructCode;

            TokenIndex = parseBody.TokenIndex;

            //return new StructFunc();

            return ret;
            //return base.Parse();
        }
    }
}
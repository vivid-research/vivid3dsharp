﻿
using VividScript.Reporting;
using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseModule : VividParser
    {
        public ParseModule(TokenStream stream, int index = 0) : base(stream, index)
        {
        }

        public override CodeStruct Parse()
        {
            // Console.WriteLine("Parsing Program:");

            StructModule module = new StructModule();

            int index = TokenIndex;

            while (true)
            {
                var tok = NextToken();

                if (tok == null) break;

                switch (tok.Type)
                {
                    case TokenType.Var:

                        BackToken();

                        var parseVars = new ParseVars(Stream, TokenIndex);



                        var sdv = parseVars.Parse() as StructDefineVar;

                        TokenIndex = parseVars.TokenIndex;

                        module.AddVars(sdv);

                        int aa = 0;
                        break;
                    case TokenType.Module:

                        if (PeekNextToken().Type != TokenType.ID)
                        {
                            ErrorReport.CompileError("Expecting module name.", Stream, TokenIndex);
                        }
                        string name = NextToken().Actual;

                        module.Name = name;

                        break;

                    case TokenType.Class:

                        var classParser = new ParseClass(Stream, TokenIndex);

                        var cls = classParser.Parse();

                        TokenIndex = classParser.TokenIndex;

                        module.AddClass(cls as StructClass);

                        break;

                    case TokenType.Func:

                        var funcParser = new ParseFunc(Stream, TokenIndex - 2);

                        var func = funcParser.Parse() as StructFunc;

                        TokenIndex = funcParser.TokenIndex;

                        module.AddFunc(func);

                        break;
                }

                if (tok == null)
                {
                    break;
                }
            }

            return module;

            //return base.Parse();
        }
    }
}
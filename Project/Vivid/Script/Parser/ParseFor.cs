﻿using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Parser
{
    public class ParseFor : VividParser
    {
        public ParseFor(TokenStream stream, int index) : base(stream, index)
        {
        }

        public override CodeStruct Parse()
        {
            var forS = new StructFor();

            var t = NextToken();

            if (t.Type == TokenType.For)
            {
                t = NextToken();
            }

            var assP = new ParseAssign(Stream, TokenIndex);

            forS.Initial = assP.Parse() as StructAssign;

            TokenIndex = assP.TokenIndex;

            var expP = new ParseExpr(Stream, TokenIndex);

            forS.Expr = expP.Parse() as StructExpr;

            TokenIndex = expP.TokenIndex;

            assP = new ParseAssign(Stream, TokenIndex);

            forS.Inc = assP.Parse() as StructAssign;

            TokenIndex = assP.TokenIndex;

            t = NextToken();

            if (t.Type != TokenType.EndLine)
            {
                BackToken();
            }

            var parseC = new ParseCode(Stream, TokenIndex);

            forS.Code = parseC.Parse() as StructCode;

            TokenIndex = parseC.TokenIndex;

            //t = NextToken();

            return forS;
        }
    }
}
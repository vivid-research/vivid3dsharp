﻿using VividScript.Parser;
using VividScript.Struct;
using VividScript.Tokenizer;

namespace VividScript.Compile
{
    public class Compiler
    {
        private TokenStream Stream
        {
            get;
            set;
        }

        public Compiler(TokenStream stream)
        {
            Stream = stream;
        }

        public Compiler(string path)
        {
            var toker = new Tokenizer.Tokenizer();

            toker.FromFile(path);

            Stream = toker.Stream;
        }

        public CompileResult Compile()
        {
            var result = CompileResult.OK;

            var parser = new ParseModule(Stream, 0);

            result.Module = (StructModule)parser.Parse();

            return result;
        }
    }

    public class CompileResult
    {
        public CompileResultCode Code
        {
            get;
            set;
        }

        public StructModule Module
        {
            get;
            set;
        }

        public CompileResult(CompileResultCode code)
        {
            Code = code;
        }

        public static CompileResult OK = new CompileResult(CompileResultCode.Compiled);

#pragma warning disable CS0114 // 'CompileResult.ToString()' hides inherited member 'object.ToString()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        public string ToString()
#pragma warning restore CS0114 // 'CompileResult.ToString()' hides inherited member 'object.ToString()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        {
            return "Result:" + Code;
        }
    }

    public enum CompileResultCode
    {
        Compiled, Failed, Exception
    }
}
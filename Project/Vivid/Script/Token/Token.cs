﻿namespace VividScript.Tokenizer
{
    public enum TokenType
    {
        // - Types
        String, Int, Long, Single, Double, Short, Byte, Bool, Void, Number, Module, Var, Array,

        // - Flow
        If, While, Wend, For, Next, Do, Loop, Goto, Label, Begin, End, EndLine, Comma, Else,

        // - Headers
        Func, Method, Class, State, Program,

        // - Spacing
        LeftPara, RightPara, LeftClosure, RightClosure,

        // - Type Type
        Const, Static, Global, Local,

        // - Other
        ID, Assign, Sep,

        // - Operators
        Plus, Minus, Times, Divide, Equals, Ref, GreaterThan, LessThan, LeftArray, RightArray,

        // -
        New, Access,
        Return
    }

    public class Token
    {
        public TokenType Type
        {
            get;
            set;
        }

        public string Actual
        {
            get;
            set;
        }

        public int LineNumber
        {
            get;
            set;
        }

        public int CharNumber
        {
            get;
            set;
        }

        public int CharLength
        {
            get;
            set;
        }

        public Token(TokenType type, string actual)
        {
            Type = type;
            Actual = actual;
        }

#pragma warning disable CS0114 // 'Token.ToString()' hides inherited member 'object.ToString()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        public string ToString()
#pragma warning restore CS0114 // 'Token.ToString()' hides inherited member 'object.ToString()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        {
            return "Token:" + Type.ToString() + " Actual:" + Actual;
        }
    }
}
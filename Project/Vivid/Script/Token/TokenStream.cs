﻿using System.Collections.Generic;

namespace VividScript.Tokenizer

{
    public class TokenStream
    {
        public List<Token> Tokens
        {
            get;
            set;
        }

        public int Count
        {
            get
            {
                return Tokens.Count;
            }
        }

        public int CurrentIndex
        {
            get;
            set;
        }

        public TokenStream()
        {
            Tokens = new List<Token>();
            CurrentIndex = 0;
        }

        public void AddToken(Token token)
        {
            Tokens.Add(token);
        }

        public Token this[int id]
        {
            get
            {
                return Tokens[id];
            }
        }
    }
}
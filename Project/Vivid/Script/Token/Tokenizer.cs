﻿//#define DEBUGTOKER

using System.IO;

namespace VividScript.Tokenizer
{
    public class TokenInput
    {
        public string[] Input
        {
            get;
            set;
        }

        public TokenInput()
        {
        }

        public void FromFile(string file)
        {
            Input = File.ReadAllLines(file);
        }

        public void FromString(string str)
        {
            Input = new string[1];
            Input[0] = str;

        }
    }

    public class Tokenizer
    {
        public TokenStream Stream
        {
            get;
            set;
        }

        public Tokenizer()
        {
        }

        private int CurrentLine = 0;
        private int CurrentChr = 0;

        public void FromString(string str)
        {

            var input = new TokenInput();
            input.FromString(str);

            CurrentLine = 0;
            Stream = new TokenStream();

            ParseLine(input.Input[0]);


        }

        public void FromFile(string path)
        {
            var input = new TokenInput();
            input.FromFile(path);

            CurrentLine = 0;

            Stream = new TokenStream();

            foreach (var line in input.Input)
            {
#if (DEBUGTOKER)
                Console.WriteLine("L:" + line);
#endif
                ParseLine(line);
                CurrentLine++;
            }

#if (DEBUGTOKER)
            int tn = 0;
            Console.WriteLine("Tokens:" + Stream.Count);
            foreach(var tok in Stream.Tokens)
            {
                Console.WriteLine(tn.ToString()+":"+tok.ToString());
                tn++;
            }
#endif
        }

        public bool IsNum(string chr)
        {
            bool skip1 = false;
            bool first = true;
            if (chr[0] == "-"[0])
            {
                skip1 = true;
            }

            for (int i = 0; i < chr.Length; i++)
            {
                if (i == 0 && skip1) continue;
                var c = chr[i];

                if ((c >= "0"[0] && c <= "9"[0]) || c == "."[0])
                {
                }
                else
                {
#if (DEBUGTOKER)
                    Console.WriteLine("Not Number:" + chr);
#endif
                    return false;
                }
            }
#if (DEBUGTOKER)
            Console.WriteLine("Number:" + chr);
#endif
            return true;
        }

        public bool IsSpace(string chr)
        {
            if (chr == " ")
            {
                return true;
            }
            if (chr == " ")
            {
                return true;
            }
            return false;
        }

        public bool IsSingle(string chr)
        {
            return false;
        }

        public bool IsDouble(string chr)
        {
            return false;
        }

        public bool IsText(string chr)
        {
            for (int i = 0; i < chr.Length; i++)
            {
                var c = chr[i];

                if ((c >= "a"[0] && c <= "z"[0]) || (c >= "A"[0] && c <= "Z"[0]))
                {
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        private string OpList = "()+-/*=.,<>[]";

        public bool IsOp(string chr)
        {
            if (chr.Length > 2)
            {
                return false;
            }

            for (int i = 0; i < chr.Length; i++)
            {
                var c = chr[i];
                if (OpList.Contains(c))
                {
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public void ParseLine(string line)
        {
            //line = OptimizeLine(line);

            string currentToke = "";

            CurrentChr = 0;

            int tokC = Stream.Count;

            for (int c = 0; c < line.Length; c++)
            {
                string chr = line[c].ToString();
                string nchr = "";
                if (c < line.Length - 1)
                {
                    nchr = line[c + 1].ToString();
                }
                //Console.WriteLine("C:" + chr);

                if (chr == "\"")
                {
                    if (currentToke.Length > 0)
                    {
                        GenerateToken(currentToke);
                        currentToke = "";
                    }
                    var str = "";
                    int c2 = c + 1;
                    while (true)
                    {
                        str += line[c2].ToString();
                        c2++;
                        if (line[c2] == "\""[0])
                        {
                            GenerateStringToken(str);
                            c = c2;
                            break;
                            //break;
                        }
                    }
                    continue;
                }

                void CheckCur()
                {
                    if (currentToke.Length > 0)
                    {
                        GenerateToken(currentToke);
                        currentToke = "";
                    }
                }

                string GetNum()
                {
                    string num = "";
                    for (int nc = c; nc < line.Length; nc++)
                    {
                        string cc = line[nc].ToString();

                        if (!IsNum(cc))
                        {
                            c--;
                            return num;
                        }
                        num += cc;
                        c++;
                    }
                    c--;
                    return num;
                }

                if (IsSpace(chr))
                {
                    currentToke = GenerateToken(currentToke);
                    continue;
                }
                else
                {
                    if (IsNum(chr))
                    {
                        //  c--;
                        CheckCur();
                        currentToke = GetNum();
                        GenerateToken(currentToke);
                        currentToke = "";
                        continue;
                    }

                    if (IsOp(chr))
                    {
                        CheckCur();

                        if (chr == "=" && nchr == "=")
                        {
                            currentToke = "==";
                            c += 1;
                            chr = "";
                            //c += 3;
                        }
                        currentToke = GenerateToken(currentToke);
                        GenerateToken(chr);
                        continue;
                    }

#if (DEBUGTOKER)
                    Console.WriteLine("C:" + chr);
#endif

                    currentToke += chr.ToString();
                }

                CurrentChr++;
            }

            if (currentToke.Length > 0)
            {
                if (IsSpace(currentToke))
                {
                }
                else
                {
                    GenerateToken(currentToke);
                }
            }

            //OptimizeStream();

            if (Stream.Count > tokC)
            {
                var toke = new Token(TokenType.EndLine, "");
                Stream.AddToken(toke);
            }
        }

        private string GenerateStringToken(string str)
        {
            var tok = new Token(TokenType.String, str);
            tok.Actual = str;
            tok.LineNumber = CurrentLine;
            tok.CharNumber = CurrentChr - str.Length;
            tok.CharLength = str.Length;
            //return str;

            //if (prev != null)
            //{
            //   tok.CharNumber = prev.
            //}

            Stream.AddToken(tok);
            return str;
        }

        private string GenerateToken(string currentToke)
        {
            if (currentToke.Length == 0) return "";

#if (DEBUGTOKER)
            Console.WriteLine("Toke:" + currentToke + "|");
#endif
            //Token tok = new Token();

            Token tok = new Token(TokenType.Begin, currentToke);

            switch (currentToke)
            {
                case ".":
                    tok.Type = TokenType.Access;
                    break;
                case "new":
                    tok.Type = TokenType.New;
                    break;
                case "ret":
                case "return":
                    tok.Type = TokenType.Return;
                    break;

                case "[":
                    tok.Type = TokenType.LeftArray;
                    break;

                case "]":
                    tok.Type = TokenType.RightArray;
                    break;

                case "array":
                    tok.Type = TokenType.Array;
                    break;

                case "else":

                    tok.Type = TokenType.Else;
                    break;

                case "var":
                    tok.Type = TokenType.Var;
                    break;

                case "<":
                    tok.Type = TokenType.LessThan;
                    break;

                case ">":
                    tok.Type = TokenType.GreaterThan;
                    break;

                case ",":
                    tok.Type = TokenType.Comma;
                    break;

                case "void":
                    tok.Type = TokenType.Void;
                    break;

                case "int":
                    tok.Type = TokenType.Int;
                    break;

                case "single":
                case "float":
                    tok.Type = TokenType.Single;
                    break;

                case "double":
                    tok.Type = TokenType.Double;
                    break;

                case "short":
                    tok.Type = TokenType.Short;
                    break;

                case "byte":
                    tok.Type = TokenType.Byte;
                    break;

                case "string":
                    tok.Type = TokenType.String;
                    break;

                case "bool":
                    tok.Type = TokenType.Bool;
                    break;

                case "long":
                    tok.Type = TokenType.Long;
                    break;

                case "==":
                    tok.Type = TokenType.Equals;
                    break;

                case "=":
                    tok.Type = TokenType.Assign;
                    break;

                case "+":
                    tok.Type = TokenType.Plus;
                    break;

                case "*":
                    tok.Type = TokenType.Times;
                    break;

                case "-":
                    tok.Type = TokenType.Minus;
                    break;

                case "/":
                    tok.Type = TokenType.Divide;
                    break;

                case "class":
                    tok.Type = TokenType.Class;
                    break;

                case "(":
                    tok.Type = TokenType.LeftPara;
                    break;

                case ")":
                    tok.Type = TokenType.RightPara;
                    break;

                case "end":
                    tok.Type = TokenType.End;
                    break;

                case "func":
                case "function":
                    tok.Type = TokenType.Func;
                    break;

                case "meth":
                case "method":
                    tok.Type = TokenType.Method;
                    break;

                case "for":
                    tok.Type = TokenType.For;
                    break;

                case "next":
                    tok.Type = TokenType.Next;
                    break;

                case "while":
                    tok.Type = TokenType.While;
                    break;

                case "wend":
                    tok.Type = TokenType.Wend;
                    break;

                case "if":
                    tok.Type = TokenType.If;
                    break;


                case "module":
                    tok.Type = TokenType.Module;
                    break;

                default:

                    if (IsText(currentToke))
                    {
                        tok.Type = TokenType.ID;
                        goto Done1;
                    }

                    if (IsNum(currentToke))
                    {
                        if (IsSingle(currentToke))
                        {
                            tok.Type = TokenType.Number;
                            goto Done1;
                        }
                        if (IsDouble(currentToke))
                        {
                            tok.Type = TokenType.Number;
                            goto Done1;
                        }
                        tok.Type = TokenType.Number;
                        goto Done1;
                    }

                    break;
            }

        Done1:
            tok.Actual = currentToke;
            tok.LineNumber = CurrentLine;
            tok.CharNumber = CurrentChr - currentToke.Length;
            tok.CharLength = currentToke.Length;

            //if (prev != null)
            //{
            //   tok.CharNumber = prev.
            //}

            Stream.AddToken(tok);
            currentToke = "";
            prev = tok;
            return currentToke;
        }

        private Token prev = null;

        public string OptimizeLine(string input)
        {
            return "";
        }
    }
}
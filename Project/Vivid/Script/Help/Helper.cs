﻿using VividScript.Tokenizer;

namespace VividScript.Help
{
    public static class Helper
    {
        public static bool IsOp(Token type)
        {
            switch (type.Type)
            {
                case TokenType.Plus:
                case TokenType.Minus:
                case TokenType.Times:
                case TokenType.Divide:
                case TokenType.Equals:
                case TokenType.LessThan:
                case TokenType.GreaterThan:
                    return true;
                    break;
            }
            return false;
        }

        public static bool IsVarType(Token type)
        {
            switch (type.Type)
            {
                case TokenType.Void:
                case TokenType.Int:
                case TokenType.Short:
                case TokenType.Byte:
                case TokenType.Single:
                case TokenType.Double:
                case TokenType.Long:
                case TokenType.Bool:
                    if (IsNum(type.Actual))
                    {
                        return false;
                    }
                    return true;
                    break;
            }
            return false;
        }

        public static bool IsNum(string chr)
        {
            bool first = true;
            for (int i = 0; i < chr.Length; i++)
            {
                var c = chr[i];

                if ((c >= "0"[0] && c <= "9"[0]) || c == "."[0])
                {
                }
                else
                {
#if (DEBUGTOKER)
                    Console.WriteLine("Not Number:" + chr);
#endif
                    return false;
                }
            }
#if (DEBUGTOKER)
            Console.WriteLine("Number:" + chr);
#endif
            return true;
        }
    }
}
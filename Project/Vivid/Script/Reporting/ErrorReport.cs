﻿using System;

using VividScript.Tokenizer;

namespace VividScript.Reporting
{
    public class ErrorReport
    {
        public static void RuntimeError(string err)
        {
            Console.WriteLine("Runtime Error:" + err);
        }

        public static void CompileError(string err, TokenStream stream, int index)
        {
            Console.WriteLine("Compile Error:" + err);
            Console.WriteLine(stream[index].ToString());
        }
    }
}
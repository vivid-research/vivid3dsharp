﻿using System.Collections.Generic;

namespace VividScript.Struct
{
    public class StructParams : CodeStruct
    {
        public List<StructVar> Params
        {
            get;
            set;
        }

        public StructParams()
        {
            Params = new List<StructVar>();
        }

        public void AddParam(StructVar v)
        {
            Params.Add(v);
        }
    }
}
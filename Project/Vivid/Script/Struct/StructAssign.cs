﻿using VividScript.Run;

namespace VividScript.Struct
{
    public class StructAssign : CodeStruct
    {
        public StructVar Target
        {
            get;
            set;
        }

        public StructExpr Expr
        {
            get;
            set;
        }

        public StructExpr ArrayIndex
        {
            get;
            set;
        }

        public override dynamic Exec()
        {
            //Target.SetValue(Expr.Exec());

            if (ArrayIndex != null)
            {
                int ai = (int)ArrayIndex.Exec();
                RunStack.This.SetArrayValue(Target.Name, ai, Expr.Exec());
            }
            else
            {
                if (Target.Names != null && Target.Names.Count > 0)
                {
                    RunStack.This.SetValue(Target.Names, Expr.Exec());
                }
                else
                {
                    RunStack.This.SetValue(Target.Name, Expr.Exec());
                }
            }
            return base.Exec();
        }
    }
}
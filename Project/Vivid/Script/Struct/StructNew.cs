﻿
using VividScript.Run;

namespace VividScript.Struct
{
    public class StructNew : CodeStruct
    {

        public string ClsName
        {
            get;
            set;
        }

        public StructCallParams Params
        {
            get;
            set;
        }

        public override dynamic Exec()
        {

            dynamic[] pars = new dynamic[Params.Pars.Count];

            for (int i = 0; i < pars.Length; i++)
            {
                pars[i] = Params.Pars[i].Exec();
            }


            return RunStack.This.NewClass(ClsName, pars);


            return base.Exec();
        }

    }
}

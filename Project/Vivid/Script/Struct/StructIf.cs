﻿namespace VividScript.Struct
{
    public class StructIf : CodeStruct
    {
        public StructExpr Exp
        {
            get;

            set;
        }

        public StructCode TrueCode
        {
            get;
            set;
        }

        public StructIf ElseIf
        {
            get;
            set;
        }

        public StructCode ElseCode
        {
            get;
            set;
        }

        public StructIf()
        {
        }

        public override dynamic Exec()
        {
            if (Exp.Exec() == 1)
            {
                TrueCode.Exec();
            }
            else
            {
                if (ElseIf != null)
                {
                    ElseIf.Exec();
                    return null;
                }

                if (ElseCode != null)
                {
                    ElseCode.Exec();
                }
                //Console.WriteLine("false if");
            }

            return null;
            //return base.Exec();
        }
    }
}
﻿namespace VividScript.Struct
{
    public class StructReturn : StructCode
    {
        public StructExpr Expr
        {
            get;
            set;
        }

        public override dynamic Exec()
        {
            if (Expr != null)
            {
                return Expr.Exec();
            }
            return null;
        }
    }
}
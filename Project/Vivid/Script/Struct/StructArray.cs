﻿using VividScript.Run;

namespace VividScript.Struct
{
    public class StructArray : CodeStruct
    {
        public StructVar Var
        {
            get;
            set;
        }

        public StructVar[] Vars
        {
            get;
            set;
        }

        public override dynamic Exec()
        {
            Var.Vars = Vars;
            RunStack.This.Add(Var);

            return base.Exec();
        }
    }
}
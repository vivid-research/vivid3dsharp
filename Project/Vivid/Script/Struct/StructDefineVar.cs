﻿using System.Collections.Generic;

using VividScript.Run;
using VividScript.Tokenizer;

namespace VividScript.Struct
{
    public class StructDefineVar : CodeStruct
    {
        public TokenType VarType
        {
            get;
            set;
        }

        public List<StructVar> Vars
        {
            get;
            set;
        }

        public List<StructExpr> Init
        {
            get;
            set;
        }

        public Dictionary<string, StructVar> VarsTable
        {
            get;
            set;
        }

        public override dynamic Exec()
        {
            int ii = 0;
            foreach (var v in Vars)
            {
                if (Init.Count > ii)
                {
                    v.Value = Init[ii].Exec();
                }

                RunStack.This.Add(v);
                ii++;
            }

            return null;
            //return base.Exec();
        }

        public StructDefineVar()
        {
            Vars = new List<StructVar>();
            VarsTable = new Dictionary<string, StructVar>();
            VarType = TokenType.Begin;
            Init = new List<StructExpr>();
        }

        public void AddVar(StructVar v)
        {
            Vars.Add(v);
            VarsTable.Add(v.Name, v);
        }
    }
}
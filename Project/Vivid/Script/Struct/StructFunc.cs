﻿using System;

using VividScript.Tokenizer;

namespace VividScript.Struct
{
    public class StructFunc : CodeStruct
    {
        public TokenType ReturnType
        {
            get;
            set;
        }

        public StructParams Params
        {
            get;
            set;
        }

        public StructCode Code
        {
            get;
            set;
        }

        public StructFunc()
        {
            ReturnType = TokenType.Void;
            Params = null;
            Code = null;
        }

        public override dynamic Exec()
        {
            return Code.Exec();

            //return base.Exec();
        }

        public override void Debug()
        {
            Console.WriteLine("Func:" + Name + " Return:" + ReturnType);
            if (Params != null)
            {
                Console.WriteLine("Params:True");
                foreach (var p in Params.Params)
                {
                    p.Debug();
                }
            }
            else
            {
                Console.WriteLine("Params:False");
            }
            //base.Debug();
        }
    }
}
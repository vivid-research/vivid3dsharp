﻿using System.Collections.Generic;

using VividScript.Reporting;
using VividScript.Run;

namespace VividScript.Struct
{
    public class StructExpr : CodeStruct
    {
        public Expr Exp
        {
            get;
            set;
        }

        public StructExpr()
        {
            Exp = new Expr();
        }

        public override dynamic Exec()
        {
            int vv = 0;

            return Exp.Eval();

            //return base.Exec();
        }
    }

    public class Expr
    {
        public List<ExprElement> Elements
        {
            get;
            set;
        }

        public Expr()
        {
            Elements = new List<ExprElement>();
        }

        public void Add(ExprElement ele)
        {
            Elements.Add(ele);
        }

        public void AddArrayVar(string id, StructExpr index)
        {
            ExprElement ele = new ExprElement();
            ele.Var = new StructVar(id);
            ele.Value = 0.0;
            ele.Type = ElementType.Array;
            ele.Index = index;
            Elements.Add(ele);
        }

        public void AddFlatCall(StructFlatCall call)
        {
            ExprElement ele = new ExprElement();
            ele.Call = call;
            ele.Value = null;
            ele.Type = ElementType.Call;
            Elements.Add(ele);
        }
        public void AddVar(string[] acc)
        {

            ExprElement ele = new ExprElement();
            ele.Var = new StructVar(acc);
            ele.Value = 0.0;
            ele.Type = ElementType.Var;
            Elements.Add(ele);

        }
        public void AddVar(string id)
        {
            ExprElement ele = new ExprElement();
            ele.Var = new StructVar(id);
            ele.Value = 0.0;
            ele.Type = ElementType.Var;
            Elements.Add(ele);
        }

        public void Add(double num)
        {
            ExprElement ele = new ExprElement(num);
            Elements.Add(ele);
        }

        public void Add(string text)
        {
            ExprElement ele = new ExprElement(text);
            Elements.Add(ele);
        }

        public void AddNew(StructNew n)
        {
            ExprElement ele = new ExprElement(n);
            Elements.Add(ele);
        }
        public void Add(OperatorType op)
        {
            ExprElement ele = new ExprElement(op);
            Elements.Add(ele);
        }

        public void Add(Expr exp)
        {
            ExprElement ele = new ExprElement(exp);
            Elements.Add(ele);
        }

        public dynamic Eval()
        {
            if (Elements.Count == 1)
            {
                if (Elements[0].New != null)
                {

                    return Elements[0].New.Exec();
                }
                if (Elements[0].Type == ElementType.Call)
                {
                    return Elements[0].Call.Exec();
                }
                if (Elements[0].Type == ElementType.SubExp)
                {
                    return Elements[0].Exp.Eval();
                }
                if (Elements[0].Type == ElementType.Var)
                {
                    if (Elements[0].Var.Names != null)
                    {
                        return RunStack.This.GetValue(Elements[0].Var.Names);
                    }
                    else
                    {
                        return RunStack.This.GetValue(Elements[0].Var.Name);
                    }
                }
                if (Elements[0].Type == ElementType.Array)
                {
                    return RunStack.This.GetArrayValue(Elements[0].Var.Name, (int)Elements[0].Index.Exec());
                }
                return Elements[0].Value;
            }

            if (Elements[0].Value is string)
            {
                string res = "";
                foreach (var ele in Elements)
                {
                    switch (ele.OpType)
                    {
                        case OperatorType.None:
                            if (ele.Type == ElementType.SubExp)
                            {
                                var ar = ele.Exp.Eval();
                                res = res + ar;
                            }
                            else
                            {
                                res = res + ele.Value;
                            }
                            break;

                        case OperatorType.Plus:
                            break;

                        default:
                            ErrorReport.RuntimeError("Only + operator is functional with string expressions.");
                            break;
                    }
                }
                return res;
            }
            else
            {
                if (Elements.Count > 1)
                {
                    //if(Elements[0].Value is int || Elements[0].Value is double)
                    {
                        List<ExprElement> output = new List<ExprElement>();
                        Stack<ExprElement> stack = new Stack<ExprElement>();

                        int Priority(ExprElement op)
                        {
                            if (op.OpType == OperatorType.Pow2)
                                return 4;
                            if (op.OpType == OperatorType.Times || op.OpType == OperatorType.Divide)
                                return 3;
                            if (op.OpType == OperatorType.Plus || op.OpType == OperatorType.Mins)
                                return 2;
                            else
                                return 1;
                        }

                        void ToRPNInt(List<ExprElement> list)
                        {
                            output.Clear();
                            dynamic av = null;
                            for (int i = 0; i < list.Count; i++)
                            {
                                var ep = list[i];

                                switch (ep.OpType)
                                {
                                    case OperatorType.None:

                                        if (ep.Type == ElementType.Value)
                                        {
                                            output.Add(ep);
                                        }
                                        else if (ep.Type == ElementType.SubExp)
                                        {
                                            output.Add(new ExprElement(ep.Exp.Eval()));
                                        }
                                        else if (ep.Type == ElementType.Var)
                                        {
                                            if (ep.Var.Names != null)
                                            {
                                                output.Add(new ExprElement(RunStack.This.GetValue(ep.Var.Names)));
                                            }
                                            else
                                            {
                                                output.Add(new ExprElement(RunStack.This.GetValue(ep.Var.Name)));
                                            }

                                            //output.Add(new ExprElement(RunStack.This.GetValue(ep.Var.Name)));
                                        }
                                        else if (ep.Type == ElementType.Call)
                                        {
                                            output.Add(new ExprElement((double)ep.Call.Exec()));
                                        }

                                        break;

                                    default:
                                        while (stack.Count > 0 && Priority(stack.Peek()) >= Priority(ep))
                                            output.Add(stack.Pop());
                                        stack.Push(ep);

                                        break;
                                }
                            }

                            while (stack.Count > 0)
                            {
                                output.Add(stack.Pop());
                            }
                        }

                        int CalcInt()
                        {
                            stack.Clear();
                            for (int i = 0; i < output.Count; i++)
                            {
                                if (output[i].Type == ElementType.Operator)
                                {
                                    var right = stack.Pop();
                                    var left = stack.Pop();
                                    switch (output[i].OpType)
                                    {
                                        case OperatorType.Equals:
                                            var ne7 = new ExprElement((left.Value == right.Value) ? 1 : 0);
                                            stack.Push(ne7);
                                            break;

                                        case OperatorType.LessThan:
                                            var ne5 = new ExprElement((left.Value > right.Value) ? 1 : 0);
                                            stack.Push(ne5);
                                            break;

                                        case OperatorType.GreaterThan:
                                            var ne6 = new ExprElement((left.Value < right.Value) ? 1 : 0);
                                            stack.Push(ne6);
                                            break;

                                        case OperatorType.Plus:
                                            ExprElement ne1 = new ExprElement(left.Value + right.Value);
                                            stack.Push(ne1);
                                            break;

                                        case OperatorType.Times:
                                            ExprElement ne2 = new ExprElement(left.Value * right.Value);
                                            stack.Push(ne2);
                                            break;

                                        case OperatorType.Divide:
                                            ExprElement ne3 = new ExprElement(left.Value / right.Value);
                                            stack.Push(ne3);
                                            break;

                                        case OperatorType.Mins:
                                            ExprElement ne4 = new ExprElement(left.Value - right.Value);
                                            stack.Push(ne4);
                                            break;
                                    }
                                }
                                else
                                {
                                    stack.Push(output[i]);
                                }
                            }

                            return (int)stack.Peek().Value;
                        }

                        ToRPNInt(Elements);
                        var ev = CalcInt();

                        return ev;

                        int cc = 0;
                        /*
                        for(int i = 0; i < Elements.Count; i++)
                        {
                            if (Elements[i].OpType == OperatorType.None)
                            {
                                ops.Push(Elements[i].Value.ToString());
                            }
                            else
                            {
                                switch (Elements[i].OpType)
                                {
                                    case OperatorType.Plus:
                                        ops.Push("+");
                                        break;

                                    case OperatorType.Mins:
                                        ops.Push("-");
                                        break;

                                    case OperatorType.Divide:
                                        ops.Push("/");
                                        break;

                                    case OperatorType.Times:
                                        ops.Push("*");
                                            break;
                                }
                                //ops.Push(Elements[i].OpType)
                            }
                        }

                        double evalRpn(Stack<string> ops)
                        {
                            string tk = ops.Pop();
                            double x, y;

                            if(!double.TryParse(tk,out x))
                            {
                                y = evalRpn(ops); x = evalRpn(ops);
                                if(tk == "+")
                                {
                                    x += y;
                                }else if
                                (tk == "-")
                                {
                                    x -= y;
                                }else if
                                    (tk == "*")
                                {
                                    x *= y;
                                }else if(tk=="/") {
                                    x /= y;
                                }
                                else
                                {
                                    throw new Exception("Illegal eval");
                                }
                            }

                            return x;
                        }

                        //ops[ = "+";

                        ops = new Stack<string>();

                        ops.Push("4");
                        ops.Push("5");
                        ops.Push("+");
                        */

                        //double r = evalRpn(ops);

                        //int vv = 09;
                    }
                }
                if (Elements[0].Value is string)
                {
                }
            }

            return null;
        }
    }

    public enum ElementType
    {
        Value, Var, String, Object, Operator, SubExp, Array, Call
    }

    public enum OperatorType
    {
        Plus, Mins, Times, Divide, Equals, None, Pow2, LessThan, GreaterThan
    }

    public class ExprElement
    {
        public StructFlatCall Call
        {
            get;
            set;
        }

        public ElementType Type
        {
            get;
            set;
        }

        public OperatorType OpType
        {
            get;
            set;
        }

        public StructVar Var
        {
            get;
            set;
        }

        public dynamic Value
        {
            get;
            set;
        }

        public Expr Exp
        {
            get;
            set;
        }

        public StructExpr Index
        {
            get;
            set;
        }

        public StructNew New
        {
            get;
            set;
        }

        public ExprElement(StructNew n)
        {
            Value = null;
            Exp = null;
            OpType = OperatorType.None;
            Var = null;
            New = n;
        }

        public ExprElement(string text)
        {
            Value = text;
            Exp = null;
            OpType = OperatorType.None;
            Var = null;
        }

        public ExprElement()
        {
            Exp = null;
            Value = 0;
            OpType = OperatorType.None;
            Var = null;
        }

        public ExprElement(double num)
        {
            Exp = null;
            Value = num;
            Var = null;
            Type = ElementType.Value;
            OpType = OperatorType.None;
        }

        public ExprElement(Expr exp)
        {
            Exp = exp;
            Value = 0;
            Var = null;
            Type = ElementType.SubExp;
            OpType = OperatorType.None;
        }

        public ExprElement(OperatorType op)
        {
            Exp = null;
            Value = 0;
            Var = null;
            Type = ElementType.Operator;
            OpType = op;
        }
    }
}
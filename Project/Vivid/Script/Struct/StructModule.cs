﻿using System;
using System.Collections.Generic;

namespace VividScript.Struct
{
    public class StructModule : CodeStruct
    {
#pragma warning disable CS0108 // 'StructModule.Name' hides inherited member 'CodeStruct.Name'. Use the new keyword if hiding was intended.
        public string Name
#pragma warning restore CS0108 // 'StructModule.Name' hides inherited member 'CodeStruct.Name'. Use the new keyword if hiding was intended.
        {
            get;
            set;
        }

        public List<StructClass> Classes
        {
            get;
            set;
        }

        public List<StructFunc> Funcs
        {
            get;
            set;
        }

        public List<StructVar> GlobalVars
        {
            get;
            set;
        }

        public Dictionary<string, StructClass> ClassesTable
        {
            get;
            set;
        }

        public Dictionary<string, StructFunc> FuncsTable
        {
            get;
            set;
        }

        public StructModule()
        {
            Classes = new List<StructClass>();
            ClassesTable = new Dictionary<string, StructClass>();
            Funcs = new List<StructFunc>();
            FuncsTable = new Dictionary<string, StructFunc>();
            GlobalVars = new List<StructVar>();
            Name = "name";
        }

        public void AddClass(StructClass cls)
        {
            Classes.Add(cls);
            ClassesTable.Add(cls.Name, cls);
        }

        public void AddVars(StructDefineVar v)
        {
            foreach (var vv in v.Vars)
            {
                vv.Value = 0.0;
                GlobalVars.Add(vv);
            }
        }
        public void AddFunc(StructFunc func)
        {
            Funcs.Add(func);
            FuncsTable.Add(func.Name, func);
        }

        public override void Debug()
        {
            Console.WriteLine("Module:" + Name);
            foreach (var f in Funcs)
            {
                Console.WriteLine("Func:" + f.Name);
            }
            foreach(var c in Classes)
            {
                Console.WriteLine("Class:" + c.Name);
                foreach(var cf in c.InstanceFuncs)
                {
                    Console.WriteLine(" Method:" + cf.Name);
                }
                foreach(var csf in c.StaticFuncs)
                {
                    Console.WriteLine("Static Function:" + csf.Name);
                }
            }
        }
    }
}
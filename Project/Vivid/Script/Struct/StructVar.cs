﻿using System;
using System.Collections.Generic;

using VividScript.Tokenizer;

namespace VividScript.Struct
{
    public class StructVar : CodeStruct
    {
        public TokenType VarType
        {
            get;
            set;
        }

        public dynamic Value
        {
            get;
            set;
        }

        public StructVar[] Vars
        {
            get;
            set;
        }

        public List<String> Names
        {
            get;
            set;
        }

        public void SetValue(dynamic value)
        {
            Value = value;
        }

        public StructVar(string[] name)
        {
            Names = new List<string>();
            for (int i = 0; i < 255; i++)
            {
                string tn = name[i];
                if (tn != null && tn.Length > 0)
                {
                    Names.Add(tn);
                }
                else
                {
                    return;
                }
            }
        }
        public StructVar(string name)
        {
            Name = name;
            //VarType  = TokenTyp
            VarType = TokenType.Var;
            Value = 0;
        }

        public override void Debug()
        {
            Console.WriteLine("Var:" + Name + " Type:" + VarType.ToString());
            //base.Debug();
        }
    }
}
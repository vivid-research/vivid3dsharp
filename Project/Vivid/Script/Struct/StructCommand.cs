﻿namespace VividScript.Struct
{
    public class StructCommand : CodeStruct
    {

        public StructFlatCall FlatCall = null;
        public override dynamic Exec()
        {
            if (FlatCall != null)
            {
                FlatCall.Exec();
            }
            return base.Exec();
        }

    }
}

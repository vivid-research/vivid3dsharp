﻿namespace VividScript.Struct
{
    public class CodeStruct
    {
        public string Name
        {
            get;
            set;
        }

        public CodeStruct()
        {
            Name = "";
        }

        public virtual void Debug()
        {
        }

        public virtual dynamic Exec()
        {
            return null;
        }

        //public virtual dynamic Execute()
        // {
        //}
    }
}
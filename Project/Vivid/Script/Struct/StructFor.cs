﻿namespace VividScript.Struct
{
    public class StructFor : CodeStruct
    {
        public StructAssign Initial
        {
            get;
            set;
        }

        public StructExpr Expr
        {
            get;
            set;
        }

        public StructAssign Inc
        {
            get;
            set;
        }

        public StructCode Code
        {
            get;
            set;
        }

        public override dynamic Exec()
        {
            Initial.Exec();
            while (Expr.Exec() == 1)
            {
                Code.Exec();
                Inc.Exec();
            }
            return null;
            //return base.Exec();
        }
    }
}
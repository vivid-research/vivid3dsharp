﻿using System;
using System.Collections.Generic;

namespace VividScript.Struct
{
    public class StructClass : CodeStruct
    {
        public List<StructFunc> InstanceFuncs
        {
            get;
            set;
        }

        public Dictionary<string, StructFunc> InstanceFuncsTable
        {
            get;
            set;
        }

        public List<StructFunc> StaticFuncs
        {
            get;
            set;
        }

        public Dictionary<string, StructFunc> StaticFuncsTable
        {
            get;
            set;
        }

        public List<StructVar> InstanceVars
        {
            get;
            set;
        }

        public Dictionary<string, StructVar> InstanceVarsTable
        {
            get;
            set;
        }

        public List<StructVar> StaticVars
        {
            get;
            set;
        }

        public Dictionary<string, StructVar> StaticVarsTable
        {
            get;
            set;
        }

        public StructClass(string name)
        {
            Name = name;
            InstanceVars = new List<StructVar>();
            InstanceVarsTable = new Dictionary<string, StructVar>();
            StaticVars = new List<StructVar>();
            StaticVarsTable = new Dictionary<string, StructVar>();
            InstanceFuncs = new List<StructFunc>();
            InstanceFuncsTable = new Dictionary<string, StructFunc>();
            StaticFuncs = new List<StructFunc>();
            StaticFuncsTable = new Dictionary<string, StructFunc>();
        }

        public void AddInstanceFunc(StructFunc f)
        {
            InstanceFuncs.Add(f);
            InstanceFuncsTable.Add(f.Name, f);
        }

        public void AddStaticFunc(StructFunc f)
        {
            StaticFuncs.Add(f);
            StaticFuncsTable.Add(f.Name, f);
        }

        public void AddInstanceVar(StructVar v)
        {
            InstanceVars.Add(v);
            InstanceVarsTable.Add(v.Name, v);
        }

        public void AddStaticVar(StructVar v)
        {
            StaticVars.Add(v);
            StaticVarsTable.Add(v.Name, v);
        }

        public StructVar GetVar(string name)
        {
            if (InstanceVarsTable.ContainsKey(name))
            {
                return InstanceVarsTable[name];
            }
            if (StaticVarsTable.ContainsKey(name))
            {
                return StaticVarsTable[name];
            }


            //Console.WriteLine("Var not found:" + name + " Class:" + Name);

            return null;
        }

        public StructFunc GetFunc(string name)
        {
            if (InstanceFuncsTable.ContainsKey(name))
            {
                return InstanceFuncsTable[name];
            }
            if (StaticFuncsTable.ContainsKey(name))
            {
                return StaticFuncsTable[name];
            }

            //    Console.WriteLine("Func not found:" + name + " Class:" + Name);

            return null;
        }

        public Run.ClassInstance CreateInstance()
        {

            var ci = new Run.ClassInstance(this);

            return ci;


        }

        public override void Debug()
        {
            Console.WriteLine("Class:" + Name);
            Console.WriteLine("Instance Vars:" + InstanceVars.Count);
            foreach (var iv in InstanceVars)
            {
                iv.Debug();
            }
            Console.WriteLine("Static Vars:" + StaticVars.Count);
            foreach (var sv in StaticVars)
            {
                sv.Debug();
            }
            Console.WriteLine("Instance Funcs:" + InstanceFuncs.Count);
            foreach (var iff in InstanceFuncs)
            {
                iff.Debug();
            }
            Console.WriteLine("Static Funcs:" + StaticFuncs.Count);
            foreach (var sf in StaticFuncs)
            {
                sf.Debug();
            }
            //return base.ToString();
        }
    }
}
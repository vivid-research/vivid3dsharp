﻿using System.Collections.Generic;

namespace VividScript.Struct
{
    public class StructCallParams : CodeStruct
    {
        public List<StructExpr> Pars
        {
            get;
            set;
        }

        public StructCallParams()
        {
            Pars = new List<StructExpr>();
        }
    }
}
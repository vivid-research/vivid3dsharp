﻿
using VividScript.Run;

namespace VividScript.Struct
{
    public class StructFlatCall : CodeStruct
    {
        public string FuncName
        {
            get;
            set;
        }

        public StructCallParams Params
        {
            get;
            set;
        }

        public override dynamic Exec()
        {
            //Console.WriteLine("Running:" + FuncName);


            dynamic[] pars = new dynamic[Params.Pars.Count];

            for (int i = 0; i < pars.Length; i++)
            {
                dynamic p = Params.Pars[i].Exec();
                pars[i] = p;
            }

            return RunStack.This.RunFunc(FuncName, pars);

            return null;
            //return base.Exec();
        }
    }
}
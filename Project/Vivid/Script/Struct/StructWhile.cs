﻿namespace VividScript.Struct
{
    public class StructWhile : CodeStruct
    {
        public StructExpr Expr
        {
            get;
            set;
        }

        public StructCode Code
        {
            get;
            set;
        }

        public override dynamic Exec()
        {
            while (Expr.Exec() == 1)
            {
                Code.Exec();
            }

            return null;
            //return base.Exec();
        }
    }
}
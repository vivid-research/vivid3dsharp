﻿namespace VividScript.Struct
{
    public class StructCall : CodeStruct
    {
        public string[] FuncNames
        {
            get;
            set;
        }

        public StructCallParams Params
        {
            get;
            set;
        }
        public override dynamic Exec()
        {
            dynamic[] pars = new dynamic[Params.Pars.Count];

            for (int i = 0; i < pars.Length; i++)
            {
                dynamic p = Params.Pars[i].Exec();
                pars[i] = p;
            }

            return Run.RunStack.This.RunFunc(FuncNames, pars);

            return base.Exec();
        }

    }
}

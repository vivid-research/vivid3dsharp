﻿using System.Collections.Generic;

namespace VividScript.Struct
{
    public class StructCode : CodeStruct
    {
        public List<CodeStruct> Code
        {
            get;
            set;
        }

        public int CodeIndex
        {
            get;
            set;
        }

        public StructCode()
        {
            Code = new List<CodeStruct>();
            CodeIndex = 0;
        }

        public void AddCode(CodeStruct code)
        {
            Code.Add(code);
        }

        public override dynamic Exec()
        {
            foreach (var code in Code)
            {
                if (code is StructReturn)
                {
                    return code.Exec();
                }
                code.Exec();
            }

            return null;
            //return base.Exec();
        }

        public CodeStruct NextCode()
        {
            var ret = Code[CodeIndex];
            CodeIndex++;
            return ret;
        }
    }
}
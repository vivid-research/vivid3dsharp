﻿using System.IO;

using Vivid.Scene;
using System;
using System.Threading;
namespace Vivid.Scripting
{
    public class NodeComponent
    {
        public Node3D Node
        {
            get;
            set;
        }

        public Entity3D Entity
        {
            get;
            set;
        }

        public AnimEntity3D AnimEntity
        {
            get;
            set;
        }

        public string FilePath
        {
            get;
            set;
        }

   

        public string Code
        {
            get;
            set;
        }

        public dynamic Link
        {
            get;
            set;
        }

        public string FullPath
        {
            get;
            set;
        }

        public DateTime DiskTime = DateTime.Now;
        public Vivid.Resonance.UI SUI = null;

        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }

        private string _Name = "NodeScript";

        public Vivid.Reflect.ClassIO ClassCopy = null;

        public string GetName()
        {
            return Name;
        }

        public bool Updated
        {
            get
            
           {

                return false;
                var time = System.IO.File.GetLastWriteTime(FullPath);
                int ct = time.CompareTo(DiskTime);
                if (time.CompareTo(DiskTime) != 0)
                {
                    Thread.Sleep(1000);
                    return true;
                }

                return false;
            }
        }

    

        public void LoadAndCompile()
        {
            Code = File.ReadAllText(FilePath);
            Compile();
        }

        public void Compile()
        {
        }

        public void SetNode(Node3D node)
        {
            Link.Node = node;
            if (node is Entity3D)
            {
                Link.Entity = node as Entity3D;
            }
        }

        public virtual void SaveNode()
        {

        }

        public virtual void LoadNode()
        {

        }

        public virtual void Loaded()
        {

        }

        public virtual void Begin()
        {
        }

        public virtual void End()
        {
        }

        public virtual void Pause()
        {
        }

        public virtual void Resume()
        {
        }

        public virtual void Update()
        {
            Link.Update();
        }

        public virtual void Draw()
        {
        }
    }
}
﻿using System.Collections.Generic;

using VividScript.Struct;

namespace VividScript.Run
{
    public class CodeScope
    {
        public CodeScope Root
        {
            get;
            set;
        }

        public List<StructVar> Vars
        {
            get;
            set;
        }

        public Dictionary<string, StructVar> VarsTable
        {
            get;
            set;
        }

        public List<StructFunc> Funcs
        {
            get;
            set;
        }

        public Dictionary<string, StructFunc> FuncsTable
        {
            get;
            set;
        }

        public CodeScope()
        {
            Vars = new List<StructVar>();
            Funcs = new List<StructFunc>();
            VarsTable = new Dictionary<string, StructVar>();
            FuncsTable = new Dictionary<string, StructFunc>();
            Root = null;
        }

        public CodeScope(CodeScope root)
        {
            Vars = new List<StructVar>();
            Funcs = new List<StructFunc>();
            VarsTable = new Dictionary<string, StructVar>();
            FuncsTable = new Dictionary<string, StructFunc>();
            Root = root;
        }

        public void Add(StructModule mod)
        {
            foreach (var f in mod.Funcs)
            {
                Add(f);
            }
            foreach (var gv in mod.GlobalVars)
            {
                Add(gv);
            }
        }

        public void Add(StructVar v)
        {
            if (VarsTable.ContainsKey(v.Name)) return;
            Vars.Add(v);
            VarsTable.Add(v.Name, v);
        }

        public void Add(StructFunc f)
        {
            Funcs.Add(f);
            FuncsTable.Add(f.Name, f);
        }

        public StructVar GetVar(string name)
        {
            if (VarsTable.ContainsKey(name))
            {
                return VarsTable[name];
            }
            if (Root != null)
            {
                return Root.GetVar(name);
            }
            return null;
        }

        public StructFunc GetFunc(string name)
        {
            if (FuncsTable.ContainsKey(name))
            {
                return FuncsTable[name];
            }
            if (Root != null)
            {
                return Root.GetFunc(name);
            }
            return null;
        }
    }
}
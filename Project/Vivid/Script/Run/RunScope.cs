﻿namespace VividScript.Run
{
    public class RunScope
    {
        public RunStack Stack
        {
            get;
            set;
        }

        public RunScope()
        {
            Stack = null;
        }

        public RunScope(RunStack stack)
        {
            Stack = stack;
        }

        public void SetStack(RunStack stack)
        {
            Stack = stack;
        }
    }
}
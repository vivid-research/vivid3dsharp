﻿using System.Collections.Generic;

using VividScript.Struct;

namespace VividScript.Run
{
    public class ClassInstance
    {

        public string BaseType
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public List<StructVar> InstanceVars
        {
            get;
            set;
        }

        public List<StructFunc> InstanceFuncs
        {
            get
                  ;
            set;
        }

        public ClassInstance(StructClass cls)
        {

            BaseType = cls.Name;
            InstanceVars = new List<StructVar>();

            foreach (var v in cls.InstanceVars)
            {

                var nv = new StructVar(v.Name);
                nv.Name = v.Name;
                nv.Value = 0;
                InstanceVars.Add(nv);


            }

            InstanceFuncs = cls.InstanceFuncs;

        }

    }
}

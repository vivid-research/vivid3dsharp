﻿//#define DEBUGRUNNER

using System;
using System.Collections.Generic;

using Vivid.Texture;

using VividScript.Reporting;
using VividScript.Struct;

namespace VividScript.Run
{
    public delegate dynamic SysFunc(params dynamic[] input);

    public class SystemFunc
    {
        public string Name
        {
            get;
            set;
        }

        public SysFunc Func
        {
            get;
            set;
        }

        public SystemFunc(string name)
        {
            Name = name;
            Func = null;
        }

        public dynamic Call(params dynamic[] pars)
        {
            return Func.Invoke(pars);
        }
    }

    public class RunStack
    {
        public List<SystemFunc> SystemFuncs
        { get; set; }

        public static RunStack This
        {
            get;
            set;
        }

        public Stack<CodeScope> Scope
        {
            get;
            set;
        }

        public List<StructModule> Modules
        {
            get;
            set;
        }

        public void PushClass(ClassInstance cls)
        {

            foreach (var v in cls.InstanceVars)
            {
                Add(v);
            }


            foreach (var f in cls.InstanceFuncs)
            {
                Scope.Peek().Add(f);
            }


        }


        public dynamic RunCommand(string com)
        {

            Tokenizer.Tokenizer toker = new Tokenizer.Tokenizer();

            toker.FromString(com);

            var str = toker.Stream;

            var pl = new VividScript.Parser.ParseCommand(str, 0);

            var sc = pl.Parse();

            sc.Exec();

            return null;
        }

        public dynamic NewClass(string name, params dynamic[] pars)
        {

            foreach (var mod in Modules)
            {
                foreach (var cls in mod.Classes)
                {
                    if (cls.Name == name)
                    {

#if DEBUGRUNNER
                        Console.WriteLine("Creating class instance:" + name);
#endif
                        var res = cls.CreateInstance();

                        PushScope();

                        PushClass(res);



                        RunFunc(name, pars);



                        PopScope();

                        return res;

                    }
                }
            }
            return null;

        }
        Vivid.Draw.IntelliDrawSingle IDS = null;

        public RunStack()
        {
            Modules = new List<StructModule>();
            Scope = new Stack<CodeScope>();
            RegisterSystemFuncs();
           // IDS = new Vivid.Draw.IntelliDrawSingle();
           // IDS.Blend = Vivid.Draw.BlendType.Alpha;
            RunStack.This = this;

            //            RegisterSystemFuncs();
        }

        public void RegisterSystemFunc(string name, SysFunc func)
        {
            var nfunc = new SystemFunc(name);
            nfunc.Func = func;
            SystemFuncs.Add(nfunc);
        }

        Texture2D tex1;
        Random rnd = new Random(Environment.TickCount);
        public void RegisterSystemFuncs()

        {

            var rand = new SystemFunc("rand");

            rand.Func = (p) =>
            {

                return (double)rnd.Next((int)p[0], (int)p[1]);

            };

            var loadTexture = new SystemFunc("loadTexture");

            loadTexture.Func = (p) =>
            {
                Texture2D tex = new Texture2D((string)p[0], true);

                return tex;
            };

            var idBegin = new SystemFunc("idBegin");

            idBegin.Func = (p) =>
            {

                IDS.BeginDraw();

                return null;

            };
            var idEnd = new SystemFunc("idEnd");

            idEnd.Func = (p) =>
            {
                IDS.EndDraw();
                return null;
            };


            var idDraw = new SystemFunc("idRect");

            idDraw.Func = (p) =>
            {
                if (tex1 == null)
                {
                    tex1 = new Texture2D("pong/bat1.png", true);
                }
                IDS.DrawImg((int)p[0], (int)p[1], (int)p[2], (int)p[3], (Texture2D)p[4], new OpenTK.Mathematics.Vector4(1, 0, 1, 1));
                return null;
            };

            var printf = new SystemFunc("printf");

            printf.Func = (p) =>
            {
                if (p.Length == 0)
                {
                    //    Console.WriteLine("printf:Null");
                }
                else
                {

                    System.Console.WriteLine(p[0]);
                    return null;
                }
                return null;
            };

            SystemFuncs = new List<SystemFunc>();

            SystemFuncs.Add(printf);
            SystemFuncs.Add(idBegin);
            SystemFuncs.Add(idEnd);
            SystemFuncs.Add(idDraw);
            SystemFuncs.Add(loadTexture);
            SystemFuncs.Add(rand);
        }

        public void UpdateSystemScope()
        {
            Scope = new Stack<CodeScope>();
            var nscope = new CodeScope();
            foreach (var mod in Modules)
            {
                nscope.Add(mod);
            }
            Scope.Push(nscope);
        }

        private void PushScope()
        {
            if (Scope.Count == 0)
            {
                Scope.Push(new CodeScope());
            }
            else
            {
                var nscope = new CodeScope(Scope.Peek());
                Scope.Push(nscope);
            }
        }

        private void PopScope()
        {
            Scope.Pop();
        }

        public void Add(StructVar v)
        {
            Scope.Peek().Add(v);
        }

        public void SetArrayValue(string target, int index, dynamic val)
        {
            var tv = Scope.Peek().GetVar(target);

            tv.Vars[index].Value = val;
        }

        public dynamic GetValue(List<String> names)
        {
            bool first = true;
            // PushScope();
            foreach (var n in names)
            {
                string lay = n;

                var av = GetValue(lay);



                if (av is ClassInstance)
                {
                    if (!first)
                    {
                        PopScope();
                    }
                    PushScope();
                    first = false;
                    PushClass(av);
                }
                else
                {
                    var ret = GetValue(lay);
                    PopScope();
                    return ret;
                }
                first = false;

            }
            return null;
            //PopScope();
        }
        public void SetValue(List<string> names, dynamic val)
        {
            bool first = true;
            // PushScope();
            foreach (var n in names)
            {
                string lay = n;

                var av = GetValue(lay);



                if (av is ClassInstance)
                {
                    if (!first)
                    {
                        PopScope();
                    }
                    PushScope();
                    first = false;
                    PushClass(av);
                }
                else
                {
                    SetValue(lay, val);
                    PopScope();
                    return;
                }
                first = false;

            }
            //PopScope();

        }
        public void SetValue(string target, dynamic val)
        {
            if (val == null)
            {
                //  Console.WriteLine("Warning: Var:" + target + " Set to null.");
            }
            Scope.Peek().GetVar(target).Value = val;
            int vv = 0;
        }

        public dynamic GetArrayValue(string target, int index)
        {
            return Scope.Peek().GetVar(target).Vars[index].Value;
        }

        public dynamic GetValue(string target)
        {
            return Scope.Peek().GetVar(target).Value;
        }

        private SystemFunc FindSystemFunc(string name)
        {
            foreach (var f in SystemFuncs)
            {
                if (f.Name == name)
                {
                    return f;
                }
            }
            return null;
        }

        public dynamic RunFunc(string[] names, params dynamic[] pars)
        {
            PushScope();
            for (int i = 0; i < names.Length - 1; i++)
            {
                if (names[i + 1] == null)
                {
                    int a = 0;
                    var r = RunFunc(names[i], pars);
                    PopScope();
                    return r;


                }
                var av = GetValue(names[i]);
                if (av is ClassInstance)
                {
                    PushClass(av);
                }
                else
                {
                    int aa = 0;
                }


            }
            PopScope();
            return null;
        }

        public dynamic RunFunc(StructFunc func, params dynamic[] pars)
        {
            PushScope();

            if (func.Params != null)
            {
                int i = 0;
                foreach (var p in func.Params.Params)
                {
                    p.Value = pars[i];
                    RunStack.This.Add(p);
                    i++;
                }
            }

            var res = func.Exec();

            PopScope();

            return res;
        }
        public dynamic RunFunc(string name, params dynamic[] pars)
        {
            This = this;
            var func = Scope.Peek().GetFunc(name);

            if (func == null)
            {
                var sfunc = FindSystemFunc(name);
                if (sfunc != null)
                {
                    PushScope();
                    var r2 = sfunc.Func?.Invoke(pars);
                    PopScope();
                    return r2;
                }
                else
                {
                    ErrorReport.RuntimeError("No function called:" + name + " found.");
                }
            }

            PushScope();

            if (func.Params != null)
            {
                int i = 0;
                foreach (var p in func.Params.Params)
                {
                    p.Value = pars[i];
                    RunStack.This.Add(p);
                    i++;
                }
            }

            var res = func.Exec();

            PopScope();

            //dynamic res = null;
            return res;
        }

        public void AddModule(StructModule module)
        {
            Modules.Add(module);
            UpdateSystemScope();
        }
    }
}
﻿using OpenTK.Graphics.OpenGL4;

using System;
using System.Collections.Generic;

using Vivid.Effect;
using Vivid.Scene.Node;

namespace Vivid.Scene.Optimized
{
    public class FastRenderList
    {
        public List<RenderNode> List = new List<RenderNode>();
        public Cam3D Cam = null;
        public List<Light3D> Lights = new List<Light3D>();
        public FXMultiPass3D FXMp;

        public void Add(Entity3D ent)
        {
            List.Add(new RenderNode(ent));
        }

        public void Add(Light3D l)
        {
            Lights.Add(l);
        }

        public void Sort()
        {
            foreach (var node in List)
            {
                float dis = node.Entity.Dist(Cam);
                node.ZDistCam = dis;
                Console.WriteLine("Dis:" + dis);
            }

            var na = List.ToArray();
            int nl = na.Length;

            if (na.Length == 1)
            {
                Sorted = na;
                return;
            }
            while (true)
            {
                int n2 = 0;
                bool done = true;
                while (true)
                {
                    if (na[n2].ZDistCam > na[n2 + 1].ZDistCam)
                    {
                        var t = na[n2];
                        na[n2] = na[n2 + 1];
                        na[n2 + 1] = t;
                        done = false;
                    }
                    n2++;
                    if (n2 >= (nl - 1))
                    {
                        break;
                    }
                }
                if (done) break;
            }

            Console.WriteLine("Sorted:");

            for (int i = 0; i < na.Length; i++)
            {
                Console.WriteLine("N:" + i + " Dis:" + na[i].ZDistCam);
            }

            Console.WriteLine("Done.");
            Sorted = na;
        }

        public RenderNode[] Sorted = null;

        public FastRenderList(SceneGraph3D graph)
        {
            AddNodes(graph.Root);
            foreach (var l in graph.Lights)
            {
                Add(l);
            }
            Cam = graph.Cams[0];
            FXMp = new FXMultiPass3D();
        }

        private void AddNodes(Node3D node)
        {
            if (node is Entity3D && node.NoSave == false)
            {
                var ent = node as Entity3D;
                if (ent.Meshes.Count > 0)
                {
                    Add(ent);
                }
            }

            foreach (var sn in node.Sub)
            {
                AddNodes(sn);
            }
        }

        public void RenderSorted()
        {
            bool first = false;
            int ms = Environment.TickCount;
            int rc = 0;
            foreach (var n in Sorted)
            {
                int res = 0;
                if (!first)
                {
                    if (n.Entity.Bounds.W > 50)
                    {
                        res = 1;
                        goto Skip1;
                    }
                    int qid;
                    GL.GenQueries(1, out qid);
                    GL.Disable(EnableCap.Blend);
                    GL.DepthFunc(DepthFunction.Less);
                    GL.DepthMask(false);
                    GL.ColorMask(false, false, false, false);
                    GL.BeginQuery(QueryTarget.AnySamplesPassed, qid);
                    n.Entity.RenderBounds();
                    GL.EndQuery(QueryTarget.AnySamplesPassed);
                    GL.Flush();
                    GL.GetQueryObject(qid, GetQueryObjectParam.QueryResult, out res);
                    //GL.Enable(EnableCap.Blend);
                    GL.DepthMask(true);
                    GL.ColorMask(true, true, true, true);
                    //GL.DepthFunc(DepthFunction.Lequal);
                    //\

                    //Console.WriteLine("Res:" + res);

                    /*
                    if(res=)
                    {
                        Console.WriteLine("True:");
                    }
                    else
                    {
                        Console.WriteLine("False");
                    }
                    */
                }
            Skip1:
                if (first)
                {
                    RenderNode(n); rc++;
                }
                else
                {
                    if (res == 1)
                    {
                        RenderNode(n); rc++;
                    }
                }
                first = false;
            }
            GL.Flush();
            int rt = Environment.TickCount - ms;
            Console.WriteLine("RenderTime:" + rt + " ms Nodes:" + rc);
        }

        public void RenderNormal()
        {
            FXG.Cam = Cam;

            if (Sorted != null)
            {
                RenderSorted();
                return;
            }
            int ms = Environment.TickCount;
            foreach (var node in List)
            {
                RenderNode(node);
            }
            GL.Flush();
            int rt = Environment.TickCount - ms;

            Console.WriteLine("2RenderTime:" + rt + " ms");
        }

        private void RenderNode(RenderNode node)
        {
            FXG.Local = node.Entity.World;
            bool first = true;
            foreach (var l in Lights)
            {
                Light3D.Active = l;
                Light3D.Active.ShadowFB.Cube.Bind(2);

                if (first)
                {
                    first = false;
                    GL.Disable(EnableCap.Blend);
                }
                else
                {
                    GL.Enable(EnableCap.Blend);
                    GL.BlendFunc(BlendingFactor.One, BlendingFactor.One);
                }

                foreach (var mesh in node.Entity.Meshes)
                {
                    mesh.Material.Bind();

                    FXMp.Bind();

                    mesh.Viz.Bind();
                    mesh.Viz.Visualize();
                    mesh.Viz.Release();

                    FXMp.Release();

                    mesh.Material.Release();
                }
            }
        }
    }

    public class RenderNode
    {
        public Entity3D Entity;
        public float ZDistCam = 0;

        public RenderNode(Entity3D ent)
        {
            Entity = ent;
        }
    }
}
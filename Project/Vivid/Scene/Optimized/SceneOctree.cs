﻿using System;

namespace Vivid.Scene.Optimized
{
    public class SceneOctree
    {
        public static int NodesRendered = 0;
        public SceneGraph3D Graph = null;
        public OctreeNode Root = null;
        public static Vivid.Culling.Frustum Fr;

        public SceneOctree(SceneGraph3D graph)
        {
            Graph = graph;

            Bounds bb = graph.GetBounds(false);
            Root = new OctreeNode(Graph, bb);
            Root.Final();
            Console.WriteLine("Active Nodes:" + OctreeNode.CC);
            Console.WriteLine("Total Tris:" + OctreeNode.TTris);
            Fr = new Vivid.Culling.Frustum();
        }
    }
}
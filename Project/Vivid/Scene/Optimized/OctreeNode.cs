﻿using OpenTK.Mathematics;

using System;
using System.Collections.Generic;

using Vivid.Data;
using Vivid.Effect;

namespace Vivid.Scene.Optimized
{
    public class OctreeNode
    {
        public OctreeNode[] Child = new OctreeNode[8];
        public OctreeNode Root = null;
        public static float MinSize = 4.0f;
        public Bounds Bounds;

        public Vivid.Scene.SceneGraph3D Graph;

        public bool Used
        {
            get;
            set;
        }

        public int Tris
        {
            get;
            set;
        }

        public int _Tris = 0;

        public List<Mesh3D> Meshes
        {
            get;
            set;
        }

        public static long MaxTris = 2000;

        public bool NodeActive(int i)
        {
            return Child[i] != null;
        }

        private static int rec = 1;

        public static int MaxRoot = 3;
        private static int RootN = 0;

        public void RootNum(OctreeNode node)
        {
            RootN++;
            if (Root != null)
            {
                RootNum(Root);
            }
        }

        public static int CC = 0;
        public static int TTris = 0;

        public void Final()
        {
            int cc = 0;
            for (int i = 0; i < 8; i++)
            {
                if (Child[i] != null)
                {
                    cc++;
                    Child[i].Final();
                }
            }
            if (cc == 0)
            {
                BuildMeshes();
                TTris += Tris;
                CC++;
            }
        }

        public void BuildMeshes()
        {
            if (Tris == 0)
            {
                throw (new Exception("No tris within node."));
            }

            Meshes = Graph.GetBoundsData(Bounds);
            Console.WriteLine("Meshes:" + Meshes.Count);
            foreach (var m in Meshes)
            {
                //Console.WriteLine("M:"+m+" ")
            }
        }

        public void RenderNode(Matrix4 world, Effect3D fx, bool fc = true)
        {
            FXG.Local = Matrix4.Identity;

            if (Meshes.Count == 0)
            {
                RenderChild(world, fx);
                return;
            }

            if (fc)
            {
                if (SceneOctree.Fr.VolumeVsFrustum(Bounds.MinX + Bounds.W / 2, Bounds.MinY + Bounds.H / 2, Bounds.MinZ + Bounds.D / 2, Bounds.W, Bounds.H, Bounds.D))
                {
                    Render(world, fx);
                }
            }
            else
            {
                Render(world, fx);
            }
        }

        private void Render(Matrix4 world, Effect3D fx)
        {
            SceneOctree.NodesRendered++;
            foreach (var m in Meshes)
            {
                m.Material.Bind();
                fx.Bind();
                m.Viz.Bind();
                m.Viz.Visualize();
                m.Viz.Release();
                fx.Release();
                m.Material.Release();
            }

            RenderChild(world, fx);
        }

        private void RenderChild(Matrix4 world, Effect3D fx)
        {
            for (int i = 0; i < 8; i++)
            {
                if (Child[i] != null)
                {
                    Child[i].RenderNode(world, fx, true);
                }
            }
        }

        public OctreeNode(SceneGraph3D graph, Bounds bb, OctreeNode root = null)
        {
            RootN = 0;
            //RootNum(root);
            //if (RootN > 3) return;

            Graph = graph;
            Bounds = bb;

            int tric = graph.BoundsTris(bb);

            if (tric < MaxTris && root != null)
            {
                Tris = tric;
                return;
            }
            else
            {
                Tris = tric;
            }

            Meshes = new List<Mesh3D>();
            Used = true;

            Bounds b1 = new Bounds();

            b1.MinX = bb.MinX;
            b1.MaxX = bb.MinX + bb.W / 2;
            b1.MinY = bb.MinY;
            b1.MaxY = bb.MinY + bb.H / 2;
            b1.MinZ = bb.MinZ;
            b1.MaxZ = bb.MinZ + bb.D / 2;

            b1.W = bb.W / 2;
            b1.H = bb.H / 2;
            b1.D = bb.D / 2;

            Bounds b2 = new Bounds();

            b2.MinX = bb.MinX + bb.W / 2;
            b2.MaxX = bb.MaxX;
            b2.MinY = bb.MinY;
            b2.MaxY = bb.MinY + bb.H / 2;
            b2.MinZ = bb.MinZ;
            b2.MaxZ = bb.MinZ + bb.D / 2;

            b2.W = bb.W / 2;
            b2.H = bb.H / 2;
            b2.D = bb.D / 2;

            Bounds b3 = new Bounds();

            b3.MinX = bb.MinX;
            b3.MinY = bb.MinY;
            b3.MinZ = bb.MinZ + bb.D / 2;

            b3.MaxX = bb.MinX + bb.W / 2;
            b3.MaxY = bb.MinY + bb.H / 2;
            b3.MaxZ = bb.MaxZ;

            b3.W = bb.W / 2;
            b3.H = bb.H / 2;
            b3.D = bb.D / 2;

            Bounds b4 = new Bounds();

            b4.MinX = bb.MinX + bb.W / 2;
            b4.MaxX = bb.MaxX;
            b4.MinY = bb.MinY;
            b4.MaxY = bb.MinY + bb.H / 2;
            b4.MinZ = bb.MinZ + bb.D / 2;
            b4.MaxZ = bb.MaxZ;

            b4.W = bb.W / 2;
            b4.H = bb.H / 2;
            b4.D = bb.D / 2;

            Bounds b5 = new Bounds();

            b5.MinX = bb.MinX + bb.W / 2;
            b5.MaxX = bb.MaxX;
            b5.MinY = bb.MinY + bb.H / 2;
            b5.MaxY = bb.MaxY;
            b5.MinZ = bb.MinZ;
            b5.MaxZ = bb.MinZ + bb.D / 2;

            b5.W = bb.W / 2;
            b5.H = bb.H / 2;
            b5.D = bb.D / 2;

            Bounds b6 = new Bounds();

            b6.MinX = bb.MinX;
            b6.MaxX = bb.MinX + bb.W / 2;
            b6.MinY = bb.MinY + bb.H / 2;

            b6.MaxY = bb.MaxY;
            b6.MinZ = bb.MinZ;
            b6.MaxZ = bb.MinZ + bb.D / 2;

            b6.W = bb.W / 2;
            b6.H = bb.H / 2;
            b6.D = bb.D / 2;

            Bounds b7 = new Bounds();

            b7.MinX = bb.MinX;
            b7.MinY = bb.MinY + bb.H / 2;
            b7.MinZ = bb.MinZ + bb.D / 2;

            b7.MaxX = bb.MinX + bb.W / 2;
            b7.MaxY = bb.MaxY;
            b7.MaxZ = bb.MaxZ;

            b7.W = bb.W / 2;
            b7.H = bb.H / 2;
            b7.D = bb.D / 2;

            Bounds b8 = new Bounds();

            b8.MinX = bb.MinX + bb.W / 2;
            b8.MaxX = bb.MaxX;
            b8.MinY = bb.MinY + bb.H / 2;
            b8.MaxY = bb.MaxY;
            b8.MinZ = bb.MinZ + bb.D / 2;
            b8.MaxZ = bb.MaxZ;

            b8.W = bb.W / 2;
            b8.H = bb.H / 2;
            b8.D = bb.D / 2;

            var lm1 = b1.GenMesh();
            var lm2 = b2.GenMesh();
            var lm3 = b3.GenMesh();
            var lm4 = b4.GenMesh();
            var lm5 = b5.GenMesh();
            var lm6 = b6.GenMesh();
            var lm7 = b7.GenMesh();
            var lm8 = b8.GenMesh();

            MeshLines[] ml = new MeshLines[8];
            ml[0] = lm1;
            ml[1] = lm2;
            ml[2] = lm3;
            ml[3] = lm4;
            ml[4] = lm5;
            ml[5] = lm6;
            ml[6] = lm7;
            ml[7] = lm8;

            /*
            graph.LineMeshes.Add(lm1);
            graph.LineMeshes.Add(lm2);
            graph.LineMeshes.Add(lm3);
            graph.LineMeshes.Add(lm4);
            graph.LineMeshes.Add(lm5);
            graph.LineMeshes.Add(lm6);
            graph.LineMeshes.Add(lm7);
            graph.LineMeshes.Add(lm8);
            */

            Child[0] = new OctreeNode(graph, b1, this);
            Child[1] = new OctreeNode(graph, b2, this);
            Child[2] = new OctreeNode(graph, b3, this);
            Child[3] = new OctreeNode(graph, b4, this);
            Child[4] = new OctreeNode(graph, b5, this);
            Child[5] = new OctreeNode(graph, b6, this);
            Child[6] = new OctreeNode(graph, b7, this);
            Child[7] = new OctreeNode(graph, b8, this);

            for (int i = 0; i < 8; i++)
            {
                if (Child[i].Tris == 0)
                {
                    Child[i] = null;
                    graph.LineMeshes.Remove(ml[i]);
                }
            }

            //for(int i = 0; i < 8; i++)
            //{
            //}
        }
    }
}
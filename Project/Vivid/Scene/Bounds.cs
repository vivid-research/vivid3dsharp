﻿using Vivid.Data;

namespace Vivid.Scene
{
    public class Bounds
    {
        public float X, Y, Z;
        public float W, H, D;
        public float MinX = 10000, MinY = 10000, MinZ = 10000;
        public float MaxX = -10000, MaxY = -10000, MaxZ = -10000;

        public MeshLines GenMesh()
        {
            Bounds bb = this;

            var bbm = new MeshLines(12);

            bbm.SetLine(0, bb.MinX, bb.MinY, bb.MinZ, bb.MaxX, bb.MinY, bb.MinZ);
            bbm.SetLine(1, bb.MinX, bb.MinY, bb.MinZ, bb.MinX, bb.MinY, bb.MaxZ);
            bbm.SetLine(2, bb.MaxX, bb.MinY, bb.MinZ, bb.MaxX, bb.MinY, bb.MaxZ);
            bbm.SetLine(3, bb.MinX, bb.MinY, bb.MaxZ, bb.MaxX, bb.MinY, bb.MaxZ);

            bbm.SetLine(4, bb.MinX, bb.MaxY, bb.MinZ, bb.MaxX, bb.MaxY, bb.MinZ);
            bbm.SetLine(5, bb.MinX, bb.MaxY, bb.MinZ, bb.MinX, bb.MaxY, bb.MaxZ);
            bbm.SetLine(6, bb.MaxX, bb.MaxY, bb.MinZ, bb.MaxX, bb.MaxY, bb.MaxZ);
            bbm.SetLine(7, bb.MinX, bb.MaxY, bb.MaxZ, bb.MaxX, bb.MaxY, bb.MaxZ);

            bbm.SetLine(8, bb.MinX, bb.MinY, bb.MinZ, bb.MinX, bb.MaxY, bb.MinZ);
            bbm.SetLine(9, bb.MaxX, bb.MinY, bb.MinZ, bb.MaxX, bb.MaxY, bb.MinZ);
            bbm.SetLine(10, bb.MinX, bb.MinY, bb.MaxZ, bb.MinX, bb.MaxY, bb.MaxZ);
            bbm.SetLine(11, bb.MaxX, bb.MinY, bb.MaxZ, bb.MaxX, bb.MaxY, bb.MaxZ);

            bbm.Final();

            return bbm;

            //            IDESceneView3D.MainView.Graph3D.Graph.LineMeshes.Add(bbm);
        }
    }
}
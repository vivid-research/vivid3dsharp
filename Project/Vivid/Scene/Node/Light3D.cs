﻿using OpenTK.Graphics.OpenGL4;
using OpenTK.Mathematics;

using Vivid.FrameBuffer;
using Vivid.Settings;

namespace Vivid.Scene.Node
{
    public enum LightType
    {
        Ambient, Directional, Point
    }

    public class Light3D : Node3D
    {
        public static Light3D Active = null;

        public static int LightNum = 0;

#pragma warning disable CS0108 // 'Light3D.CastShadows' hides inherited member 'Node3D.CastShadows'. Use the new keyword if hiding was intended.
        public bool CastShadows = true;
#pragma warning restore CS0108 // 'Light3D.CastShadows' hides inherited member 'Node3D.CastShadows'. Use the new keyword if hiding was intended.

        public FrameBufferCube ShadowFB = null;

        public Flare.Flare3D LensFlare = null;

        // public Vector3 AmbCE = 0.3f;
        public Texture.TextureCube ShadowMap = null;

#pragma warning disable CS0108 // 'Light3D.Type' hides inherited member 'Node3D.Type'. Use the new keyword if hiding was intended.
        public LightType Type = LightType.Point;
#pragma warning restore CS0108 // 'Light3D.Type' hides inherited member 'Node3D.Type'. Use the new keyword if hiding was intended.

        public bool EnableLight
        {
            get;
            set;
        }

        public Light3D(bool createFBO = true)
        {
            EnableLight = true;
            Diff = new Vector3(0.8f, 0.8f, 0 / 8f);
            Spec = new Vector3(0.2f, 0.2f, 0.2f);
            Amb = new Vector3(0.1f, 0.1f, 0.1f);
            Atten = 0.1f;
            if (createFBO)
            {
                CreateShadowFBO();
            }
            LightNum++;
            Range = 350;
            Name = "Light0" + LightNum;

        }

        public Vector3 Amb
        {
            get;
            set;
        }

        public float Atten
        {
            get;
            set;
        }

        public Vector3 Diff
        {
            get;
            set;
        }

        public float Range
        {
            get;
            set;
        }

        public Vector3 Spec
        {
            get;
            set;
        }

#pragma warning disable CS0108 // 'Light3D.Changed()' hides inherited member 'Node3D.Changed()'. Use the new keyword if hiding was intended.
        public void Changed()
#pragma warning restore CS0108 // 'Light3D.Changed()' hides inherited member 'Node3D.Changed()'. Use the new keyword if hiding was intended.
        { }

        public void CreateShadowFBO()
        {
            ShadowFB = new FrameBufferCube(Quality.ShadowMapWidth, Quality.ShadowMapHeight);
        }

        public void DrawShadowMap(SceneGraph3D graph)
        {
            Active = this;

            Cam3D cam = new Cam3D();
            Effect.FXG.Cam = cam;
            cam.FOV = 90;
            cam.MaxZ = Quality.ShadowDepth;

            graph.CamOverride = cam;
            cam.LocalPos = LocalPos;
            cam.MaxZ = Quality.ShadowDepth;
            // cam.LocalTurn = LocalTurn;

            int fn = 0;

            GL.Disable(EnableCap.ScissorTest);
            App.SetVP.Set(0, 0, App.AppInfo.W, App.AppInfo.H);

            TextureTarget f = ShadowFB.SetFace(fn);

            SetCam(f, cam);

            graph.RenderingShadows = true;

            graph.RenderDepth();

            SetCam(ShadowFB.SetFace(1), cam);
            graph.RenderDepth();

            // ShadowFB.Release(); graph.CamOverride = null;

            SetCam(ShadowFB.SetFace(2), cam);
            graph.RenderDepth();

            SetCam(ShadowFB.SetFace(3), cam);
            graph.RenderDepth();

            SetCam(ShadowFB.SetFace(4), cam);
            graph.RenderDepth();

            SetCam(ShadowFB.SetFace(5), cam);
            graph.RenderDepth();

            ShadowFB.Release();

            graph.RenderingShadows = false;
            graph.CamOverride = null;
        }

#pragma warning disable CS0114 // 'Light3D.Read()' hides inherited member 'Node3D.Read()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        public void Read()
#pragma warning restore CS0114 // 'Light3D.Read()' hides inherited member 'Node3D.Read()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        {
            LocalTurn = Help.IOHelp.ReadMatrix();
            LocalPos = Help.IOHelp.ReadVec3();
            Diff = Help.IOHelp.ReadVec3();
            Spec = Help.IOHelp.ReadVec3();
            Amb = Help.IOHelp.ReadVec3();
            Range = Help.IOHelp.ReadFloat();
            CastShadows = Help.IOHelp.ReadBool();
            EnableLight = Help.IOHelp.ReadBool();
        }

#pragma warning disable CS0114 // 'Light3D.Write()' hides inherited member 'Node3D.Write()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        public void Write()
#pragma warning restore CS0114 // 'Light3D.Write()' hides inherited member 'Node3D.Write()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        {
            Help.IOHelp.WriteMatrix(LocalTurn);
            Help.IOHelp.WriteVec(LocalPos);
            Help.IOHelp.WriteVec(Diff);
            Help.IOHelp.WriteVec(Spec);
            Help.IOHelp.WriteVec(Amb);
            Help.IOHelp.WriteFloat(Range);
            Help.IOHelp.WriteBool(CastShadows);
            Help.IOHelp.WriteBool(EnableLight);
        }

        private static void SetCam(TextureTarget f, Cam3D Cam)
        {
            switch (f)
            {
                case TextureTarget.TextureCubeMapPositiveX:
                    Cam.LookAtZero(new Vector3(1, 0, 0), new Vector3(0, -1, 0));
                    break;

                case TextureTarget.TextureCubeMapNegativeX:
                    Cam.LookAtZero(new Vector3(-1, 0, 0), new Vector3(0, -1, 0));
                    break;

                case TextureTarget.TextureCubeMapPositiveY:

                    Cam.LookAtZero(new Vector3(0, -1, 0), new Vector3(0, 0, -1));
                    break;

                case TextureTarget.TextureCubeMapNegativeY:
                    Cam.LookAtZero(new Vector3(0, 1, 0), new Vector3(0, 0, 1));
                    break;

                case TextureTarget.TextureCubeMapPositiveZ:
                    Cam.LookAtZero(new Vector3(0, 0, 1), new Vector3(0, -1, 0));
                    break;

                case TextureTarget.TextureCubeMapNegativeZ:
                    Cam.LookAtZero(new Vector3(0, 0, -1), new Vector3(0, -1, 0));
                    break;
            }
        }
    }
}
﻿using OpenTK.Mathematics;

using Vivid.App;
using Vivid.Help;

namespace Vivid.Scene
{
    public class Cam3D : Node3D
    {
        public Vector3 LR = new Vector3(0, 0, 0);
        public Matrix4 PP;

        public Matrix4 ProjMat
        {
            get
            {
                if (AppInfo.RW != 2048)
                {
                    int a = 0;
                }
                var nm = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(FOV), (float)AppInfo.RW / (float)AppInfo.RH, MinZ, MaxZ);

                return nm; ;
            }
        }

        public Vector3 ViewVec
        {
            get
            {
                Vector3 f = new Vector3(0, 0, 1);
                f = Vector3.TransformVector(f, World);
                return -f;
            }
        }

        public static bool GluProject(Vector3 objPos, Matrix4 matWorldViewProjection, int[] viewport, out Vector3 screenPos)
        {
            Vector4 _in;

            _in.X = objPos.X;
            _in.Y = objPos.Y;
            _in.Z = objPos.Z;
            _in.W = 1f;

            // -MATH
            Vector4 _out = Vector4.TransformRow(_in, matWorldViewProjection);

            if (_out.W <= 0.0)
            {
                screenPos = Vector3.Zero;
                return false;
            }

            _out.X /= _out.W;
            _out.Y /= _out.W;
            _out.Z /= _out.W;
            /* Map x, y and z to range 0-1 */
            _out.X = _out.X * 0.5f + 0.5f;
            _out.Y = -_out.Y * 0.5f + 0.5f;
            _out.Z = _out.Z * 0.5f + 0.5f;

            /* Map x,y to viewport */
            _out.X = _out.X * viewport[2] + viewport[0];
            _out.Y = _out.Y * viewport[3] + viewport[1];

            screenPos.X = _out.X;
            screenPos.Y = _out.Y;
            screenPos.Z = _out.Z;

            return true;
        }

        public Vector2 To2D(Vector3 pos)
        {
            var r = Project(new Vector4(pos, 1));
            r.Y = AppInfo.RH - r.Y;
            return r;
        }

        public Vector2 Project(
  Vector4 pos
  )
        {
            int screenWidth = AppInfo.RW;
            int screenHeight = AppInfo.RH;
            Matrix4 viewMatrix = CamWorld;
            Matrix4 projectionMatrix = ProjMat;

            // -MATH

            pos = Vector4.TransformRow(pos, viewMatrix);
            pos = Vector4.TransformRow(pos, projectionMatrix);
            pos.X /= pos.Z;
            pos.Y /= pos.Z;
            pos.X = (pos.X + 1) * screenWidth / 2;
            pos.Y = (pos.Y + 1) * screenHeight / 2;

            return new Vector2(pos.X, pos.Y);
        }

#pragma warning disable CS0114 // 'Cam3D.Write()' hides inherited member 'Node3D.Write()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        public void Write()
#pragma warning restore CS0114 // 'Cam3D.Write()' hides inherited member 'Node3D.Write()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        {
            IOHelp.WriteMatrix(LocalTurn);
            IOHelp.WriteVec(LocalPos);
            IOHelp.WriteFloat(MinZ);
            IOHelp.WriteFloat(MaxZ);
            IOHelp.WriteVec(LR);
        }

#pragma warning disable CS0114 // 'Cam3D.Read()' hides inherited member 'Node3D.Read()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        public void Read()
#pragma warning restore CS0114 // 'Cam3D.Read()' hides inherited member 'Node3D.Read()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        {
            LocalTurn = IOHelp.ReadMatrix();
            LocalPos = IOHelp.ReadVec3();
            MinZ = IOHelp.ReadFloat();
            MaxZ = IOHelp.ReadFloat();
            LR = IOHelp.ReadVec3();
        }

        public override void Rot(Vector3 r, Space s)
        {
            LR = r;
            CalcMat();
        }

        public override void Turn(Vector3 t, Space s)
        {
            LR = LR + t;
            CalcMat();

            //Matrix4 t = Matrix4.CreateRotationX(MathHelper.DegreesToRadians(r.X)) * Matrix4.CreateRotationY(MathHelper.DegreesToRadians(r.Y)) * Matrix4.CreateRotationZ(MathHelper.DegreesToRadians(r.Z));
            //LocalTurn = t * LocalTurn;
        }

        public void CalcMat()
        {
            //LR.X = Wrap(LR.X);
            // LR.Y = Wrap(LR.Y);
            //  LR.Z = Wrap(LR.Z);
            Vector3 r = LR;
            Matrix4 t = Matrix4.CreateRotationX(MathHelper.DegreesToRadians(r.X)) * Matrix4.CreateRotationY(MathHelper.DegreesToRadians(r.Y));// * Matrix4.CreateRotationZ(MathHelper.DegreesToRadians(r.Z));
            LocalTurn = t;
        }

        public float Wrap(float v)
        {
            if (v < 0)
            {
                v = 360 + v;
            }
            if (v > 360)
            {
                v = v - 360;
            }
            if (v < 0 || v > 360)
            {
                return Wrap(v);
            }

            return v;
        }

        public float FOV = 35;
        public bool DepthTest = true;
        public bool AlphaTest = false;
        public bool CullFace = true;
        public float MinZ = 0.1f, MaxZ = 3000;

        public Cam3D()
        {
            Rot(new Vector3(0, 0, 0), Space.Local);
        }

        public Matrix4 CamWorld
        {
            get
            {
                Matrix4 m = Matrix4.Invert(World);
                return m;
            }
        }

        public Matrix4 PrevCamWorld
        {
            get
            {
                Matrix4 m = Matrix4.Invert(PrevWorld);
                return m;
            }
        }
    }
}
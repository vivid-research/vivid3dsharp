﻿using OpenTK.Mathematics;

using System;
using System.Collections.Generic;

using Vivid.Data;
using Vivid.Material;
using Vivid.Visuals;

namespace Vivid.Scene
{
    public class LightMapOptions
    {
        public bool ReceiveLight = true;
        public bool CastShadows = true;

        public LightMapOptions()
        {
        }
    }

    public class Entity3D : Node3D
    {
#pragma warning disable CS0108 // 'Entity3D.CastShadows' hides inherited member 'Node3D.CastShadows'. Use the new keyword if hiding was intended.
        public bool CastShadows
#pragma warning restore CS0108 // 'Entity3D.CastShadows' hides inherited member 'Node3D.CastShadows'. Use the new keyword if hiding was intended.
        {
            get;
            set;
        }

        public Mesh3D BoundMesh;

#pragma warning disable CS0108 // 'Entity3D.GetName()' hides inherited member 'Node3D.GetName()'. Use the new keyword if hiding was intended.
        public string GetName()
#pragma warning restore CS0108 // 'Entity3D.GetName()' hides inherited member 'Node3D.GetName()'. Use the new keyword if hiding was intended.
        {
            return Name;
        }

        public Matrix4 GlobalInverse;

        public List<Mesh3D> Meshes
        {
            get
            {
                return _Meshes;
            }
            set
            {
                _Meshes = value;
            }
        }

        public bool NoRender
        {
            get;
            set;
        }

        public void RenderBounds()
        {
            if (BoundMesh != null)
            {
                BoundMesh.RenderMesh(World);
            }
        }

        private List<Mesh3D> _Meshes = new List<Mesh3D>();

        public void GenBoundMesh()
        {
            var bb = GetBounds2();
            if (bb.W < 0) return;
            BoundMesh = new Mesh3D(24 * 3, 8);
            Vector3 p1, p2, p3, p4, p5, p6, p7, p8;

            p1 = new Vector3(bb.MinX, bb.MinY, bb.MinZ);
            p2 = new Vector3(bb.MaxX, bb.MinY, bb.MinZ);
            p3 = new Vector3(bb.MaxX, bb.MaxY, bb.MinZ);
            p4 = new Vector3(bb.MinX, bb.MaxY, bb.MinZ);

            p5 = new Vector3(bb.MinX, bb.MinY, bb.MaxZ);
            p6 = new Vector3(bb.MaxX, bb.MinY, bb.MaxZ);
            p7 = new Vector3(bb.MaxX, bb.MaxY, bb.MaxZ);
            p8 = new Vector3(bb.MinX, bb.MaxY, bb.MaxZ);

            BoundMesh.SetVertex(0, p1);
            BoundMesh.SetVertex(1, p2);
            BoundMesh.SetVertex(2, p3);
            BoundMesh.SetVertex(3, p4);
            BoundMesh.SetVertex(4, p5);
            BoundMesh.SetVertex(5, p6);
            BoundMesh.SetVertex(6, p7);
            BoundMesh.SetVertex(7, p8);

            Tri t1, t2, t3, t4, t5, t6, t7, t8;

            BoundMesh.SetTri(0, new Tri(0, 1, 2));
            BoundMesh.SetTri(1, new Tri(2, 3, 0));
            BoundMesh.SetTri(12, new Tri(0, 2, 1));
            BoundMesh.SetTri(13, new Tri(2, 0, 3));

            BoundMesh.SetTri(2, new Tri(4, 5, 6));
            BoundMesh.SetTri(3, new Tri(6, 7, 4));
            BoundMesh.SetTri(14, new Tri(4, 6, 5));
            BoundMesh.SetTri(15, new Tri(6, 4, 7));

            BoundMesh.SetTri(4, new Tri(0, 4, 7));
            BoundMesh.SetTri(5, new Tri(7, 3, 0));
            BoundMesh.SetTri(16, new Tri(0, 7, 4)); ;
            BoundMesh.SetTri(17, new Tri(7, 0, 3));

            BoundMesh.SetTri(6, new Tri(1, 5, 7));
            BoundMesh.SetTri(7, new Tri(7, 2, 1));
            BoundMesh.SetTri(18, new Tri(1, 7, 5));
            BoundMesh.SetTri(19, new Tri(7, 1, 2));

            BoundMesh.SetTri(8, new Tri(3, 2, 6));
            BoundMesh.SetTri(9, new Tri(6, 7, 3));
            BoundMesh.SetTri(20, new Tri(3, 6, 2));
            BoundMesh.SetTri(21, new Tri(6, 3, 7));

            BoundMesh.SetTri(10, new Tri(0, 1, 5));
            BoundMesh.SetTri(11, new Tri(5, 4, 0));
            BoundMesh.SetTri(22, new Tri(0, 5, 1));
            BoundMesh.SetTri(23, new Tri(5, 0, 4));

            BoundMesh.Material = new Material3D();

            BoundMesh.Final();

            Console.WriteLine("Created bounds: W:" + bb.W + " H:" + bb.H + " D:" + bb.D);

            //Console.WriteLine("w:")
        }

        public void SetMatDiff(Vector3 dif)
        {
            foreach (var msh in _Meshes)
            {
                msh.Material.Diff = dif;
            }
            foreach (var sub in Sub)
            {
                if (sub is Entity3D)
                {
                    var se = sub as Entity3D;
                    se.SetMatDiff(dif);
                }
            }
        }

        // -PHYSX
        //   public Physics.PyObject PO = null;
        //   public Physics.PyType PyType;
        public Renderer Renderer = null;

        private float bw, bh, bd;

        private float sw, sh, sd;

        public LightMapOptions LightMapInfo = new LightMapOptions();

        public Entity3D()
        {
            CastShadows = true;
            NoRender = false;
        }

        public Bounds Bounds
        {
            get
            {
                sw = sh = sd = 20000;
                bw = bh = bd = -20000;
                GetBounds(this);
                Bounds res = new Bounds
                {
                    W = bw - sw,
                    H = bh - sh,
                    D = bd - sd,
                    MinX = sw,
                    MaxX = bw,
                    MinY = sh,
                    MaxY = bh,
                    MinZ = sd,
                    MaxZ = bd
                };
                return res;
            }
        }

        public void AddMesh(Mesh3D mesh)
        {
            Meshes.Add(mesh);
        }

        public virtual void Bind()
        {
        }

        public void Clean()
        {
            Meshes = new List<Mesh3D>();
            Renderer = null;
        }

        public void AddTorque(Vector3 tr)
        {
            // -PHYSX
            //var pd = PO as Physics.PyDynamic;
            //pd.AddTorque(tr);
        }

        public void AddForce(Vector3 force)
        {
            // -PHYSX
            //  var pd = PO as Physics.PyDynamic;
            //  pd.ApplyForce(force);
        }

        public void EnablePy(Physics.PyType type)
        {
            // -PHYSX
            /*
            PyType = type;
            switch (PyType)
            {
                case Physics.PyType.Box:
                    PO = new Physics.PyDynamic(type, this);
                    break;

                case Physics.PyType.Mesh:
                    PO = new Physics.PyStatic(this);
                    break;
            }
            */
        }

        public Bounds GetBound()
        {
            Bounds r = new Bounds();

            foreach (var mesh in Meshes)
            {
                foreach (var vert in mesh.VertexData)
                {
                    var pos = vert.Pos;

                    var npos = Vector3.TransformPosition(pos, World);

                    if (npos.X < r.MinX) r.MinX = npos.X;
                    if (npos.Y < r.MinY) r.MinY = npos.Y;
                    if (npos.Z < r.MinZ) r.MinZ = npos.Z;
                    if (npos.X > r.MaxX) r.MaxX = npos.X;
                    if (npos.Y > r.MaxY) r.MaxY = npos.Y;
                    if (npos.Z > r.MaxZ) r.MaxZ = npos.Z;
                }
                //var vert = mesh.VertexData[in
            }

            r.W = r.MaxX - r.MinX;
            r.H = r.MaxY - r.MinY;
            r.D = r.MaxZ - r.MinZ;

            return r;
        }

        public Bounds GetBounds2()
        {
            Bounds bb = new Bounds();
            foreach (Mesh3D m in Meshes)
            {
                for (int i = 0; i < m.VertexData.Length; i++)
                {
                    var vr = m.VertexData[i].Pos;

                    int vid = i * 3;
                    if (vr.X < bb.MinX)
                    {
                        bb.MinX = vr.X;
                    }
                    if (vr.X > bb.MaxX)
                    {
                        bb.MaxX = vr.X;
                    }
                    if (vr.Y < bb.MinY)
                    {
                        bb.MinY = vr.Y;
                    }
                    if (vr.Y > bb.MaxY)
                    {
                        bb.MaxY = vr.Y;
                    }
                    if (vr.Z < bb.MinZ)
                    {
                        bb.MinZ = vr.Z;
                    }
                    if (vr.Z > bb.MaxZ)
                    {
                        bb.MaxZ = vr.Z;
                    }
                }
            }
            bb.W = bb.MaxX - bb.MinX;
            bb.H = bb.MaxY - bb.MinY;
            bb.D = bb.MaxZ - bb.MinZ;
            return bb;
        }

        public void GetBounds(Entity3D node)
        {
            foreach (Mesh3D m in node.Meshes)
            {
                if (m.VertexData == null) return;
                for (int i = 0; i < m.VertexData.Length; i++)
                {
                    var vr = m.VertexData[i].Pos;

                    int vid = i * 3;
                    if (vr.X < sw)
                    {
                        sw = vr.X;
                    }
                    if (vr.X > bw)
                    {
                        bw = vr.X;
                    }
                    if (vr.Y < sh)
                    {
                        sh = vr.Y;
                    }
                    if (vr.Y > bh)
                    {
                        bh = vr.Y;
                    }
                    if (vr.Z < sd)
                    {
                        sd = vr.Z;
                    }
                    if (vr.Z > bd)
                    {
                        bd = vr.Z;
                    }
                }
            }
            foreach (Node3D snode in node.Sub)
            {
                if (snode is Entity3D || snode is Terrain.Terrain3D)
                {
                    GetBounds(snode as Entity3D);
                }
            }
        }

        public List<Vector3> GetAllVerts()
        {
            Entity3D node = this;
            List<Vector3> nl = new List<Vector3>();
            GetVerts(nl, this);
            return nl;
        }

        public List<int> GetAllTris()
        {
            List<int> l = new List<int>();
            GetTris(l, this);
            return l;
        }

        public void GetTris(List<int> tris, Entity3D node)
        {
            foreach (Mesh3D m in Meshes)
            {
                for (int i = 0; i < m.TriData.Length; i++)
                {
                    tris.Add(m.TriData[i].V0);
                    tris.Add(m.TriData[i].V1);
                    tris.Add(m.TriData[i].v2);
                }
            }
            foreach (var s_node in Sub)
            {
                GetTris(tris, (Entity3D)s_node);
            }
        }

        public void GetVerts(List<Vector3> verts, Entity3D node)
        {
            foreach (Mesh3D m in node.Meshes)
            {
                for (int i = 0; i < m.VertexData.Length; i++)
                {
                    //int vid = i * 3;

                    Vector3 nv = m.VertexData[i].Pos;// new Vector3(m.Vertices[vid], m.Vertices[vid + 1], m.Vertices[vid + 2]);
                    nv = Vector3.TransformPosition(nv, node.World);
                    verts.Add(nv);
                    // verts.Add(m.Vertices[vid]); verts.Add(m.Vertices[vid + 1]);
                    // verts.Add(m.Vertices[vid + 2]);
                }
            }
            foreach (Node3D snode in node.Sub)
            {
                GetVerts(verts, snode as Entity3D);
            }
        }

        public override void Init()
        {
            // Renderer = new VRMultiPass();
        }

        public virtual void PostRender()
        {
        }

        /// <summary> To be called AFTER data asscoiation.
#pragma warning disable CS1570 // XML comment has badly formed XML -- 'Expected an end tag for element 'summary'.'
        public virtual void PreRender()
#pragma warning restore CS1570 // XML comment has badly formed XML -- 'Expected an end tag for element 'summary'.'
        {
        }

        public override void Present(Cam3D c)
        {
            // GL.MatrixMode(MatrixMode.Projection); GL.LoadMatrix(ref c.ProjMat);
            if (NoRender) return;
            SetMats(c);
            Bind();
            PreRender();
            Render();
            PostRender();
            Release();
            // foreach (var s in Sub) { s.Present(c); }
        }

        public void PresentSSRExtrasMap(Cam3D c)
        {
            SetMats(c);
            Bind();
            PreRender();
            RenderSSRExtrasMap();
            PostRender();
            Release();
        }

        public void PresentPositionMap(Cam3D c)
        {
            SetMats(c);
            Bind();
            PreRender();
            RenderPositionMap();
            PostRender();
            Release();
        }

        public void PresentNormalMap(Cam3D c)
        {
            SetMats(c);
            Bind();
            PreRender();
            RenderNormalMap();
            PostRender();
            Release();
        }

        public override void PresentDepth(Cam3D c)
        {
            SetMats(c);
            Bind();
            PreRender();
            RenderDepth();
            PostRender();
            Release();
            foreach (Node3D s in Sub)
            {
                //     s.PresentDepth ( c );
            }
        }

        public override void Read()
        {
            LocalTurn = Help.IOHelp.ReadMatrix();
            LocalPos = Help.IOHelp.ReadVec3();
            LocalScale = Help.IOHelp.ReadVec3();
            Name = Help.IOHelp.ReadString();

            AlwaysAlpha = Help.IOHelp.ReadBool();
            On = Help.IOHelp.ReadBool();

            int mc = Help.IOHelp.ReadInt();
            for (int m = 0; m < mc; m++)
            {
                Mesh3D msh = new Mesh3D();
                msh.Read();
                //Meshes.Add ( msh );
                AddMesh(msh);
            }
            ReadScripts();
            SetMultiPass();
        }

        public Entity3D Clone()
        {
            var new_ent = new Entity3D();
            new_ent.Meshes = Meshes;
            new_ent.Renderer = Renderer;
            new_ent.NoSave = NoSave;
            new_ent.LocalPos = LocalPos;
            new_ent.LocalTurn = LocalTurn;
            new_ent.Top = Top;
            new_ent.LocalScale = LocalScale;
            new_ent.NodeScripts = NodeScripts;

            foreach (var sub in Sub)
            {
                if (sub is Entity3D)
                {
                    var se = sub as Entity3D;
                    new_ent.Sub.Add(se.Clone());
                }
            }

            return new_ent;
        }

        public void MakeQuad(float size)
        {
            Mesh3D m = new Mesh3D(12, 4);
            m.SetVertex(0, new Vector3(-size, 0, -size), Vector3.Zero, Vector3.Zero, Vector3.Zero, new Vector2(0, 0));
            m.SetVertex(1, new Vector3(size, 0, -size), Vector3.Zero, Vector3.Zero, Vector3.Zero, new Vector2(1, 0));
            m.SetVertex(2, new Vector3(size, 0, size), Vector3.Zero, Vector3.Zero, Vector3.Zero, new Vector2(1, 1));
            m.SetVertex(3, new Vector3(-size, 0, size), Vector3.Zero, Vector3.Zero, Vector3.Zero, new Vector2(0, 1));

            m.SetTri(0, 0, 1, 2);
            m.SetTri(1, 2, 3, 0);
            m.SetTri(2, 0, 2, 1);
            m.SetTri(3, 2, 0, 3);

            m.Material = new Material3D();

            m.Final();

            AddMesh(m);

            Renderer = new VRNoFx();
        }

        public virtual void Release()
        {
        }

        public virtual void Render()
        {
            Effect.FXG.Ent = this;
            foreach (Mesh3D m in Meshes)
            {
                Effect.FXG.Mesh = m;
                Renderer.Render(m);
            }
        }

        public Vivid.Pick.PickResult CamPick(int x, int y)
        {
            Pick.PickResult res = new Pick.PickResult();

            Pick.Ray mr = Pick.Picker.CamRay(SceneGraph3D.CurScene.Cams[0], x, y);

            float cd = 0;
            bool firstHit = true;
            Mesh3D hitMesh = null;
            float cu = 0, cv = 0;

            foreach (var msh in Meshes)
            {
                for (int i = 0; i < msh.TriData.Length; i++)
                {
                    var td = msh.TriData[i];

                    var r0 = msh.VertexData[td.V0].Pos;
                    var r1 = msh.VertexData[td.V1].Pos;
                    var r2 = msh.VertexData[td.v2].Pos;

                    r0 = Vector3.TransformPosition(r0, World);
                    r1 = Vector3.TransformPosition(r1, World);
                    r2 = Vector3.TransformPosition(r2, World);

                    Vector3? pr = Pick.Picker.GetTimeAndUvCoord(mr.pos, mr.dir, r0, r1, r2);
                    if (pr == null)
                    {
                    }
                    else
                    {
                        Vector3 cr = (Vector3)pr;
                        if (cr.X < cd || firstHit)
                        {
                            firstHit = false;
                            cd = cr.X;
                            hitMesh = msh;

                            cu = cr.Y;
                            cv = cr.Z;
                        }
                    }
                }
            }

            if (firstHit)
            {
                return null;
            }

            res.Dist = cd;
            res.Node = this;
            res.Pos = Pick.Picker.GetTrilinearCoordinateOfTheHit(cd, mr.pos, mr.dir);
            res.Ray = mr;
            res.UV = new Vector3(cu, cv, 0);
            res.Mesh = hitMesh;

            return res;
        }

        public virtual void RenderSSRExtrasMap()
        {
            Effect.FXG.Ent = this;
            foreach (Mesh3D m in Meshes)
            {
                Effect.FXG.Mesh = m;
                Renderer.RenderSSRExtrasMap(m);
            }
        }

        public virtual void RenderPositionMap()
        {
            Effect.FXG.Ent = this;
            foreach (Mesh3D m in Meshes)
            {
                Effect.FXG.Mesh = m;
                Renderer.RenderPositionMap(m);
            }
        }

        public virtual void RenderNormalMap()
        {
            Effect.FXG.Ent = this;
            foreach (Mesh3D m in Meshes)
            {
                Effect.FXG.Mesh = m;
                Renderer.RenderNormalMap(m);
            }
        }

        public virtual void RenderDepth()
        {
            Effect.FXG.Ent = this;
            foreach (Mesh3D m in Meshes)
            {
                Effect.FXG.Mesh = m;
                Renderer.RenderDepth(m);
            }
        }

        public void ScaleMeshes(float x, float y, float z)
        {
            DScale(x, y, z, this);
        }

        public void SetMat(Material3D mat)
        {
            foreach (Mesh3D m in Meshes)
            {
                m.Material = mat;
            }
            foreach (Node3D n in Sub)
            {
                if (n is Entity3D || n is Terrain.Terrain3D)
                {
                    ;
                }

                {
                    Entity3D ge = n as Entity3D;
                    ge.SetMat(mat);
                }
            }
        }

        public void SetMats(Cam3D c)
        {
            int w = App.AppInfo.RW;
            int h = App.AppInfo.RH;
            int aw = App.AppInfo.W;
            int ah = App.AppInfo.H;
            Effect.FXG.Proj = c.ProjMat;
            Effect.FXG.Cam = c;
            // GL.MatrixMode(MatrixMode.Modelview);
            Matrix4 mm = Matrix4.Identity;
            // mm = c.CamWorld;
            //mm = mm * Matrix4.Invert(Matrix4.CreateTranslation(c.WorldPos));

            mm = World;
            //var wp = LocalPos;
            //mm = mm*Matrix4.CreateTranslation(wp);
            //GL.LoadMatrix(ref mm);
            Effect.FXG.Local = mm;
            Effect.FXG.PrevLocal = PrevWorld;
        }

        public override void Write()
        {
            Help.IOHelp.WriteMatrix(LocalTurn);
            Help.IOHelp.WriteVec(LocalPos);
            Help.IOHelp.WriteVec(LocalScale);
            Help.IOHelp.WriteString(Name);

            Help.IOHelp.WriteBool(AlwaysAlpha);
            Help.IOHelp.WriteBool(On);

            int mc = Meshes.Count;
            Help.IOHelp.WriteInt(mc);
            foreach (Mesh3D msh in Meshes)
            {
                msh.Write();
            }
            WriteScripts();
        }

        public int SubSaveCount()
        {
            int num = 0;
            foreach (Node3D node in Sub)
            {
                if (!node.NoSave)
                {
                    num++;
                }
            }
            return num;
        }

        private void DScale(float x, float y, float z, Entity3D node)
        {
            foreach (Mesh3D m in node.Meshes)
            {
                m.Scale(x, y, z);
            }
            foreach (Node3D snode in node.Sub)
            {
                DScale(x, y, z, snode as Entity3D);
            }
        }
    }
}
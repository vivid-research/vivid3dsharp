﻿using OpenTK.Graphics.OpenGL4;
using OpenTK.Mathematics;

using System.Collections.Generic;
using System.IO;
using System.Threading;

using Vivid.Data;
using Vivid.Effect;
using Vivid.Reflect;
using Vivid.Resonance.Forms;
using Vivid.Scene.Node;
using Vivid.Scene.Optimized;

namespace Vivid.Scene
{
    public class SceneGraph3D
    {
        public Cam3D CamOverride = null;

        // public List<GraphNode3D> Nodes = new List<GraphNode3D>();
        public List<Cam3D> Cams = new List<Cam3D>();

        public List<Light3D> Lights = new List<Light3D>();

        public Node3D Root = new Entity3D();

        public SceneGraph3D SubGraph = null;

        public List<Vector3> Verts;
        public List<int> Tris;
        public bool SceneCached = false;

        public Vivid.Resonance.UI UI = null;

        public static SceneGraph3D Graph = null;

        public List<MeshLines> LineMeshes = new List<MeshLines>();

        public List<Entity3D> AlwaysOnTop = new List<Entity3D>();

        public bool RenderingShadows
        {
            get;
            set;
        }

        public bool Running
        {
            get;
            set;
        }

        public bool Paused
        {
            get;
            set;
        }

        public ClassIO ClassCopy
        {
            get;
            set;
        }

        private Bounds Working = null;

        public bool OcclusionCulling = false;

        public void EnableOccCulling()
        {
            OcclusionCulling = true;
            GenMeshBounds();
        }

        public void GenMeshBounds()
        {
            GenMB(Root);
        }

        private void GenMB(Node3D node)
        {
            if (node is Entity3D && node.NoSave == false)
            {
                var ent = node as Entity3D;
                ent.GenBoundMesh();
            }
            foreach (var sn in node.Sub)
            {
                GenMB(sn);
            }
        }

        public Bounds GetBounds(bool includeDynamic)
        {
            Working = new Bounds();

            if (!includeDynamic)
            {
                CheckBounds(Root, includeDynamic);
            }

            return Working;
        }

        private Bounds TriBB = null;
        private int TriC = 0;

        public SceneOctree Oct = null;
        public Effect.FXMultiPass3D OctFX;

        public List<Mesh3D> GetBoundsData(Bounds bb)
        {
            List<Mesh3D> meshes = new List<Mesh3D>();
            TriBB = bb;
            AddTriData(meshes, Root);

            return meshes;
        }

        public void AddTriData(List<Mesh3D> meshes, Node3D node)
        {
            if (node.Type == NodeType.Static)
            {
                if (node is Entity3D)
                {
                    if (node.NoSave) goto Skip2;
                    var ent = node as Entity3D;
                    //int tc = 0;
                    foreach (var mesh in ent.Meshes)
                    {
                        var vdat = mesh.VertexData;
                        Mesh3D cur = new Mesh3D();
                        cur.VertexData = new Vertex[mesh.VertexData.Length];
                        cur.TriData = new Tri[mesh.TriData.Length];
                        int tc = 0;
                        int vc = 0;
                        //meshes.Add(cur);

                        foreach (var tri in mesh.TriData)
                        {
                            Vector3 v0, v1, v2;

                            v0 = vdat[tri.V0].Pos;
                            v1 = vdat[tri.V1].Pos;
                            v2 = vdat[tri.v2].Pos;

                            v0 = Vector3.TransformPosition(v0, ent.World);
                            v1 = Vector3.TransformPosition(v1, ent.World);
                            v2 = Vector3.TransformPosition(v2, ent.World);

                            bool inBB(Vector3 pos, Bounds bb)
                            {
                                if (pos.X >= bb.MinX && pos.X <= bb.MaxX)
                                {
                                    if (pos.Y >= bb.MinY && pos.Y <= bb.MaxY)
                                    {
                                        if (pos.Z >= bb.MinZ && pos.Z <= bb.MaxZ)
                                        {
                                            return true;
                                        }
                                    }
                                }
                                return false;
                            }

                            if (inBB(v0, TriBB) || inBB(v1, TriBB) || inBB(v2, TriBB))
                            {
                                Vertex Transform(Vertex v, Matrix4 world)
                                {
                                    v.Pos = Vector3.TransformPosition(v.Pos, world);
                                    v.Norm = Vector3.TransformNormal(v.Norm, world);
                                    v.BiNorm = Vector3.TransformNormal(v.BiNorm, world);
                                    v.Tan = Vector3.TransformNormal(v.Tan, world);

                                    return v;
                                }

                                int vb = vc;
                                cur.SetVertex(vc++, Transform(vdat[tri.V0], ent.World));
                                cur.SetVertex(vc++, Transform(vdat[tri.V1], ent.World));
                                cur.SetVertex(vc++, Transform(vdat[tri.v2], ent.World));

                                var nt = new Tri();

                                nt.V0 = vb;
                                nt.V1 = vb + 1;
                                nt.v2 = vb + 2;

                                cur.SetTri(tc, nt);

                                tc++;
                                //tc++;
                                //if (tc == 1)
                                //{
                                //}
                            }
                        }

                        if (tc > 0)
                        {
                            cur.Final();
                            cur.Material = mesh.Material;
                            meshes.Add(cur);
                        }
                    }
                }
            }

        Skip2:

            foreach (var sn in node.Sub)
            {
                AddTriData(meshes, sn);
            }
        }

        public int BoundsTris(Bounds bb)
        {
            TriC = 0;
            TriBB = bb;
            AddTris(Root);
            return TriC;
        }

        private void AddTris(Node3D node)
        {
            if (node.Type == NodeType.Dynamic)
            {
            }
            else
            {
                if (node is Entity3D)
                {
                    var ent = node as Entity3D;

                    foreach (var mesh in ent.Meshes)
                    {
                        var vdat = mesh.VertexData;
                        foreach (var tri in mesh.TriData)
                        {
                            Vector3 v0, v1, v2;

                            v0 = vdat[tri.V0].Pos;
                            v1 = vdat[tri.V1].Pos;
                            v2 = vdat[tri.v2].Pos;

                            v0 = Vector3.TransformPosition(v0, ent.World);
                            v1 = Vector3.TransformPosition(v1, ent.World);
                            v2 = Vector3.TransformPosition(v2, ent.World);

                            bool inBB(Vector3 pos, Bounds bb)
                            {
                                if (pos.X >= bb.MinX && pos.X <= bb.MaxX)
                                {
                                    if (pos.Y >= bb.MinY && pos.Y <= bb.MaxY)
                                    {
                                        if (pos.Z >= bb.MinZ && pos.Z <= bb.MaxZ)
                                        {
                                            return true;
                                        }
                                    }
                                }
                                return false;
                            }

                            if (inBB(v0, TriBB) || inBB(v1, TriBB) || inBB(v2, TriBB))
                            {
                                TriC++;
                            }
                        }
                    }
                }
            }
            foreach (var sn in node.Sub)
            {
                AddTris(sn);
            }
        }

        public void CheckBounds(Node3D node, bool dynamic)
        {
            if (node.Type == NodeType.Dynamic && !dynamic)
            {
                goto Skip1;
            }
            if (node.NoSave)
            {
                goto Skip1;
            }
            if (node is Entity3D)
            {
                var ent = node as Entity3D;

                Bounds eb = ent.GetBound();

                if (eb.MinX < Working.MinX) Working.MinX = eb.MinX;
                if (eb.MinY < Working.MinY) Working.MinY = eb.MinY;
                if (eb.MinZ < Working.MinZ) Working.MinZ = eb.MinZ;
                if (eb.MaxX > Working.MaxX) Working.MaxX = eb.MaxX;
                if (eb.MaxY > Working.MaxY) Working.MaxY = eb.MaxY;
                if (eb.MaxZ > Working.MaxZ) Working.MaxZ = eb.MaxZ;

                Working.W = Working.MaxX - Working.MinX;
                Working.H = Working.MaxY - Working.MinY;
                Working.D = Working.MaxZ - Working.MinZ;

                //foreach(var mesh in ent.Meshes)
                //{
                //}
            }
        Skip1:
            foreach (var sn in node.Sub)
            {
                CheckBounds(sn, dynamic);
            }
        }

        public void CheckAssets()
        {
            CheckAssets(Root);
        }

        public void NodeRender()
        {
            if (!Running) return;
            Root.ScriptRender();
        }

        public void CheckAssets(Node3D node)
        {
        Recheck:
            foreach (var scr in node.NodeScripts)
            {
                if (scr.Updated)
                {
                    System.Console.WriteLine("Updated:" + scr.FullPath);
                    node.NodeScripts.Remove(scr);

                    Vivid.Scripting.NodeComponent ns = Vivid.Scripting.NodeScriptCompiler.Compile(scr.FullPath);
                    //ns.Node = node;


                    //  scr.Transfer(ns);

                    //node.NodeScripts.Add(ns);


                    goto Recheck;
                }
            }

            foreach (var sub_n in node.Sub)
            {
                CheckAssets(sub_n);
            }
        }

        public void InitUIScripts(Vivid.Resonance.UI ui)
        {
            InitUINodeScripts(Root, ui);
        }

        public void InitUINodeScripts(Node3D node, Vivid.Resonance.UI ui)
        {
            node.InitUIScripts(ui);
            foreach (var s_node in node.Sub)
            {
                InitUINodeScripts(s_node, ui);
            }
        }

        public void InitScripts()
        {
            InitNodeScripts(Root);
        }

        public static SceneGraph3D CurScene = null;

        public static void AddToScene(Node3D node)
        {
            CurScene.Root.Add(node);
            node.Top = CurScene.Root;
        }

        public static void LoadScene(string path)
        {
            // Vivid.Audio.Audio.StopSong();

            CurScene.Root = new Node3D();
            CurScene.LoadGraph(path);
            CurScene.UI.Root = new Vivid.Resonance.UIForm().Set(0, 0, App.AppInfo.W, App.AppInfo.H);
            CurScene.InitScripts();
            CurScene.InitUIScripts(CurScene.UI);
        }

        public void InitNodeScripts(Node3D node)
        {
            node.InitScripts();
            //foreach(var s_node in node.Sub)
            //{
            for (int nn = 0; nn < node.Sub.Count; nn++)
            {
                var s_node = node.Sub[nn];
                InitNodeScripts(s_node);
            }
        }

        public void UpdateScripts()
        {
            UpdateNodeScripts(Root);
        }

        public void UpdateNodeScripts(Node3D node)
        {
            node.UpdateScripts();
            foreach (var s_node in node.Sub)
            {
                UpdateNodeScripts(s_node);
            }
        }

        public void DrawScripts()
        {
            DrawNodeScripts(Root);
        }

        public void DrawNodeScripts(Node3D node)
        {
            node.DrawScripts();
            foreach (var s_node in node.Sub)
            {
                DrawNodeScripts(s_node);
            }
        }

        public virtual void Add(Cam3D c)
        {
            Cams.Add(c);
        }

        public virtual void Add(Light3D l)
        {
            Lights.Add(l);
            Root.Add(l);
        }

        public virtual void Add(Node3D n)
        {
            Root.Add(n);
            n.Top = Root;
            SceneCached = false;
            SceneChanged = true;
            n.Graph = this;
        }

        public virtual void Rebuild()
        {
            SceneCached = false;
            SceneChanged = true;
        }

        public void Begin()
        {
            if (Running) return;
            if (Paused)
            {
                Pause();
                return;
            }
            Copy();
            Root.Begin();
            //while (true)

            Running = true;
        }

        public void BeginFrame()

        {
            BeginFrameNode(Root);
            foreach (Cam3D c in Cams)
            {
                c.StartFrame();
            }
        }

        public SceneGraph3D()
        {
            InitThreads();
            Running = false;
            Paused = false;
            RenderingShadows = false;
            Root = new Node3D()
            {
                Name = "Root"
            };
            CurScene = this;
        }

        public void Remove(Node3D node)
        {
            node.Top.Sub.Remove(node);
            node.Top = null;

            //RemoveNode(Root, node);
        }

        public Node3D FindNode(string name)
        {
            if (Root == null) return null;
            return FindNode(Root, name);
        }

        public Node3D FindNode(Node3D s, string name)
        {
            if (s.Name == name)
            {
                return s;
            }
            foreach (var sub_n in s.Sub)
            {
                var fn = FindNode(sub_n, name);
                if (fn != null)
                {
                    return fn;
                }
            }
            return null;
        }

        public void RemoveNode(Node3D s, Node3D r)
        {
            if (s == r)
            {
            }
        }

        public void BeginFrameNode(Node3D node)
        {
            node.StartFrame();
            foreach (Node3D snode in node.Sub)
            {
                BeginFrameNode(snode);
            }
        }

        public void BeginRun()
        {
            Begin();
        }

        public virtual void Bind()
        {
        }

        public List<Vector3> Vert = new List<Vector3>();

        public int MaxThreads = 64;
        public Thread[] SceneTransforms = null;
        public Thread SceneCheck = null;

        public bool SceneChanged
        {
            get
            {
                return _Changed;
            }
            set
            {
                _Changed = value;
                if (_Changed == true)
                {
                    Edited?.Invoke();
                }
            }
        }

        public delegate void SceneEdited();

        public SceneEdited Edited = null;
        private bool _Changed = false;
        public Vector3[] SceneCache = null;
        public Entity3D[] SceneNodes = null;
        public Mesh3D[] SceneMeshes = null;
        public SceneIntristics[] SceneInfos = null;
        public Dictionary<int, Entity3D> SceneLut = new Dictionary<int, Entity3D>();

        public void RenderOnTop()
        {
            foreach (var ent in AlwaysOnTop)
            {
                RenderTop(ent);
                //
                //ent.Present(Cams[0]);
               // foreach(var sue in ent.Sub)
               // {
                //    RenderTop()
                //}
            }
        }

        public void RenderTop(Entity3D top)
        {
            top.Present(Cams[0]);
            foreach(var se in top.Sub)
            {
                RenderTop((Entity3D)se);
            }
        }

        public class SceneIntristics
        {
            public List<Entity3D> Ents = new List<Entity3D>();
            public bool Done = false;
            public int Begin = 0, End = 0;
            public Thread Thr = null;
            public int ThreadID = 0;
        }

        public bool SceneUpdating = false;

        public void InitThreads()

        {

            void CheckThread()
            {
                if (SceneTransforms == null)
                {
                    SceneTransforms = new Thread[MaxThreads];
                }
                while (true)
                {
                    if (SceneUpdating)
                    {
                        int dc = 0;

                        foreach (var th in SceneInfos)
                        {
                            if (th.Done) dc++;
                        }
                        if (dc == SceneInfos.Length)
                        {
                            SceneUpdating = false;
                        }
                    }
                    if (SceneChanged && SceneUpdating == false)
                    {
                        SceneUpdating = true;
                        SceneChanged = false;
                        foreach (var thr in SceneTransforms)
                        {
                            if (thr == null) continue;
                            if (thr.ThreadState == ThreadState.Running)
                            {
                                thr.Abort();
                            }
                        }

                        List<Node3D> node_list = this.GetList(true);
                        int vc = 0;
                        int bc = 0;
                        foreach (var n in node_list)
                        {
                            var ent = n as Entity3D;

                            foreach (var m in ent.Meshes)
                            {
                                vc = vc + m.TriData.Length * 3;
                                bc = bc + m.TriData.Length * 3;
                            }
                        }
                        SceneCache = new Vector3[vc];
                        SceneNodes = new Entity3D[vc];
                        SceneMeshes = new Mesh3D[vc];

                        SceneInfos = new SceneIntristics[MaxThreads];

                        int pnc = node_list.Count / MaxThreads;

                        int cnc = 0;
                        int tid = 0;
                        for (int i = 0; i < MaxThreads; i++)
                        {
                            SceneInfos[i] = new SceneIntristics();
                            SceneInfos[i].Done = true;
                        }
                        bool first = true;
                        int vv = 0;
                        foreach (var nl in node_list)
                        {
                            if (first)
                            {
                                first = false;
                                SceneInfos[tid].Begin = vv;
                                SceneInfos[tid].Done = false;
                            }
                            SceneInfos[tid].Ents.Add(nl as Entity3D);
                            Entity3D me = nl as Entity3D;
                            foreach (var msh in me.Meshes)
                            {
                                vv = vv + msh.TriData.Length * 3;
                            }

                            cnc++;
                            if (cnc >= pnc)
                            {
                                SceneInfos[tid].End = vv;
                                cnc = 0;
                                tid++;
                                first = true;
                            }
                        }
                        SceneLut.Clear();
                        tid = 0;
                        foreach (var info in SceneInfos)
                        {
                            if (info.Ents.Count > 0)
                            {
                                void procScene(object obj)
                                {
                                    SceneIntristics si = obj as SceneIntristics;
                                    int id = si.ThreadID;
                                    int sv = si.Begin;
                                    System.Console.WriteLine("Begin:" + si.Begin + " End:" + si.End);
                                    foreach (var node in si.Ents)
                                    {
                                        System.Console.WriteLine("Node:" + node.Name);
                                        Matrix4 n_m = node.World;
                                        foreach (var msh in node.Meshes)
                                        {
                                            System.Console.WriteLine("Mesh:" + msh.TriData.Length);

                                            for (int i = 0; i < msh.TriData.Length; i++)
                                            {
                                                int v0 = msh.TriData[i].V0;
                                                int v1 = msh.TriData[i].V1;
                                                int v2 = msh.TriData[i].v2;
                                                Vector3 r0, r1, r2;
                                                r0 = Rot(msh.VertexData[v0].Pos, node);
                                                r1 = Rot(msh.VertexData[v1].Pos, node);
                                                r2 = Rot(msh.VertexData[v2].Pos, node);
                                                SceneNodes[sv] = node;
                                                SceneNodes[sv + 1] = node;
                                                SceneNodes[sv + 2] = node;
                                                SceneMeshes[sv] = msh;
                                                SceneMeshes[sv + 1] = msh;
                                                SceneMeshes[sv + 2] = msh;
                                                SceneCache[sv++] = r0;
                                                SceneCache[sv++] = r1;
                                                SceneCache[sv++] = r2;
                                            }
                                        }
                                    }
                                    si.Done = true;
                                    System.Console.WriteLine("SceneThread:" + id + " complete.");
                                }

                                info.ThreadID = tid;
                                info.Thr = new Thread(new ParameterizedThreadStart(procScene));
                                info.Thr.Start(info);
                            }
                            tid++;
                        }

                        System.Console.WriteLine("Checked:" + node_list.Count + " nodes");
                        //SceneChanged = false;
                    }
                    Thread.Sleep(5);
                }
            }

            SceneCheck = new Thread(new ThreadStart(CheckThread));
            SceneCheck.Start();
        }


        public void AddOnTop(Entity3D ent)
        {
            AlwaysOnTop.Add(ent);
            foreach(var sub in ent.Sub)
            {
                AddOnTop((Entity3D)sub);
            }
        }


        public Vivid.Pick.PickResult CamPickOnTop(int x, int y)
        {
            Pick.PickResult res = new Pick.PickResult();

            Pick.Ray mr = Pick.Picker.CamRay(Cams[0], x, y);

            float cd = 0;
            bool firstHit = true;
            float cu = 0, cv = 0;
            Node3D cn = null;
            Mesh3D cm = null;

            foreach (var ent in AlwaysOnTop)
            {
                
                Matrix4 world = ent.World;
                foreach (var msh in ent.Meshes)
                {
                    foreach (var tri in msh.TriData)
                    {
                        Vector3 r0 = Vector3.TransformPosition(msh.VertexData[tri.V0].Pos, world);
                        Vector3 r1 = Vector3.TransformPosition(msh.VertexData[tri.V1].Pos, world);
                        Vector3 r2 = Vector3.TransformPosition(msh.VertexData[tri.v2].Pos, world);

                        Vector3? pr = Pick.Picker.GetTimeAndUvCoord(mr.pos, mr.dir, r0, r1, r2);
                        if (pr == null)
                        {
                        }
                        else
                        {
                            Vector3 cr = (Vector3)pr;
                            if (cr.X < cd || firstHit)
                            {
                                firstHit = false;
                                cd = cr.X;
                                cn = ent;
                                cm = msh;
                                cu = cr.Y;
                                cv = cr.Z;
                            }
                        }
                    }
                }
            }
            if (firstHit)
            {
                return null;
            }

            res.Dist = cd;
            res.Node = cn;
            res.Pos = Pick.Picker.GetTrilinearCoordinateOfTheHit(cd, mr.pos, mr.dir);
            res.Ray = mr;
            res.Mesh = cm;
            res.UV = new Vector3(cu, cv, 0);

            return res;
        }

        public Vivid.Pick.PickResult CamPick(int x, int y)
        {
            if (SceneUpdating == true) return null;
            if (SceneCache == null) return null;
            System.Console.WriteLine("Checking Scene: CacheSize:" + SceneCache.Length);

            Pick.PickResult res = new Pick.PickResult();

            Pick.Ray mr = Pick.Picker.CamRay(Cams[0], x, y);

            float cd = 0;
            bool firstHit = true;
            float cu = 0, cv = 0;
            Node3D cn = null;
            Mesh3D cm = null;

            int ms = System.Environment.TickCount;

            for (int i = 0; i < SceneCache.Length; i += 3)
            {
                var r0 = SceneCache[i];
                var r1 = SceneCache[i + 1];
                var r2 = SceneCache[i + 2];

                Vector3? pr = Pick.Picker.GetTimeAndUvCoord(mr.pos, mr.dir, r0, r1, r2);
                if (pr == null)
                {
                }
                else
                {
                    Vector3 cr = (Vector3)pr;
                    if (cr.X < cd || firstHit)
                    {
                        firstHit = false;
                        cd = cr.X;
                        cn = SceneNodes[i];
                        cm = SceneMeshes[i];
                        cu = cr.Y;
                        cv = cr.Z;
                    }
                }
            }

            ms = System.Environment.TickCount - ms;
            System.Console.WriteLine("PickTime:" + ms + " Secs:" + (ms / 1000));

            if (firstHit)
            {
                return null;
            }

            res.Dist = cd;
            res.Node = cn;
            res.Pos = Pick.Picker.GetTrilinearCoordinateOfTheHit(cd, mr.pos, mr.dir);
            res.Ray = mr;
            res.Mesh = cm;
            res.UV = new Vector3(cu, cv, 0);

            return res;
        }

        public virtual void Clean()
        {
        }

        public virtual void Clear()
        {
            Root = new Node3D();
            Root.Name = "Root";
            Lights.Clear();
            Cams.Clear();
        }

        public void Record()
        {
            Copy();
        }

        public void Copy()
        {
            if (Running) return;
            ClassCopy = new Reflect.ClassIO(this);
            ClassCopy.Copy();
            CopyNode(Root);
        }

        public void CopyNode(Node3D node)
        {
            node.CopyProps();
            foreach (Node3D nn in node.Sub)
            {
                CopyNode(nn);
            }
        }

        public void End()
        {
            if (!Running) return;
            if (Paused) Paused = false;
            Root.End();
            Restore();
            Running = false;
            Paused = false;
        }

        public void Pause()
        {
            if (Paused)
            {
                Root.Resume();
                Paused = false;
                Running = true;
                return;
            }
            if (!Running)
            {
                return;
            }
            Root.Pause();
            Running = false;
            Paused = true;
        }

        public void EndRun()
        {
            End();
        }

        public List<Node3D> GetList(bool meshesOnly)
        {
            List<Node3D> list = new List<Node3D>();
            NodeToList(Root, meshesOnly, list);
            return list;
        }

        public void LoadGraph(string file)
        {
            Cams.Clear();
            Root = null;
            Lights.Clear();

            FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read);
            BinaryReader r = new BinaryReader(fs);
            Help.IOHelp.r = r;

            if (Graph3DForm.Main != null)
            {
                Graph3DForm.Main.ReadPP(r);
            }
            else
            {
                bool eBlur, eBloom, eOutline, eDof;
                eBlur = r.ReadBoolean();
                float bl = r.ReadSingle();

                eBloom = r.ReadBoolean();
                float ml = r.ReadSingle();
                float bbl = r.ReadSingle();

                eOutline = r.ReadBoolean();

                eDof = r.ReadBoolean();
                float zf = r.ReadSingle();
                float zr = r.ReadSingle();
            }

            int cc = r.ReadInt32();
            for (int i = 0; i < cc; i++)
            {
                Cam3D nc = new Cam3D();
                nc.Read();
                Cams.Add(nc);
            }
            int lc = r.ReadInt32();
            for (int i = 0; i < lc; i++)
            {
                Light3D nl = new Light3D();
                nl.Read();
                Lights.Add(nl);
            }
            //Entity3D re = new Entity3D();
            Node3D new_e = null;

            Root = ReadNode();

            //re.Read();
            fs.Close();
            Root.SetMultiPass();
        }

        public Node3D ReadNode()
        {
            int node_t = Help.IOHelp.ReadInt();
            Node3D nn = null;
            if (node_t == 1)
            {
                Entity3D new_e = new Entity3D();
                new_e.Read();
                nn = new_e;
            }
            else if (node_t == 0)
            {
                Node3D new_n = new Node3D();
                new_n.Read();
                nn = new_n;
            }
            else
            {
                System.Environment.Exit(-3);
            }
            int sc = Help.IOHelp.ReadInt();
            for (int i = 0; i < sc; i++)
            {
                var rn = ReadNode();
                rn.Top = nn;
                nn.Sub.Add(rn);
            }
            return nn;
        }

        public void PauseRun()
        {
        }

        public virtual void Release()
        {
        }

        public virtual void RenderByTags(List<string> tags)
        {
            foreach (Node3D n in Root.Sub)
            {
                RenderNodeByTags(tags, n);
            }
        }

        public void DrawFormSolid(Vector4 col, int x = 0, int y = 0, int w = -1, int h = -1)
        {
            Vivid.Draw.Pen2D.Rect(ViewX + x, ViewY + y, w, h, col);
        }

        public void DrawForm(Texture.Texture2D tex, Vector4 col, int x = 0, int y = 0, int w = -1, int h = -1)
        {
            Draw.Pen2D.BlendMod = Draw.PenBlend.Alpha;

            Draw.Pen2D.Rect(x, y, w, h, tex, col);
        }

        public int ViewX, ViewY, ViewW, ViewH;

        public static Cam3D LastCam = null;

        public virtual void RenderEntity(Entity3D ent)
        {
            LastCam = Cams[0];

            SubGraph?.Render();
            List<string> defTags = new List<string>
            {
                "All"
            };
            RenderNodeByTags(defTags, ent);
        }

        private FXColorOnly fxco;

        public void RenderLines()
        {
            if (fxco == null)
            {
                fxco = new FXColorOnly();
            }
            FXG.Mesh = null;
            FXG.Cam = Cams[0];
            FXG.Local = Matrix4.Identity;
            OpenTK.Graphics.OpenGL4.GL.LineWidth(1.2f);
            foreach (var lm in LineMeshes)
            {
                var vz = lm.Viz;

                fxco.Bind();
                vz.Bind();
                vz.Visualize();
                vz.Release();

                fxco.Release();
                //     Console.WriteLine("Rendering Lines");
            }
        }

        public virtual void RenderBounds()
        {
            RenderNodeBounds(Root);
        }

        public void RenderNodeBounds(Node3D node)
        {
            FXG.Cam = Cams[0];
            if (node is Entity3D && node.NoSave == false)
            {
                var ent = node as Entity3D;
                ent.RenderBounds();
            }

            foreach (var sn in node.Sub)
            {
                RenderNodeBounds(sn);
            }
        }

        public FastRenderList FastRender = null;

        public virtual void Render()
        {
            if (OcclusionCulling)
            {
                if (FastRender == null)
                {
                    FastRender = new FastRenderList(this);
                    FastRender.Sort();
                }

                FastRender.RenderNormal();

                // RenderBounds();
                return;
            }

            if (Oct != null)
            {
                if (OctFX == null)
                {
                    OctFX = new FXMultiPass3D();
                }

                FXG.Cam = Cams[0];
                Light3D.Active = Lights[0];
                if (Light3D.Active != null)
                {
                    Light3D.Active.ShadowFB.Cube.Bind(2);
                }

                // Console.WriteLine("Rendering Oc-Tree");

                //SceneOctree.NodesRendered = 0;
                Oct.Root.RenderNode(Root.World, OctFX);

                //Console.WriteLine("Rendered Nodes:" + SceneOctree.NodesRendered);

                return;
            }

            LastCam = Cams[0];

            SubGraph?.Render();
            List<string> defTags = new List<string>
            {
                "All"
            };
            RenderNodeByTags(defTags, Root);
        }

        public void SetLightmapTex(Texture.Texture2D tex)
        {
            Root.SetLightmapTex(tex);
        }

        public void EditGraph(EditNode editor)
        {
            Root.Edit(editor);
        }

        public virtual void RenderNodeByTags(List<string> tags, Node3D node)
        {
            bool rt = false;
            foreach (string tag in tags)
            {
                if (node.RenderTags.Contains(tag))
                {
                    rt = true;
                    RenderThis(node);
                    break;
                }
            }
            if (rt)
            {
                foreach (Node3D n in node.Sub)
                {
                    RenderNodeByTags(tags, n);
                }
            }
        }

        public virtual void RenderSSRExtras()
        {
            GL.ClearColor(new Color4(1.0f, 1.0f, 1.0f, 1.0f));
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            if (CamOverride != null)
            {
                //foreach (var n in Nodes)
                //{
                RenderNodeSSRExtrasMap(Root, CamOverride);

                //}
            }
            else

            {
                RenderNodeSSRExtrasMap(Root, Cams[0]);
            }
        }

        public virtual void RenderPositionMap()
        {
            GL.ClearColor(new Color4(1.0f, 1.0f, 1.0f, 1.0f));
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            if (CamOverride != null)
            {
                //foreach (var n in Nodes)
                //{
                RenderNodePositionMap(Root, CamOverride);

                //}
            }
            else

            {
                RenderNodePositionMap(Root, Cams[0]);
            }
        }

        public virtual void RenderNormalMap()
        {
            GL.ClearColor(new Color4(1.0f, 1.0f, 1.0f, 1.0f));
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            if (CamOverride != null)
            {
                //foreach (var n in Nodes)
                //{
                RenderNodeNormalMap(Root, CamOverride);

                //}
            }
            else

            {
                RenderNodeNormalMap(Root, Cams[0]);
            }
        }

        public virtual void RenderDepth()
        {
            GL.ClearColor(new Color4(1.0f, 1.0f, 1.0f, 1.0f));
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            if (CamOverride != null)
            {
                //foreach (var n in Nodes)
                //{
                RenderNodeDepth(Root, CamOverride);

                //}
            }
            else

            {
                RenderNodeDepth(Root, Cams[0]);
            }
            //foreach (var c in Cams)
        }

        public virtual void RenderNode(Node3D node)
        {
            // Console.WriteLine("RenderNode:" + node.Name);
            RenderThis(node);
            foreach (Node3D snode in node.Sub)
            {
                // Console.WriteLine("Rendering Node:" + snode.Name);
                RenderNode(snode);
            }
        }

        private void RenderThis(Node3D node)
        {
            if (node.Name == "Terrain")
            {
            }
            if (CamOverride != null)
            {
                foreach (Light3D l in Lights)
                {
                    Light3D.Active = l;

                    node.Present(CamOverride);
                }
            }
            else
            {
                foreach (Cam3D c in Cams)
                {
                    if (node.AlwaysAlpha)
                    {
                        Entity3D ge = node as Entity3D;
                        if (ge.Renderer is Visuals.RMultiPass)
                        {
                            ge.Renderer = new Visuals.VRNoFx();
                        }
                        GL.Enable(EnableCap.Blend);
                        GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
                        node.Present(c);
                        continue;
                    }
                    if (node.Lit)
                    {
                        bool second = true;
                        bool first = true;

                        foreach (Light3D l in Lights)
                        {
                            Light3D.Active = l;
                            if (l.EnableLight == false) continue;
                            if (first)
                            {
                                first = false;
                                GL.Disable(EnableCap.Blend);
                            }
                            else if (second)
                            {
                                second = false;
                                GL.Enable(EnableCap.Blend);
                                GL.BlendFunc(BlendingFactor.One, BlendingFactor.One);
                            }

                            node.Present(c);

                            // foreach (var n in Nodes) { n.Present(c); }
                        }
                        GL.Disable(EnableCap.Blend);
                    }
                    else
                    {
                        if (node.FaceCamera)
                        {
                            node.LookAt(c.LocalPos, new Vector3(0, 1, 0));
                        }
                        GL.Enable(EnableCap.Blend);
                        GL.BlendFunc(BlendingFactor.Src1Alpha, BlendingFactor.OneMinusSrcAlpha);
                        GL.DepthMask(false);
                        node.Present(c);
                        GL.DepthMask(true);
                    }
                }
            }
        }

        public void RenderNodeSSRExtrasMap(Node3D node, Cam3D c)
        {
            var e3 = node as Entity3D;
            e3.PresentSSRExtrasMap(c);
            foreach (var node2 in node.Sub)
            {
                RenderNodeSSRExtrasMap(node2, c);
            }
        }

        public void RenderNodePositionMap(Node3D node, Cam3D c)
        {
            var e3 = node as Entity3D;

            e3.PresentPositionMap(c);

            foreach (var snode in node.Sub)
            {
                RenderNodePositionMap(snode, c);
            }
        }

        public void RenderNodeNormalMap(Node3D node, Cam3D c)
        {
            var e3 = node as Entity3D;

            e3.PresentNormalMap(c);

            foreach (var snode in node.Sub)
            {
                RenderNodeNormalMap(snode, c);
            }
        }

        public virtual void RenderNodeDepth(Node3D node, Cam3D c)
        {
            var e3 = node as Entity3D;

            if (node.CastDepth && node.NoSave == false)
            {
                node.PresentDepth(c);
            }
            foreach (Node3D snode in node.Sub)
            {
                RenderNodeDepth(snode, c);
            }
        }

        public virtual void RenderNodeNoLights(Node3D node)
        {
            if (CamOverride != null)
            {
                foreach (Light3D l in Lights)
                {
                    Light3D.Active = l;

                    node.Present(CamOverride);
                }
            }
            else
            {
                foreach (Cam3D c in Cams)
                {
                    GL.Disable(EnableCap.Blend);

                    // Console.WriteLine("Presenting:" + node.Name);
                    node.Present(c);

                    // foreach (var n in Nodes) { n.Present(c); }
                }
            }
            foreach (Node3D snode in node.Sub)
            {
                // Console.WriteLine("Rendering Node:" + snode.Name);
                RenderNodeNoLights(snode);
            }
        }

        public virtual void RenderNoLights()
        {
            Light3D.Active = null;
            RenderNodeNoLights(Root);
        }

        public virtual void RenderShadows()
        {
            int ls = 0;
            GL.Disable(EnableCap.Blend);
            foreach (Light3D l in Lights)
            {
                ls++;
                l.DrawShadowMap(this);
                // Console.WriteLine("LightShadows:" + ls);
            }
        }

        public void Restore()
        {
            ClassCopy.Reset();
            RestoreNode(Root);
        }

        public void RestoreNode(Node3D node)
        {
            node.RestoreProps();
            foreach (Node3D nn in node.Sub)
            {
                RestoreNode(nn);
            }
        }

        public Vector3 Rot(Vector3 p, Node3D n)
        {
            return Vector3.TransformPosition(p, n.World);
        }

        public void SaveGraph(string file)
        {
            FileStream fs = new FileStream(file, FileMode.Create, FileAccess.Write);
            BinaryWriter bw = new BinaryWriter(fs);

            Help.IOHelp.w = bw;

            Vivid.Resonance.Forms.Graph3DForm.Main.WritePP(bw);

            bw.Write(Cams.Count);
            foreach (Cam3D c in Cams)
            {
                c.Write();
            }
            bw.Write(Lights.Count);
            foreach (Light3D c in Lights)
            {
                c.Write();
            }
            Node3D r = Root as Node3D;
            SaveNode(r);
            bw.Flush();
            fs.Flush();
            fs.Close();
        }

        public void SaveNode(Node3D node)
        {
            if (node is Entity3D)
            {
                Help.IOHelp.WriteInt(1);
                node.Write();
            }
            else if (node is Node3D)
            {
                Help.IOHelp.WriteInt(0);
                node.Write();
            }
            else
            {
                System.Environment.Exit(-2);
            }
            int nc = 0;
            foreach (var sub in node.Sub)
            {
                if (sub.NoSave == false)
                {
                    nc++;
                }
            }
            Help.IOHelp.WriteInt(nc);
            foreach (var sub in node.Sub)
            {
                if (sub.NoSave) continue;
                SaveNode(sub);
            }
        }

        public virtual void Update()
        {
            //var tp = new XInput.XPad(0);
            if (!Running) return;
            if (Root == null) return;
            foreach (var l in Lights)
            {
                UpdateNode(l);
            }
            UpdateNode(Root);
        }

        public virtual void UpdateNode(Node3D node)
        {
            node.Update();
        }

        private void NodeToList(Node3D node, bool meshes, List<Node3D> list)
        {
            if (meshes)
            {
                if (node is Entity3D)
                {
                    list.Add(node);
                }
            }
            else
            {
                list.Add(node);
            }
            foreach (Node3D n2 in node.Sub)
            {
                NodeToList(n2, meshes, list);
            }
        }
    }
}
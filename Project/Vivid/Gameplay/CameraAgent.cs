﻿using OpenTK.Mathematics;

namespace Vivid.Gameplay
{
    public class CameraAgent : ClassAgent
    {
        public Scene.Cam3D Cam
        {
            get;
            set;
        }

        public Scene.Entity3D Target
        {
            get;
            set;
        }

        public Vector3 CamOffSet
        {
            get
            {
                return _CamOffset;
            }
            set
            {
                _CamOffset = value;
            }
        }

        private Vector3 _CamOffset = new Vector3(0, 10, 30);

        public Vector3 CamTarget
        {
            get
            {
                return _CamTarget;
            }
            set
            {
                _CamTarget = value;
            }
        }

        private Vector3 _CamTarget = new Vector3(0, 0, 0);

        public virtual void Init()
        {
        }

        public virtual void Update()
        {
        }
    }
}
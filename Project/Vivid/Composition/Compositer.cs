﻿namespace Vivid.Composition
{

    /// <summary>
    /// All new compositors should inherit this class.
    /// </summary>
    public class Compositer
    {
        /// <summary>
        /// The individual frames used to generate the final compositor.
        /// </summary>
        public FrameType[] Types
        {
            get;
            set;
        }

        /// <summary>
        /// The name of the compositor.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The scene graph being used.
        /// </summary>
        public Scene.SceneGraph3D Graph
        {
            get => _Graph;
            set
            {
                _Graph = value;
                foreach (FrameType type in Types)
                {
                    type.Graph = value;
                }
                if (InputFrame != null)
                {
                    InputFrame.Graph = value;
                }
            }
        }

        private Scene.SceneGraph3D _Graph = null;

        /// <summary>
        /// The output frame of the compositor.
        /// </summary>
        public FrameType OutputFrame = null;

        /// <summary>
        /// The input frame of the compositor.
        /// </summary>
        public FrameType InputFrame
        {
            get => _IF;
            set
            {
                _IF = value;
                foreach (FrameType f in Types)
                {
                }
            }
        }

        private FrameType _IF;

        /// <summary>
        /// The blend type of the frame.
        /// </summary>
        public FrameBlend Blend = FrameBlend.Add;

        /// <summary>
        /// You should call this via Base(...) - Types = the number of unique frame types your compositor
        /// will use.
        /// </summary>
        /// <param name="types">Number of compositors.</param>
        public Compositer(int types)
        {
            Types = new FrameType[types];
        }


        /// <summary>
        /// Generates all the require internal frame types.
        /// </summary>
        public void GenerateFrames()
        {
            if (InputFrame != null)
            {
                InputFrame.Generate();
            }
            foreach (FrameType type in Types)
            {
                type.Generate();
            }
        }


        /// <summary>
        /// Presents the frame
        /// </summary>
        /// <param name="frame">Frame number.</param>
        public void PresentFrame(int frame)
        {
            Types[frame].Present();
        }

        public virtual void PreGen()
        {
        }

        /// <summary>
        /// Process the compositor.
        /// </summary>
        public virtual void Process()
        {
            GenerateFrames();
        }

        public virtual void Render()
        {
        }
    }

    /// <summary>
    /// The frameblend enum.
    /// </summary>
    public enum FrameBlend
    {
        /// <summary>
        /// Solid = no blending. Alpha = alpha blending. Add = Additive. Mod = Modulate.
        /// </summary>
        Solid, Alpha, Add, Mod
    }
}
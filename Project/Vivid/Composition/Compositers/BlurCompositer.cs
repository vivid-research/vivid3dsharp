﻿namespace Vivid.Composition.Compositers
{
    public class BlurCompositer : Compositer
    {
        public void SetBlur(float b)
        {
            var fe = Types[0] as FrameTypes.FrameEffect;

            var fx = fe.FX as PostProcess.Processes.VEBlur;

            fx.Blur = b;
        }

        public float GetBlur()
        {
            var fe = Types[0] as FrameTypes.FrameEffect;

            var fx = fe.FX as PostProcess.Processes.VEBlur;
            return fx.Blur;
        }

        public BlurCompositer() : base(1)
        {
            Name = "BlurPP";

            InputFrame = new FrameTypes.FrameColor();

            Types[0] = new FrameTypes.FrameEffect();

            FrameTypes.FrameEffect fe = Types[0] as FrameTypes.FrameEffect;

            fe.FX = new PostProcess.Processes.VEBlur();

            dynamic bb = fe.FX;

            bb.Blur = 0.0025f;

            Types[0].TexBind.Add(InputFrame);

            OutputFrame = Types[0];

            Blend = FrameBlend.Add;
        }

        public override void PreGen()
        {
            return;
            Types[0] = InputFrame;
            Types[1].TexBind.Clear();
            Types[1].TexBind.Add(InputFrame);
            //Types [ 0 ] = InputFrame;
        }
    }
}
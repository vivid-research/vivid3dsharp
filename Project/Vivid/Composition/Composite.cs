﻿using OpenTK.Graphics.OpenGL4;

using System;
using System.Collections.Generic;

/// <summary>
/// The composition class allows you to build effective and advanced post processing effects,
/// achieved via compositions of frame types and compositors them self.
/// </summary>
namespace Vivid.Composition
{
    /// <summary>
    /// Mix mode. Determines how compositors are blended together.
    /// </summary>
    public enum MixMode
    {
        /// <summary>
        /// Final = just final compositor. Add, additive blends them.
        /// </summary>
        Final, Add, Defined
    }

    public delegate void PresentRender(Texture.Texture2D tex);

    /// <summary>
    /// The composite class is the base class, that manages and renders all active compositors.
    /// </summary>
    public class Composite
    {
        public int qva = 0, qvb = 0;

        public PresentRender Render = null;

        private Vivid.Scene.SceneGraph3D _G;

        private PostProcess.VEQuadR QuadFX;

        /// <summary>
        /// Creates the composite class.
        /// </summary>
        public Composite()
        {
            Composites = new List<Compositer>();
            QuadFX = new PostProcess.VEQuadR();
            GenQuad();
            Mix = MixMode.Final;
        }

        /// <summary>
        /// The list of active compositors.
        /// </summary>
        public List<Compositer> Composites
        {
            get;
            set;
        }

        /// <summary>
        /// The current 3D graph being used.
        /// </summary>
        public Vivid.Scene.SceneGraph3D Graph
        {
            get => _G;
            set
            {
                _G = value;
                foreach (Compositer cos in Composites)
                {
                    cos.Graph = value;
                }
            }
        }

        /// <summary>
        /// Active mix mode.
        /// </summary>
        public MixMode Mix
        {
            get; set;
        }

        /// <summary>
        /// Adds a new compositor.
        /// </summary>
        /// <param name="compositor">The compositor to add</param>
        public void AddCompositer(Compositer compositor)
        {
            compositor.Graph = Graph;
            Composites.Add(compositor);
        }

        //Composites [ 1 ].PresentFrame ( 1 );
        public void DrawQuad()
        {
            // FB.BB.Bind ( 0 );

            QuadFX.Bind();

            GL.BindVertexArray(qva);

            GL.BindBuffer(BufferTarget.ArrayBuffer, qvb);
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);

            GL.DrawArrays(PrimitiveType.Triangles, 0, 6);

            GL.DisableVertexAttribArray(0);
            // GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            QuadFX.Release();

            //FB.BB.Release ( 0 );
        }

        public void GenQuad()
        {
            qva = GL.GenVertexArray();

            GL.BindVertexArray(qva);

            float[] qd = new float[18];

            qd[0] = -1.0f;
            qd[1] = -1.0f;
            qd[2] = 0.0f;
            qd[3] = 1.0f; qd[4] = -1.0f; qd[5] = 0.0f;
            qd[6] = -1.0f; qd[7] = 1.0f; qd[8] = 0.0f;
            qd[9] = -1.0f; qd[10] = 1.0f; qd[11] = 0.0f;
            qd[12] = 1.0f; qd[13] = -1.0f; qd[14] = 0.0f;
            qd[15] = 1.0f; qd[16] = 1.0f; qd[17] = 0.0f;

            qvb = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, qvb);
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(18 * 4), qd, BufferUsageHint.StaticDraw);
            // GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        private int db = 0;

        /// <summary>
        /// Any pre-final frame rendering.
        /// </summary>
        public void PreRernder()
        {
            FrameType ot = null;
            // GL.Disable(EnableCap.Blend);
            foreach (Compositer cos in Composites)
            {
                // cos.InputFrame = ot;
                if (ot != null)
                {
                    // cos.PreGen ( );
                    cos.InputFrame.FrameBuffer = ot.FrameBuffer;
                    cos.InputFrame.Regenerate = false;
                }

                cos.Process();
                ot = cos.OutputFrame;

                // cos.PresentFrame ( 0 );
            }
        }

        /// <summary>
        /// This renders the final frame, with all active compositors applied.
        /// </summary>
        public void RenderFinal()
        {
            Mix = MixMode.Final;
            //return;
            switch (Mix)
            {
                case MixMode.Final:

                    //                    GL.Disable(EnableCap.Blend);

                    Compositer fc = Composites[Composites.Count - 1];

                    //fc.OutputFrame.FrameBuffer.BB.Bind ( 0 );

                    // DrawQuad ( );

                    Render?.Invoke(fc.OutputFrame.FrameBuffer.BB);
                    //Render.Invoke(fc.InputFrame.FrameBuffer.BB);

                    //fc.InputFrame.FrameBuffer.BB.SaveDebug("debug/debug" + db + ".dat");
                    db++;
                    //fc.OutputFrame.FrameBuffer.BB.Release ( 0 );

                    break;

                case MixMode.Add:

                    Vivid.Draw.BlendType oldBlend = Vivid.Draw.IntelliDraw.Blend;

                    foreach (Compositer cos in Composites)
                    {
                        switch (cos.Blend)
                        {
                            case FrameBlend.Add:
                                //     GL.Enable(EnableCap.Blend);
                                //     GL.BlendFunc(BlendingFactor.One, BlendingFactor.One);
                                break;
                        }

                        //cos.OutputFrame.FrameBuffer.BB.Bind ( 0 );
                        // Vivid.Draw.Pen2D.BlendMod = Draw.PenBlend.Additive;
                        // Vivid.Draw.IntelliDraw.Blend =
                        Render?.Invoke(cos.OutputFrame.FrameBuffer.BB);
                        Vivid.Draw.IntelliDraw.Blend = Draw.BlendType.Add;
                        //  DrawQuad ( );
                        //return;

                        //    cos.OutputFrame.FrameBuffer.BB.Release ( 0 );
                    }
                    Vivid.Draw.IntelliDraw.Blend = oldBlend;
                    //GL.Disable(EnableCap.Blend);
                    break;
            }
        }

        /// <summary>
        /// Sets the current scene graph to use.
        /// </summary>
        /// <param name="graph">The current graph you wish to use.</param>
        public void SetGraph(Scene.SceneGraph3D graph)
        {
            Graph = graph;
            foreach (Compositer cos in Composites)
            {
                cos.Graph = graph;
            }
        }
    }
}
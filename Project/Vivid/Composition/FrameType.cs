﻿using OpenTK.Graphics.OpenGL4;

using System;
using System.Collections.Generic;

using Vivid.FrameBuffer;

namespace Vivid.Composition
{
    /// <summary>
    /// Frametypes are the basis of compositors. They can be inherited to create new frame types.
    /// Built in frame-types include "Color" "Depth" and  "Effect"
    /// </summary>
    public class FrameType
    {
        /// <summary>
        /// Name of the frame-type.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Should it be regenerated every frame?
        /// </summary>
        public bool Regenerate
        {
            get;
            set;
        }

        /// <summary>
        /// Which textures are bound to the frametype before render.
        /// </summary>
        public List<FrameType> TexBind
        {
            get; set;
        }

        /// <summary>
        /// The FrameBuffer this frame will be rendered to.
        /// </summary>
        public FrameBufferColor FrameBuffer
        {
            get;
            set;
        }

        /// <summary>
        /// The Width
        /// </summary>
        public int FrameWidth
        {
            get;
            set;
        }

        /// <summary>
        /// The Height
        /// </summary>
        public int FrameHeight
        {
            get;
            set;
        }

        /// <summary>
        /// The scene graph.
        /// </summary>
        public Scene.SceneGraph3D Graph
        {
            get;
            set;
        }

        public PostProcess.VEQuadR QuadFX = null;
        public static int FW, FH;


        /// <summary>
        /// This creates the initial data of the frame-type.
        /// </summary>
        /// <param name="format">Which internal texture format to use.,</param>
        public FrameType(Texture.TextureFormat format = Texture.TextureFormat.Normal)
        {
            Regenerate = true;

            FrameWidth = FW;
            FrameHeight = FH;
            FrameBuffer = new FrameBufferColor(FrameWidth, FrameHeight, format);
            QuadFX = new PostProcess.VEQuadR();
            GenQuad();
            TexBind = new List<FrameType>();
            //FrameBuffer
        }

        public int qva = 0, qvb = 0;

        public void DrawQuad()
        {
            // FB.BB.Bind ( 0 );

            // QuadFX.Bind ( );

            GL.BindVertexArray(qva);

            GL.BindBuffer(BufferTarget.ArrayBuffer, qvb);
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);

            GL.DrawArrays(PrimitiveType.Triangles, 0, 6);

            GL.DisableVertexAttribArray(0);
            // GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            //QuadFX.Release ( );

            //FB.BB.Release ( 0 );
        }

        public void GenQuad()
        {
            qva = GL.GenVertexArray();

            GL.BindVertexArray(qva);

            float[] qd = new float[18];

            qd[0] = -1.0f;
            qd[1] = -1.0f;
            qd[2] = 0.0f;
            qd[3] = 1.0f; qd[4] = -1.0f; qd[5] = 0.0f;
            qd[6] = -1.0f; qd[7] = 1.0f; qd[8] = 0.0f;
            qd[9] = -1.0f; qd[10] = 1.0f; qd[11] = 0.0f;
            qd[12] = 1.0f; qd[13] = -1.0f; qd[14] = 0.0f;
            qd[15] = 1.0f; qd[16] = 1.0f; qd[17] = 0.0f;

            qvb = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, qvb);
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(18 * 4), qd, BufferUsageHint.StaticDraw);
            // GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        /// <summary>
        /// Binds this frame-types FrameBuffer.
        /// </summary>
        public void BindTarget()
        {
            FrameBuffer.Bind();
        }

        /// <summary>
        /// Releases this frame-types FrameBuffer.
        /// </summary>
        public void ReleaseTarget()
        {
            FrameBuffer.Release();
        }

        public virtual void Generate()
        {
        }

        /// <summary>
        /// Binds any textures associated with this frame-type.
        /// </summary>
        public void BindTex()
        {
            int tn = 0;
            foreach (FrameType tex in TexBind)
            {
                tex.FrameBuffer.BB.Bind(tn);
                tn++;
            }
        }

        /// <summary>
        /// Releases the textures associated with this frame-type.
        /// </summary>
        public void ReleaseTex()
        {
            int tn = 0;
            foreach (FrameType tex in TexBind)
            {
                tex.FrameBuffer.BB.Release(tn);
                tn++;
            }
        }

        /// <summary>
        /// Presents the frame-type.
        /// </summary>
        public void Present()
        {
            FrameBuffer.BB.Bind(0);
            QuadFX.Bind();
            DrawQuad();
            QuadFX.Release();
            FrameBuffer.BB.Release(0);
        }
    }
}
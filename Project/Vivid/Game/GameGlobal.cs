﻿namespace Vivid.Game
{
    public enum GameType
    {
        Final, Editor
    }

    public class GameGlobal
    {
        public static GameType Mode = GameType.Editor;

        public static void SetPP(bool blur, bool bloom, bool dof, bool outline)
        {
            switch (Mode)
            {
                case GameType.Editor:
                    Vivid.Resonance.Forms.Graph3DForm.PP(blur, bloom, outline, dof);
                    break;
            }
        }
    }
}
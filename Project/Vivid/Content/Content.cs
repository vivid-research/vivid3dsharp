﻿using Vivid.Texture;

/// <summary>
/// Content encapsulates various types of engine content. Such as (3D,Textures and Scripts)
/// </summary>
namespace Vivid.Content
{
    /// <summary>
    /// Base class.
    /// </summary>
    public class ContentEntry
    {
        /// <summary>
        /// Path to the entry.
        /// </summary>
        public string Path = "";
        /// <summary>
        /// Local name.
        /// </summary>
        public string Name = "";
        /// <summary>
        /// File extension/type.
        /// </summary>
        public string Ext = "";
        public int DrawX, DrawY;
        public bool Over = false;

        /// <summary>
        /// The full path to the content on your HD.
        /// </summary>
        public string FullPath
        {
            get
            {
                return Path;
            }
        }

        public int VisualX, VisualY;
        public Texture2D Image;


        /// <summary>
        /// This will load the content based on type, and return the loaded content.
        /// </summary>
        /// <returns></returns>
        public virtual dynamic Load()
        {
            return null;
        }
    }

    /// <summary>
    /// ContentSound is sound files.
    /// </summary>
    public class ContentSound : ContentFile
    {
        /// <summary>
        /// The sound(.wav/mp3)
        /// </summary>
        /// <param name="name">It's local name.</param>
        /// <param name="path">It's HD path.</param>
        public ContentSound(string name, string path) : base(name, path)
        {
            Path = path;
            Name = name;
        }
    }

    /// <summary>
    /// Just a proxy class for folders.
    /// </summary>
    public class ContentFolder : ContentEntry
    {
        public ContentFolder(string name, string path)
        {
            Path = path;
            Name = name;
        }
    }

    /// <summary>
    /// Base line file class for enteries.
    /// </summary>
    public class ContentFile : ContentEntry
    {
        public ContentFile(string name, string path)
        {
            Name = name;
            Path = path;
        }
    }

    /// <summary>
    /// Model class for content that will be loaded via the engine's Import system.
    /// </summary>
    public class ContentModel : ContentFile
    {
        public ContentModel(string name, string path) : base(name, path)
        {
        }

        public override dynamic Load()
        {
            return Import.Import.ImportNode(Path);
        }
    }

    /// <summary>
    /// Class to contain textures, and will be loaded via Texture2D class.
    /// </summary>
    public class ContentTexture : ContentFile
    {
        public ContentTexture(string name, string path) : base(name, path)
        {
        }

        public override dynamic Load()
        {
            return new Texture2D(Path, LoadMethod.Single, true);

            //return base.Load();
        }
    }

    /// <summary>
    /// Contains vividScript script and can load and compile via Load method.
    /// </summary>
    public class ContentScript : ContentFile
    {
        public ContentScript(string name, string path) : base(name, path)
        {
        }

        /// <summary>
        /// Will load and compile the VividScript file.
        /// </summary>
        /// <returns></returns>
        public override dynamic Load()
        {
            var r = Path;
            return r;
        }
    }
}
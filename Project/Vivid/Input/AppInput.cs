﻿using OpenTK.Mathematics;

namespace Vivid.Input
{
    public class AppInput
    {
        public static Vector3 MouseMove;
        public static bool[] MouseButton = new bool[32];
    }
}
﻿using OpenTK.Graphics.OpenGL4;

using System;
using System.Collections.Generic;

using Vivid.Data;

namespace Vivid.Visuals
{
    public class VLines : Visualizer
    {
        private List<Line3D> Lines = null;
        private int vert_vbo;
        private int va, eb;

        public VLines(List<Line3D> lines) : base(0, 0)
        {
            Lines = lines;
            Final();
        }

        public override void Final()
        {
            //base.Final();

            va = GL.GenVertexArray();
            GL.BindVertexArray(va);

            float[] verts = new float[Lines.Count * 6];

            //float[] norms = new float[md.NumVertices * 3];
            //float[] bi = new float[md.NumVertices * 3];
            // float[] tan = new float[md.NumVertices * 3];
            //float[] uv = new float[md.NumVertices * 2];
            int vi = 0;
            for (int i = 0; i < Lines.Count; i++)
            {
                var l = Lines[i];

                verts[vi] = l.X;
                verts[vi + 1] = l.Y;
                verts[vi + 2] = l.Z;
                verts[vi + 3] = l.X1;
                verts[vi + 4] = l.Y1;
                verts[vi + 5] = l.Z1;

                //verts[vi] = md.VertexData[i].Pos.X;
                //verts[vi + 1] = md.VertexData[i].Pos.Y;
                //verts[vi + 2] = md.VertexData[i].Pos.Z;

                vi += 6;
            }
            vi = 0;

            vert_vbo = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vert_vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(Lines.Count * 6 * 4), verts, BufferUsageHint.DynamicDraw);
            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, 0);

            //GL.EnableVertexAttribArray(1);

            /*
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, md.Data.Components * 4, 12);
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, false, md.Data.Components * 4, 20);
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 3, VertexAttribPointerType.Float, false, md.Data.Components * 4, 32);
            GL.EnableVertexAttribArray(4);
            GL.VertexAttribPointer(4, 3, VertexAttribPointerType.Float, false, md.Data.Components * 4, 44);
            */
            GL.BindVertexArray(0);
            GL.DisableVertexAttribArray(0);

            eb = GL.GenBuffer();

            uint[] indices = new uint[Lines.Count * 2];

            int ti = 0;
            for (int i = 0; i < Lines.Count; i++)
            {
                indices[ti] = (uint)ti;
                indices[ti + 1] = (uint)ti + 1;
                //indices[ti] = (uint)md.TriData[i].V0;
                //indices[ti + 1] = (uint)md.TriData[i].V1;
                //indices[ti + 2] = (uint)md.TriData[i].v2;

                ti += 2;
            }

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, eb);
            GL.BufferData(BufferTarget.ElementArrayBuffer, new IntPtr(Lines.Count * 2 * 4), indices, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        public override void Bind()
        {
            //base.Bind();
            GL.BindVertexArray(va);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, eb);
            GL.EnableVertexAttribArray(0);
        }

        public override void Release()
        {
            GL.BindVertexArray(0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }

        public override void Visualize()
        {
            GL.DrawElements(BeginMode.Lines, Lines.Count * 2, DrawElementsType.UnsignedInt, 0);
        }
    }
}
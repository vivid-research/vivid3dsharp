﻿namespace Vivid.RenderPipelines
{
    public class Renderer
    {
        public virtual void Init()
        {
        }

        public virtual void Update()
        {
        }

        public virtual void Render()
        {
        }
    }
}
﻿namespace Vivid.Attribs
{
    [System.AttributeUsage(System.AttributeTargets.Class |
                         System.AttributeTargets.Struct)
  ]
    public class AuthorAttribute : System.Attribute
    {
        private string name;
        public double version;

        public AuthorAttribute(string name)
        {
            this.name = name;
            version = 1.0;
        }
    }

    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class EditAttrib : System.Attribute
    {
        private string Name;

        public EditAttrib(string name)
        {
            Name = name;
        }
    }
}
﻿using OpenTK.Mathematics;

using Vivid.Font2;
using Vivid.Texture;

namespace Vivid.Resonance
{
    public class UITheme
    {
        public Texture2D ButtonBody
        {
            get;
            set;
        }

        public Texture2D WindowTitle
        {
            get;
            set;
        }

        public Texture2D WindowBody
        {
            get;
            set;
        }

        public FontTTF MenuFont
        {
            get;
            set;
        }

        public FontTTF GeneralFont
        {
            get;
            set;
        }

        public Vector4 BackgroundCol
        {
            get;
            set;
        }

        public Vector4 ForegroundCol
        {
            get;
            set;
        }

        public Vector4 TextCol
        {
            get;
            set;
        }
    }
}
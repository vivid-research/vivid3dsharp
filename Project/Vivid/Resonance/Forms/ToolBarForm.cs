﻿using OpenTK.Mathematics;

using System.Collections.Generic;

namespace Vivid.Resonance.Forms
{
    public class ToolBarForm : UIForm
    {
        public List<ToolItem> Items = new List<ToolItem>();
        public ToolItem OverItem = null;
        private bool clicked = false;

        public ToolBarForm()
        {
            Clip = false;
            MouseLeave = () =>
            {
                return;
                OverItem = null;
            };
            MouseUp = (b) =>
            {
                return;
                clicked = false;
            };
            MouseDown = (b) =>
            {
                return;
                if (OverItem != null && clicked == false)
                {
                    clicked = true;
                    OverItem.Click?.Invoke();
                }
            };

            Resized = () =>
            {
                if (Root != null)
                {
                    W = App.AppInfo.W;
                }
            };

            MouseMove = (x, y, dx, dy) =>
            {
                int dx1 = 3;
                return;

                foreach (var item in Items)
                {
                    int iw = UI.Font.Width(item.Text);

                    iw += 8;

                    if (x >= dx1 && y >= 1 && x < dx1 + iw && y <= H - 2)
                    {
                        OverItem = item;
                        break;
                    }
                    //   DrawFormSolid(new OpenTK.Vector4(0.2f, 0.2f, 0.2f, 0.8f), dx, 1, iw, H - 2);
                    //  DrawFormSolid(new OpenTK.Vector4(0.5f, 0.5f, 0.5f, 0.8f), dx + 2, 3, iw - 4, H - 6);

                    // DrawText(item.Text, dx + 4, 2, new OpenTK.Vector4(0.9f, 0.9f, 0.9f, 0.8f));

                    dx1 = dx1 + iw;
                }
            };

            Draw = () =>
            {
                DrawFormSolid(UI.CurUI.Theme.BackgroundCol);

                int dx = 3;

                return;

                foreach (var item in Items)
                {
                    int iw = UI.Font.Width(item.Text);

                    iw += 8;

                    DrawFormSolid(new Vector4(0.2f, 0.2f, 0.2f, 0.8f), dx, 1, iw, H - 2);
                    DrawFormSolid(new Vector4(0.3f, 0.3f, 0.3f, 0.8f), dx + 2, 3, iw - 4, H - 6);

                    if (OverItem == item)
                    {
                        DrawFormSolid(new Vector4(0.5f, 0.5f, 0.5f, 0.8f), dx + 2, 3, iw - 4, H - 6);
                    }

                    DrawText(item.Text, dx + 4, 2, new Vector4(0.9f, 0.9f, 0.9f, 0.8f));

                    dx = dx + iw;
                }
            };
        }

        private int toolX = 6;
        private Texture.Texture2D White;
        public void AddSeperator()
        {

            if (White == null)
            {
                White = new Texture.Texture2D("data/edit/white1.png", false);
            }
            var i = new ImageForm().Set(toolX + 4, 10, 1, 24) as ImageForm;
            i.SetImage(White);
            toolX += 12;
            Add(i);


        }
        public ToolItem AddLabel(string lab)
        {

            ToolItem i = new ToolItem();
            var labc = new LabelForm().Set(toolX, 3, Font.Width(lab) + 10, 25, lab) as LabelForm;
            Add(labc);
            toolX += Font.Width(lab) + 10;
            i.Form = labc;
            return i;
        }

        public ToolItem AddList(params string[] ops)
        {
            ToolItem i = new ToolItem();

            var sl = new DropDownListForm().Set(toolX, 7, 110, 30, "") as DropDownListForm;
            for(int j = 0; j < ops.Length; j++)
            {
                sl.AddItem(ops[j]);
            }
            Add(sl);
            toolX += 120;
            i.Form = sl;
            return i;
        }

        public ToolItem AddItem(Texture.Texture2D icon)
        {

            ToolItem i = new ToolItem();
            i.Button = new ButtonForm().Set(toolX, 5, 42, 36, "") as ButtonForm;
            i.Button.Icon = icon;
            Add(i.Button);
            toolX += 48;
            return i;

        }
        public ToolItem AddItem(string text)
        {
            if (text == "||")
            {
                toolX += 15;
                return null;
            }
            var item = new ToolItem();

            item.Button = new ButtonForm().Set(toolX, 5, Font.Width(text) + 15, 23, text) as ButtonForm;
            Add(item.Button);
            toolX += item.Button.W + 4;

            return item;

            //item.Text = text;
            //Items.Add(item);
            //return item;
        }
    }

    public class ToolItem
    {
        public string Text = "";
        public ToolClick Click = null;
        public ButtonForm Button;
        public UIForm Form;
    }

    public delegate void ToolClick();
}
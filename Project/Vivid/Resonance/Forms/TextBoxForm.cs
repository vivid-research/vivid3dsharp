﻿using OpenTK.Mathematics;
using OpenTK.Windowing.GraphicsLibraryFramework;

using System;

namespace Vivid.Resonance.Forms
{
    public delegate void OnEnter(string text);

    public class TextBoxForm2 : UIForm
    {
        public int ClaretI = 0;
        public bool Active = false;
        public int StartI = 0;
        public bool ShowClaret = false;
        public int NextClaret;
        public int ClaretE = 0;
        public OnEnter Enter = null;

        public TextBoxForm2()
        {
            void KeyPressFunc(OpenTK.Windowing.GraphicsLibraryFramework.Keys key, bool shift)
            {
                string k = "";
                switch (key)
                {
                    case Keys.LeftAlt:

                        break;

                    case Keys.Right:
                        if (Text.Length == 1)
                        {
                            if (ClaretI == 0)
                            {
                                ClaretI = 1;
                                return;
                            }
                        }
                        if (Text.Length == 0)
                        {
                            return;
                        }
                        if (Text.Length > 1)
                        {
                            if (ClaretI < Text.Length)
                            {
                                ClaretI++;
                            }
                        }
                        if (ClaretI > ClaretE)
                        {
                            if (StartI < Text.Length)
                            {
                                StartI++;
                            }
                        }
                        return;
                        break;

                    case Keys.Left:

                        if (ClaretI == StartI)
                        {
                            if (StartI > 0)
                            {
                                StartI--;
                            }
                        }
                        if (Text.Length == 1)
                        {
                            ClaretI = 0;
                            return;
                        }
                        if (Text.Length == 0)
                        {
                            return;
                        }

                        if (Text.Length > 1)
                        {
                            if (ClaretI > 0)
                            {
                                ClaretI--;
                                return;
                            }
                        }
                        return;
                        break;

                    case Keys.Backspace:

                        if (Text.Length == 1)
                        {
                            ClaretI = 0;
                            Text = "";
                            return;
                        }
                        if (Text.Length > 1)
                        {
                            if (ClaretI == Text.Length)
                            {
                                Text = Text.Substring(0, Text.Length - 1);
                                ClaretI--;
                                return;
                            }
                            string os = Text.Substring(0, ClaretI - 1) + Text.Substring(ClaretI);
                            Text = os;
                            ClaretI--;
                            return;
                        }

                        break;

                    case Keys.Delete:
                        if (Text.Length == 1 && ClaretI == 0)
                        {
                            Text = "";
                            ClaretI = 0;
                            return;
                        }
                        if (Text.Length > 1 && ClaretI < Text.Length)
                        {
                            if ((Text.Length - ClaretI) > 1)
                            {
                                Text = Text.Substring(0, ClaretI) + Text.Substring(ClaretI + 1);
                            }
                            else
                            {
                                Text = Text.Substring(0, Text.Length - 1);
                            }
                            return;
                        }
                        return;
                        break;

                    case Keys.Comma:
                        k = ",";
                        if (shift) k = "<";
                        break;

                    case Keys.Period:
                        k = ".";
                        if (shift) k = ">";
                        break;

                    case Keys.Slash:
                        k = "/";
                        if (shift) k = "?";
                        break;

                    case Keys.Semicolon:
                        k = ";";
                        if (shift) k = ":";
                        break;

                    case Keys.Backslash:
                        k = "\\";
                        if (shift) k = "|";
                        break;

                    case Keys.Space:

                        k = " ";
                        break;

                    case Keys.Apostrophe:
                        k = "\"";
                        break;

                    case Keys.Minus:
                        k = "-";
                        if (shift) k = "_";
                        break;

                    case Keys.KeyPadAdd:
                        k = "=";
                        if (shift) k = "+";
                        break;

                    case Keys.D0:
                        k = "0";
                        if (shift) k = ")";
                        break;

                    case Keys.D1:
                        k = "1";
                        if (shift) k = "!";
                        break;

                    case Keys.D2:
                        k = "2";
                        if (shift) k = "@";
                        break;

                    case Keys.D3:
                        k = "3";
                        if (shift) k = "#";
                        break;

                    case Keys.D4:
                        k = "4";
                        if (shift) k = "$";
                        break;

                    case Keys.D5:
                        k = "5";
                        if (shift) k = "%";
                        break;

                    case Keys.D6:
                        k = "6";
                        if (shift) k = "^";
                        break; ;
                    case Keys.D7:
                        k = "7";
                        if (shift) k = "&";
                        break;

                    case Keys.D8:
                        k = "8";
                        if (shift) k = "*";
                        break;

                    case Keys.D9:
                        k = "9";
                        if (shift) k = "(";
                        break;

                    case Keys.Enter:
                    case Keys.KeyPadEnter:
                        Enter?.Invoke(Text);
                        ShowClaret = false;
                        Active = false;
                        return;
                        break;

                    default:

                        k = shift ? key.ToString().ToUpper() : key.ToString().ToLower();
                        break;
                }
                if (Text.Length == 0)
                {
                    Text = k;
                    ClaretI = 1;
                }
                else
                {
                    if (ClaretI == Text.Length)
                    {
                        Text = Text + k;
                        ClaretI++;

                        int iv = StartI;
                        string sw = Text.Substring(iv);
                        if (Font.Width(sw) > W)
                        {
                            StartI++;
                        }
                        return;
                    }
                    string os = Text.Substring(0, ClaretI) + k;
                    if (Text.Length > ClaretI)
                    {
                        os = os + Text.Substring(ClaretI);
                    }
                    Text = os;
                    ClaretI++;
                }
            }
            KeyPress = KeyPressFunc;

            void UpdateFunc()
            {
                if (Active)
                {
                    if (Environment.TickCount > NextClaret)
                    {
                        ShowClaret = ShowClaret ? false : true;
                        NextClaret = Environment.TickCount + 450;
                        Console.WriteLine("Claret:" + ShowClaret.ToString());
                    }
                }
            }

            Update = UpdateFunc;

            void ActiveFunc()
            {
                Active = true;
                if (Text == "")
                {
                }
                else
                {
                    ClaretI = Text.Length;
                }
            }

            void DeactiveFunc()
            {
                Active = false;
            }

            Deactivate = DeactiveFunc;
            Activate = ActiveFunc;

            void DrawFunc()
            {
                Col = new Vector4(0.2f, 0.2f, 0.2f, 1.0f);
                //DrawFormSolid(UI.CurUI.Theme.ForegroundCol, -1,0, 1 , H );
                //DrawFormSolid(UI.CurUI.Theme.ForegroundCol, W + 1, 0, 1, H);
                //DrawFormSolid(UI.CurUI.Theme.ForegroundCol, 1, -1, W-1, 1);
                //DrawFormSolid(UI.CurUI.Theme.ForegroundCol, 0, H + 1, W, 1);

                DrawFormSolid(UI.CurUI.Theme.BackgroundCol * 1.5f);

                /*
                for (int i = StartI; i < Text.Substring(StartI).Length; i++)
                {
                    tw += UI.Font.Width(Text.Substring(i, 1));
                    if (tw > W - 10) break;
                    ii++;
                }
                */
                //vc = ClaretI - StartI;

                if (Text == null)
                {
                    Text = " ";
                    return;
                }

                string dis = Text.Substring(StartI);

                int cc = 0;
                int t2 = 0;
                string rtxt = "";
                for (int i = 0; i < dis.Length; i++)
                {
                    rtxt = rtxt + dis.Substring(i, 1);
                    string cr = dis.Substring(i, 1);
                    t2 += Font.Width(cr);
                    if (t2 > W - 30)
                    {
                        break;
                    }
                    cc++;
                }
                ClaretE = cc;

                DrawText(rtxt, 5, 0, UI.CurUI.Theme.TextCol);

                if (Text.Length == 0)
                {
                    ClaretI = 0;
                }

                if (ShowClaret)
                {
                    // Console.WriteLine("Claret!");
                    int cx = 0;
                    if (Text.Length > 0)
                    {
                        int cv = 0;
                        for (int i = StartI; i < ClaretI; i++)
                        {
                            int cw = Font.Width(Text.Substring(i, 1));
                            cx = cx + (cw);
                            cv++;
                            if (cv > cc)
                            {
                                break;
                            }
                        }
                    }
                    DrawFormSolid(UI.CurUI.Theme.ForegroundCol, cx + (Text.Length > 0 ? 5 : 0), 0, 2, 20);
                }
            }

            Draw = DrawFunc;
        }
    }
}
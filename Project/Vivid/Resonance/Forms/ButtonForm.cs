﻿using OpenTK.Mathematics;

using System;

using Vivid.Draw;
using Vivid.Texture;

namespace Vivid.Resonance.Forms
{
    public class ButtonForm : UIForm
    {
        private bool Pressed = false, Over = false;
        private Vector4 NormCol = new Vector4(0.7f, 0.7f, 0.7f, 1f);
        private Vector4 OverCol = new Vector4(0.95f, 0.95f, 0.95f, 1f);
        private Vector4 PressCol = new Vector4(1.2f, 1.2f, 1.2f, 1);
        // public static Vivid.Audio.VSoundSource BleepSrc;
        //  public static Vivid.Audio.VSoundSource BingSrc;
        //  public Vivid.Audio.VSoundSource LocalBleep = null;
        public static Texture2D ButTex = null;
        public static Texture2D HighTex = null;
        public bool Highlight = false;
        public static int lastBing = 0;
        private bool pn = false;
        public Texture2D Icon = null;
        public ButtonForm()
        {
            Selectable = true;

            //     LocalBleep = BleepSrc;
            if (Font == null)
            {
                Font = new Font2.FontTTF("data/font/editfont1.ttf", 6);
            }

            if (ButTex == null)
            {
                // ButTex = UI.CurUI.Theme.ButtonBody;

                //HighTex = new Texture2D("IDE/ui/buttongrey2.png", LoadMethod.Single, true);
            }
            // SetImage(ButTex);
            Col = NormCol;
            CoreTex = null;

            void DrawFunc()
            {
                Pen2D.BlendMod = PenBlend.Alpha;
                if (Root is ScrollBarV)
                {
                    int vv = 1;
                    CoreTex = new Texture2D("ide/ui/winbody3.png", true);
                    pn = true;
                }
                else

                {
                    if (pn)
                    {
                        int vv = 5;
                    }
                    if (CoreTex == null)
                    {
                        CoreTex = UI.CurUI.Theme.ButtonBody;
                    }
                }
                //   DrawFormSolid(new Vector4(0, 0, 0, 1));
                if (Highlight)
                {
                    // DrawForm(HighTex, -2, -2, W + 4, H + 4, false);
                }
                if (Environment.TickCount > pressT && pressTB)
                {
                    if (Over)
                    {
                        Col = OverCol;
                    }
                    else
                    {
                        Col = NormCol;
                    }
                    pressTB = false;
                }
                var pc = Col;
                Col = new Vector4(0.1f, 0.1f, 0.1f, 0.95f);
                DrawForm(CoreTex, -1, -1, W + 2, H + 2);
                Col = pc;
                DrawForm(CoreTex, new Vector4(Col.X * UI.CurUI.FadeAlpha, Col.Y * UI.CurUI.FadeAlpha, Col.Z * UI.CurUI.FadeAlpha, UI.CurUI.FadeAlpha), 1, 1, W - 2, H - 2);

                if (Highlight)
                {

                    DrawFormSolid(new Vector4(0, 0.8f, 0.8f, 1), 2, 2, W - 4, 3);
                    DrawFormSolid(new Vector4(0, 0.8f, 0.8f, 1), 2, 2, 3, H - 4);
                    DrawFormSolid(new Vector4(0, 0.8f, 0.8f, 1), 2, H - 4, W - 4, 3);
                    DrawFormSolid(new Vector4(0, 0.8f, 0.8f, 1.0f), W - 4, 2, 3, H - 4);
                }
                if (Icon != null)
                {
                    DrawForm(Icon, 8, 8, W - 16, H - 16);
                }

                if (Text == "") return;


                //DrawText(Text, (W / 2 - Font.Width(Text) / 2)+4, (H / 2 - Font.Height())+4, new Vector4(0, 0, 0, 1));
                 DrawText(Text, W / 2 - Font.Width(Text) / 2, H / 2 - Font.Height(Text), UI.CurUI.Theme.TextCol);
            }

            void MouseEnterFunc()
            {
                if (Pressed == false)
                {
                    Col = OverCol;
                }

                int nt = Environment.TickCount;
                if (nt > lastBing + 200)
                {
                    // Vivid.Audio.Audio.PlaySource(BingSrc);
                    lastBing = nt;
                }
                Over = true;
            }

            void MouseLeaveFunc()
            {
                if (Pressed == false)
                {
                    Col = NormCol;
                }
                Over = false;
            }

            void MouseMoveFunc(int x, int y, int dx, int dy)
            {
                if (Pressed)
                {
                    // Drag?.Invoke(dx, dy);
                }
            }

            void MouseDownFunc(int b)
            {
                Col = PressCol;
                // Vivid.Audio.Audio.PlaySource(LocalBleep);
                pressT = Environment.TickCount + 1000;
                pressTB = true;
                Pressed = true;
            }

            void MouseUpFunc(int b)
            {
                if (Over)
                {
                    Col = OverCol;
                }
                else
                {
                    Col = NormCol;
                }
                Pressed = false;
                Console.WriteLine("CLicked!");
                if (Click != null)
                {
                    Console.WriteLine("Has click");
                }
                Click?.Invoke(b);
            }

            Draw = DrawFunc;
            MouseEnter = MouseEnterFunc;
            MouseLeave = MouseLeaveFunc;
            MouseMove = MouseMoveFunc;
            MouseDown = MouseDownFunc;
            MouseUp = MouseUpFunc;
        }

        private int pressT = 0;
        private bool pressTB = false;
    }
}
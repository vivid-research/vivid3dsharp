﻿using OpenTK.Mathematics;

using System;

using Vivid.Texture;

namespace Vivid.Resonance.Forms
{
    public delegate void ValueChanged(float v);

    public class ScrollBarV : UIForm
    {
        public float Cur = 0;
        public float Max = 1;
        public float CurView = 0.1f;
        public static Texture2D But;
        public PlainButtonForm ScrollBut;
        public ValueChanged ValueChange = null;

        public void SetMax(float max)
        {
            Max = max;
            float ys = H / Max;

            float by = H * ys;
            // ScrollBut.H = (int)by;
            if (Max < H)
            {
                NotUse = true;
            }
            else
            {
                NotUse = false;
            }
        }

        public int GetCur()
        {

            float dh = (float)H / (float)Max;
            float ah = dh * (float)H;
            if (ah > H) ah = H;

            float SI = (float)(SY) / (float)(H - ah);

            float av = Max * SI;

            return (int)av;

        }

        private bool NotUse = false;
        public static Texture2D ScrollTex = null;
        bool moving = false;
        float SY = 0;
        public ScrollBarV()
        {
            if (ScrollTex == null)
            {
                ScrollTex = new Texture2D("ide/ui/winBody3.png", LoadMethod.Single, true);
            }
            if (But == null)
            {
                But = new Texture2D("data/UI/Skin/but_normal.png", LoadMethod.Single, true);
            }
            // ScrollBut = new PlainButtonForm().Set(0, 0, 10, 10, "") as PlainButtonForm;
            // ScrollBut.CoreTex = ScrollTex;

            MouseDown = (b) =>
            {

                moving = true;

            };

            MouseUp = (b) =>
            {
                moving = false;
            };



            MouseMove = (x, y, xd, yd) =>
            {
                //Environment.Exit(0);


                if (Max < 0) Max = 0;
                float dh = (float)H / (float)Max;
                float ah = dh * (float)H;
                if (ah > H)
                {
                    ah = H;
                    return;
                }

                if (moving)
                {

                    SY = SY + yd;
                    if (SY < 0) SY = 0;
                    if (SY > (H - ah))
                    {
                        SY = H - ah;
                    }

                    Console.WriteLine("SY:" + SY);
                    //changed();
                    ValueChange?.Invoke(GetCur());

                };

            };

            void DrawFunc()
            {
                float ah = 0;
                if (H > Max)
                {
                    ah = H;
                    //		printf("H:%d Max:%d \n", H, Max);

                }
                else
                {

                    float dh = (float)H / (float)Max;
                    ah = dh * (float)H;
                    if (ah > H) ah = H;
                }
                Console.WriteLine("RY:" + SY);
                DrawFormSolid(new Vector4(0.8f, 0.8f, 0.8f, 1), 1, (int)SY, W, (int)ah);
                //   DrawFormSolid(new Vector4(0.3f, 0.3f, 0.3f, 0.8f));
                //    ScrollBut.CoreTex = ScrollTex;
            }


            void ChangedFunc()
            {
                //ScrollBut.X = 0;
                //ScrollBut.Y = 0;

                //ScrollBut.W = W;
                //ScrollBut.H = 20;
            }

            Changed = ChangedFunc;

            Draw = () =>
            {
                float ah = 0;
                if (H > Max)
                {
                    ah = H;
                    //		printf("H:%d Max:%d \n", H, Max);

                }
                else
                {

                    float dh = (float)H / (float)Max;
                    ah = dh * (float)H;
                    if (ah > H) ah = H;
                }
                //ah = H;

                DrawFormSolid(new Vector4(0.8f, 0.8f, 0.8f, 1), 1, (int)SY, W, (int)ah);

            };
            //Add(ScrollBut);

            void DragFunc(int x, int y)
            {
                if (NotUse) return;
                ScrollBut.Y += y;
                Console.WriteLine("Y:" + y + " SY:" + ScrollBut.Y);

                if (ScrollBut.Y < 0)
                {
                    ScrollBut.Y = 0;
                }

                if (ScrollBut.Y > (H - ScrollBut.H))
                {
                    ScrollBut.Y = (H - ScrollBut.H);
                }

                float xs = (float)ScrollBut.Y / ((float)H - (float)ScrollBut.H);

                Cur = Max * xs;

                ValueChange?.Invoke(Cur);
            }

            //    ScrollBut.Drag = DragFunc;
        }
    }
}
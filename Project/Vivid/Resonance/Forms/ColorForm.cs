﻿using OpenTK.Mathematics;
namespace Vivid.Resonance.Forms
{
    public class ColorForm : UIForm
    {
        public Vector4 Col = UI.CurUI.Theme.BackgroundCol;
        public ColorForm()
        {
            Draw = () =>
            {
                //DrawFormSolid(UI.CurUI.Theme.ForegroundCol, 1, 1, W, 2);
                DrawFormSolid(Col, 1, 2, W - 2, H - 2);
            };
        }
    }
}
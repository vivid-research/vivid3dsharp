﻿using System;

//using Vivid

namespace Vivid.Resonance.Forms
{
    public class HorizontalSliderForm : UIForm
    {
        public float Value = 0;
        public float Max = 10;
        public float Min = 0.0001f;
        public ValueChanged ValChanged;

        public float GetValue()
        {
            float vp = (float)Slider.X / (float)W;

            Console.WriteLine("X:" + Slider.X + " W:" + W + " VP:" + vp);
            return Min + Max * vp;
        }

        public void SetValue(float v)
        {
            Value = v;
            float vv = Value / Max;
            if (vv < Min) vv = Min;
            Slider.X = (int)((float)W * vv);
        }

        public ButtonForm Slider;

        public HorizontalSliderForm()
        {
            Draw = () =>
            {
                DrawFormSolid(UI.CurUI.Theme.BackgroundCol);
            };
            Slider = new ButtonForm();
            Add(Slider);
            Slider.Drag = (x, y) =>
            {
                Slider.X += x;
                if (Slider.X < 0) Slider.X = 0;
                if (Slider.X > W) Slider.X = W;
                ValChanged?.Invoke(GetValue());
            };

            AfterSet = () =>
            {
                Slider.Set(0, -5, 10, 15, "");
            };
        }
    }
}
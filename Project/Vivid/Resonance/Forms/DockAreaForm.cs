﻿using Vivid.Texture;

namespace Vivid.Resonance.Forms
{
    public class DockAreaForm : UIForm
    {
        public static Texture2D PlaceHolder = null;

        public DockAreaForm()
        {
            if (PlaceHolder == null)
            {
                PlaceHolder = new Texture2D("IDE/UI/placeholder.png", false);
            }
            AfterSet = () =>
            {
                foreach (var f in Forms)
                {
                    f.Set(0, 0, W - 5, H, f.Text);
                    f.Docked = true;
                }
            };
            Draw = () =>
            {
                // DrawFormSolid(UI.CurUI.Theme.BackgroundCol);
            };
        }
    }
}
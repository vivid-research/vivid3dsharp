﻿using Vivid.Texture;

namespace Vivid.Resonance.Forms
{
    public class GroupForm : UIForm
    {
        public static Texture.Texture2D GroupImg = null;

        public GroupForm()
        {
            Draw = () =>
            {
                if (GroupImg == null)
                {
                    GroupImg = new Texture2D("data/ui/group1.png", LoadMethod.Single, true);
                }

                DrawFormSolid(UI.CurUI.Theme.ForegroundCol);
                DrawFormSolid(UI.CurUI.Theme.BackgroundCol, 1, 1, W - 2, H - 2);
            };
        }
    }
}
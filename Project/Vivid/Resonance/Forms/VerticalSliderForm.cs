﻿
using System;

namespace Vivid.Resonance.Forms
{
    public class VerticalSliderForm : UIForm
    {
        public ValueChanged ValueChange;
        public int Max
        {
            get;
            set;
        }

        public int RenderH
        {
            get;
            set;
        }
        public int Cur
        {
            get;
            set;
        }
        bool moveto = false;
        public int GetCur()
        {
            float dh = (float)H / (float)Max;
            float ah = dh * H;
            if (ah > H) ah = H;
            //float cy = Cur
            //;
            float bh = H - ah;
            float mi = Cur / bh;
            Console.WriteLine("Cur:" + (int)(float)Max * mi);

            int v = (int)((float)Max * mi);
            if (v < 0) v = 0;
            if (v > H) v = H;
            return (int)(float)(Max * mi); ;
        }
        public VerticalSliderForm()
        {
            Max = 100;


            void DrawFunc()
            {

                float dh = (float)H / (float)Max;
                float ah = dh * H;
                if (ah > H) ah = H;
                if (ah < 30) return;
                DrawFormSolid(UI.CurUI.Theme.ForegroundCol, 0, Cur, W, (int)ah,true);
            }

            this.MouseMove = (x, y, xd, yd) =>
            {
                //Console.WriteLine("X:" + x + " Y:" + y);
                float dh = (float)H / (float)Max;
                float ah = dh * H;
                if (ah > H) ah = H;
                //  DrawFormSolid(UI.CurUI.Theme.ForegroundCol, 0, Cur, W, (int)ah);
                if (y > Cur && y < Cur + ah)
                {
                }
                else
                {
                 //   return;
                }

             



                //
                dh = (float)H / (float)Max;
                ah = dh * H;
                if (ah > H) ah = H;

                int maxh = Max;


                if (moveto)
                {
                    Cur = Cur + yd;
                    if((Cur+ah)>H)
                    {
                        Cur = Cur - yd;
                        return;
                    }
                    if (Cur > maxh) Cur = maxh;
                    if (Cur < 0) Cur = 0;
                    if (ValueChange != null)
                    {
                        ValueChange(GetCur());
                    }
                }
            };

            this.MouseDown = (b) =>
            {

                moveto = true;
            };

            this.MouseUp = (b) =>
            {
                moveto = false;
            };

            Draw = DrawFunc;


        }

    }
}

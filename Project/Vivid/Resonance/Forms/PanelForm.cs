﻿using Vivid.Texture;

namespace Vivid.Resonance.Forms
{
    public class PanelForm : UIForm
    {
        public static Texture2D Tex = null;

        public PanelForm()
        {
            if (Tex == null)
            {
                Tex = new Texture2D("data/nxUI/window/title2.png", LoadMethod.Single, false);
            }
            void DrawFunc()
            {
                DrawForm(Tex, 0, 0, Font.Width(Text) + 5, 25);
                DrawText(Text, 2, 2, UI.CurUI.Theme.TextCol);
                DrawForm(Tex, 0, 25, -1, -1, false);
            }

            Draw = DrawFunc;
        }
    }
}
﻿using OpenTK.Mathematics;
using System.Collections.Generic;

using Vivid.Texture;

namespace Vivid.Resonance.Forms
{
    public class WindowForm : UIForm
    {
        public static Texture2D TitleImg = null;
        public static Texture2D BodyImg = null;
        public static Texture2D BodyNorm = null;
        private ButtonForm resize;

        public bool LockedPos = false;
        public bool LockedSize = false;
        public static Texture2D Shadow = null;
        public UIForm Body, Title;
        public ButtonForm Close = null;

        public List<WindowForm> Tabs = new List<WindowForm>();

        public void AddTab(WindowForm tab)
        {

            Tabs.Add(tab);


        }

        public WindowForm ActiveTab = null;

        public WindowForm()
        {
            if (Shadow == null)
            {
                Shadow = new Texture2D("data/UI/Shadow1.png", LoadMethod.Single, true);
                TitleImg = new Texture2D("ide/ui/windowTitle3.png", LoadMethod.Single, true);
                BodyImg = new Texture2D("ide/ui/winbody4.png", LoadMethod.Single, true);
                BodyNorm = new Texture2D("data/UI/normal/winnorm5.jpg", LoadMethod.Single, false);
            }

            // Title = new PlainButtonForm().Set(0, 0, Font.Width(Text) + 20, 11, Text).SetImage(UI.CurUI.Theme.WindowTitle);

            Body = new ColorForm().Set(0, 25, W, H - 27, "");
            Body.Col = new Vector4(1, 1, 1, 0.82f);
            Body.Peak = false;
            Body.Refract = false;

            //   Close = new ButtonForm().Set(Title.W + 2, 4, 16, 16, "x") as ButtonForm;

            //  Title.Add(Close);

            //close.Click = (b) =>
            //{
            //};

            //body.Blur = 0.1f;
            // body.RefractV = 0.72f;

            resize = (ButtonForm)new ButtonForm().Set(W - 14, H - 14, 14, 14, "");

            AfterSet = () =>
            {
                Text = Text;
            };

            void ResizeDrag(int x, int y)
            {
                if (Docked) return;
                if (LockedSize)
                {
                    return;
                }

                Set(X, Y, W + x, H + y, Text);
                Body.Set(0, 27, W, H - 29, "");
                resize.X = W - 14;
                resize.Y = H - 14;
            }

            resize.Drag = ResizeDrag;

            void DragFunc(int x, int y)
            {
                //Environment.Exit(0);

                if (LockedPos)
                {
                    return;
                }
                if (Docked) return;
                X = X + x;
                Y = Y + y;
            }

            // Title.Drag = DragFunc;

            //   Add(Title);
            Add(Body);
            //  Others.Add(Title);
            Others.Add(Body);
            //  Add(resize);

            void ChangedFunc()
            {
                // title.Text = Text;
                //Title.W = Font.Width(Text) + 10;
                //Close.X = Title.W + 0;
                //Close.Y++;
                //Title.H = 26;
                if (Body != null)
                {
                    Body.Y = 0;
                    Body.H = H ;
                    Body.W = W;
                }
                resize.X = W - 14;
                resize.Y = H - 20;
                SubChanged?.Invoke();
            }

            Changed = ChangedFunc;


            void DrawFunc()
            {
                // DrawFormBlur ( Shadow, 0.1f, new Vector4 ( 0.9f, 0.9f, 0.9f, 0.98f ), 5, 5, W + 30, H + 30 );
                //DrawForm(TitleImg, 0, 0, W, 20);
                if (tabOVer == 0)
                {
                    DrawFormSolid(UI.CurUI.Theme.ForegroundCol * 0.6f, 0, 0, Font.Width(Text) + 10, 30);
                }
                else
                {
                    DrawFormSolid(UI.CurUI.Theme.ForegroundCol * 0.3f, 0, 0, Font.Width(Text) + 10, 30);
                }
                DrawText(Text, 2, 2, UI.CurUI.Theme.TextCol * 1);
                int dx = Font.Width(Text) + 10 + 2;
                int ti = 1;
                foreach (var tab in Tabs)
                {
                    if (tabOVer == ti)
                    {
                        DrawFormSolid(UI.CurUI.Theme.ForegroundCol * 0.6f, dx, 0, Font.Width(tab.Text) + 10, 30);

                    }
                    else
                    {
                        DrawFormSolid(UI.CurUI.Theme.ForegroundCol * 0.3f, dx, 0, Font.Width(tab.Text) + 10, 30);
                    }
                    DrawText(tab.Text, dx + 2, 2, UI.CurUI.Theme.TextCol * 1);
                    dx = dx + Font.Width(tab.Text) + 10 + 2;
                    ti++;
                }

                tabOVer = -1;
            }

            MouseDown = (b) =>
            {
                if (tabOVer > 0)
                {
                    //Instead = Tabs[tabOVer - 1];
                    //Tabs[tabOVer - 1].Root = Root;
                    this.Forms.Remove(Body);
                    this.Forms.Add(Tabs[tabOVer - 1].Body);
                    CurBody = Tabs[tabOVer - 1].Body;
                    Tabs[tabOVer - 1].Body.Root = this;
                }
                if (tabOVer == 0)
                {
                    if (CurBody != null)
                    {
                        this.Forms.Remove(CurBody);
                        Forms.Add(Body);
                        CurBody = null;
                    }
                }
            };

            MouseMove = (x, y, xd, yd) =>
            {
                tabOVer = -1;
                if (x >= 0 && x <= Font.Width(Text) + 10)
                {
                    if (y >= 0 && y <= 30)
                    {
                        tabOVer = 0;
                    }
                }
                int ti = 0;
                int dx = Font.Width(Text) + 10 + 2;
                foreach (var tab in Tabs)
                {
                    if (x >= dx && x <= dx + Font.Width(tab.Text) + 10)
                    {
                        if (y >= 0 && y <= 30)
                        {
                            tabOVer = 1 + ti;
                        }
                    }
                    ti++;
                    dx = dx + Font.Width(tab.Text) + 10 + 2;
                }
            };

            Draw = DrawFunc;
        }
        int tabOVer = -1;
        UIForm CurBody = null;
        public WindowForm NoResize()
        {
            Forms.Remove(resize);
            return this;
        }
    }
}
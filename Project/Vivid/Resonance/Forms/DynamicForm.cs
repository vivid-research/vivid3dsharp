﻿using OpenTK.Mathematics;

namespace Vivid.Resonance.Forms
{
    public delegate Vector2 DynamicDraw(Vector2 Coord);

    public class DynamicForm
    {
        public DynamicDraw DrawFunc = null;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vivid.Resonance.Forms
{
    public class VerticalScrollerForm : UIForm
    {

        public ValueChanged ValueChange = null;

        public VerticalScrollerForm()
        {

            Draw = () =>
            {

                DrawFormSolid(UI.CurUI.Theme.ForegroundCol, 0, GetBarY(), 15, BarHeight(), true);

                CurValue();
            };

            MouseDown = (b) =>
            {
                if (b == 0)
                {

                    bdown = true;

                }

            };

            MouseUp = (b) =>
            {

                if (b == 0)
                {
                    bdown = false;
                }

            };

            MouseMove = (x, y, xd, yd) =>
            {

                //if (y > GetBarY() && y < GetBarY() + BarHeight())
                {
                    // Console.WriteLine("MOVING");


                    if (bdown)
                    {

                        barY += yd;
                        if (barY + BarHeight() > H)
                        {
                            barY = barY - yd;
                        }
                        if (barY < 0) barY = 0;

                        Console.WriteLine("Moved Bar");

                        ValueChange?.Invoke(CurValue());

                    }
                }
            };

        }
        bool bdown = false;
        int barY = 0;
        public int GetBarY()
        {
            return barY;
        }

        public int MaxValue = 0;
        public int CurValue()
        {

            int y1 = GetBarY() + BarHeight();
            int y2 = H - y1;
            int y3 = H - BarHeight();
            float fv = (float)y2 / (float)y3;

            fv = 1.0f - fv;

            return (int)((float)(MaxValue) * fv);

     

            

            return 0;
        }

        public int BarHeight()
        {

            float bf = (float)H / (float)MaxValue;

            return (int)(float)((float)(H) * bf);

        }

    }
}

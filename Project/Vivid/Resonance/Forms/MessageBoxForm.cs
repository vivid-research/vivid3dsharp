﻿using Vivid.Resonance.Forms;

namespace VividEngine.Resonance.Forms
{
    public class MessageBoxForm : WindowForm
    {
        public ButtonForm OK, Cancel;
        public LabelForm Msg;

        public MessageBoxForm(string msg)
        {
            AfterSet = () =>
            {
                Title.Text = Text;
                OK = new ButtonForm().Set(5, Body.H - 35, 60, 30, "OK") as ButtonForm;
                Cancel = new ButtonForm().Set(Body.W - 65, Body.H - 35, 60, 30, "Cancel") as ButtonForm;
                Msg = new LabelForm().Set(5, 5, 20, 20, msg) as LabelForm;
                Body.Add(OK, Cancel, Msg);
            };
        }
    }
}
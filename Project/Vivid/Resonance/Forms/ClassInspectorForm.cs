﻿using System;
using OpenTK.Mathematics;

using System.Collections.Generic;
using System.Reflection;

using Vivid.Content;
using Vivid.Script;
using Vivid.Texture;
namespace Vivid.Resonance.Forms
{
    public class ClassInspectorForm : UIForm
    {

        public static dynamic Cls = null;
        public static dynamic ClsD = null;
        public VerticalScrollerForm Scroller;
        public ClassInspectorForm()
        {

            Draw = () =>
            {
                if(Scroller == null)
                {
                    Scroller = new VerticalScrollerForm();
                    Scroller.Set(W - 10, 0, 15, H, "");
                    Add(Scroller);

                }
                DrawFormSolid(UI.CurUI.Theme.BackgroundCol,0,0,-1,-1,true);

            };

        }

        bool used = false;
        Dictionary<string, bool> Open = new Dictionary<string, bool>();
        public void Rebuild()
        {

            Forms.Clear();

            var type = Cls.GetType();

            Dictionary<string, List<PropertyInfo>> InfoMap = new Dictionary<string, List<PropertyInfo>>();
            InfoMap.Add("Misc", new List<PropertyInfo>());

            foreach (var prop in type.GetProperties())
            {
                bool added = false;
                foreach (var a in prop.GetCustomAttributesData())
                {
                    var ty = a.ToString().Substring(a.ToString().IndexOf("\"") + 1);
                    ty = ty.Substring(0, ty.Length - 3);
                    Console.WriteLine("NodeType:" + ty);
                    List<PropertyInfo> pl;
                    if (Open.ContainsKey(ty))
                    {

                    }
                    else
                    {
                        Open.Add(ty, false);
                    }
                    if (InfoMap.ContainsKey(ty))
                    {
                        pl = InfoMap[ty];
                    }
                    else
                    {
                        InfoMap.Add(ty, new List<PropertyInfo>());
                        pl = InfoMap[ty];
                    }
                    pl.Add(prop);
                    //InfoMap.add
                    added = true;

                }
                if (!added)
                {
                    List<PropertyInfo> pl = InfoMap["Misc"];
                    pl.Add(prop);
                    if(Open.ContainsKey("Misc"))
                    {

                    }
                    else
                    {
                        Open.Add("Misc", false);
                    }
                    int a = 5;
                }
            }

            int dy = 5;

            foreach(var key in InfoMap.Keys)
            {
                Console.WriteLine("Group:" + key);

                var cf = new ColorForm().Set(0, dy, W-15, 30) as ColorForm;
                cf.Col = UI.CurUI.Theme.BackgroundCol * 1.5f;
                Add(cf);

                var glab = new LabelForm().Set(5, dy + 1, 20, 20, key);

                Add(glab);

                var obut = new ButtonForm().Set(W - 70, dy + 6, 40, 20, "\\/\\");

                Add(obut);

                obut.Click = (b) =>
                {

                    Open[key] = Open[key] ? false : true;
                    SetClass(Cls);

                };

                dy = dy + 35;

                bool open = Open[key];

                if (open)
                {
                 

                    foreach (var prop in InfoMap[key])
                    {
                        Console.WriteLine("Prop:" + prop.Name);

                        PropertyInfo info = prop;

                        var plab = new LabelForm().Set(5, dy - 2, 5, 5, info.Name);
                        dy = dy + 30;

                        used = true;

                        string name = info.PropertyType.Name;

                        if (name.Length > 3)
                        {
                            if (name.Substring(0, 4) == "List")
                            {
                                name = "List";
                            }
                        }

                        if (info.PropertyType.IsEnum)
                        {
                            name = "Enum";
                        }

                        dy=AddProp(name,info,dy);

                        if (used)
                        {
                            Add(plab);
                        }
                        else
                        {
                            dy = dy - 30;
                        }


                    }
                }
            }

            //Console.WriteLine("DY:" + dy);

            if (dy > Scroller.H)
            {
              
              //  dy = dy - Scroller.H;

            }
            Scroller.MaxValue = dy;


            Console.WriteLine("BarHeight:" + Scroller.BarHeight());
            Scroller.ValueChange = (v)=>{
                     OffY = -(int)v;
                Console.WriteLine("V:" + v + " MAX:" + dy);
            };
            //while (true) { }
        }

        int AddProp(string name,PropertyInfo info,int dy)
        {
            int midx = 2;
            switch (name)
            {
                case "VSoundSource":

                    /*
                    var sndsrc = info.GetValue(ClsD) as Vivid.Audio.VSoundSource;

                    string sname = "None";

                    Vivid.Texture.Texture2D stex;

                    if (sndsrc != null)
                    {
                        sname = sndsrc.Name;
                        stex = SoundImg;
                    }
                    else
                    {
                        stex = NoSoundImg;
                    }

                    var snd_name = new TextBoxForm().Set(5, dy, 120, 25, sname);

                    Add(snd_name);

                    dy += 30;

                    var snd_prev = new ImageForm().Set(5, dy, 128, 128).SetImage(stex);

                    snd_prev.DraggedObj = (o) =>
                    {
                        // -EDIT

                        var drago = o as DragObject;
                        Console.WriteLine("Prev!");
                        if (drago.DragObj is Vivid.Content.ContentEntry)
                        {
                            var ce = drago.DragObj as Vivid.Content.ContentEntry;
                            // var ntex = new Texture2D(ce.FullPath, LoadMethod.Single, true);

                            var sobj = Vivid.Audio.Audio.LoadSound(ce.FullPath);
                            //Console.WriteLine("Setting Content");
                            try
                            {
                                info.SetValue(ClsD, sobj);
                                ClassEditorForm.Main.SetClass(ClsD);
                                //SetObj(ClsD);
                            }
                            catch
                            {
                            }
                        }
                    };

                    snd_prev.CanDrop = true;

                    Add(snd_prev);

                    dy += 135;

                    //use = true;

                    */
                    break;

                case "Enum":

                    var p_enum = info.GetValue(ClsD);
                    used = true;
                    dy = dy - 27;
                    DropDownListForm list_f = new DropDownListForm().Set(70, dy + 1, 160, 25) as DropDownListForm;

                    foreach (var enum_name in info.PropertyType.GetEnumNames())
                    {
                        list_f.AddItem(enum_name);
                    }

                    list_f.CurrentItem = p_enum.ToString();

                    list_f.SelectedItem = (item) =>
                    {
                        string ai = "";
                        int ee = 0;
                        foreach (var enum_name in info.PropertyType.GetEnumNames())
                        {
                            if (enum_name == item)
                            {
                                break;
                            }
                            ee++;
                        }
                        info.SetValue(ClsD, ee);
                    };

                    Add(list_f);

                    dy += 35;
                    used = true;
                    //use = true;
                    break;

                case "Material3D":

                    dy = dy + 30;
                    var mat = info.GetValue(ClsD) as Vivid.Material.Material3D;

                    //-INSPECT

                    var m_name = new TextBoxForm().Set(5, dy, 120, 25, mat.Name);

                    var m_edit = new ButtonForm().Set(130, dy, 60, 25, "Edit");
                    used = true;
                    m_edit.Click = (b) =>
                    {
                        ClassEditorForm.Main.SetClass(mat);
                        //SetObj(mat);
                    };

                    Add(m_name);
                    Add(m_edit);
                    dy += 30;
                    //use = true;

                    break;

                case "string":
                case "String":
                    var str = info.GetValue(ClsD) as string;
                    if (str == null)
                    {
                        str = "";
                    }
                    // dy = dy + 30;
                    used = true;

                    // -INSPECT
                    var str_box = new TextBoxForm().Set(5, dy, 220, 25, str);
                    //Console.WriteLine("TB==" + str + "!");
                    if (info.Name.Contains("Path"))
                    {
                        var path_sel = new ButtonForm().Set(230, dy, 60, 25, "Select");
                        Add(path_sel);

                        path_sel.Click = (b) =>
                        {
                            var path_r = new RequestFileForm("Select file...");
                            UI.CurUI.Top = path_r;
                            path_r.Selected = (path) =>
                            {
                                info.SetValue(ClsD, path);
                                UI.CurUI.Top = null;
                                //                                    SetObj(Obj);
                                ClassEditorForm.Main.SetClass(ClsD);
                            };
                        };
                    }

                    Add(str_box);
                    //use = true;
                    dy += 30;
                    break;

                case "Texture2D":
                    dy = dy + 45;
                    used = true;
                    var tex = info.GetValue(ClsD) as Vivid.Texture.Texture2D;

                    if (tex == null)
                    {
                        info.SetValue(ClsD, WhiteTex);
                        tex = WhiteTex;
                    }

                    var t_name = new TextBoxForm().Set(5, dy, 120, 25, tex.Name);

                    Add(t_name);

                    dy += 30;

                    var m_prev = new ImageForm().Set(5, dy, 128, 128, "").SetImage(tex) as ImageForm;

                    var set_image = new ButtonForm().Set(140, dy, 80, 25, "Set Image") as ButtonForm;

                    Add(set_image);

                    set_image.Click = (b) =>
                    {
                        var sir = new RequestFileForm("Select image..", "C:/");

                        UI.CurUI.Top = sir;

                        sir.Selected = (path) =>
                        {
                            UI.CurUI.Top = null;
                            info.SetValue(ClsD, new Vivid.Texture.Texture2D(path, true));
                            ClassEditorForm.Main.SetClass(ClsD);
                            //SetObj(Obj);
                        };
                    };

                    dy += 136;

                    var nm_name = new TextBoxForm().Set(5, dy, 120, 25, tex.Name);

                    m_prev.DraggedObj = (o) =>
                    {
                        // -EDIT

                        var drago = o as DragObject;
                        Console.WriteLine("Prev!");
                        if (drago.DragObj is Vivid.Content.ContentEntry)
                        {
                            var ce = drago.DragObj as Vivid.Content.ContentEntry;
                            var ntex = new Vivid.Texture.Texture2D(ce.FullPath, true);
                            //Console.WriteLine("Setting Content");
                            try
                            {
                                info.SetValue(ClsD, ntex);
                                ClassEditorForm.Main.SetClass(ClsD);
                            }
                            catch
                            {
                            }
                        }
                    };

                    m_prev.CanDrop = true;

                    Add(m_prev);

                    //use = true;

                    break;

                case "List":

                    var p_list = info.GetValue(ClsD);
                    used = true;
                    dynamic p_l = p_list;
                    if (p_l != null)
                    {
                       // dy = dy + 27;
                        foreach (dynamic litem in p_l)
                        {
                            // -INSPECT
                            UIForm l_itemname;
                            UIForm l_edit;
                            UIForm l_remove;
                            try
                            {
                                l_itemname = new TextBoxForm().Set(8, dy, 200, 25, litem.GetName());
                                l_edit = new ButtonForm().Set(214, dy, 60, 25, "Edit") as ButtonForm;
                                l_remove = new ButtonForm().Set(276, dy, 80, 25, "Remove") as ButtonForm;
                            }
                            catch
                            {
                                continue;
                            }

                            Add(l_itemname);
                            Add(l_remove);
                            Add(l_edit);

                            l_edit.Click = (b) =>
                            {
                                SetClass(litem);
                            };

                            l_remove.Click = (b) =>
                            {
                                p_l.Remove(litem);
                                SetClass(ClsD);
                            };

                            dy += 35;
                        }
                    }

                    //dy = dy + 10;
                    var l_dragzone = new DragZoneForm().Set(6, dy, 200, 25) as DragZoneForm;

                    l_dragzone.DraggedObj = (o) =>
                    {
                        // -EDIT

                        if (o.DragObj is Vivid.Content.ContentEntry)
                        {
                            var ce = o.DragObj as Vivid.Content.ContentEntry;
                            var le = ce.Load();

                            // var les = new NodeVS(le, ClsD);
                            // p_l.Add(les);
                            if (ce is ContentScript)
                            {
                                var nc = ClsD.AddComponent(Vivid.Scripting.NodeScriptCompiler.Compile(le));
                                nc.Loaded();
                                nc.Node = ClsD;
                                nc.Name = le;
                                // le.InitNode();

                                p_l.Add(nc);
                            }
                            SetClass(ClsD);
                        };
                    };
                    Add(l_dragzone);
                    dy = dy + 25;

                    break;
                case "Single":

                    float vals = info.GetValue(ClsD);
                    used = true;
                    var vs_box = new TextBoxForm().Set(30 + midx - 25, dy, 75, 25, vals.ToString()) as TextBoxForm;
                    vs_box.Enter = (v) =>
                    {
                        try
                        {
                            float v1 = float.Parse(v);
                            info.SetValue(ClsD, v1);
                            ClsD.Changed();
                        }
                        catch
                        {

                        }
                    };

                    Add(vs_box);
                    dy = dy + 27;

                    break;

                case "Vector3":

                    var vec3 = info.GetValue(ClsD);
                    used = true;
                    dy = dy;

                    var x_b = new ColorForm().Set(4, dy - 2, 28, 26) as ColorForm;
                    x_b.Col = UI.CurUI.Theme.BackgroundCol * 2.0f;

                    var y_b = new ColorForm().Set(109, dy - 2, 28, 26) as ColorForm;
                    y_b.Col = UI.CurUI.Theme.BackgroundCol * 2;// new Vector4(0.3,0.3f, 0.3f, 1.0f);

                    var z_b = new ColorForm().Set(214, dy - 2, 28, 26) as ColorForm;
                    z_b.Col = UI.CurUI.Theme.BackgroundCol * 2;// new Vector4(0, 0.3f, 0.3f, 1.0f);



                    Add(x_b, y_b, z_b);


                    var x_lab = new LabelForm().Set(5 + midx + 2, dy - 2, 25, 25, "X") as LabelForm;
                    var y_lab = new LabelForm().Set(110 + midx + 2, dy - 2, 25, 25, "Y");
                    var z_lab = new LabelForm().Set(215 + midx + 2, dy - 2, 25, 25, "Z");

                    var x_box = new TextBoxForm().Set(30 + midx + 1, dy, 75, 25, vec3.X.ToString()) as TextBoxForm;
                    var y_box = new TextBoxForm().Set(135 + midx + 1, dy, 75, 25, vec3.Y.ToString()) as TextBoxForm;
                    var z_box = new TextBoxForm().Set(240 + midx + 1, dy, 75, 25, vec3.Z.ToString()) as TextBoxForm;

                    x_box.Enter = (val) =>
                    {
                        try
                        {
                            vec3.X = float.Parse(val);
                        }
                        catch
                        {
                        }
                        info.SetValue(ClsD, vec3);
                        ClsD.Changed();
                    };

                    y_box.Enter = (val) =>
                    {
                        try
                        {
                            vec3.Y = float.Parse(val);
                        }
                        catch
                        {
                        }
                        info.SetValue(ClsD, vec3);
                        ClsD.Changed();
                    };

                    z_box.Enter = (val) =>
                    {
                        try
                        {
                            vec3.Z = float.Parse(val);
                        }
                        catch
                        {
                        }
                        info.SetValue(ClsD, vec3);
                        ClsD.Changed();
                    };

                    Add(x_lab, y_lab, z_lab);
                    Add(x_box, y_box, z_box);

                    dy = dy + 27;

                    break;

                case "Boolean":
                    used = true;
                    var b_val = info.GetValue(ClsD);

                    dy = dy - 28;
                    midx = 70;

                    midx = Font.Width(info.Name) + 10;

                    var bool_f = new CheckBoxForm().Set(midx + 4, dy + 6, 15, 15) as CheckBoxForm;

                    bool_f.Checked = b_val;

                    Add(bool_f);

                    //Body.Add(bool_f);

                    bool_f.Check = (c) =>
                    {
                        info.SetValue(Cls, c);
                        //  prop.SetValue(obj, c);
                    };

                    dy = dy + 27;
                    //py = py + 15;
                    //use = true;

                    break;

                default:
                    used = false;
                    //dy = dy - 27;
                    //dy = dy + 25;
                    break;
            }

            return dy;
        }

        public void SetClass(dynamic cls)
        {
            Cls = cls;
            ClsD = cls;
            Rebuild();
            Add(Scroller);

        }


    }
}

﻿using OpenTK.Mathematics;

using System;

namespace Vivid.Resonance.Forms
{
    public class DragObject : UIForm
    {
        public Texture.Texture2D DragImg = null;
        public string Label = "";

        public Object DragObj;
        public Object DragSubObj;

        public DragObject()
        {
            W = 64;
            H = 64;

            void DrawFunc()
            {
                DrawForm(DragImg, new Vector4(0.9f, 0.9f, 0.9f, 0.65f));
                DrawText(Label, H / 2 - 5, 5, new Vector4(1, 1, 1, 1));
            }

            Draw = DrawFunc;
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace Vivid.Resonance.Forms
{

    public class AppMenuForm : UIForm
    {

        public List<MenuItem> Items = new List<MenuItem>();
        private Texture.Texture2D iRight;

        public void AddItem(MenuItem item)
        {

            Items.Add(item);

        }

        public MenuItem AddItem(string item)
        {

            Items.Add(new MenuItem(item));
            return Items[Items.Count - 1];
        }


        private MenuItem OverItem;
        private MenuItem OpenMenu;

        bool MenuBounds(MenuItem it, int dx, int dy, int x, int y)
        {

            int mh = 0;

            foreach (var i in it.Items)
            {
                if (i.Seperator)
                {
                    mh = mh + 8;
                }
                else
                {
                    mh = mh + 27;
                }


            }

            mh = mh + 15;

            if (x > dx & x < dx + 200 & y > dy & y < dy + mh)
            {
                return true;
            }

            foreach (var i in it.Items)
            {
                if (i.Open && i.Items.Count > 0)
                {

                    if (MenuBounds(i, dx + 200, dy, x, y))
                    {
                        return true;
                    }

                    //DrawMenu(i, mx + 209, my - 33);


                }
            }
            return false;
        }

        public override bool InBounds(int x, int y)
        {

            int mx = 30;



            foreach (var i in Items)
            {

                if (i.Open)
                {

                    if (MenuBounds(i, mx - 15, 25, x, y))
                    {
                        return true;
                    }

                }
                mx = mx + Font.Width(i.Text);

                mx = mx + 40;

            }
            return base.InBounds(x, y);

        }
        MenuItem Opened = null;

        public AppMenuForm()
        {

            iRight = new Texture.Texture2D("data/edit/iRight.png", true);
            Font = UI.CurUI.Theme.MenuFont;

            MouseDown = (b) =>
            {

                if (OverItem != null)
                {
                    if (OverItem.Items.Count > 0)
                    {
                        if (OverItem.Open)
                        {
                            OverItem.Open = false;
                        }
                        else
                        {
                            if (Opened != null)
                            {
                                Opened.Open = false;
                            }
                            OverItem.Open = true;
                            Opened = OverItem;
                        }
                    }
                    else
                    {
                        OverItem.Click?.Invoke();
                    }

                }

                /*
                if (OverItem != null)
                {
                    if (OpenMenu != null)
                    {
                        OpenMenu.Open = false;
                    }
                    OverItem.Open = OverItem.Open ? false : true;
                    if (OverItem.Open)
                    {
                        OpenMenu = OverItem;
                    }
                }
                */
            };

            MouseMove = (x, y, xd, yd) =>
            {

                int mx = 35;
                OverItem = null;

                foreach (MenuItem it in Items)
                {

                    if (x > mx - 30 && x < mx + Font.Width(it.Text) + 60 & y < 25)
                    {
                        OverItem = it;
                    }


                    if (it.Open)
                    {
                        Console.WriteLine("Checking meun!");
                        CheckMenu(it, mx - 15, 25, x, y);

                    }
                    mx = mx + Font.Width(it.Text) + 40;

                }

            };

            void CheckMenu(MenuItem it, int x, int y, int xm, int ym)
            {

                int my = y;
                int mx = x;

                Console.WriteLine("-------------");
                foreach (var i in it.Items)
                {

                    Console.WriteLine("I:" + i.Text + " X:" + mx + " Y:" + my);
                    if (xm > mx && xm < mx + 200 & ym > my + 8 & ym < my + 40)
                    {
                        OverItem = i;
                        Console.WriteLine("CM:" + i.Text);
                    }
                    if (i.Seperator)
                    {
                        my = my + 8;
                    }
                    else
                    {
                        my = my + 27;
                    }
                    if (i.Open)
                    {
                        CheckMenu(i, mx + 235, my - 20, xm, ym);
                    }

                }

            }

            void DrawMenu(MenuItem it, int x, int y)
            {

                //int mh = it.Items.Count * 27;
                int mh = 0;

                foreach (var i in it.Items)
                {
                    if (i.Seperator)
                    {
                        mh = mh + 8;
                    }
                    else
                    {
                        mh = mh + 27;
                    }


                }

                mh = mh + 15;

                DrawFormSolid(UI.CurUI.Theme.ForegroundCol, x - 1, y - 1, 242, mh + 2);
                DrawFormSolid(UI.CurUI.Theme.BackgroundCol * 0.5f, x, y, 240, mh);

                int my = y + 6;
                int mx = x + 35;

                foreach (var i in it.Items)
                {
                    if (OverItem == i && i.Seperator == false)
                    {
                        DrawFormSolid(new OpenTK.Mathematics.Vector4(0.3f, 0.3f, 0.3f, 1.0f), mx - 10, my + 2, 210, 25);

                    }
                    if (i.Seperator)
                    {
                        DrawFormSolid(new OpenTK.Mathematics.Vector4(0.3f, 0.3f, 0.3f, 1.0f), mx, my + 2, 160, 1);
                        my = my + 6;
                    }
                    else
                    {
                        DrawText(i.Text, mx, my, UI.CurUI.Theme.ForegroundCol);
                        my = my + 27;
                    }
                    if (i.Icon != null)
                    {

                        DrawForm(i.Icon, x + 10, my - 20, 14, 14);

                    }
                    if (i.Items.Count > 0)
                    {

                        DrawForm(iRight, mx + 180, my - 22, 14, 14);

                    }
                    if (i.Open && i.Items.Count > 0)
                    {

                        DrawMenu(i, mx + 209, my - 33);

                    }
                }

            }

            Draw = () =>
            {

                DrawFormSolid(UI.CurUI.Theme.BackgroundCol);

                int mx = 30;

                foreach (MenuItem it in Items)
                {

                    if (it == OverItem)
                    {


                        DrawFormSolid(new OpenTK.Mathematics.Vector4(0.35f, 0.35f, 0.35f, 1.0f), mx - 10, 6, Font.Width(it.Text) + 20, 20);
                    }

                    DrawText(it.Text, mx, 2, UI.CurUI.Theme.ForegroundCol);

                    if (it.Open)
                    {
                        DrawMenu(it, mx - 15, 31);

                    }

                    mx = mx + Font.Width(it.Text);

                    mx = mx + 40;


                }


            };

        }

    }
}

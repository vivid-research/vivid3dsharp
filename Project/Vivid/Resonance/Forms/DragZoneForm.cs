﻿using OpenTK.Mathematics;

using System;
using System.Collections.Generic;

namespace Vivid.Resonance.Forms
{
    public class DragZoneForm : UIForm
    {
        public List<DragZone> Zones = new List<DragZone>();
        public DragZone ActiveZone = null;

        public DragZone Bottom, Top, Left, Right, Center;

        public DragZone GetZone(int zx, int zy)
        {
            foreach (var z in Zones)
            {
                if (zx > z.X && zx < z.X + z.W)
                {
                    if (zy > z.Y && zy < z.Y + z.H)
                    {
                        return z;
                    }
                }
            }
            return null;
        }

        public void SetLeft(DragZoneWindowForm form)
        {
            AddToZone(Left.CX, Left.CY, form);
            return;
        }

        public void SetRight(DragZoneWindowForm form)
        {
            AddToZone(Right.CX, Right.CY, form);
        }

        public void FindSize(int x, int y)
        {
        }

        public void SetBottom(DragZoneWindowForm form)
        {
            AddToZone(Bottom.CX, Bottom.CY, form);
            return;
        }

        public void SetCenter(DragZoneWindowForm form)
        {
            AddToZone(Center.CX, Center.CY, form);
        }

        public void SetTop(DragZoneWindowForm form)
        {
            AddToZone(Right.CX, Right.CY, form);
        }

        public int MarchUp(int x, int y, UIForm ex)
        {
            y = y + GY;
            for (int cy = y; cy > 0; cy -= 1)
            {
                var f = UI.CurUI.GetTopForm(x, cy);
                if (ex.Contains(f)) continue;
                if (f != this)
                {
                    return cy - GY;
                }
            }
            return 0;
        }

        public int MarchLeft(int x, int y, UIForm ex)
        {
            for (int cx = x; cx > 0; cx -= 1)
            {
                var f = UI.CurUI.GetTopForm(cx, y);
                if (ex.Contains(f)) continue;
                if (f != this)
                {
                    return cx;
                }
            }

            return 0;
        }

        public int MarchDown(int x, int y, UIForm ex)
        {
            y = y + GY;
            for (int cy = y; cy < H; cy++)
            {
                var f = UI.CurUI.GetTopForm(x, cy);
                if (ex.Contains(f)) continue;
                if (f != this)
                {
                    return cy - GY;
                }
            }
            return H;
        }

        public int MarchRight(int x, int y, UIForm ex)
        {
            for (int cx = x; cx < W; cx += 1)
            {
                var f = UI.CurUI.GetTopForm(cx, y);
                if (ex.Contains(f)) continue;
                if (f != this)
                {
                    return cx;
                }
            }

            return W;
        }

        public void AddToZone(int zx, int zy, DragZoneWindowForm form)
        {
            foreach (var z in Zones)
            {
                if (zx > z.X && zx < z.X + z.W)
                {
                    if (zy > z.Y && zy < z.Y + z.H)
                    {
                        z.Form = form;
                        if (form.Root != null)
                        {
                            form.Root.Forms.Remove(form);
                        }
                        //  Add(form);
                        int mx = z.CX;
                        int my = z.CY;

                        int azx = z.OX;
                        int azy = z.OY;
                        int azw = z.OW;
                        int azh = z.OH;

                        UI.CurUI.UpdateTop();
                        int LeftX = MarchLeft(mx, my, form);
                        int RightX = MarchRight(mx, my, form);
                        int TopY = MarchUp(mx, my, form);
                        int BotY = MarchDown(mx, my, form);
                        if (z.CheckLeft)
                        {
                            azx = LeftX + 2;
                            azw -= LeftX + 4;
                        }

                        if (z.CheckRight)
                        {
                            azw = RightX - azx - 2;
                        }

                        if (z.CheckUp)
                        {
                            azy = TopY + 2;
                            azh -= TopY + 4;
                        }

                        if (z.CheckDown)
                        {
                            azh = BotY - azy - 2;
                        }

                        //    int RightX = MarchRight(mx, my);
                        //   int TopY = MarchUp(mx, my);
                        //  int BotY = MarchDown(mx, my);
                        Add(form);
                        form.Set(azx, azy, azw - 10, azh - 25, form.Text);
                        form.AfterSetup();
                        form.DragZone = this;

                        form.Zone = z;
                        return;
                    }
                }
            }
        }

        public DragZoneForm()
        {
            AfterSet = () =>
            {
                Zones.Clear();

                //var z1 = new DragZone(0, 0, W, H / 4,0,0,W,H/4);
                // var z2 = new DragZone(0, H - (H / 4), W, H / 4, 0, H - (H / 4), W, H / 4);
                // var z3 = new DragZone(0, 0, W / 4, H, 0, 0, W / 4, H);
                // var z4 = new DragZone(W - (W / 4), 0, W / 4, H, W - (W / 4), 0, W / 4, H);
                //var z5 = new DragZone(W / 2 - 64, H / 2 - 64, 128, 128, 0, 0, W, H);
                //Zones.Add(z1);
                //Zones.Add(z2);
                //Zones.Add(z3);
                //Zones.Add(z4);
                //Zones.Add(z5);

                var z1 = new DragZone(W / 2, (H / 4) - ((H / 4) / 2), true, true, false, false);
                var z2 = new DragZone(W / 2, H - ((H / 4) - (H / 4 / 2)), true, true, false, false);
                var z3 = new DragZone((W / 4) / 2, (H / 2), false, false, true, true);
                var z4 = new DragZone(W - ((W / 4) / 2), (H / 2), false, false, true, true);
                var z5 = new DragZone(W / 2, H / 2, true, true, true, true);
                z1.OX = 0;
                z1.OY = 0;
                z1.OW = W;
                z1.OH = (H / 4);

                z2.OX = 0;
                z2.OY = H - (H / 4);
                z2.OW = W;
                z2.OH = H / 4;

                z3.OX = 0;
                z3.OY = 0;
                z3.OW = W / 4;
                z3.OH = H;

                z4.OX = W - (W / 4);
                z4.OY = 0;
                z4.OW = W / 4;
                z4.OH = H;

                z5.OX = 0;
                z5.OY = 0;
                z5.OW = W;
                z5.OH = H;

                Zones.Add(z1);
                Zones.Add(z2);
                Zones.Add(z3);
                Zones.Add(z4);
                Zones.Add(z5);
                Top = z1;
                Bottom = z2;
                Left = z3;
                Right = z4;
                Center = z5;

                //Bottom = z2;
                //Left = z3;
                //Right = z4;
                //Center = z5;
            };
            void MM(int x, int y, int xd, int yd)
            {
                //Console.WriteLine("Checking zones x:"+x+" Y:"+y);
                if (UI.CurUI.DragWin == null) return;
                ActiveZone = null;
                var z = GetZone(x, y);
                if (z != null)
                {
                    ActiveZone = z;
                }
                Console.WriteLine("AAX:" + x + " AAY:" + y);
            }

            MouseMove = MM;

            Draw = () =>
            {
                DrawFormSolid(new Vector4(0, 0, 0, 1));
                DrawFormSolid(UI.CurUI.Theme.BackgroundCol * 2, 2, 2, W - 4, H - 4);

                if (ActiveZone != null)
                {
                    DrawFormSolid(new Vector4(0, 1, 1, 1), ActiveZone.OX, ActiveZone.OY, ActiveZone.OW, ActiveZone.OH);
                }
            };
        }
    }

    public class DragZone
    {
        public int X, Y, W, H;
        public int OX, OY, OW, OH;
        public UIForm Form;
        public bool Occupied = false;
        public int CX, CY;
        public bool CheckLeft, CheckRight, CheckUp, CheckDown;

        public DragZone(int cx, int cy, bool cl, bool cr, bool cu, bool cd)
        {
            CX = cx;
            CY = cy;
            CheckLeft = cl;
            CheckRight = cr;
            CheckUp = cu;
            CheckDown = cd;
            X = CX - 64;
            Y = CY - 64;
            W = 128;
            H = 128;
            //  X = CX - 64;
        }

        public DragZone(int x, int y, int w, int h, int ox, int oy, int ow, int oh)
        {
            X = x; Y = y; W = w; H = h;
            OX = ox; OY = oy; OW = ow; OH = oh;
        }
    }
}
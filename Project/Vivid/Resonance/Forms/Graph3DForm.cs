﻿using OpenTK.Mathematics;

using System.IO;

using Vivid.App;
using Vivid.Composition;
using Vivid.Composition.Compositers;
using Vivid.Draw;
using Vivid.Scene;
using Vivid.Texture;

namespace Vivid.Resonance.Forms
{
    public class Graph3DForm : UIForm
    {
        public SceneGraph3D Graph = null;
        public FrameBuffer.FrameBufferColor FB;
        public Cam3D Cam = null;
        public Texture2D LightIcon;
        public Composite Comp;
#pragma warning disable CS0108 // 'Graph3DForm.Blur' hides inherited member 'UIForm.Blur'. Use the new keyword if hiding was intended.
        public Compositer Blur, Bloom, Dof, SSR, Outline, BloomUI;
#pragma warning restore CS0108 // 'Graph3DForm.Blur' hides inherited member 'UIForm.Blur'. Use the new keyword if hiding was intended.

        public void ReadPP(BinaryReader r)
        {
            eBlur = r.ReadBoolean();
            float bl = r.ReadSingle();

            eBloom = r.ReadBoolean();
            float ml = r.ReadSingle();
            float bbl = r.ReadSingle();

            eOutline = r.ReadBoolean();

            eDof = r.ReadBoolean();
            float zf = r.ReadSingle();
            float zr = r.ReadSingle();

            SetPP(eBlur, eBloom, eOutline, eDof);

            SetBlur(bl);
            SetBloom(ml, bbl);
            SetDof(zr, zf);
        }

        public void WritePP(BinaryWriter w)
        {
            var bc = Main.Blur as BlurCompositer;
            var blc = Main.Bloom as BloomCompositer;
            //bc.MinLevel = t;
            //bc.BlurLevel = b;
            //bc.Changed();

            w.Write(eBlur);
            w.Write(bc.GetBlur());

            w.Write(eBloom);
            w.Write(blc.MinLevel);
            w.Write(blc.BlurLevel);

            w.Write(eOutline);
            var dofC = Dof as DOFCompositer;

            //dofC.SetZFocus(f);
            //dofC.SetZRange(r);

            w.Write(eDof);
            w.Write(dofC.GetZFocus());
            w.Write(dofC.GetZRange());
        }

        public Node3D SelectOthers(int x, int y)
        {
            foreach (var l in Graph.Lights)
            {
                AppInfo.RW = W;
                AppInfo.RH = H;
                var p2 = Graph.Cams[0].To2D(l.LocalPos);
                //var rpos =
                AppInfo.RW = AppInfo.W;
                AppInfo.RH = AppInfo.H;
                //p2.Y = H - p2.Y;

                if (x >= p2.X - 32 && x <= p2.X + 32 && y >= p2.Y - 32 && y <= p2.Y + 32)
                {
                    return l;
                }
                // if (p2.X < 0 || p2.Y < 0 || p2.X > W || p2.Y > H) continue;

                //  DrawForm(LightIcon, new OpenTK.Vector4(1, 1, 1, 1), (int)p2.X - 32, (int)p2.Y - 32, 64, 64);
            }

            return null;
        }

        public void SetBloom(float t, float b)
        {
            var bc = Main.Bloom as BloomCompositer;
            bc.MinLevel = t;
            bc.BlurLevel = b;
            bc.Changed();
        }

        public void SetDof(float r, float f)
        {
            var dofC = Dof as DOFCompositer;

            dofC.SetZFocus(f);
            dofC.SetZRange(r);
        }

        public void SetBlur(float ba)
        {
            var bc = Main.Blur as BlurCompositer;

            bc.SetBlur(ba);
        }

        private static bool eBlur, eBloom, eOutline, eDof;

        public static void PP(bool blur, bool bloom, bool outline, bool dof)
        {
            eBlur = blur;
            eBloom = bloom;
            eOutline = outline;
            eDof = dof;
            Main.SetPP(blur, bloom, outline, dof);
        }

        public static Graph3DForm Main;

        public IntelliDrawSingle IDS;

        public void SetPP(bool blur, bool bloom, bool outline, bool dof)
        {
            Comp.Composites.Clear();

            eBlur = blur;
            eBloom = bloom;
            eOutline = outline;
            eDof = dof;
            if (bloom)
            {
                Comp.AddCompositer(Bloom);
            };
            if (blur)
            {
                Comp.AddCompositer(Blur);
            };
            if (outline)
            {
                Comp.AddCompositer(Outline);
            }
            if (dof)
            {
                Comp.AddCompositer(Dof);
            }
        }

        public Graph3DForm()
        {
            //            FB = new FrameBuffer.FrameBufferColor();
            Main = this;
            Graph = new SceneGraph3D();
            Cam = new Cam3D();
            IDS = new IntelliDrawSingle();

            Graph.Add(Cam);
            LightIcon = new Texture2D("IDE/Edit/LightIcon4.png", LoadMethod.Single, true);

            Draw = () =>
            {
                //FB.Bind();

                Graph.RenderShadows();
                if (Comp != null)
                {
                    if (Comp.Composites.Count > 0)
                    {
                        Comp.PreRernder();
                    }
                }
                FB.Bind();

                if (Comp != null)
                {
                    if (Comp.Composites.Count == 0)
                    {
                        Graph.Render();
                        Graph.RenderLines();
                        OpenTK.Graphics.OpenGL4.GL.Clear(OpenTK.Graphics.OpenGL4.ClearBufferMask.DepthBufferBit);
                        Graph.RenderOnTop();
                    }
                    else
                    {
                        Comp.RenderFinal();
                    }
                }
                else
                {
                    Graph.Render();
                    Graph.RenderLines();
                    OpenTK.Graphics.OpenGL4.GL.Clear(OpenTK.Graphics.OpenGL4.ClearBufferMask.DepthBufferBit);
                    Graph.RenderOnTop();
                }

                Graph.NodeRender();

                FB.Release();

                DrawFormSolid(UI.CurUI.Theme.ForegroundCol, -1, -1, W + 2, H + 2);
                DrawForm(FB.BB, 0, 0, -1, -1, true);

                foreach (var l in Graph.Lights)
                {
                    AppInfo.RW = W;
                    AppInfo.RH = H;
                    var p2 = Graph.Cams[0].To2D(l.LocalPos);
                    //var rpos =
                    AppInfo.RW = AppInfo.W;
                    AppInfo.RH = AppInfo.H;
                    //p2.Y = H - p2.Y;

                    Vector3 lp = l.LocalPos;
                    Vector3 cp = Graph.Cams[0].LocalPos;

                    Vector3 cn = Graph.Cams[0].Transform(new Vector3(0, 0, -1));

                    cn = cn - cp;

                    Vector3 ln = lp - cp;

                    cn.Normalize();
                    ln.Normalize();

                    var lnr = Vector3.Dot(cn, ln);

                    //  System.Console.WriteLine("LNR:" + lnr);

                    if (p2.X < 0 || p2.Y < 0 || p2.X > W || p2.Y > H) continue;

                    if (lnr > 0.1f)
                    {
                        DrawForm(LightIcon, new Vector4(1, 1, 1, 1), (int)p2.X - 32, (int)p2.Y - 32, 64, 64);
                    }
                }
            };
        }

        public override void AfterSetup()
        {
            //base.AfterSetup();
            if (W > 64 && H > 64)
            {
                FB = new FrameBuffer.FrameBufferColor(W, H);

                FrameType.FW = W * 2;
                FrameType.FH = H * 2;

                Comp = new Composite();

                Comp.SetGraph(Graph);

                Blur = new BlurCompositer();
                Outline = new OutlineCompositer();
                Bloom = new BloomCompositer();
                Dof = new DOFCompositer();

                Comp.Render = (b) =>
                {
                    IDS.BeginDraw();
                    IDS.DrawImg(0, 0, AppInfo.RW, AppInfo.RH, b, new Vector4(1, 1, 1, 1), true);
                    IDS.EndDraw();

                    //IntelliDraw.BeginDraw();

                    //   IntelliDraw.DrawImg(0, 0, Vivid.App.AppInfo.RW, AppInfo.RH, b, new OpenTK.Vector4(1, 1, 1, 1), true);
                    //DrawForm(b, 0, 0, -1, -1, true);

                    //IntelliDraw.EndDraw();
                };
            }
        }
    }
}
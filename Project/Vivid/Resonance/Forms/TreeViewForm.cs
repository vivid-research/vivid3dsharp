﻿using OpenTK.Mathematics;

using System;

namespace Vivid.Resonance.Forms
{
    public delegate void NodeSelected(TreeNode node);

    public class TreeViewForm : UIForm
    {
        public TreeNode Selected = null;
#pragma warning disable CS0108 // 'TreeViewForm.Root' hides inherited member 'UIForm.Root'. Use the new keyword if hiding was intended.
        public TreeNode Root = null;
#pragma warning restore CS0108 // 'TreeViewForm.Root' hides inherited member 'UIForm.Root'. Use the new keyword if hiding was intended.
        public NodeSelected Select = null;
        public static Texture.Texture2D BodyImg = null;
        public TextBoxForm FilterBox;
        public string FilterString;

        public TreeViewForm()
        {
            if (BodyImg == null)
            {
                BodyImg = new Texture.Texture2D("data/nxUI/bg/winBody4.png", Texture.LoadMethod.Single, true);
            }

            var fblab = new LabelForm().Set(5, 5, 5, 5, "Search");

            FilterBox = new TextBoxForm().Set(65, 7, 165, 25, "") as TextBoxForm;

            FilterBox.Enter = (fs) =>
            {
                FilterString = fs;
            };

            FilterString = "";

            Add(fblab);
            Add(FilterBox);

            Draw = () =>
            {
                // GL.Enable(EnableCap.ScissorTest);
                // GL.Scissor(GX, GY, W, H);

                //DrawFormSolid(UI.CurUI.Theme.ForegroundCol, 0, 0, W + 1, 1);
                DrawFormSolid(UI.CurUI.Theme.BackgroundCol, 1, 1, W - 1, H);
                DrawFormSolid(UI.CurUI.Theme.BackgroundCol * 2, 1, 1, W, 35);

                int y = 37;
                Over = OverNode(Root, ref y, 5);
                y = 37;
                DrawNode(Root, ref y, 15);
                if (ActNode != null)
                {
                    DrawText(ActNode.Name, UI.MX - GX, UI.MY - GY);
                };

                //GL.Disable(EnableCap.ScissorTest);
            };

            DoubleClick = (b) =>
            {
                if (Selected != null && Selected.Click != null)
                {
                    Selected.Click(b);
                }
                if (Selected != null)
                {
                    Console.WriteLine("Double:" + Selected.Name);
                }
            };

            MouseDown = (b) =>
              {
                  Console.WriteLine("TreeClick");
                  int y = 37;
                  ClickNode(Root, ref y, 15);
                  // y = 37;
                  // ActNode = OverNode(Root, ref y, 15);
                  ActNode = Over;
                  Selected = ActNode;
                  Select?.Invoke(Selected);
              };
            MouseUp = (b) =>
            {
                if (Over != null && ActNode != null)
                {
                    if (ActNode != Over)
                    {
                        if (ActNode.Root != null && ActNode.Root != Over && Over.Root != ActNode)
                        {
                            ActNode.Root.Nodes.Remove(ActNode);
                            Over.Nodes.Add(ActNode);
                            ActNode.Root = Over;
                        }
                    }
                }
                ActNode = null;
            };
        }

        public override void AfterSetup()
        {
            if (base.Root != null)
            {
                W = base.Root.W;
                H = base.Root.H;
            }
        }

        protected TreeNode ActNode = null;
        protected TreeNode Over = null;

        public TreeNode OverNode(TreeNode node, ref int y, int x = 5)
        {
            if (node == null) return null;
            //DrawText(node.Name, x + 4, y);
            if (UI.MX >= GX)
            {
                if (UI.MY >= GY + y)
                {
                    if (UI.MX <= (GX + W))
                    {
                        if (UI.MY <= (GY + y + 20))
                        {
                            return node;
                        }
                    }
                }
            }
            foreach (var snode in node.Nodes)
            {
                y = y + 25;
                var on = OverNode(snode, ref y, x + 15);
                if (on != null) return on;
            }
            return null;
        }

        public void ClickNode(TreeNode node, ref int y, int x = 5)
        {
            if (node == null) return;

            if (node.Open)
            {
                if (UI.MX >= (GX + (x - 8)))
                {
                    if (UI.MY >= (GY + (y + 10)))
                    {
                        if (UI.MX <= (GX + (x)))
                        {
                            if (UI.MY <= (GY + (y + 19)))
                            {
                                node.Open = false;
                            }
                        }
                    }
                }
                //DrawLine(x - 4, y + 10, x + 4, y + 10, new Vector4(0, 0, 0, 1));

                //y = y + 25;

                foreach (var sn in node.Nodes)
                {
                    y = y + 25;
                    ClickNode(sn, ref y, x + 15);
                }
            }
            else
            {
                if (UI.MX >= (GX + (x - 8)))
                {
                    if (UI.MY >= (GY + (y + 10)))
                    {
                        if (UI.MX <= (GX + (x)))
                        {
                            if (UI.MY <= (GY + (y + 19)))
                            {
                                node.Open = true;
                            }
                        }
                    }
                }
            }
        }

        private bool ChildContains(TreeNode node, string filter)
        {
            if (node.Name.Contains(filter)) return true;
            foreach (var sn in node.Nodes)
            {
                if (ChildContains(sn, filter)) return true;
            }
            return false;
        }

        public void DrawNode(TreeNode node, ref int y, int x = 5)
        {
            if (node == null) return;

            if (FilterString != "")
            {
                if (!ChildContains(node, FilterString))
                {
                    return;
                }
            }

            if (node == Over)
            {
                DrawFormSolid(new Vector4(0.1f, 0.1f, 0.1f, 0.9f), 0, y, W - 2, 27);
            }
            if (node == Selected)
            {
                DrawFormSolid(new Vector4(0.0f, 0.4f, 0.4f, 0.6f), 0, y, W - 2, 25);
            }

            if (node.Nodes.Count > 0)
            {
                DrawFormSolid(new Vector4(1, 1, 1, 1), x - 8, y + 10, 8, 9);
                //DrawFormSolid(new Vector4(0.2f, 0.2f, 0.2f, 1.0f), x - 8, y + 15, 8, 3);

                if (node.Open)
                {
                    DrawFormSolid(new Vector4(0.2f, 0.2f, 0.2f, 1.0f), x - 7, y + 11, 6, 7);
                }
            }

            DrawText(node.Name, x + 4, y, new Vector4(1, 1, 1, 1));
            if (node.Open)
            {
                //DrawLine(x - 4, y + 10, x + 4, y + 10, new Vector4(0, 0, 0, 1));

                // y = y + 25;
                foreach (var sn in node.Nodes)
                {
                    y = y + 25;
                    DrawNode(sn, ref y, x + 15);
                }
            }
            else
            {
                // DrawLine(x - 4, y + 10, x + 4, y + 10, new Vector4(0, 0, 0, 1));
                //  DrawLine(x, y + 5, x, y + 15, new Vector4(0, 0, 0, 1));
            }
        }
    }
}
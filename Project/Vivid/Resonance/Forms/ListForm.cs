﻿using OpenTK.Mathematics;

using System.Collections.Generic;

using Vivid.Texture;

namespace Vivid.Resonance.Forms
{
    public delegate void ListChanged(ItemForm active);
    public delegate void ListSelected(ItemForm item);
    public class ListForm : UIForm
    {
        public ListChanged ItemChanged = null;
        public VerticalSliderForm Scroller = null;
        public List<ItemForm> Items = new List<ItemForm>();
        public ItemForm Over = null, Sel = null;
        public ListSelected Selected = null;
        public ListForm()
        {
            PushArea = true;
            Col = new Vector4(0.2f, 0.2f, 0.2f, 0.9f);
            Scroller = new VerticalSliderForm();

            AfterSet = () =>
            {
                Scroller.Set(W - 10, 0, 10, H);

            };

            void DrawFunc()
            {
                DrawFormSolid(UI.CurUI.Theme.BackgroundCol * 2);

                int dy = 5 - Scroller.GetCur();
                int y2 = 5;
                foreach (var item in Items)
                {
                    int dx = 2;
                    if (dy < H - 10 && dy > 2)
                    {
                        if (item == Over)
                        {
                            DrawFormSolid(UI.CurUI.Theme.BackgroundCol * 3, 1, dy, W - 3, 23);
                        }
                        if (item == Sel)
                        {
                            DrawFormSolid(UI.CurUI.Theme.BackgroundCol * 4, 1, dy, W - 3, 23);
                        }
                        if (item.Pic != null)
                        {

                            DrawForm(item.Pic, 2, dy, 16, 16);
                            dx = 23;
                        }
                        DrawText(item.Text, dx, dy - 5, UI.CurUI.Theme.TextCol);
                    }
                    Scroller.Max = y2 - (H / 2);

                    Scroller.RenderH = H;
                    dy = dy + 27;
                    y2 = y2 + 27;
                }
            }

            MouseDown = (b) =>
            {
                if (Over != null)
                {
                    Sel = Over;
                    Selected?.Invoke(Over);
                }
            };

            MouseMove = (x, y, xd, yd) =>
            {
                Over = null;
                int dy = 5 - Scroller.GetCur();
                int y2 = 5;
                foreach (var item in Items)
                {
                    int dx = 2;
                    if (dy < H - 10 && dy > 2)
                    {
                        //if (item.Pic != null)
                        // {
                        if (x > 0 && y < W && y > dy && y < dy + 25)
                        {
                            Over = item;
                        }
                        //   DrawForm(item.Pic, 2, dy, 16, 16);
                        dx = 23;
                        //}
                        // DrawText(item.Text, dx, dy - 5, UI.CurUI.Theme.TextCol);
                    }
                    //Scroller.Max = y2;

                    //Scroller.RenderH = H;
                    dy = dy + 27;
                    y2 = y2 + 27;
                }

            };

            void ChangedFunc()
            {
                /*
                Scroller.X = W - 10;
                Scroller.Y = 2;
                Scroller.W = 10;
                Scroller.H = H;

                Scroller.Changed?.Invoke();
                foreach (ItemForm item in Items)
                {
                    Forms.Remove(item);
                }

                float sh = 20;// (Scroller.H - Scroller.ScrollBut.H);
                float mh = Items.Count * 20;

                float dh = sh / mh;
                if (dh < 0.03f)
                {
                    //Scroller.Max = Scr
                    //    sm1 = (float)Scroller.H * 0.03f;
                }

               // Scroller.ScrollBut.H = (int)(dh * Scroller.H);
                if (dh < 0.1f)
                {
                    //Scroller.ScrollBut.H = 6;
                    //Scroller.Max
                    //Scroller.Max = Scroller.Max - 10;
                }

              
               // float ly = Scroller.Cur / Scroller.Max;
              //  float mh2 = ly * ((Items.Count + 1) * 22);

                if (Scroller.ScrollBut.H > H)
                //{
               //     Scroller.ScrollBut.H = H;
             //   }
                //ly = -(ly * H);
                //ly = -(mh2);
                //ly = ly - 20;

                foreach (ItemForm item in Items)
                {
                    //var newi = new ItemForm().Set(5, (int)ly, W - 15, 20, item.Text) as ItemForm;
                    ItemForm newi = item;
                    newi.W = W - 5;
                    newi.H = 25;
                    //newi.Pic = item.Pic;
                    if (ly > H - 22 || ly < 0)
                    {
                        newi.Render = false;
                        newi.CheckBounds = false;
                    }
                    else
                    {
                        newi.CheckBounds = true;
                        newi.Render = true;
                    }
                    newi.Y = (int)ly;
                    ly = ly + 22;
                    //  newi.ViewX = 0;
                    //  newi.ViewY = 0;
                    // newi.ViewW = W;
                    // newi.ViewH = H;
                    newi.Click = (b) =>
                    {
                        ItemChanged?.Invoke(newi);
                    };
                    Add(newi);
                }

                if (Scroller.ScrollBut.H < 5)
                {
                    Scroller.ScrollBut.H = 5;
                }

                Scroller.ScrollBut.Drag.Invoke(0, 25);

                //Scroller.ScrollBut.Drag(0, 15);
                */
            }

            Changed = ChangedFunc;

            Draw = DrawFunc;

            Scroller = new VerticalSliderForm();

            void PostDragFunc(int x, int y)
            {
                //Scroller.Cur = Scroller.ScrollBut.Y / Scroller.H;
                //float my = Scroller.Max / Scroller.H;
                //Scroller.Cur = Scroller.Cur * my;

                //Scroller.Cur = Scroller.ScrollBut.Y;
                //Changed?.Invoke();
            }

            ///Scroller.ScrollBut.PostDrag = PostDragFunc;

            Add(Scroller);
            //Scroller.ScrollBut.Drag.Invoke(0, 25);
            //Scroller.ScrollBut.PostDrag.Invoke(0, 15);
            //PostDragFunc(0, 5);
            //Scroller.ScrollBut.Drag(0, -5);
        }

        public void Clear()
        {
            foreach (ItemForm i in Items)
            {
                Forms.Remove(i);
            }
            Items.Clear();
        }

        public ItemForm AddItem(ItemForm item)
        {
            Items.Add(item);
            return item;
            //Changed?.Invoke();
        }

        public ItemForm AddItem(string text, Texture2D pic)
        {
            ItemForm nitem = new ItemForm
            {
                Text = text,
                Pic = pic,
                W = W - 20,
                H = 20
            };
            Items.Add(nitem);
            return nitem;
            //Changed?.Invoke();
        }
    }
}
﻿using OpenTK.Windowing.GraphicsLibraryFramework;
using System;
using System.Collections.Generic;

namespace Vivid.Resonance.Forms
{
    public class TextBoxForm : UIForm
    {
        public OnEnter Enter = null;

        public bool Active = false;

        public bool CaretOn = true;

        public int NextCaret = Environment.TickCount + 300;

        public int[] TextX = new int[2555];

        int EditX = 0;
#pragma warning disable CS0108 // 'TextBoxForm.ViewX' hides inherited member 'UIForm.ViewX'. Use the new keyword if hiding was intended.
        int ViewX = 0;
#pragma warning restore CS0108 // 'TextBoxForm.ViewX' hides inherited member 'UIForm.ViewX'. Use the new keyword if hiding was intended.

        public override void SetText(string txt)
        {
            if (txt == null) txt = "";
            base.SetText(txt);
            EditX = txt.Length;
        }
        int mmx=0, mmy=0;
        public Stack<String> PrevText = new Stack<string>();
        public TextBoxForm()
        {
            Text = "";

            MouseMove = (x, y, xd, yd) =>
            {
                mmx = x;
                mmy = y;

            };

            MouseDown = (b) =>
            {

                int cx = 0;
                for(int i = 0; i < TextX.Length;i++)
                {
                    if (TextX[i] == 0)
                    {
                        
                        break;
                    }
                    if(mmx>TextX[i])
                    {
                        cx = i;
                    }
                    else
                    {
                        if (i == 0)
                        {
                            cx = -1;
                        }
                        break;
                    }

                }
                EditX = cx + 1;
                //EditX = mmx / 10;


            };

            Enter = (string t) =>
            {


            };
            void DrawFunc()
            {

                int tc = Environment.TickCount;

                if (tc > NextCaret)
                {
                    NextCaret = tc + 300;
                    CaretOn = CaretOn ? false : true;
                }

                UpdatePos();
                DrawFormSolid(UI.CurUI.Theme.BackgroundCol*1.5f);
                if (Active && CaretOn)
                {
                    if (EditX > 0)
                    {
                        DrawFormSolid(UI.CurUI.Theme.ForegroundCol, TextX[EditX - 1] + 2, 0, 2, H);
                    }
                    else
                    {
                        DrawFormSolid(UI.CurUI.Theme.ForegroundCol, 2, 0, 2, H);
                    }
                }
                DrawText(Text, 2, -1, UI.CurUI.Theme.ForegroundCol * 2);


            }

            Activate = () =>
            {

                CaretOn = true;
                NextCaret = Environment.TickCount;
                Active = true;

            };

            Deactivate = () =>
            {
                CaretOn = false;
                Active = false;
            };

            void UpdatePos()
            {
                int ax = 0;
                if (EditX < 1) return;
                for (int i = 0; i < Text.Length; i++)
                {
                    TextX[i] = Font.Width(Text.Substring(0, i + 1));
                    //   ax = ax + Font.Width(Text[i].ToString());
                }

            }

            void AddChar(string key)
            {
                if (Text.Length == 0)
                {
                    Text = key;
                    EditX = 1;
                    Enter.Invoke(Text);
                    return;
                }
                if (EditX == 0)
                {
                    Text = key + Text;
                    EditX = 1;
                    Enter.Invoke(Text);

                    return;
                }
                if (EditX == Text.Length)
                {
                    Text += key;
                    EditX++;
                    Enter.Invoke(Text);

                    return;
                }
                string sub = Text.Substring(EditX);
                Text = Text.Substring(0, EditX);
                Text += key;
                Text += sub;
                EditX++;
                Text = Text;
                Enter.Invoke(Text);



            }

            void BackSpace()
            {

                if (EditX == 0) return;
                if (EditX == Text.Length)
                {
                    Text = Text.Substring(0, Text.Length - 1);
                    EditX--;
                    return;
                }

                var sub = Text.Substring(0, EditX - 1);
                var sub2 = Text.Substring(EditX);
                Text = sub + sub2;
                EditX--;




            }

            void Delete()
            {

            }

            void KeyPressFunc(OpenTK.Windowing.GraphicsLibraryFramework.Keys key, bool shift)
            {
                switch (key)
                {
                    case Keys.Up:
                        if (PrevText.Count > 0)
                        {
                            SetText(PrevText.Pop());

                        }
                        return;
                        break;
                    case Keys.LeftBracket:
                        AddChar("(");
                        UpdatePos();
                        return;
                    case Keys.RightBracket:
                        AddChar(")");
                        UpdatePos();
                        return;
                    case Keys.Enter:
                        PrevText.Push(Text);
                        Enter?.Invoke(Text);
                        break;
                    case Keys.Space:
                        AddChar(" ");
                        UpdatePos();
                        return;
                        break;
                    case Keys.Backspace:
                        BackSpace();
                        return;
                        break;
                    case Keys.Delete:
                        Delete();
                        return;
                        break;
                    case Keys.Comma:
                        AddChar(",");
                        UpdatePos();
                        return;
                    case Keys.Period:
                        AddChar(".");
                        UpdatePos();
                        break;
                    case Keys.Left:
                        if (EditX > 0)
                        {
                            EditX--;
                            return;
                        }
                        return;
                    case Keys.Right:
                        if (EditX < Text.Length)
                        {
                            EditX++;
                        }
                        return;
                        break;
                }
                string nk = "0123456789";
                var ak = key.ToString();
                if (ak.Length == 2 && ak.Contains("D"))
                {
                    var cn = ak.Substring(1);
                    if (nk.Contains(cn))
                    {
                        if (shift)
                        {
                            if (cn == "9")
                            {
                                AddChar("(");
                                UpdatePos();
                                return;
                            }
                            if (cn == "0")
                            {
                                AddChar(")");
                                UpdatePos();
                                return;
                            }
                        }

                        AddChar(int.Parse(cn).ToString());
                        UpdatePos();
                        return;
                    };
                }
                var ck = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

                if (ck.Contains(ak))
                {

                    if (shift)
                    {
                        AddChar(key.ToString());
                    }
                    else
                    {
                        AddChar(key.ToString().ToLower());
                    }
                }
                UpdatePos();


            }

            KeyPress = KeyPressFunc;

            Draw = DrawFunc;

        }
    }
}

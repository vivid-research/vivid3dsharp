﻿namespace Vivid.Resonance.Forms
{
    public class LabelButtonForm : UIForm
    {
        bool over = false;
        public LabelButtonForm()
        {

            void DrawFunc()
            {
                float bc = 0.8f;
                if (over)
                {
                    bc = 1.1f;
                }
                else
                {

                }
                // DrawFormSolid(UI.CurUI.Theme.BackgroundCol*bc);
                DrawTextBig(Text, 10, 0, UI.CurUI.Theme.TextCol * bc);
                W = FontBig.Width(Text) + 20;
                H = FontBig.Height(Text) * 2;

            }

            MouseEnter = () =>
            {

                over = true;

            };

            MouseLeave = () =>
            {

                over = false;

            };

            Draw = DrawFunc;

        }


    }
}

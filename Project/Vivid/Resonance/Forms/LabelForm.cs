﻿namespace Vivid.Resonance.Forms
{
    public class LabelForm : UIForm
    {
        public LabelForm()
        {
            void EV_Draw()
            {
                DrawText(Text, 3, 3, UI.CurUI.Theme.TextCol);
            }

            Draw = EV_Draw;
        }
    }
}
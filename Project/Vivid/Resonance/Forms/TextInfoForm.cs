﻿using System.Collections.Generic;

using Vivid.Resonance;

namespace KnightEngine.Resonance.Forms
{
    public class TextInfoForm : UIForm
    {
        public List<string> Lines = new List<string>();

        public TextInfoForm()
        {
            void DrawFunc()
            {
                DrawFormSolid(Col);

                int dx = 5;
                int dy = 10;
                foreach (var l in Lines)
                {
                    DrawText(l, dx, dy);
                    dy = dy + 18;
                }
            }

            Draw = DrawFunc;
        }
    }
}
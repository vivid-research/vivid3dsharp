﻿using System.Collections.Generic;

using Vivid.Texture;

namespace Vivid.Resonance.Forms
{
    public class DropDownListForm : UIForm
    {
        public string CurrentItem = "";
        public Texture2D CurrentImage = null;
        public List<string> Items = new List<string>();
        public List<Texture2D> ItemImage = new List<Texture2D>();
#pragma warning disable CS0108 // 'DropDownListForm.Open' hides inherited member 'UIForm.Open'. Use the new keyword if hiding was intended.
        public bool Open = false;
#pragma warning restore CS0108 // 'DropDownListForm.Open' hides inherited member 'UIForm.Open'. Use the new keyword if hiding was intended.
        public SelectItem SelectedItem = null;

        public DropDownListForm()
        {
            Draw = () =>
            {
                DrawFormSolid(UI.CurUI.Theme.ForegroundCol, -1, -1, W + 2, H + 2);
                DrawFormSolid(UI.CurUI.Theme.BackgroundCol);
                if (CurrentImage != null)
                {
                    DrawForm(CurrentImage, 2, 2, 32, H - 4);
                    DrawText(CurrentItem, 7 + 36, 3, UI.CurUI.Theme.TextCol);
                }
                else
                {
                    DrawText(CurrentItem, 7, 3, UI.CurUI.Theme.TextCol);
                }
            };

            MouseDown = (b) =>
            {
                if (Open)
                {
                    Open = false;
                    Forms.Clear();
                }
                else
                {
                    int y = 0;
                    UIForm top = new UIForm().Set(GX, GY, W, H);
                    //UI.CurUI.Top = top;

                    Open = true;
                    foreach (var item in Items)
                    {
                        var ib = new ButtonForm().Set(0, H + y, W, 25, item) as ButtonForm;
                        y = y + 25;
                        Add(ib);
                        int ii = 0;
                        ib.Click = (bt) =>
                        {
                            CurrentItem = item;
                            foreach (var i in Items)
                            {
                                if (i == item)
                                {
                                    CurrentImage = ItemImage[ii];
                                }
                                ii++;
                            }
                            //CurrentImage =

                            Open = false;
                            Forms.Clear();
                            //UI.CurUI.Top = null;
                            SelectedItem?.Invoke(item);
                        };
                    }
                }
            };
        }

        public void AddItem(string item, Texture2D img = null)
        {
            if (Items.Count == 0)
            {
                CurrentItem = item;
                CurrentImage = img;
            }
            Items.Add(item);
            ItemImage.Add(img);
        }
    }

    public delegate void SelectItem(string item);
}
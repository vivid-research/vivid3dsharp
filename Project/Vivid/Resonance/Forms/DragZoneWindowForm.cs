﻿namespace Vivid.Resonance.Forms
{
    public class DragZoneWindowForm : WindowForm
    {
        public DragZone Zone = null;
        public DragZoneForm DragZone = null;

        public DragZoneWindowForm()
        {
            void DragFunc(int x, int y)
            {
                //Environment.Exit(0);

                UI.CurUI.DragWin = this;
                UI.CurUI.NoPress();
                Zone.Form = null;
                Root.Forms.Remove(this);

                if (LockedPos)
                {
                    return;
                }
                if (Docked) return;
                X = X + x;
                Y = Y + y;
            }
            Title.Drag = DragFunc;
        }
    }
}
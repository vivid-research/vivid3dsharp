﻿using OpenTK.Mathematics;

using Vivid.Texture;

namespace Vivid.Resonance.Themes
{
    public class BlueTheme : UITheme

    {
        public BlueTheme()
        {
            this.BackgroundCol = new Vector4(0f, 0.2f, 0.2f, 1.0f);
            this.ForegroundCol = new Vector4(0.2f, 0.9f, 0.9f, 1.0f);
            this.GeneralFont = new Font2.FontTTF("app/ui/theme/blue/font1.ttf", 13);
            this.MenuFont = new Font2.FontTTF("app/ui/theme/blue/editfont2.ttf", 5);
            this.TextCol = new Vector4(0.2f, 0.9f, 0.9f, 1.0f);
            this.WindowTitle = new Texture2D("app/ui/theme/blue/windowTitle3.png", true);
            this.WindowBody = new Texture2D("app/ui/theme/blue/winbody4.png", true);
            this.ButtonBody = new Texture2D("app/ui/theme/blue/buttongrey1.png", true);
        }
    }
}
﻿using OpenTK.Mathematics;

using Vivid.Texture;

namespace Vivid.Resonance.Themes
{
    public class EditorTheme : UITheme
    {
        public EditorTheme()
        {
            this.BackgroundCol = new Vector4(0.15f, 0.15f, 0.15f, 1.0f);
            this.ForegroundCol = new Vector4(0.9f, 0.9f, 0.9f, 1.0f);
            this.GeneralFont = new Font2.FontTTF("app/ui/theme/editor/font1.ttf", 13);
            this.MenuFont = new Font2.FontTTF("app/ui/theme/editor//editfont2.ttf", 11);
            this.TextCol = new Vector4(0.9f, 0.9f, 0.9f, 1.0f);
            this.WindowTitle = new Texture2D("app/ui/theme/editor/windowTitle3.png", true);
            this.WindowBody = new Texture2D("app/ui/theme/editor/winbody4.png", true);
            this.ButtonBody = new Texture2D("app/ui/theme/editor/buttongrey1.png", true);
        }
    }
}
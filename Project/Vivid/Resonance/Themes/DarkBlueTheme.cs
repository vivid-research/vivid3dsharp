﻿using OpenTK.Mathematics;

using Vivid.Texture;

namespace Vivid.Resonance.Themes
{
    public class DarkBlueTheme : UITheme
    {
        public DarkBlueTheme()
        {
            this.BackgroundCol = new Vector4(0, 0.2f, 0.2f, 1);
            this.ForegroundCol = new Vector4(0.1f, 0.6f, 0.6f, 1.0f);
            this.GeneralFont = new Font2.FontTTF("app/ui/theme/darkblue/font1.ttf", 13);
            this.MenuFont = new Font2.FontTTF("app/ui/theme/darkblue/editfont2.ttf", 11);
            this.TextCol = new Vector4(0.2f, 0.8f, 0.8f, 1.0f);
            this.WindowTitle = new Texture2D("app/ui/theme/darkblue/windowTitle3.png", true);
            this.WindowBody = new Texture2D("app/ui/theme/darkblue/winbody4.png", true);
            this.ButtonBody = new Texture2D("app/ui/theme/darkblue/buttongrey1.png", true);
        }
    }
}
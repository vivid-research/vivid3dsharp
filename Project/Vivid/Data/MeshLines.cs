﻿using System.Collections.Generic;

using Vivid.Visuals;

namespace Vivid.Data
{
    public class Line3D
    {
        public float X, Y, Z;
        public float X1, Y1, Z1;
        public float R, G, B;
    }

    /// <summary>
    /// Mesh lines are debugging meshes, that are comprised of Lines, not triangles. These lines
    /// can be set pragmatically to visualize debug info or etc.
    /// </summary>
    public class MeshLines
    {
        public List<Line3D> Lines = new List<Line3D>();
        public Visualizer Viz = null;

        /// <summary>
        /// Creates a meshlines with the given number of lines.
        /// </summary>
        /// <param name="lines"></param>
        public MeshLines(int lines = 0)
        {
            Lines = new List<Line3D>();
            for (int i = 0; i < lines; i++)
            {
                Lines.Add(new Line3D());
            }
        }

        /// <summary>
        /// Sets the given lines data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="z1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="z2"></param>
        /// <param name="r"></param>
        /// <param name="g"></param>
        /// <param name="b"></param>
        public void SetLine(int id, float x1, float y1, float z1, float x2, float y2, float z2, float r = 1, float g = 1, float b = 1)
        {
            Lines[id].X = x1;
            Lines[id].Y = y1;
            Lines[id].Z = z1;

            Lines[id].X1 = x2;
            Lines[id].Y1 = y2;
            Lines[id].Z1 = z2;
            Lines[id].R = r;
            Lines[id].G = g;
            Lines[id].B = b;
        }

        /// <summary>
        /// Call this once the meshLines have been defined and you're ready to render.
        /// </summary>
        public void Final()
        {
            Viz = new VLines(Lines);
            Viz.Final();
        }
    }
}
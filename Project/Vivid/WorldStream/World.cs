﻿using System.Collections.Generic;

namespace Vivid.WorldStream
{
    public class World
    {


        public void AddChunk(WorldChunk chunk)
        {

            Chunks.Add(chunk);

        }
        public List<WorldChunk> Chunks = new List<WorldChunk>();
        public List<WorldChunk> Loaded = new List<WorldChunk>();
        public List<WorldChunk> Loading = new List<WorldChunk>();

    }
}

﻿using System.Collections.Generic;
using Vivid.Data;
using Vivid.Scene;

namespace Vivid.WorldStream
{
    public class WorldChunk
    {
        public int WorldX = 0, WorldZ = 0;

        public List<Mesh3D> Meshes = new List<Mesh3D>();

        public List<WorldChunk> NextChunks = new List<WorldChunk>();
        public Bounds Bounding = new Bounds();

        public float X, Y, Z;

        public string ChunkPath = "";

    }
}

﻿using OpenTK.Graphics.OpenGL;
using OpenTK.Windowing.Desktop;

using System;
using System.Collections.Generic;

using Vivid.State;


/// <summary>
/// The Vivid.App namespace contains all classes related to creating a new application.
/// </summary>
namespace Vivid.App
{

    /// <summary>
    /// This is the main application class.
    /// You should inherit this class to create your own application, IF, you are
    /// coding your app and not using VividScript and the IDE.
    /// </summary>
    public class Application : OpenTK.Windowing.Desktop.GameWindow
    {

        /// <summary>
        /// You should set this to your initial State, before running the application.
        /// </summary>
        public static AppState InitState = null;

        public static Stack<AppState> States = new Stack<AppState>();

        /// <summary>
        /// Pushes a new state onto the application stack.
        /// </summary>
        /// <param name="state">The state to push onto the stack.</param>
        public static void PushState(AppState state)
        {
            States.Push(state);
            state.InitState();
            state.InitUI();
        }

        /// <summary>
        /// Pops the current state from off the stack.
        /// </summary>
        public static void PopState()
        {
            if (States.Count == 0) return;
            States.Pop();
        }

        /// <summary>
        /// This should be called via your application's constructor.
        /// </summary>
        /// <param name="name">Name of the application(Appears on the window title)</param>
        /// <param name="gameSet">Game settings(See OpenTK)</param>
        /// <param name="nativeSet">Native application settings(See OpenTK)</param>
        public Application(string name, GameWindowSettings gameSet, NativeWindowSettings nativeSet) : base(gameSet, nativeSet)
        {
            this.Load += LoadApp;
            this.UpdateFrame += UpdateApp;
            this.RenderFrame += RenderApp;
            this.MouseMove += MouseMoveApp;
            this.MouseDown += MouseDownApp;
            this.MouseUp += Application_MouseUp;
            this.KeyDown += KeyDownApp;
            this.KeyUp += KeyUpApp;
            //AppInfo.W = 
            Input.Input.InitInput();
            Import.Import.RegDefaults();
        }

        private void KeyUpApp(OpenTK.Windowing.Common.KeyboardKeyEventArgs obj)
        {
            Input.Input.SetKey(obj.Key, false);

            //throw new NotImplementedException();
        }

        private void KeyDownApp(OpenTK.Windowing.Common.KeyboardKeyEventArgs obj)
        {
            Input.Input.SetKey(obj.Key, true);
            //throw new NotImplementedException();
        }

        private void Application_MouseUp(OpenTK.Windowing.Common.MouseButtonEventArgs obj)
        {
            switch (obj.Button)
            {
                case OpenTK.Windowing.GraphicsLibraryFramework.MouseButton.Left:
                    Input.Input.MB[0] = false;

                    break;

                case OpenTK.Windowing.GraphicsLibraryFramework.MouseButton.Right:
                    Input.Input.MB[1] = false;
                    break;

                case OpenTK.Windowing.GraphicsLibraryFramework.MouseButton.Middle:
                    Input.Input.MB[2] = false;
                    break;
            }
            //throw new NotImplementedException();
        }

        private void MouseDownApp(OpenTK.Windowing.Common.MouseButtonEventArgs obj)
        {
            switch (obj.Button)
            {
                case OpenTK.Windowing.GraphicsLibraryFramework.MouseButton.Left:
                    Input.Input.MB[0] = true;
                    break;

                case OpenTK.Windowing.GraphicsLibraryFramework.MouseButton.Right:
                    Input.Input.MB[1] = true;
                    break;

                case OpenTK.Windowing.GraphicsLibraryFramework.MouseButton.Middle:
                    Input.Input.MB[2] = true;
                    break;
            }
            //throw new NotImplementedException();
        }

        private void MouseMoveApp(OpenTK.Windowing.Common.MouseMoveEventArgs obj)
        {
            Input.Input.MX = (int)obj.X;
            Input.Input.MY = (int)obj.Y;
            Input.Input.MDX = (int)obj.DeltaX;
            Input.Input.MDY = (int)obj.DeltaY;

            //throw new NotImplementedException();
        }

        private void SetupGL()
        {
            AppInfo.W = Size.X; //Size.With;
            AppInfo.H = Size.Y;
            AppInfo.RW = Size.X;
            AppInfo.RH = Size.Y;
            //Console.WriteLine("W:" + Width + " H:" + Height);

            SetVP.Set(0, 0, AppInfo.W, AppInfo.H);
            GL.Scissor(0, 0, AppInfo.W, AppInfo.H);
            GL.Disable(EnableCap.Blend);
            GL.Disable(EnableCap.Texture2D);
            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Back);
            GL.Disable(EnableCap.StencilTest);
            GL.Disable(EnableCap.ScissorTest);
            // GL.Disable(EnableCap.Lighting);

            //GL.DepthFunc(DepthFunction.Greater);

            GL.Enable(EnableCap.DepthTest);
            GL.DepthRange(0, 1);

            GL.ClearDepth(1.0f);
            GL.DepthFunc(DepthFunction.Lequal);
        }

        private void LoadApp()
        {
            Console.WriteLine("Load App");
            SetupGL();
            Vivid.Font2.FontTTF.Init();
            Init();
            if (InitState != null)
            {
                PushState(InitState);
            }
            //throw new NotImplementedException();
        }

        /// <summary>
        /// This will be called once per frame, you should override it to
        /// provide your own functionality.
        /// </summary>
        public virtual void Update()
        {
        }

        /// <summary>
        /// This will be called once per frame. All your custom app-wide rendering
        /// should be placed here.
        /// </summary>
        public virtual void Render()
        {
        }


        /// <summary>
        /// This will be called once just before your application run state beings.(Which is update>>render>>update>>render)
        /// </summary>
        public virtual void Init()
        {
        }

        private int nextFps = 0;
        private int fps = 0, frame = 0;

        private void RenderApp(OpenTK.Windowing.Common.FrameEventArgs obj)
        {
            //Console.WriteLine("Render app.");

            GL.ClearColor(0, 0, 0, 1);

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            int time = Environment.TickCount;

            if (time > nextFps)
            {
                nextFps = time + 1000;
                fps = frame;
                frame = 0;
            }
            frame++;
            Title = "Fps:" + fps.ToString();

            Render();
            if (States.Count > 0)
            {
                var state = States.Peek();
                state.DrawState();
            }

            SwapBuffers();
            //    throw new NotImplementedException();
        }

        private void UpdateApp(OpenTK.Windowing.Common.FrameEventArgs obj)
        {
            //Console.WriteLine("Update app.");
            Update();

            if (States.Count > 0)
            {
                var state = States.Peek();
                state.UpdateState();
            }

            //  throw new NotImplementedException();
        }
    }


    /// <summary>
    /// AppInfo contains app-wide render dimensions.
    /// </summary>
    public class AppInfo
    {
        /// <summary>
        /// Width,Height and RenderTarget Width,Height.
        /// </summary>
        public static int W, H, RH;

        public static int RW
        {
            get
            {

                return _W;

            }
            set
            {
                _W = value;
            }
        }
        static int _W;
    }

    public static class SetVP
    {
        public static int X = 0, Y = 0, W = 0, H = 0;

        public static void Set(int x, int y, int w, int h)
        {
            GL.Viewport(x, y, w, h);
            X = x;
            Y = y;
            W = w;
            H = h;
        }
    }
}
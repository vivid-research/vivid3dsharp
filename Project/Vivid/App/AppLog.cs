﻿using System;

namespace Vivid.App
{

    /// <summary>
    /// Let's you log messages to both the engine and to file , if set so.
    /// </summary>
    public static class AppLog
    {
        /// <summary>
        /// Log a message to file and engine.
        /// </summary>
        /// <param name="msg">The general message.</param>
        /// <param name="area">The type of log.</param>
        public static void Log(string msg, string area = "")
        {
            Console.WriteLine(msg + "@" + area);
        }
    }
}
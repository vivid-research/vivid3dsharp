﻿using Vivid.Resonance.Forms;
namespace Vivid3D
{
    public class SceneTree : TreeViewForm
    {

        public SceneTree()
        {

            Root = new TreeNode("Scene");
            Root.AddItem("Node1");
            Root.AddItem("Node2");
            Root.AddItem("Node3");
            Select = (node) =>
            {
                if (node == null) return;
                EditGlobal.View.SelectNode(node.Data);

            };


        }

        public void Rebuild()
        {

            Root = new TreeNode("Scene");
            var gr = EditGlobal.View.Graph;
            AddNode(Root, gr.Root);

        }

        public void SelectNode(Vivid.Scene.Node3D node)
        {
            SelectIf(Root, node);
        }

        public void SelectIf(TreeNode n,Vivid.Scene.Node3D sn)
        {
            if(n.Data == sn)
            {
                Over = n;
                this.ActNode = n;
                this.Selected = n;
                return;
            }
            foreach(var sub in n.Nodes)
            {
                SelectIf(sub, sn);
            }

        }

        void AddNode(TreeNode r, Vivid.Scene.Node3D n)
        {
            r.Name = n.Name;
            r.Data = n;
            foreach (var s in n.Sub)
            {
                TreeNode nn = new TreeNode(s.Name);
                r.AddItem(nn);
                AddNode(nn, s);
            }
        }

    }
}

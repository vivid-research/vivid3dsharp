﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Vivid.Content;
using Vivid.Resonance;
using Vivid.Resonance.Forms;
using Vivid.Texture;




namespace Vivid3D
{

    public delegate void LoadContent(string path, string name);

    public class IDEContentBrowserForm : UIForm
    {
        public static string ContentPath = "c:/content/";

        public List<ContentEntry> Enteries = new List<ContentEntry>();

        public static Texture2D ImgFile, ImgFolder;

        public ContentEntry ActiveEntry = null;

        public bool DragContent = false;

        public static Stack<string> Paths = new Stack<string>();

        public static LoadContent Load = null;

        public static IDEContentBrowserForm Main = null;

        private VerticalScrollerForm scroller = null;

        public static void Rescan()
        {
            Main.ScanContent();
        }

        public IDEContentBrowserForm()
        {
            Main = this;
            ScissorTest = true;

            if (ImgFile == null)
            {
                ImgFolder = new Texture2D("data/icon/folder1.png", LoadMethod.Single, true);
                ImgFile = new Texture2D("data/icon/file1.png", LoadMethod.Single, true);
            }

            ContextMenu = new ContextMenuForm();

            Draw = () =>
            {

                DrawFormSolid(UI.CurUI.Theme.BackgroundCol);

            };

            var create = ContextMenu.AddItem("Create");

            var new_folder = create.Menu.AddItem("New Folder");
            var new_script = create.Menu.AddItem("New Script");
            Clip = true;
            new_script.Click = () =>
            {
                var spath = CurrentPath;
                Console.WriteLine("New Script. Path:" + CurrentPath);
                if (CurrentPath.Substring(CurrentPath.Length - 1) != "\\")
                {
                    spath += "\\";
                }

                //  var nsf = new NewScriptForm().Set(AppInfo.W / 2 - 200, AppInfo.H / 2 - 60, 400, 120, "New Script") as NewScriptForm;
                //UI.CurUI.Top = nsf;
                //nsf.Path = spath;
            };

            /*
            MouseMove = (x, y, xd, yd) =>
            {
                if (DragContent)
                {
                    if (x > 0 && y > 0)
                    {
                        ActiveEntry.VisualX += xd;
                        ActiveEntry.VisualY += yd;
                        if (ActiveEntry.VisualX < 10) ActiveEntry.VisualX = 0;
                        if (ActiveEntry.VisualY < 30) ActiveEntry.VisualY = 30;
                    }
                    return;
                }
                ActiveEntry = null;
                foreach (var entry in Enteries)
                {
                    //if (entry.VisualY + OffY < 2) continue;
                    //if (entry.VisualY + OffY + 64 > H) continue;
                    entry.Over = false;
                    if (x > entry.VisualX && x < entry.VisualX + 64)
                    {
                        if (y > entry.DrawY + OffY && y < (entry.DrawY + OffY) + 64)
                        {
                            entry.Over = true;
                            //  Console.WriteLine("Hit");
                            ActiveEntry = entry;
                        }
                    }
                }
            };
            */
            DoubleClick = (b) =>
            {
                if (b == 0)
                {
                    if (ActiveEntry != null)
                    {
                        if (ActiveEntry is ContentFolder)
                        {
                            ContentPath = ActiveEntry.Path;
                          //  Paths.Push(ContentPath);
                            ActiveEntry = null;
                            ScanContent();
                            return;
                        }
                        if (ActiveEntry is ContentScript)
                        {
                            Process np = Process.Start(ActiveEntry.FullPath, "");
                            return;
                        }

                        if (ActiveEntry is ContentFile)
                        {
                            Load?.Invoke(ActiveEntry.Path, ActiveEntry.Name);
                            // IDESceneGraph3D.Rebuild();
                            return;
                        }
                    }
                }
            };


            MouseDown = (b) =>
            {
                if (b == 2)
                {
                    if (Paths.Count > 1)
                    {
                        Paths.Pop();
                        Paths.Pop();
                        if (Paths.Count > 0)
                        {
                            var cp = ContentPath;

                            ContentPath = Paths.Peek();
                            if (cp == ContentPath)
                            {
                                Paths.Pop();
                                ContentPath = Paths.Peek();
                            }

                            ScanContent();

                        }
                        else
                        {
                            ContentPath = OriginalPath;
                            ScanContent();
                        }
                    }
                }
                if (b == 0 && ActiveEntry != null)
                {
                    var dragobj = new DragObject();

                    dragobj.Label = ActiveEntry.Name;
                    dragobj.DragObj = ActiveEntry;
                    dragobj.DragImg = ActiveEntry.Image;

                    //DragObject.

                    UI.CurUI.DragObj = dragobj;
                    //DragContent = true;
                    //Enteries.Remove(ActiveEntry);
                    //Enteries.Add(ActiveEntry);
                }
            };

            /*
            MouseUp = (b) =>
            {
                if (b == 0 && ActiveEntry != null)
                {
                    DragContent = false;
                }
            };
            */
            /*
            Draw = () =>
            {
                if (Body != null)
                {
                //    Forms.Remove(Body);
               //     Body = null;
                }
              //  DrawFormSolid(UI.CurUI.Theme.ForegroundCol, 0, 25+-OffY, W, H);
              //  DrawFormSolid(UI.CurUI.Theme.BackgroundCol, 1, 26 - OffY, W - 2, H - 2);

                /*
                foreach(var entry in Enteries)
                {
                    //if (entry.VisualY+OffY < 2) continue;
                    //if (entry.VisualY+OffY + 64 > H) continue;
                    if(ActiveEntry == entry)
                    {
                        DrawFormSolid(UI.CurUI.Theme.ForegroundCol*0.3f, entry.VisualX-6, entry.VisualY, 76, 84);
                    }
                    //i///f (ActiveEntry != entry)
                    //{
                        DrawForm(entry.Image, entry.VisualX, entry.VisualY, 64, 64);
                    //}

                    entry.DrawX = entry.VisualX;
                    entry.DrawY = entry.VisualY;// + OffY;

                    int rx = entry.Name.Length;
                    for(int i = 1; i < entry.Name.Length; i++) {
                        int aw = Font.Width(entry.Name.Substring(0, i));
                        if (aw > 110)
                        {
                            rx = i;
                            break;
                        }
                    }

                    string dt = entry.Name.Substring(0, rx);

                    if (ActiveEntry == entry)
                    {
                        DrawFormSolid(new Vector4(0.2f, 0.2f, 0.2f, 1), entry.VisualX, entry.VisualY + 58, Font.Width(entry.Name), 25);
                        DrawText(entry.Name, entry.VisualX, entry.VisualY + 58, UI.CurUI.Theme.TextCol);
                    }
                    else
                    {
                        DrawText(dt, entry.VisualX, entry.VisualY + 58, UI.CurUI.Theme.TextCol);
                    }
                }

                foreach(var en in Enteries)
                {
                    if (en.Over == true) continue;
                    DrawFormSolid(new Vector4(1, 0, 0, 1), en.DrawX, en.DrawY,64,64);
                }
                */


        }

        public static string CurrentPath = "";

        public void ScanContent()
        {
            int vx = 10;
            int vy = 5;

            CurrentPath = ContentPath;

            Enteries.Clear();

            Forms.Clear();
            
            if(OriginalPath == "")
            {
                OriginalPath = ContentPath;
            }
            Paths.Push(ContentPath);


            foreach (var folder in new DirectoryInfo(ContentPath).GetDirectories())
            {
                var new_Entry = new ContentFolder(folder.Name, folder.FullName);

                new_Entry.VisualX = vx;
                new_Entry.VisualY = vy;
                new_Entry.Image = ImgFolder;

                Enteries.Add(new_Entry);
                var cItem = new ContentItemForm();
                cItem.Entry = new_Entry;
                Add(cItem);
                cItem.Set(vx, vy, 64, 64);
                vx = vx + 128;

                if (vx > W - 158)
                {
                    vx = 10;
                    vy = vy + 128;
                }
                scroller.MaxValue = (vy);
            }

            foreach (var file in new DirectoryInfo(ContentPath).GetFiles())
            {
                ContentFile new_entry;

                switch (file.Extension)
                {
                    case ".wav":
                    case ".mp3":
                    case ".ogg":
                        new_entry = new ContentSound(file.Name, file.FullName);
                        break;

                    case ".gltf":
                    case ".bin":
                    case ".3ds":
                    case ".x":
                    case ".obj":
                    case ".fbx":
                   case ".blend":
                    case ".b3d":
                        new_entry = new ContentModel(file.Name, file.FullName);
                        break;

                    case ".jpg":
                    case ".png":
                    case ".bmp":
                        new_entry = new ContentTexture(file.Name, file.FullName);
                        break;

                    case ".cs":
                        new_entry = new ContentScript(file.Name, file.FullName);
                        break;

                    default:
                        continue;
                        //new_entry = new ContentFile(file.Name, file.FullName);
                        break;
                }
                //var new_entry = new ContentFile(file.Name,file.FullName);

                new_entry.VisualX = vx;
                new_entry.VisualY = vy;
                new_entry.Image = ImgFile;
                new_entry.Ext = file.Extension;

                Enteries.Add(new_entry);
                var cItem = new ContentItemForm();
                cItem.Entry = new_entry;
                Add(cItem);
                cItem.Set(vx, vy, 64, 64);

                vx = vx + 128;

                if (vx > W - 158)
                {
                    vx = 10;
                    vy = vy + 128;
                }

                scroller.MaxValue =vy;
            }

            AfterSetup();
            //Add(scroller);
            int v = 0;
        }
        string OriginalPath = "";
        public override void AfterSetup()
        {
            //base.AfterSetup();
            if (scroller == null)
            {
                scroller = new VerticalScrollerForm().Set(W - 15, 3, 10, H - 6) as VerticalScrollerForm;
                //  Add(scroller);
            }
            Add(scroller);
            //scroller.ScrollBut.CoreTex = new Texture2D("ide/ui/winbody3.png", true);
            scroller.Set(W - 15, 3, 10, H - 55);
            scroller.ValueChange = (v) =>
            {

                foreach (var i in Forms)
                {

                    if (i is ContentItemForm)
                    {
                        i.OffY = -(int)v;
                    }
                }

            };
            //scroller.ViewX = GX;
            //scroller.ViewY = GY + 25;
            //scroller.ViewW = W;
            // scroller.ViewH = H - 35;
            //scroller.ScrollBut.ViewX = GX;
            //scroller.ScrollBut.ViewY = GY + 25;
            //scroller.ScrollBut.ViewW = W;
            //scroller.ScrollBut.ViewH = H - 35;


            if (Enteries.Count > 1)
            {
                scroller.MaxValue =(Enteries[Enteries.Count - 1].VisualY);
            }
        }
    }
}
﻿namespace Vivid3D
{
    public class EditGlobal
    {
        public static SceneView View;
        public static SceneTree Tree;
        public static IDEContentBrowserForm Browser;
        public static Vivid.Resonance.Forms.ClassInspectorForm Inspector;
    }
}

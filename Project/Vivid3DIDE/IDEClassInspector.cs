﻿using Vivid.Resonance.Forms;
using Vivid.Resonance;

namespace Vivid3D
{
    public class IDEClassInspector : UIForm 
    {
        public static IDEClassInspector Main;
        public InspectorForm Form;

        public override void AfterSetup()
        {
            if (Form == null)
            {
                Form = new InspectorForm().Set(0, 0, W, H, "") as InspectorForm;
                Add(Form);
                Main = this;
            }
            Form.Set(0, 0, W, H);
            //base.AfterSetup();
        }
    }
}
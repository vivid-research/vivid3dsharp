﻿using OpenTK.Windowing.Desktop;

using Vivid.App;
namespace Vivid3D
{
    public class Vivid3DApp : Application
    {

        public Vivid3DApp(GameWindowSettings gw, NativeWindowSettings ws) : base("Vivid3D", gw, ws)
        {

        }

    }
}

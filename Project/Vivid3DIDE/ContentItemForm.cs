﻿using Vivid.Content;
using Vivid.Resonance;

namespace Vivid3D
{
    public class ContentItemForm : UIForm
    {
        private bool Over = false;
        public ContentEntry Entry;

        public ContentItemForm()
        {
            MouseMove = (x, y, dx, dy) =>
            {
                Over = true;
            };

            MouseDown = (b) =>
            {
                if (b != 0) return;
                var dragobj = new Vivid.Resonance.Forms.DragObject();

                dragobj.Label = Entry.Name;
                dragobj.DragObj = Entry;
                dragobj.DragImg = Entry.Image;

                //DragObject.

                UI.CurUI.DragObj = dragobj;
            };

            DoubleClick = (b) =>
            {
                if (b == 0)
                {
                    var ActiveEntry = Entry;

                    if (ActiveEntry is ContentFolder)
                    {
                        IDEContentBrowserForm.ContentPath = ActiveEntry.Path;

                        ////IDEContentBrowserForm.ContentPath = ActiveEntry.Path;
                        IDEContentBrowserForm.Paths.Push(ActiveEntry.Path);

                        ActiveEntry = null;
                        IDEContentBrowserForm.Main.ScanContent();

                        return;
                    }
                    if (ActiveEntry is ContentScript)
                    {
                        // Process np = Process.Start(ActiveEntry.FullPath, "");
                        return;
                    }

                    if (ActiveEntry is ContentFile)
                    {
                        IDEContentBrowserForm.Load?.Invoke(ActiveEntry.Path, ActiveEntry.Name);
                        // VividIDE.Forms.Editor3D.IDESceneGraph3D.Rebuild();

                        return;
                    }
                }
            };

            Draw = () =>
            {
                //  Console.Wri
                if (Over)
                {
                    DrawFormSolid(UI.CurUI.Theme.ForegroundCol * 0.3f, -4, -4, W + 8, H + 8);
                }
                DrawForm(Entry.Image, 0, 0, W, H);
                if (Over)
                {
                    DrawFormSolid(UI.CurUI.Theme.ForegroundCol * 0.4f, 5, 64, Font.Width(Entry.Name), 25);
                }
                DrawText(Entry.Name, 5, 64, UI.CurUI.Theme.TextCol);
                Over = false;
            };
        }
    }
}
﻿using OpenTK.Mathematics;

using Vivid.Data;
using Vivid.Scene;

namespace Vivid3D
{
    public class GizmoGen
    {

        public static Entity3D RotationGizmo(float size,float width)
        {

            Entity3D r = new Entity3D();

            Mesh3D rx, ry, rz;

            ry = new Mesh3D(720*12, 720*4);

            int vn = 0;
               

            for(int a = 0; a < 360; a++)
            {

                float aa = MathHelper.DegreesToRadians(a);
                float a2 = MathHelper.DegreesToRadians(a - 90);
                float a3 = MathHelper.DegreesToRadians(a + 90);

                float ax, az;
                float mx, mz;

                mx = (float)MathHelper.Cos(a2) * width / 2;
                mz = (float)MathHelper.Sin(a2) * width / 2;

                ax = (float)MathHelper.Cos(aa) * size / 2;
                az = (float)MathHelper.Sin(aa) * size / 2;

                float x1 = ax + mx;
                float z1 = az + mz;
                float x2 = ax - mx;
                float z2 = az - mz;

                ry.SetVertex(vn++, new Vector3(x1, 0, z1));
                ry.SetVertex(vn++, new Vector3(x2, 0, z2));

            }

            int tn = 0;

            for(int a = 0; a < 360*2-20; a+=2)
            {

                int v0, v1, v2, v3;

                v0 = a;
                v1 = a + 1;
                v2 = a + 2;
                v3 = a + 3;

                ry.SetTri(tn++, new Tri(v0, v1, v2));
                ry.SetTri(tn++, new Tri(v1, v3, v2));
                ry.SetTri(tn++, new Tri(v0, v2, v1));
                ry.SetTri(tn++, new Tri(v1, v2, v3));

            }

           

            ry.Final();

            ry.Material = new Vivid.Material.Material3D();

            ry.Material.Diff = new Vector3(0, 1, 0);

            // RX

            rx = new Mesh3D(720 * 12, 720 * 4);

            vn = 0;


            for (int a = 0; a < 360; a++)
            {

                float aa = MathHelper.DegreesToRadians(a);
                float a2 = MathHelper.DegreesToRadians(a - 90);
                float a3 = MathHelper.DegreesToRadians(a + 90);

                float ax, az;
                float mx, mz;

                mx = (float)MathHelper.Cos(a2) * width / 2;
                mz = (float)MathHelper.Sin(a2) * width / 2;

                ax = (float)MathHelper.Cos(aa) * size / 2;
                az = (float)MathHelper.Sin(aa) * size / 2;

                float x1 = ax + mx;
                float z1 = az + mz;
                float x2 = ax - mx;
                float z2 = az - mz;

                rx.SetVertex(vn++, new Vector3(0, z1, x1));
                rx.SetVertex(vn++, new Vector3(0, z2, x2));

            }

            tn = 0;

            for (int a = 0; a < 360 * 2 - 20; a += 2)
            {

                int v0, v1, v2, v3;

                v0 = a;
                v1 = a + 1;
                v2 = a + 2;
                v3 = a + 3;

                rx.SetTri(tn++, new Tri(v0, v1, v2));
                rx.SetTri(tn++, new Tri(v1, v3, v2));
                rx.SetTri(tn++, new Tri(v0, v2, v1));
                rx.SetTri(tn++, new Tri(v1, v2, v3));

            }



            rx.Final();

            rx.Material = new Vivid.Material.Material3D();

            rx.Material.Diff = new Vector3(1, 0, 0);


            //rz

            rz = new Mesh3D(720 * 12, 720 * 4);

           vn = 0;


            for (int a = 0; a < 360; a++)
            {

                float aa = MathHelper.DegreesToRadians(a);
                float a2 = MathHelper.DegreesToRadians(a - 90);
                float a3 = MathHelper.DegreesToRadians(a + 90);

                float ax, az;
                float mx, mz;

                mx = (float)MathHelper.Cos(a2) * width / 2;
                mz = (float)MathHelper.Sin(a2) * width / 2;

                ax = (float)MathHelper.Cos(aa) * size / 2;
                az = (float)MathHelper.Sin(aa) * size / 2;

                float x1 = ax + mx;
                float z1 = az + mz;
                float x2 = ax - mx;
                float z2 = az - mz;

                rz.SetVertex(vn++, new Vector3(x1, z1, 0));
                rz.SetVertex(vn++, new Vector3(x2, z2, 0));

            }

             tn = 0;

            for (int a = 0; a < 360 * 2 - 20; a += 2)
            {

                int v0, v1, v2, v3;

                v0 = a;
                v1 = a + 1;
                v2 = a + 2;
                v3 = a + 3;

                rz.SetTri(tn++, new Tri(v0, v1, v2));
                rz.SetTri(tn++, new Tri(v1, v3, v2));
                rz.SetTri(tn++, new Tri(v0, v2, v1));
                rz.SetTri(tn++, new Tri(v1, v2, v3));

            }



            rz.Final();

            rz.Material = new Vivid.Material.Material3D();

            rz.Material.Diff = new Vector3(0, 0, 1);


            r.AddMesh(rx);
            r.AddMesh(ry);
            r.AddMesh(rz);

            r.SetColorOnly();

            return r;


        }
        public static Entity3D TranslateGizmo(float size,float len)
        {

            Entity3D r = new Entity3D();

            Vivid.Import.AssImpImport.Optimize = false;

            r =(Entity3D) Vivid.Import.Import.ImportNode("data/ide/cone1.fbx");

            Vivid.Import.AssImpImport.Optimize = true;

            Entity3D tX = (Entity3D)r.FindNode("mX");
            Entity3D tY = (Entity3D)r.FindNode("mY");
            Entity3D tZ = (Entity3D)r.FindNode("mZ");

            tX.Meshes[0].Material.Diff = new Vector3(1, 0.1f, 0.1f);
            tY.Meshes[0].Material.Diff = new Vector3(0.1f, 1, 0.1f);
            tZ.Meshes[0].Material.Diff = new Vector3(0.1f, 0.1f, 1);

            r.SetColorOnly();

            return r;

        }
        public static Entity3D TranslateGizmo2(float size, float len)
        {
            Entity3D r = new Entity3D();

            Mesh3D X, Y, Z;

            X = new Mesh3D(9 * 2, 7);
            Y = new Mesh3D(9 * 2, 7);
            Z = new Mesh3D(9 * 2, 7);

            X.SetVertex(0, new Vector3(0, 0, -size / 2));
            X.SetVertex(1, new Vector3(len, 0, -size / 2));
            X.SetVertex(2, new Vector3(len, 0, size / 2));
            X.SetVertex(3, new Vector3(0, 0, size / 2));

            X.SetVertex(4, new Vector3(len, 0, -size));
            X.SetVertex(5, new Vector3(len + size, 0, 0));
            X.SetVertex(6, new Vector3(len, 0, size));

            X.SetTri(0, 0, 1, 2);
            X.SetTri(1, 2, 3, 0);
            X.SetTri(2, 0, 2, 1);
            X.SetTri(3, 2, 0, 3);

            X.SetTri(4, 4, 5, 6);
            X.SetTri(5, 4, 6, 5);

            r.AddMesh(X);

            Z.SetVertex(0, new Vector3(-size / 2, 0, 0));
            Z.SetVertex(1, new Vector3(size / 2, 0, 0));
            Z.SetVertex(2, new Vector3(size / 2, 0, len));
            Z.SetVertex(3, new Vector3(-size / 2, 0, len));

            Z.SetVertex(4, new Vector3(-size, 0, len));
            Z.SetVertex(5, new Vector3(size, 0, len));
            Z.SetVertex(6, new Vector3(0, 0, len + size));

            //  Z.SetVertex(1, new OpenTK.Vector3(len, 0, -size / 2));

            Z.SetTri(0, 0, 1, 2);
            Z.SetTri(1, 2, 3, 0);
            Z.SetTri(2, 0, 2, 1);
            Z.SetTri(3, 2, 0, 3);

            Z.SetTri(4, 4, 5, 6);
            Z.SetTri(5, 4, 6, 5);

            r.AddMesh(Z);

            Y.SetVertex(0, new Vector3(-size / 2, 0, 0));
            Y.SetVertex(1, new Vector3(size / 2, 0, 0));
            Y.SetVertex(2, new Vector3(size / 2, len, 0));
            Y.SetVertex(3, new Vector3(-size / 2, len, 0));

            Y.SetVertex(4, new Vector3(-size, len, 0));
            Y.SetVertex(5, new Vector3(size, len, 0));
            Y.SetVertex(6, new Vector3(0, len + size, 0));

            Y.SetTri(0, 0, 1, 2);
            Y.SetTri(1, 2, 3, 0);
            Y.SetTri(2, 0, 2, 1);
            Y.SetTri(3, 2, 0, 3);

            Y.SetTri(4, 4, 5, 6);
            Y.SetTri(5, 4, 6, 5);

            X.Material = new Vivid.Material.Material3D();

            X.Final();

            Z.Material = new Vivid.Material.Material3D();

            Z.Final();

            Y.Material = new Vivid.Material.Material3D();

            Y.Final();

            r.AddMesh(Y);

            X.Material.Diff = new Vector3(0.5f, 0, 0.5f);
            Y.Material.Diff = new Vector3(1, 1, 0);
            Z.Material.Diff = new Vector3(0, 0.5f, 0.5f);

            r.SetColorOnly();

            r.SetPos(new Vector3(0, 0.1f, 0)); ;//= new Action<OpenTK.Vector3>()
            //{
            //};

            return r;
        }
    }
}
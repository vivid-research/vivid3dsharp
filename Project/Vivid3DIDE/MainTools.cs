﻿using Vivid.App;
using Vivid.Resonance.Forms;
namespace Vivid3D
{
    public class MainTools : ToolBarForm
    {

        public MainTools()
        {

            Set(0, 25, AppInfo.W, 45);

            var newProj = AddItem(new Vivid.Texture.Texture2D("data/edit/iNewProj.png", true));
            AddItem(new Vivid.Texture.Texture2D("data/edit/iLoadProj.png", true));
            AddItem(new Vivid.Texture.Texture2D("data/edit/iSaveProj.png", true));
            AddSeperator();
            var iSelect = AddItem(new Vivid.Texture.Texture2D("data/edit/iSelect.png", true));
            AddSeperator();
            AddLabel("Space");
            var i1 = AddList("Global", "Local", "Smart");


            DropDownListForm df = i1.Form as DropDownListForm;

            df.SelectedItem = (item) =>
            {

                switch (item)
                {
                    case "Local":
                        SceneView.SpaceMode = Locality.Local;
                        break;
                    case "Global":
                        SceneView.SpaceMode = Locality.Global;
                        break;
                }

            };

            var iMove = AddItem(new Vivid.Texture.Texture2D("data/edit/iMove.png", true));
            var iRotate = AddItem(new Vivid.Texture.Texture2D("data/edit/iRotate.png", true));
            var iScale = AddItem(new Vivid.Texture.Texture2D("data/edit/iScale.png", true));
            AddSeperator();
            var playItem = AddItem(new Vivid.Texture.Texture2D("data/edit/iPlay2.png", true));

            var stopItem = AddItem(new Vivid.Texture.Texture2D("data/edit/iStop.png", true));
            AddItem(new Vivid.Texture.Texture2D("data/edit/iPause.png", true));

            iMove.Button.Highlight = true;

            playItem.Button.Click = (b) =>
            {

                SceneView.SVMain.BeginPlay();

            };

            stopItem.Button.Click = (b) =>
            {

                SceneView.SVMain.StopPlay();

            };

            iSelect.Button.Click = (b) =>
            {
                iSelect.Button.Highlight = true;
                iMove.Button.Highlight = false;
                iRotate.Button.Highlight = false;
                iScale.Button.Highlight = false;
                SceneView.GizmoMode = GizmoType.Select;
            };

            iMove.Button.Click = (b) =>
            {
                iSelect.Button.Highlight = false;
                iMove.Button.Highlight = true;
                iRotate.Button.Highlight = false;
                iScale.Button.Highlight = false;
                //SceneView.GizmoMode = GizmoType.Translate;
                //EditGlobal.View.Set
                SceneView.SetGizmo(GizmoType.Translate);
            };

            iRotate.Button.Click = (b) =>
            {

                iSelect.Button.Highlight = false; ;
                iMove.Button.Highlight = false;
                iRotate.Button.Highlight = true;
                iScale.Button.Highlight = false;
                SceneView.SetGizmo(GizmoType.Rotate);
                //SceneView.GizmoMode = GizmoType.Rotate;

            };

            iScale.Button.Click = (b) =>
            {
                iSelect.Button.Highlight = false;
                iMove.Button.Highlight = false;
                iRotate.Button.Highlight = false;
                iScale.Button.Highlight = true;
                SceneView.GizmoMode = GizmoType.Scale;
            };

        }

    }
}

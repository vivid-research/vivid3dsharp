﻿using Vivid.App;

namespace Vivid3D
{
    public class MainEditState : Vivid.State.AppState
    {

        public override void InitState()
        {
            SUI = new Vivid.Resonance.UI();
            Menu = new MainMenu();
            //base.InitState();
            Split1 = new MainSplit();
            MainUI = new Vivid.Resonance.UIForm().Set(0, 0, AppInfo.W, AppInfo.H);
            Menu.Set(0, 0, AppInfo.W, 30);
            Tools = new MainTools();


            MainUI.Add(Split1);
            MainUI.Add(Tools);
            MainUI.Add(Menu);

            SUI.Root = MainUI;
            EditGlobal.Tree.Rebuild();
        }

        public override void UpdateState()
        {
            //base.UpdateState();
            SUI.Update();
        }

        public override void DrawState()
        {
            //base.DrawState();
            SUI.Render();
        }

        private Vivid.Resonance.UIForm MainUI;
        private MainMenu Menu;
        private MainSplit Split1;
        private MainTools Tools;
    }
}

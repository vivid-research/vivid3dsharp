﻿using Vivid.App;
using Vivid.Resonance.Forms;
namespace Vivid3D
{
    public class MainSplit : VerticalSplitterForm
    {

        public MainSplit()
        {

            Set(0, 75, AppInfo.W, AppInfo.H - 25);
            SetSplit(250);
            Split2 = new SecondSplit();
            SetRight(Split2);
            Tree = new SceneTree();
            SetLeft(Tree);
            EditGlobal.Tree = Tree;



        }
        SceneTree Tree;
        SecondSplit Split2;
    }
}

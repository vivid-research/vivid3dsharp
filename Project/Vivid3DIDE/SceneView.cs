﻿using OpenTK.Mathematics;
using OpenTK.Windowing.GraphicsLibraryFramework;

using System;

using Vivid.App;
using Vivid.Data;
using Vivid.Resonance.Forms;
using Vivid.Scene;
using Vivid.Scene.Node;
namespace Vivid3D
{
    public enum Locality
    {
        Local, Global
    }

    public enum GizmoType
    {
        Translate, Rotate, Scale,Select
    }

    public enum MoveMode
    {
        Free, Snap, NearestTri
    }
    public class SceneView : Graph3DForm
    {

        public Node3D SelectedEntity = null;

        public static SceneView SVMain = null;

        private bool Rotating = false;
        private float CamPitch = 0, CamYaw = 0;
        bool firstSetup = true;
        Entity3D gTrans, gRot, gScale;

        Entity3D tX;
        Entity3D tY;
        Entity3D tZ;
        public Entity3D CurGizmo, GizmoRot, GizmoScale;
        public static MoveMode SnapMode = MoveMode.Free;
        public static Locality SpaceMode = Locality.Global;
        public static GizmoType GizmoMode = GizmoType.Translate;
        public bool MoveX, MoveY, MoveZ;
        int MouseX, MouseY;

        public void SelectNode(Node3D node)
        {
            if (node is Entity3D)
            {
                SelectedEntity = node as Entity3D;
                CurGizmo.LocalPos = node.LocalPos;
                //    InspectorForm.Main.SetObj(SelectedEntity);
                // IDESceneGraph3D.Main.SelectNode(node);
                EditGlobal.Inspector.SetClass(node);
            }
            else if (node is Light3D)
            {

                SelectedEntity = node;
                CurGizmo.LocalPos = node.LocalPos;
                //InspectorForm.Main.SetObj(node);
                EditGlobal.Inspector.SetClass(node);
                CurGizmo.NoRender = false;
            }
        }
       

        public void MoveInFront()
        {

            if (SelectedEntity == null) return;
            SelectedEntity.LocalPos = EditCam.Transform(new Vector3(0, 0, -10));
            SelectedEntity.LookAt(EditCam.Transform(new Vector3(0,0,-15)), new Vector3(0, 1, 0));
            CurGizmo.LocalPos = SelectedEntity.LocalPos;
            Graph.Rebuild();
        }
        public void MoveToHere()
        {
            if (SelectedEntity == null) return;
            SelectedEntity.LocalPos = EditCam.LocalPos;
            CurGizmo.LocalPos = SelectedEntity.LocalPos;
            Graph.Rebuild();

        }

        public void BeginPlay()
        {

            Graph.BeginRun();

        }

        public void StopPlay()
        {

            Graph.EndRun();

        }

        public SceneView()
        {

            SVMain = this;

            MouseDown = (b) =>
            {
                if (b == 1)
                {
                    Rotating = true;
                }
                if (b == 0 && (MoveX == false && MoveY == false && MoveZ == false))
                {
                    AppInfo.RW = W;
                    AppInfo.RH = H;

                    var on = SelectOthers(MouseX, MouseY);

                    if (on != null)
                    {
                        if (on is Light3D)
                        {
                            SelectedEntity = on;
                            CurGizmo.LocalPos = SelectedEntity.LocalPos;
                            Graph.Rebuild();
                            EditGlobal.Tree.SelectNode(on);
                            EditGlobal.Inspector.SetClass(on);
                            //IDESceneGraph3D.Select(on);
                            //   InspectorForm.Main.SetObj(on);
                            
                            CurGizmo.NoRender = false;
                            return;
                        }
                    }

                    AppInfo.RW = W;
                    AppInfo.RH = H;
                    var r2 = Graph.CamPickOnTop(MouseX, MouseY);
                    bool done = false;
                    if (r2 != null && CurGizmo.NoRender == false)
                    {
                        done = true;
                        Console.WriteLine("Hit!");
                        CurGizmo.NoRender = false;

                        if (GizmoMode == GizmoType.Translate)
                        {
                            if (r2.Node == tX)
                            {
                                MoveX = true;
                                MoveY = false;
                                MoveZ = false;
                                CurGizmo.NoRender = false;
                                //    Environment.Exit(0);
                            }
                            if (r2.Node == tZ)
                            {
                                MoveZ = true;
                                MoveX = false;
                                MoveY = false;
                                CurGizmo.NoRender = false;
                            }
                            if (r2.Node == tY)
                            {
                                MoveY = true;
                                MoveX = false;
                                MoveZ = false;
                                CurGizmo.NoRender = false;
                            }
                        }
                        if(GizmoMode == GizmoType.Rotate)
                        {
                            if(r2.Mesh == gRot.Meshes[0])
                            {
                                MoveX = true;
                                MoveY = false;
                                MoveZ = false;
                            }
                            if(r2.Mesh == gRot.Meshes[1])
                            {
                                MoveY = true;
                                MoveX = false;
                                MoveZ = false;
                            }
                            if(r2.Mesh == gRot.Meshes[2])
                            {
                                MoveZ = true;
                                MoveX = false;
                                MoveY = false;
                            }


        
                        }
                    }

                    if (!done)
                    {
                        var r = Graph.CamPick(MouseX, MouseY);
                        if (r != null)
                        {
                            Console.WriteLine("Hit");
                            if (r.Node != CurGizmo)
                            {
                                SelectedEntity = r.Node as Entity3D;
                                CurGizmo.LocalPos = r.Node.LocalPos;
                                // InspectorForm.Main.SetObj(SelectedEntity);
                                CurGizmo.NoRender = false;
                                EditGlobal.Inspector.SetClass(SelectedEntity);
                                EditGlobal.Tree.SelectNode(SelectedEntity);
                            }
                            else
                            {
                            };
                            //r.Node.SetPos(new OpenTK.Vector3(r.Node.LocalPos.X +1, 0, 0));
                            Graph.Rebuild();
                        }

                        else
                        {
                         //   CurGizmo.NoRender = true;
                        }
                    }
                    AppInfo.RW = AppInfo.W;
                    AppInfo.RH = AppInfo.H;
                }
            };
            MouseUp = (b) =>
            {
                if (b == 1)
                {
                    Rotating = false;
                }
                if (MoveX || MoveY || MoveZ)
                {
                    MoveX = MoveY = MoveZ = false;
                    Graph.Rebuild();
                }

            };

            KeyPressFree = (k, shift) =>
            {
                float speed = 0.1f;

                if (shift) speed = 0.2f;

                switch (k)
                {
                    case Keys.W:

                        EditCam.Move(new Vector3(0, 0, -speed), Space.Local);

                        break;

                    case Keys.S:

                        EditCam.Move(new Vector3(0, 0, speed), Space.Local);

                        break;

                    case Keys.A:

                        EditCam.Move(new Vector3(-speed, 0, 0), Space.Local);

                        break;

                    case Keys.D:

                        EditCam.Move(new Vector3(speed, 0, 0), Space.Local);

                        break;
                }

            };

            Update = () =>
            {

                Graph.Update();
                //Console.WriteLine("Update!");


            };

            MouseMove = (x, y, xd, yd) =>
            {
                MouseX = x;
                MouseY = y;
                Console.WriteLine("XD:" + xd + " YD:" + yd);

                float gxd, gyd, gzd;

                gxd = CurGizmo.LocalPos.X - EditCam.LocalPos.X;
                gyd = CurGizmo.LocalPos.Y - EditCam.LocalPos.Y;
                gzd = CurGizmo.LocalPos.Z - EditCam.LocalPos.Z;


                float dis = (float)Math.Sqrt((gxd * gxd) + (gyd * gyd) + (gzd * gzd));

                if (GizmoMode == GizmoType.Translate)
                {
                    dis = dis * 0.03f;
                }
                else
                {
                    dis = dis * 0.05f;
                }


                CurGizmo.Scale = new Vector3(dis, dis, dis);
                tX.Meshes[0].Material.Diff = new Vector3(1.0f, 0.1f, 0.1f);
                tY.Meshes[0].Material.Diff = new Vector3(0.1f, 1.0f, 0.1f);
                tZ.Meshes[0].Material.Diff = new Vector3(0.1f, 0.1f, 1.0f);
                gRot.Meshes[0].Material.Diff = new Vector3(1, 0, 0);
                gRot.Meshes[1].Material.Diff = new Vector3(0, 1, 0);
                gRot.Meshes[2].Material.Diff = new Vector3(0, 0, 1);
                AppInfo.RW = W;
                AppInfo.RH = H;
                var r2 = Graph.CamPickOnTop(MouseX, MouseY);
                bool done = false;
                if (r2 != null)
                {
                    done = true;
                    Console.WriteLine("Hit!");
                    CurGizmo.NoRender = false;

                    if (GizmoMode == GizmoType.Translate)
                    {
                        if (r2.Node == tX)
                        {

                            tX.Meshes[0].Material.Diff = new Vector3(1.0f, 0.3f, 0.3f);

                            //
                            //MoveX = true;
                            //MoveY = false;
                            //MoveZ = false;
                            //CurGizmo.NoRender = false;

                            //    Environment.Exit(0);
                        }
                        if (r2.Node == tY)
                        {
                            tY.Meshes[0].Material.Diff = new Vector3(0.6f, 1.0f, 0.6f);
                        }
                        if (r2.Node == tZ)
                        {
                            tZ.Meshes[0].Material.Diff = new Vector3(0.3f, 0.3f, 1.0f);
                        }
                    }

                    if (GizmoMode == GizmoType.Rotate)
                    {
                        if (r2.Mesh == gRot.Meshes[0])
                        {
                            r2.Mesh.Material.Diff = new Vector3(1.0f, 0.3f, 0.3f);
                            //MoveX = true;
                            //MoveY = false;
                            //MoveZ = false;
                        }
                        if (r2.Mesh == gRot.Meshes[1])
                        {
                            r2.Mesh.Material.Diff = new Vector3(0.6f, 1.0f, 0.6f);
                            //MoveY = true;
                            //MoveX = false;
                            //MoveZ = false;
                        }
                        if (r2.Mesh == gRot.Meshes[2])
                        {
                            r2.Mesh.Material.Diff = new Vector3(0.3f, 0.3f, 1.0f);
                            //MoveZ = true;
                            //MoveX = false;
                            //MoveY = false;

                        }


                    }

                }


                    AppInfo.RW = AppInfo.W;
                AppInfo.RH = AppInfo.H;

                if (Rotating)
                {
                    CamPitch -= yd * 0.25f;
                    CamYaw -= xd * 0.25f;

                    EditCam.Rot(new Vector3(CamPitch, CamYaw, 0), Space.Local);
                }
                else
                {
                    switch (SnapMode)
                    {
                        case MoveMode.Free:

                            if (SpaceMode == Locality.Local)
                            {
                                if (SelectedEntity == null) return;
                                if (MoveX)
                                {
                                    if (GizmoMode == GizmoType.Translate)
                                    {
                                        SelectedEntity.Move(new Vector3((float)xd * 0.03f, 0, 0), Space.Local);
                                        CurGizmo.LocalPos = SelectedEntity.LocalPos;
                                    }
                                    else if (GizmoMode == GizmoType.Rotate)
                                    {
                                        SelectedEntity.Turn(new Vector3(yd, 0, 0), Space.Local);
                                    };
                                    break;
                                }
                                if (MoveY)
                                {
                                    if (GizmoMode == GizmoType.Translate)
                                    {
                                        SelectedEntity.Move(new Vector3(0, -yd * 0.03f, 0), Space.Local);
                                        CurGizmo.LocalPos = SelectedEntity.LocalPos;
                                    }
                                    else if (GizmoMode == GizmoType.Rotate)
                                    {
                                        SelectedEntity.Turn(new Vector3(0, xd, 0), Space.Local);
                                    };
                                }
                                if (MoveZ)
                                {
                                    if (GizmoMode == GizmoType.Translate)
                                    {
                                        SelectedEntity.Move(new Vector3(0, 0, -xd * 0.03f), Space.Local);
                                        CurGizmo.LocalPos = SelectedEntity.LocalPos;
                                    }
                                    else if (GizmoMode == GizmoType.Rotate)
                                    {
                                        SelectedEntity.Turn(new Vector3(0, 0, xd), Space.Local);
                                    }
                                }
                            }

                            if (SpaceMode == Locality.Global)
                            {
                                if (SelectedEntity == null) return;
                                if (MoveX)
                                {
                                    if (GizmoMode == GizmoType.Translate)
                                    {
                                        SelectedEntity.LocalPos = SelectedEntity.LocalPos + new Vector3((float)xd * 0.03f, 0, 0);
                                        CurGizmo.LocalPos = SelectedEntity.LocalPos;
                                    }
                                    else if (GizmoMode == GizmoType.Rotate)
                                    {
                                        SelectedEntity.Turn(new Vector3(yd, 0, 0), Space.World);
                                    };
                                }
                                if (MoveY)
                                {
                                    if (GizmoMode == GizmoType.Translate)
                                    {
                                        //yd = 5;

                                        SelectedEntity.LocalPos = SelectedEntity.LocalPos + new Vector3(0, -yd * 0.03f, 0);
                                        CurGizmo.LocalPos = SelectedEntity.LocalPos;
                                    }
                                    else if (GizmoMode == GizmoType.Rotate)
                                    {
                                        SelectedEntity.Turn(new Vector3(0, xd, 0), Space.World);
                                    };
                                }
                                if (MoveZ)
                                {
                                    if (GizmoMode == GizmoType.Translate)
                                    {
                                        SelectedEntity.LocalPos = SelectedEntity.LocalPos + new Vector3(0, 0, -xd * 0.03f);
                                        CurGizmo.LocalPos = SelectedEntity.LocalPos;
                                    }
                                    else if (GizmoMode == GizmoType.Rotate)
                                    {
                                        SelectedEntity.Turn(new Vector3(0, 0, xd), Space.World);
                                    }
                                }
                            }

                            break;

                        case MoveMode.Snap:

                            if (MoveX)
                            {
                                if (Math.Abs(xd) > 12)
                                {
                                    SelectedEntity.LocalPos = SelectedEntity.LocalPos + new Vector3(Math.Sign(xd), 0, 0);
                                    CurGizmo.LocalPos = SelectedEntity.LocalPos;
                                }
                            }
                            if (MoveY)
                            {
                                if (Math.Abs(yd) > 12)
                                {
                                    SelectedEntity.LocalPos = SelectedEntity.LocalPos + new Vector3(0, -Math.Sign(yd), 0);
                                    CurGizmo.LocalPos = SelectedEntity.LocalPos;
                                }
                            }

                            if (MoveZ)
                            {
                                if (Math.Abs(yd) > 12)
                                {
                                    SelectedEntity.LocalPos = SelectedEntity.LocalPos + new Vector3(0, 0, Math.Sign(yd));
                                    CurGizmo.LocalPos = SelectedEntity.LocalPos;
                                }
                            }

                            break;
                    }

                }
            };

        }

        public static void SetGizmo(GizmoType ty)
        {

            SceneView.GizmoMode = ty;
            switch (ty)
            {

                case GizmoType.Translate:
                    SceneView.SVMain.CurGizmo = SceneView.SVMain.gTrans;

                    break;
                case GizmoType.Rotate:
                    SceneView.SVMain.CurGizmo = SceneView.SVMain.gRot;
                    break;
            }
            SceneView.SVMain.Graph.AlwaysOnTop.Clear();
            SceneView.SVMain.Graph.AddOnTop(SceneView.SVMain.CurGizmo);
            if (SceneView.SVMain.SelectedEntity != null)
            {
                SceneView.SVMain.CurGizmo.LocalPos = SceneView.SVMain.SelectedEntity.LocalPos;
            }
            float gxd, gyd, gzd;

            Entity3D CurGizmo = SceneView.SVMain.CurGizmo;
            Cam3D EditCam = SceneView.SVMain.EditCam;

            gxd = CurGizmo.LocalPos.X - EditCam.LocalPos.X;
            gyd = CurGizmo.LocalPos.Y - EditCam.LocalPos.Y;
            gzd = CurGizmo.LocalPos.Z - EditCam.LocalPos.Z;


            float dis = (float)Math.Sqrt((gxd * gxd) + (gyd * gyd) + (gzd * gzd));

            if (GizmoMode == GizmoType.Translate)
            {
                dis = dis * 0.03f;
            }
            else
            {
                dis = dis * 0.05f;
            }


            CurGizmo.Scale = new Vector3(dis, dis, dis);
        }

     
        public override void AfterSetup()
        {
            base.AfterSetup();
            if (firstSetup)
            {
                var grid = new MeshLines(2000);
                //CurGizmo = GizmoGen.RotationGizmo(6, 1.6f);

                gRot = GizmoGen.RotationGizmo(6, 2.4f);
                gTrans = GizmoGen.TranslateGizmo(0.5f, 2.5f);
                CurGizmo = gTrans;
                //GizmoMove.NoRender = true;
                tX = (Entity3D)CurGizmo.FindNode("mX");
                tY = (Entity3D)CurGizmo.FindNode("mY");
                tZ = (Entity3D)CurGizmo.FindNode("mZ");
                Graph.AddOnTop(CurGizmo);
                int li = 0;
                for (int x = 0; x < 100; x++)
                {
                    float rx = x - 50;
                    float rz = -50;
                    grid.SetLine(li, rx, 0, rz, rx, 0, 50);
                    li++;
                    grid.SetLine(li, -50, 0, rx, 50, 0, rx);
                    li++;
                }

                grid.Final();

                Graph.LineMeshes.Add(grid);

                EditCam = Graph.Cams[0];

                EditCam.SetPos(new Vector3(0, 6, 20));

                Light3D l1 = new Light3D();
                l1.Diff = new Vector3(1, 1, 1);
                l1.Range = 800;
                Graph.Add(l1);
                firstSetup = false;
            }

        }

        Cam3D EditCam;

    }
}

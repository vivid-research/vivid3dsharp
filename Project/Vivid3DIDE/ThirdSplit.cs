﻿using Vivid.Resonance.Forms;
namespace Vivid3D
{
    public class ThirdSplit : VerticalSplitterForm
    {

        public ThirdSplit()
        {

            SetSplit(1030);
            SceneV = new SceneView();
            Inspector = new ClassInspectorForm();
                SetLeft(SceneV);
            SetRight(Inspector);
            EditGlobal.View = SceneV;
            EditGlobal.Inspector = Inspector;
        }

        SceneView SceneV;
        //IDEClassInspector Inspector; 
        ClassInspectorForm Inspector;

    }
}

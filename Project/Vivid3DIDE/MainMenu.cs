﻿using System;
using Vivid.Resonance.Forms;
using Vivid.Resonance;
namespace Vivid3D
{
    public class MainMenu : Vivid.Resonance.Forms.AppMenuForm
    {

        MenuItem ProjMenu, EditMenu;
        MenuItem ProjNew;
        MenuItem NewProj;
        MenuItem NewScene;
        MenuItem NewScript;
        void InitProject()
        {

            ProjMenu = AddItem("Project");
            ProjNew = ProjMenu.AddItem("New", new Vivid.Texture.Texture2D("data/edit/iNewProj.png", true));

            ProjNew.AddItem("Project");
            ProjNew.AddItem("Scene");
            ProjNew.AddItem("Script");



            ProjMenu.AddSeperator();
            var setCon = ProjMenu.AddItem("Set Content Folder");


            setCon.Click = () =>
            {

                UI.CurUI.Top = new RequestFileForm("Select Content Folder", "", true);


            };

            ProjMenu.AddItem("Load Project", new Vivid.Texture.Texture2D("data/edit/iLoadProj.png", true));
            ProjMenu.AddItem("Save Project", new Vivid.Texture.Texture2D("data/edit/iSaveProj.png", true));
            ProjMenu.AddSeperator();
            ProjMenu.AddItem("Close Project", new Vivid.Texture.Texture2D("data/edit/iCloseProj.png", true));
            var em = ProjMenu.AddItem("Exit Vivid3D");
            em.Click = () =>
            {

                Environment.Exit(1);

            };


        }

        void InitEdit()
        {

            EditMenu = AddItem("Edit");
            EditMenu.AddItem("Copy");
            EditMenu.AddItem("Cut");
            EditMenu.AddSeperator();
            EditMenu.AddItem("Paste");
            EditMenu.AddSeperator();
            EditMenu.AddItem("Delete");
            EditMenu.AddSeperator();
            var miMoveToHere = EditMenu.AddItem("Move To Here");

            miMoveToHere.Click = () =>
            {

                EditGlobal.View.MoveToHere();

            };

            var aiMoveInFront = EditMenu.AddItem("Move In Front Of Here");


            aiMoveInFront.Click=()=>{

                EditGlobal.View.MoveInFront();

            };


        }

        public MainMenu()
        {

            InitProject();
            InitEdit();
        }

    }
}

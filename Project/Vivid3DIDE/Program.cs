﻿using OpenTK.Windowing.Desktop;

using System;

using Vivid.App;



namespace Vivid3D
{
    class Program
    {
        static void Main(string[] args)
        {
            var gs = GameWindowSettings.Default;
           // gs.IsMultiThreaded = true;
            //gs =/ new GameWindowSettings()

            var ns = new NativeWindowSettings();
            ns.NumberOfSamples = 16;
            //ns.Flags = OpenTK.Windowing.Common.ContextFlags.
        
            ns.API = OpenTK.Windowing.Common.ContextAPI.OpenGL;
            ns.APIVersion = new Version(4, 5);
            ns.AutoLoadBindings = true;
            ns.Flags = OpenTK.Windowing.Common.ContextFlags.Default;
            ns.IsEventDriven = false;
            ns.IsFullscreen = false;
            ns.Profile = OpenTK.Windowing.Common.ContextProfile.Compatability;
            ns.Size = new OpenTK.Mathematics.Vector2i(1720, 880);
            ns.StartFocused = true;
            ns.StartVisible = true;
            ns.Title = "Vivid3D IDE";
            // gs.IsMultiThreaded = true;
            gs.UpdateFrequency = 50;
            gs.RenderFrequency = 25;

            ns.WindowBorder = OpenTK.Windowing.Common.WindowBorder.Fixed;
            ns.WindowState = OpenTK.Windowing.Common.WindowState.Normal;

            Console.WriteLine("Creating application.");

            var initState = new MainEditState();// VividIDE.States.SceneEditorState();

            Application.InitState = initState;

            Vivid3DApp Test = new Vivid3DApp(gs, ns);
            Console.WriteLine("Running application");
            Test.Run();

        }
    }
}

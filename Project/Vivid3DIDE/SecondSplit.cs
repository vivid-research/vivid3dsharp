﻿using Vivid.Resonance.Forms;
namespace Vivid3D
{

    //Top = Scene and Inspector
    //Bot = Content Browser
    public class SecondSplit : HorizontalSplitterForm
    {

        public SecondSplit()
        {

            Split3 = new ThirdSplit();



        }
        public override void AfterSetup()
        {
            base.AfterSetup();
            SetSplit(600);
            SetTop(Split3);
            ContentBrowse = new IDEContentBrowserForm();
            SetBottom(ContentBrowse);
            ContentBrowse.ScanContent();
            EditGlobal.Browser = ContentBrowse;
            IDEContentBrowserForm.Load = (p, n) =>
            {

                EditGlobal.View.Graph.Add(Vivid.Import.Import.ImportNode(p));
                EditGlobal.Tree.Rebuild();
                EditGlobal.View.Graph.Rebuild();
            };
        }


        public ThirdSplit Split3;
        public IDEContentBrowserForm ContentBrowse;
    }
}
